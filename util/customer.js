var _ = require('lodash'),
    async = require('async'),
    modeler = require('../modeler'),
    logger = require('winston'),
    util = require('util');

module.exports = exports = {
    create: function(options, cb) {
        logger.silly('[customer create] creating customer ' + options.customer.name);
        async.waterfall([

            function(cb) {
                //identify the database with the fewest customers
                var tasks = {};
                _.each(modeler.dbhash, function(value, key) {
                    tasks[key] = function(cb) {
                        modeler.dbhash[key].Customer.count()
                            .exec(function(err, r) {
                                cb(err, r);
                            });
                    };
                });
                async.parallel(tasks, function(err, result) {
                    var memberName = _.chain(result)
                        .pairs()
                        .sortBy(function(pair) {
                            return pair[1];
                        })
                        .value()[0][0];

                    var newCustomer = (new modeler.dbhash[memberName].Customer(options.customer));
                    newCustomer.createdBy = options.createdBy;
                    newCustomer.modifiedBy = options.modifiedBy;
                    newCustomer.save(function(err, customer) {
                        if (!!err) {
                            logger.error('[customer create] problem creating: ' + util.inspect(err));
                        } else {
                            logger.silly('[customer create] customer created.');
                        }
                        cb(err, customer);
                    });
                });
            },
            function(customer, cb) {
                logger.silly('[customer create] making tenant hash ' + customer._id);
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customer._id
                    }
                }, function(err, models) {
                    cb(err, models, {
                        customer: customer
                    });
                });
            }
        ], function(err, models, p) {
            cb(err, models, p.customer);
        });
    },
    list: function(options, cb) {
        var tasks = [];
        _.each(modeler.dbhash, function(value, key) {
            tasks.push(function(cb) {
                modeler.dbhash[key].Customer.find()
                    .lean()
                    .exec(function(err, customers) {
                        cb(err, _.map(customers, function(customer) {
                            return {
                                dbkey: key,
                                customerId: customer._id,
                                customerName: customer.name
                            };
                        }));
                    });
            });
        });

        async.waterfall([

            function(cb) {
                async.parallel(tasks, function(err, r) {
                    cb(err, {
                        customers: r
                    });
                });
            },
            function(p, cb) {
                p.customers = _.chain(p.customers)
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .compact()
                    .value();
                cb(null, p);
            }
        ], function(err, p) {
            cb(err, p);
        });
    }
};
