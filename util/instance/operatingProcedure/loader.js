var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var operatingProcedureInstanceIds = extractOptionIds(options, {
        single: 'operatingProcedureInstance',
        array: 'operatingProcedureInstances'
    });

    logger.silly('[operatingProcedure instance loader] instance IDs: ' + util.inspect(operatingProcedureInstanceIds));
    async.waterfall([

        function(cb) {
            if (!operatingProcedureInstanceIds || operatingProcedureInstanceIds.length == 0) {
                return cb('Bad operating procedure instance ID in OP instance loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.operatingProcedure.OperatingProcedure',
                    query: {
                        _id: operatingProcedureInstanceIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for op instance ids ' + util.inspect(operatingProcedureInstanceIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[operatingProcedure instance loader] loading instance');
            models.instances.operatingProcedure.OperatingProcedure.find({
                _id: {
                    $in: operatingProcedureInstanceIds
                }
            })
                .exec(function(err, operatingProcedures) {
                    cb(err, models, {
                        operatingProcedures: operatingProcedures
                    });
                });
        },
        function(models, p, cb) {
            if (!p.operatingProcedures) {
                return cb('operating procedure instance could not be found for ids ' + util.inspect(operatingProcedureInstanceIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the sections
            models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'sections',
                model: models.instances.operatingProcedure.Section
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data
            models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'rangedData',
                model: models.instances.operatingProcedure.ranged.OperatingProcedure
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data def
            models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'rangedData.operatingProcedureDefinitionRange',
                model: models.definitions.operatingProcedure.ranged.OperatingProcedure
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the sections' ranged data
            models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'sections.rangedData',
                model: models.instances.operatingProcedure.ranged.Section
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the sections' ranged data
            models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'sections.rangedData.sectionDefinitionRange',
                model: models.definitions.operatingProcedure.ranged.Section
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            async.parallel(_.map(p.operatingProcedures, function(operatingProcedure) {
                return function(cb) {
                    if (!!options.flatten) {
                        operatingProcedure = operatingProcedure.toObject();
                        logger.silly('[operatingProcedure instance loader] flattening OP instance');
                        //flatten the sections and the OP
                        async.parallel({
                            OP: function(cb) {
                                logger.silly('[operatingProcedure instance loader] range: ' + moment(options.startDate)
                                    .toDate() + ' to ' + moment(options.endDate)
                                    .toDate());
                                datesplice.getByDateRange(operatingProcedure, 'rangedData', options.startDate, options.endDate, function(err, flatOP) {
                                    //copy the shortname up from the OP def
                                    cb(err, flatOP);
                                });
                            },
                            sections: function(cb) {
                                async.parallel(_.map(operatingProcedure.sections, function(section) {
                                    return function(cb) {
                                        datesplice.getByDateRange(section, 'rangedData', options.startDate, options.endDate, function(err, flatSection) {
                                            cb(err, flatSection);
                                        });
                                    };
                                }), function(err, r) {
                                    cb(err, r);
                                });
                            }
                        }, function(err, r) {
                            operatingProcedure = r.OP;
                            operatingProcedure.sections = r.sections;

                            //remove def id
                            delete operatingProcedure.operatingProcedureDefinitionRange._id;
                            //copy stuff from the def up a level
                            operatingProcedure = _.defaults(operatingProcedure, operatingProcedure.operatingProcedureDefinitionRange);
                            //remove the def range
                            delete operatingProcedure.operatingProcedureDefinitionRange;

                            _.each(operatingProcedure.sections, function(section) {
                                delete section.sectionDefinitionRange._id;
                                section = _.defaults(section, section.sectionDefinitionRange);
                                delete section.sectionDefinitionRange;
                            });

                            cb(err, operatingProcedure);
                        });
                    } else {
                        cb(null, operatingProcedure);
                    }
                };
            }), function(err, r) {
                p.operatingProcedures = r;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        logger.silly('[operatingProcedure instance loader] instance retreived.');
        if (err) {
            logger.error('[operatingProcedure instance loader] ' + util.inspect(err));
        }

        cb(err, models, (p || {})
            .operatingProcedures.length == 1 && !!options.operatingProcedureInstance ? p.operatingProcedures[0] : (p || {})
            .operatingProcedures);

    });
};
