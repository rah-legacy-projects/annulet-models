var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    instanceLoader = require('./loader'),
    definitionLoader = require('../../definition/operatingProcedure/loader');

_.mixin(require('annulet-util')
    .lodashMixins);


module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  actionCustomerUser: id or object of the customer user,
    //}

    var actionCustomerUserId = _.extractId(options, 'actionCustomerUser'),
        customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('Bad customer user for op status: ' + options.customerUser);
            }
            cb();
        },
        function(cb) {
            logger.silly('[op peek] customer user = ' + customerUserId);
            if (!models) {
                logger.silly('[op peek] retrieving models ');
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: customerUserId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('modelset not found for customeruser ' + customerUserId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[op peek] getting customer user');
            //identify the customer user
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                return cb('customer user ' + customerUserId + ' could not be identified for op status.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[op peek] getting OP instances');
            //get the OP instances for the customer user
            models.instances.operatingProcedure.OperatingProcedure.find({
                customerUser: p.customerUser._id
            })
                .exec(function(err, operatingProcedureInstances) {
                    p.instances = operatingProcedureInstances;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[op peek] getting OP defs for customer');
            //get the OP definitions for the customer
            models.definitions.operatingProcedure.OperatingProcedure.find({
                active: true,
                deleted: false,
                isDraftOf: null
            })
                .exec(function(err, operatingProcedureDefinitions) {
                    if (!!err) {
                        logger.error('[op peek] problem getting defs by customer: ' + util.inspect(err));
                    }
                    logger.silly('[op peek] op defs: ' + operatingProcedureDefinitions.length);
                    p.definitions = operatingProcedureDefinitions;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if ((p.instances || [])
                .length == 0) {
                return cb(null, models, p);
            }
            //load instances with loader, flattened
            instanceLoader(models, {
                actionCustomerUser: options.actionCustomerUser,
                operatingProcedureInstances: _.pluck(p.instances, '_id'),
                flatten: true
            }, function(err, models, instances) {
                p.instances = instances;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if ((p.definitions || [])
                .length == 0) {
                logger.silly('[op peek] no definitions found, skipping load');
                return cb(null, models, p);
            }
            //load instances with loader, flattened
            definitionLoader(models, {
                actionCustomerUser: options.actionCustomerUser,
                operatingProcedureDefinitions: _.pluck(p.definitions, '_id'),
                flatten: true
            }, function(err, models, definitions) {
                p.definitions = definitions;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op peek] getting status listing');
            var instances = _.map(p.instances, function(i) {
                return {
                    definitionId: i.operatingProcedureDefinition.toString(),
                    instanceId: i._id.toString(),
                    title: i.title,
                    description: i.description,
                    completedDate: i.completedDate,
                    shortName: i.shortName,
                    created:i.created
                };
            });
            var definitions = _.chain(p.definitions)
                .reject(function(d) {
                    //filter any defs that have an instance
                    return (_.any(instances, function(i) {
                        return i.definitionId == d._id.toString();
                    }))
                })
                .map(function(d) {
                    return {
                        definitionId: d._id.toString(),
                        instanceId: null,
                        title: d.title,
                        description: d.description,
                        completedDate: null,
                        shortName: d.shortName,
                        created: null
                    };
                })
                .value();



            p.list = instances.concat(definitions);
            cb(null, models, p);
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[op instance status] ' + util.inspect(err));
        }
        cb(err, models, p.list);
    });
};
