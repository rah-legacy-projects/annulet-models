var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    fixCustomerUser = require('../../fixCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  operatingProcedureInstance: obj or id of the operatingProcedure to load,
    //  customerUser: the customerUser
    //}

    var operatingProcedureInstanceId = _.extractId(options, 'operatingProcedureInstance'),
        customerUserId = _.extractId(options, 'customerUser');
    logger.silly('[operatingProcedure loader] id: ' + operatingProcedureInstanceId);
    async.waterfall([

        function(cb) {
            if (!operatingProcedureInstanceId) {
                return cb('bad operating procedure instance passed to opins loader: ' + options.operatingProcedureInstance);
            }
            cb(null);
        },
        function(cb) {
            options = fixCustomerUser(options);
            if (!models) {
                modeler.db({
                    collection: 'instances.operatingProcedure.OperatingProcedure',
                    query: {
                        _id: operatingProcedureInstanceId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for op instance ' + operatingProcedureInstanceId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.debug('[operatingProcedure loader] loading instance');
            models.instances.operatingProcedure.OperatingProcedure.findOne({
                _id: operatingProcedureInstanceId.toObjectId()
            })
                .populate({
                    path: 'sections',
                    model: models.instances.operatingProcedure.Section
                })
                .lean()
                .exec(function(err, operatingProcedure) {
                    cb(err, models, {
                        operatingProcedure: operatingProcedure
                    });
                });
        },
        function(models, p, cb) {
            if (!p.operatingProcedure) {
                return cb('could not find op instance for id ' + operatingProcedureInstanceId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //make an item activity for the creation of the op
            (new models.tracking.ItemActivity({
                changedBy: customerUserId.toString(),
                state: 'Opened',
                itemType: 'instances.operatingProcedure.OperatingProcedure',
                item: p.operatingProcedure._id,
                message: {
                    firstName: options.customerUser.user.firstName,
                    lastName: options.customerUser.user.lastName,
                    email: options.customerUser.user.email,
                    itemTitle: p.operatingProcedure.title
                }
            }))
                .save(function(err) {
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        logger.silly('[operatingProcedure loader] load complete.');
        cb(err, models, (p || {})
            .operatingProcedure);
    });
};
