var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var auxiliaryDocumentInstanceIds = extractOptionIds(options, {
        single: 'auxiliaryDocumentInstance',
        array: 'auxiliaryDocumentInstances'
    });
    async.waterfall([

        function(cb) {
            if (!auxiliaryDocumentInstanceIds || auxiliaryDocumentInstanceIds == 0) {
                return cb('Bad operating procedure instance ID in auxdox instance loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.auxiliaryDocument.AuxiliaryDocument',
                    query: {
                        _id: auxiliaryDocumentInstanceIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for auxdox instance id ' + util.inspect(auxiliaryDocumentInstanceIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[auxiliaryDocument instance loader] loading instance');
            models.instances.auxiliaryDocument.AuxiliaryDocument.find({
                _id: {
                    $in: auxiliaryDocumentInstanceIds
                }
            })
                .exec(function(err, auxiliaryDocuments) {
                    cb(err, models, {
                        auxiliaryDocuments: auxiliaryDocuments
                    });
                });
        },
        function(models, p, cb) {
            if (!p.auxiliaryDocuments) {
                return cb('operating procedure instance could not be found for id ' + util.inspect(auxiliaryDocumentInstanceIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the OP's ranged data
            models.instances.auxiliaryDocument.AuxiliaryDocument.populate(p.auxiliaryDocuments, {
                path: 'rangedData',
                model: models.instances.auxiliaryDocument.ranged.AuxiliaryDocument
            }, function(err, auxiliaryDocuments) {
                p.auxiliaryDocuments = auxiliaryDocuments;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data def
            models.instances.auxiliaryDocument.AuxiliaryDocument.populate(p.auxiliaryDocuments, {
                path: 'rangedData.auxiliaryDocumentDefinitionRange',
                model: models.definitions.auxiliaryDocument.ranged.AuxiliaryDocument
            }, function(err, auxiliaryDocuments) {
                p.auxiliaryDocuments = auxiliaryDocuments;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if (!!options.flatten) {
                async.parallel(_.map(p.auxiliaryDocuments, function(auxiliaryDocument) {
                    return function(cb) {
                        auxiliaryDocument = auxiliaryDocument.toObject();
                        logger.silly('[auxiliaryDocument instance loader] flattening auxdox instance');
                        //flatten the sections and the OP
                        async.parallel({
                            auxdoc: function(cb) {
                                datesplice.getByDateRange(auxiliaryDocument, 'rangedData', options.startDate, options.endDate, function(err, flatAuxDoc) {
                                    cb(err, flatAuxDoc);
                                });
                            },
                        }, function(err, r) {
                            auxiliaryDocument = r.auxdoc;

                            //remove def id
                            delete auxiliaryDocument.auxiliaryDocumentDefinitionRange._id;
                            //copy stuff from the def up a level
                            auxiliaryDocument = _.defaults(auxiliaryDocument, auxiliaryDocument.auxiliaryDocumentDefinitionRange);
                            //remove the def range
                            delete auxiliaryDocument.auxiliaryDocumentDefinitionRange;

                            _.each(auxiliaryDocument.sections, function(section) {
                                delete section.sectionDefinitionRange._id;
                                section = _.defaults(section, section.sectionDefinitionRange);
                                delete section.sectionDefinitionRange;
                            });

                            cb(err, auxiliaryDocument);
                        });
                    };
                }), function(err, r) {
                    p.auxiliaryDocuments = r;
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
    ], function(err, models, p) {
        logger.silly('[auxiliaryDocument instance loader] instance retreived.');
        cb(err, models, (p || {})
            .auxiliaryDocuments.length == 1 && !!options.auxiliaryDocumentInstance ? p.auxiliaryDocuments[0] : (p || {})
            .auxiliaryDocuments);
    });
};
