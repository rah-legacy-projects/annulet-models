var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  trainingInstance: obj or id of the training to load
    //}

    var trainingInstanceId = _.extractId(options, 'trainingInstance');
    logger.silly('[training loader] id: ' + trainingInstanceId);
    async.waterfall([

        function(cb) {
            if (!trainingInstanceId) {
                return cb('bad training instance id for loading instance: ' + options.trainingInstance);
            }
            cb()
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.training.Training',
                    query: {
                        _id: trainingInstanceId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified while loading training: ' + trainingInstanceId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.debug('[training loader] loading instance');
            models.instances.training.Training.findOne({
                _id: trainingInstanceId.toObjectId()
            })
                .populate({
                    path: 'sections',
                    model: models.instances.training.Section
                })
                .lean()
                .exec(function(err, training) {
                    cb(err, models, {
                        training: training
                    });
                });
        },
    ], function(err, models, p) {
        logger.silly('[training loader] load complete.');
        cb(err, models, (p || {})
            .training);
    });
};
