var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var trainingInstanceId = _.extractId(options, 'trainingInstance');
    logger.silly('[training instance loader] td id: ' + trainingInstanceId);
    async.waterfall([

        function(cb) {
            if (!trainingInstanceId) {
                return cb('Bad training instance ID in training instance loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.training.Training',
                    query: {
                        _id: trainingInstanceId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for training instance id ' + trainingInstanceId);
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[training instance loader] loading instance');
            models.instances.training.Training.findOne(trainingInstanceId.toObjectId())
                .exec(function(err, training) {
                    cb(err, models, {
                        training: training
                    });
                });
        },
        function(models, p, cb) {
            if (!p.training) {
                return cb('training instance could not be found for id ' + trainingInstanceId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the sections
            models.instances.training.Training.populate(p.training, {
                path: 'sections',
                model: models.instances.training.Section
            }, function(err, training) {
                p.training = training;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data
            models.instances.training.Training.populate(p.training, {
                path: 'rangedData',
                model: models.instances.training.ranged.Training
            }, function(err, training) {
                p.training = training;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data def
            models.instances.training.Training.populate(p.training, {
                path: 'rangedData.trainingDefinitionRange',
                model: models.definitions.training.ranged.Training
            }, function(err, training) {
                p.training = training;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the sections' ranged data
            models.instances.training.Training.populate(p.training, {
                path: 'sections.rangedData',
                model: models.instances.training.ranged.Section
            }, function(err, training) {
                p.training = training;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the sections' ranged data
            models.instances.training.Training.populate(p.training, {
                path: 'sections.rangedData.sectionDefinitionRange',
                model: models.definitions.training.ranged.Section
            }, function(err, training) {
                p.training = training;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if (!!options.flatten) {
                p.training = p.training.toObject();
                logger.silly('[training instance loader] flattening training instance');
                //flatten the sections and the OP
                async.parallel({
                    OP: function(cb) {
                        datesplice.getByDateRange(p.training, 'rangedData', options.startDate, options.endDate, function(err, flatOP) {
                            cb(err, flatOP);
                        });
                    },
                    sections: function(cb) {
                        async.parallel(_.map(p.training.sections, function(section) {
                            return function(cb) {
                                datesplice.getByDateRange(section, 'rangedData', options.startDate, options.endDate, function(err, flatSection) {
                                    cb(err, flatSection);
                                });
                            };
                        }), function(err, r) {
                            cb(err, r);
                        });
                    }
                }, function(err, r) {
                    p.training = r.OP;
                    p.training.sections = r.sections;

                    //remove def id
                    delete p.training.trainingDefinitionRange._id;
                    //copy stuff from the def up a level
                    p.training = _.defaults(p.training, p.training.trainingDefinitionRange);
                    //remove the def range
                    delete p.training.trainingDefinitionRange;

                    _.each(p.training.sections, function(section) {
                        delete section.sectionDefinitionRange._id;
                        section = _.defaults(section, section.sectionDefinitionRange);
                        delete section.sectionDefinitionRange;
                    });

                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
    ], function(err, models, p) {
        logger.silly('[training instance loader] instance retreived.');
        cb(err, models, (p || {})
            .training);
    });
};
