var _ = require('lodash'),
    async = require('async'),
    modeler = require('../../../modeler'),
    logger = require('winston'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    util = require('util');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //options:
    //{
    //  alertRange: the alert range object or id,
    //  actionCustomerUser
    //}
    //  models: (optional) the models set to query against
    
    var alertRangeId = _.extractId(options, 'alertRange');
    logger.silly('[alert dismiss] dismissing alert range ' + alertRangeId);
    async.waterfall([

        function(cb) {
            if (!alertRangeId) {
                return cb('bad alert ID for dismissal: ' + options.alertRange);
            }
            cb();
        },
        function(cb) {
            options = fixActionCustomerUser(options);
            logger.silly('[alert dismiss] getting models');
            if (!models) {
                modeler.db({
                    collection: 'instances.alerts.ranged.Alert',
                    query: {
                        _id: alertRangeId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, {});
                });
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('modelset not found for alert range id: ' + alertRangeId);
            }
            cb(null, models, p);
        },

        function(models, p, cb) {
            logger.silly('[alert dismiss] getting alert');
            //find the alert if any
            models.instances.alert.ranged.Alert.findOne({
                _id: alertRangeId.toObjectId()
            })
                .exec(function(err, alertRange) {
                    alertRange.dismissedDate = new Date();
                    alertRange.modifiedBy = options.actionCustomerUser._id,
                    alertRange.save(function(err, alertRange) {
                        p.alertRange = alertRange;
                        cb(err, models, p);
                    });
                });
        },
    ], function(err, models, p) {
        logger.silly('[alert dismiss] dismissed range: ' + util.inspect((p||{}).alertRange))
        cb(err, models, (p || {})
            .alertRange);
    });
}
