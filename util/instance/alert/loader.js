var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {

    var alertInstanceIds = extractOptionIds(options, {
        single: 'alertInstance',
        array: 'alertInstances'
    });

    async.waterfall([

        function(cb) {
            if (!alertInstanceIds || alertInstanceIds.length == 0) {
                return cb('Bad alert instance ID in alert loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.alert.Alert',
                    query: {
                        _id: alertInstanceIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for alert id ' + util.inspect(alertInstanceIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up instanceault options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[alert instance loader] loading instance');
            models.instances.alert.Alert.find({
                _id: {
                    $in: alertInstanceIds
                }
            })
                .exec(function(err, alerts) {
                    cb(err, models, {
                        alerts: alerts
                    });
                });
        },
        function(models, p, cb) {
            if (!p.alerts || p.alerts.length == 0) {
                return cb('alert instance could not be found for ids ' + util.inspect(alertInstanceIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the alert's ranged data
            models.instances.alert.Alert.populate(p.alerts, {
                path: 'rangedData',
                model: models.instances.alert.ranged.Alert
            }, function(err, alerts) {
                p.alerts = alerts;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            models.instances.alert.Alert.populate(p.alerts, {
                path: 'rangedData.alertDefinitionRange',
                model: models.definitions.alert.ranged.Alert
            }, function(err, alerts) {
                p.alerts = alerts;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[alert loader] alert loaded.');
            if (!!options.flatten) {
                async.parallel(_.map(p.alerts, function(alert) {
                    return function(cb) {
                        alert = alert.toObject();
                        logger.silly('[alert loader] alert obj: ' + util.inspect(alert.rangedData));
                        datesplice.getByDateRange(alert, 'rangedData', options.startDate, options.endDate, function(err, flatAlert) {
                            //hack: add range type
                            flatAlert._rangeType = 'instances.alert.ranged.Alert';
                            cb(err, flatAlert);
                        });
                    }
                }), function(err, alerts) {
                    p.alerts = alerts;
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!!options.flatten) {
                p.alerts = _.map(p.alerts, function(alert) {
                    //pull the range data up a level
                    alert.markdown = alert.alertDefinitionRange.markdown;
                    alert.alertLevel = alert.alertDefinitionRange.alertLevel;
                    alert.alertDefinitionRange = alert.alertDefinitionRange._id;
                    return alert;
                });
            }
            cb(null, models, p);
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[alert loader] problem loading instance: ' + util.inspect(err));
        }
        logger.silly('[alert loader] finished loading.');

        cb(err, models, (p || {})
            .alerts.length == 1 && !!options.alertInstance ? p.alerts[0] : (p || {})
            .alerts);
    });
};
