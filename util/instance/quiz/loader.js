var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler'),
    extractOptionIds = require('../../extractOptionIds');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  quizInstance: obj or id of the quiz to load
    //}
    var quizInstanceIds = extractOptionIds(options, {
        single: 'quizInstance',
        array: 'quizInstances'
    });
    async.waterfall([

        function(cb) {
            if (!quizInstanceIds || quizInstanceIds.length == 0) {
                return cb('bad quiz instance id for loading: ' + util.inspect(quizInstanceIds));
            }
            cb(null);
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.quiz.Quiz',
                    query: {
                        _id: quizInstanceIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be loaded for quiz instance load: ' + util.inspect(quizInstanceIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.debug('[quiz loader] loading instance');
            models.instances.quiz.Quiz.find({
                _id: {
                    $in: quizInstanceIds
                }
            })
                .exec(function(err, quizzes) {
                    cb(err, models, {
                        quizzes: quizzes
                    });
                });
        },
        function(models, p, cb) {
            if (!p.quizzes) {
                return cb('quiz not found for loading: ' + util.inspect(quizInstanceIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //populate the attempts
            models.instances.quiz.Quiz.populate(p.quizzes, {
                path: 'attempts',
                model: models.instances.quiz.ranged.Attempt
            }, function(err, quizzes) {
                p.quizzes = quizzes;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //populate the quiz def
            models.instances.quiz.Quiz.populate(p.quizzes, {
                path: 'quizDefinition',
                model: models.definitions.quiz.Quiz
            }, function(err, quizzes) {
                p.quizzes = quizzes;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the def range
            models.instances.quiz.Quiz.populate(p.quizzes, {
                path: 'attempts.quizDefinitionRange',
                model: models.definitions.quiz.ranged.Quiz
            }, function(err, quizzes) {
                p.quizzes = quizzes;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the question instances
            models.instances.quiz.Quiz.populate(p.quizzes, {
                path: 'attempts.questions',
                model: models.instances.quiz.Question
            }, function(err, quizzes) {
                p.quizzes = quizzes;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        logger.silly('[quiz loader] load complete.');
        if(!!err){
            logger.error(util.inspect(err));
        }
        cb(err, models, (p || {quizzes:[]})
            .quizzes.length == 1 && !!options.quizInstance ? p.quizzes[0] : (p || {})
            .quizzes);
    });
};
