var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    quizLoader = require('./quizLoader'),
    modeler = require('../../../modeler');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  quizInstance: obj or id of the quiz to load
    //}
    var quizInstanceId = _.extractId(options, 'quizInstance');
    logger.silly('[quiz scorer] loading quiz instance id ' + quizInstanceId);
    async.waterfall([

        function(cb) {
            if (!quizInstanceId) {
                return cb('Bad quiz instance id for scoring: ' + options.quizInstance);
            }
            cb();
        },
        function(cb) {
            //load the quiz
            quizLoader(models, {
                quizInstance: quizInstanceId.toObjectId()
            }, function(err, models, qw) {
                logger.silly('[quiz scorer] quiz loaded.');
                cb(err, models, qw);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz scorer] scoring questions');
            //score the questions
            //nb: if one of the questions does not have any selected answers,
            //nb: the quiz is not scorable and will bubble up as incomplete
            async.parallel(_.map(p.quiz.questions, function(questionWrap) {
                return function(cb) {
                    questionWrap.question.score(function(err, score) {
                        cb(err, score);
                    });
                }
            }), function(err, scores) {
                p.scores = scores;
                cb(err, models, p);;
            });
        },
        function(models, p, cb) {
            //reduce the out of into a total
            var outOf = _.chain(p.scores)
                .map(function(score) {
                    return score.outOf;
                })
                .reduce(function(sum, num) {
                    return sum + num;
                }, 0)
                .value();
            //reduce the correctly selected into a total
            var totalScore = _.chain(p.scores)
                .map(function(score) {
                    return score.score;
                })
                .reduce(function(sum, num) {
                    return sum + num;
                }, 0)
                .value();
            //call back with the overall score
            p.overallScore = {
                outOf: outOf,
                totalScore: totalScore,
                overallScore: ((totalScore / outOf) * 100),
                passing: ((totalScore / outOf) * 100) >= p.quiz.passingPercentage
            };
            cb(null, models, p);
        }
    ], function(err, models, p) {
        if (!!err) {
            cb(err, models);
        } else {
            cb(err, models, p);
        }
    });
};
