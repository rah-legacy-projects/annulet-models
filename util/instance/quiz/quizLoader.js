var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  quizInstance: obj or id of the quiz to load
    //}
    var quizInstanceId = _.extractId(options, 'quizInstance');
    async.waterfall([

        function(cb) {
            if (!quizInstanceId) {
                return cb('bad quiz instance id for loading: ' + options.quizInstance);
            }
            cb(null);
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.quiz.Quiz',
                    query: {
                        _id: quizInstanceId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb){
            if(!models){
                return cb('modelset could not be loaded for quiz instance load: ' + quizInstanceId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.debug('[quiz loader] loading instance');
            models.instances.quiz.Quiz.findOne({
                _id: quizInstanceId.toObjectId()
            })
                .lean()
                .exec(function(err, quiz) {
                    cb(err, models, {
                        quiz: quiz
                    });
                });
        },
        function(models, p, cb){
            if(!p.quiz){
                return cb('quiz not found for loading: ' + quizInstanceId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.debug('[quiz loader] getting question instances');
            async.parallel(_.map(p.quiz.questions, function(questionWrap) {
                return function(cb) {
                    models.instances.quiz[questionWrap.questionType].findOne({
                        _id: questionWrap.question
                    })
                        .exec(function(err, question) {
                            logger.silly('[quiz loader] question type: ' + util.inspect(questionWrap));
                            if(!!err){
                                logger.error('[quiz loader] problem loading question: ' + util.inspect(err));
                            }
                            logger.silly('[quiz loader] question found: ' + util.inspect(question));
                            questionWrap.question = question;
                            cb(err, {
                                question: question,
                                questionType: questionWrap.questionType
                            });
                        });
                };

            }), function(err, questions) {
                cb(err, models, p);
            });
        },
    ], function(err, models, loadedQuizInstance) {
        logger.silly('[quiz loader] load complete.');
        cb(err, models, loadedQuizInstance);
    });
};
