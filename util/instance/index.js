module.exports = exports = {
	alert: require("./alert"),
	auxiliaryDocument: require("./auxiliaryDocument"),
	operatingProcedure: require("./operatingProcedure"),
	quiz: require("./quiz"),
	training: require("./training"),
	workflow: require("./workflow"),
};
