var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId;

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) the models to query against
    //options: {
    //  workflowItem: the obj or id to load
    //}

    var crawler = function(models, workflowItem, cb) {
        logger.debug('[workflow loader] crawling ' + workflowItem.name);
        async.parallel({
            items: function(cb) {
                models.instances.workflow.WorkflowItem.populate(workflowItem, {
                    path: 'sequence',
                    model: models.instances.workflow.WorkflowItem
                }, function(err, workflowItem) {
                    cb(err, workflowItem);
                });
            },
            sequence: function(cb) {
                models.instances.workflow.WorkflowItem.populate(workflowItem, {
                    path: 'items',
                    model: models.instances.workflow.WorkflowItem
                }, function(err, workflowItem) {
                    cb(err, workflowItem);
                });
            }
        }, function(err, wf) {
            var itemsPopulate = [];
            _.each(workflowItem.items, function(item) {
                itemsPopulate.push(function(cb) {
                    crawler(models, item, cb);
                });
            });

            var sequencePopulate = [];
            _.each(workflowItem.sequence, function(seq) {
                sequencePopulate.push(function(cb) {
                    crawler(models, seq, cb);
                });
            });

            async.parallel({
                sequences: function(cb) {
                    async.parallel(sequencePopulate, cb);
                },
                items: function(cb) {
                    async.parallel(itemsPopulate, cb);
                }
            }, function(err, r) {
                cb(err, models, r);
            });

        });
    };


    var workflowItemId = _.extractId(options, 'workflowItem');
    logger.silly('[workflow instance loader] wf item id: ' + workflowItemId);
    async.waterfall([

        function(cb) {
            if (!workflowItemId) {
                return cb('bad workflow item id for instance loader: ' + options.workflowItem)
            }
            cb();
        },
        function(cb) {
            if (!models) {
                query = {
                    collection: 'instances.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemId.toObjectId()
                    }
                };
                modeler.db(query, function(err, models) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    cb(err, models);
                });
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('could not identify modelset for ' + workflowItemId + ' during wf isntance load');
            }
            cb(null, models);
        },
        function(models, cb) {
            //if the workflow item ID is passed in, find the workflow item to crawl
            models.instances.workflow.WorkflowItem.findOne({
                _id: workflowItemId.toObjectId()
            })
                .exec(function(err, wfItem) {
                    cb(err, models, wfItem);
                });
        },
        function(models, p, cb) {
            if (!p) {
                return cb('workflow item ' + workflowItemId + 'not found during wf instance load');
            }
            cb(null, models, p);
        },
    ], function(err, models, workflowItem) {
        if (!!err) {
            return cb(err);
        }
        crawler(models, workflowItem, function(err, models, rootItem) {
            if (!!workflowItem.toObject) {
                cb(err, models, workflowItem.toObject());
            } else {
                cb(err, models, workflowItem);
            }
        });
    });
};
