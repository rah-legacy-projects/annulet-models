var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler'),
    instanceLoader = require('./loader'),
    fixActionCustomerUser = require('../../fixActionCustomerUser');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var workflowItemInstanceId = _.extractId(options, 'workflowItemInstance');
    logger.silly('[wf update flat] wf item instance id: ' + workflowItemInstanceId);
    async.waterfall([

        function(cb) {
            options = fixActionCustomerUser(options);
            cb(null, models, {});
        },
        function(models, p, cb) {
            instanceLoader(models, {
                workflowItemInstance: workflowItemInstanceId,
            }, function(err, models, workflowItemInstance) {
                p.original = workflowItemInstance;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            var crawl = function(update, original, cb) {
                async.waterfall([

                        function(cb) {
                            cb(null, {});
                        },
                        function(pp, cb) {
                            async.series(_.map(update.items, function(err, updatedItem) {
                                return function(cb) {
                                    var originalItem = _.find(original.items, function(originalItem) {
                                        return updatedItem._id.toString() == originalItem._id.toString();
                                    });
                                    crawl(updatedItem, originalItem, cb);
                                };
                            }), function(err, r) {
                                cb(err, r);
                            });
                        },
                        function(pp, cb) {
                            async.series(_.map(update.sequences, function(err, updatedSequence) {
                                return function(cb) {
                                    var originalSequence = _.find(original.sequences, function(originalSequence) {
                                        return updatedSequence._id.toString() == originalSequence._id.toString();
                                    });
                                    crawl(updatedSequence, originalSequence, cb);
                                };
                            }), function(err, r) {
                                cb(err, r);
                            });
                        },
                        function(pp, cb) {
                            //get the range from the original
                            var originalRange = _.find(original.rangedData, function(r) {
                                return r._id.toString() == update._rangeId;
                            });

                            //update only the keys that exist
                            _.chain(update)
                                .keys()
                                .filter(function(key) {
                                    return _.chain(originalRange.toObject())
                                        .keys()
                                        .any(function(okey) {
                                            return okey == key;
                                        })
                                        .value() && !(/\_id|\_\_t|\_\_v|itemDefinitionRange/.test(key))
                                })
                                .each(function(key) {
                                    logger.silly('\t\t updating ' + key);
                                    originalRange[key] = update[key];
                                })
                                .value();


                            //hack: set item def range to object id
                            originalRange.itemDefinitionRange = originalRange.itemDefinitionRange._id;

                            originalRange.save(function(err, originalRange) {
                                if (!!err) {
                                    logger.error('[wf update flat] problem saving original range: ' + util.inspect(err));
                                }
                                pp.originalRange = originalRange;
                                cb(err, pp);
                            });
                        },
                        function(pp, cb) {
                            //update only keys that exist
                            _.chain(update)
                                .keys()
                                .filter(function(key) {
                                    return _.chain(original.toObject())
                                        .keys()
                                        .any(function(okey) {
                                            return okey == key;
                                        })
                                        .value() && !(/\_id|\_\_t|\_\_v|itemDefinitionRange/.test(key))
                                })
                                .each(function(key) {
                                    original[key] = update[key];
                                })
                                .value();

                            original.save(function(err, original) {
                                if (!!err) {
                                    logger.error('[wf update flat] problem saving original container: ' + util.inspect(err));
                                }
                                pp.original = original;
                                cb(err, pp);
                            });
                        }
                    ],
                    function(err, pp) {
                        cb(err, pp);
                    });
            };

            crawl(options.workflowItemInstance, p.original, function(err, r) {
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            instanceLoader(models, {
                workflowItemInstance: workflowItemInstanceId,
                flatten: options.flatten
            }, function(err, models, workflowItemInstance) {
                p.updated = workflowItemInstance;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        logger.silly('[wf update flat] finished');
        cb(err, models, p.updated);
    });
};
