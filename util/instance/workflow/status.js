var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler'),
    instanceLoader = require('./loader'),
    definitionLoader = require('../../definition/workflow/loader'),
    stamper = require('../../definition/workflow/stamper');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var customerUserId = _.extractId(options, 'customerUser');

    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('bad customer user getting wf status ' + options.customerUser);
            }
            cb();
        },
        function(cb) {
            if (!!models) {
                logger.silly('[wf status] models passed, skipping model derivation');
                cb(null, models);
            } else {
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: customerUserId.toObjectId()
                    }
                }, cb);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for customer user ' + customerUserId);
            }
            cb(null, models);
        },
        function(models, cb) {
            //get user
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    cb(err, models, {
                        customerUser: customerUser
                    });
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                return cb('customer user ' + customerUserId + ' could not be found for wf statusing');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[wf status] populating wf instances');
            async.waterfall([

                function(cb) {
                    //populate the workflow instances
                    models.auth.CustomerUser.populate(p.customerUser, {
                        path: 'workflowInstances',
                        model: models.instances.workflow.WorkflowItem
                    }, function(err, customerUser) {
                        if (!!err) {
                            logger.error(util.inspect(err));
                        }
                        cb(err, customerUser);
                    });
                }
            ], function(err, customerUser) {
                p.customerUser = customerUser;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {

            //get the workflow definitions for the employee classes
            models.definitions.workflow.ranged.workflowItemTypes.Root.find({
                deleted: false,
                active: true,
                employeeClass: {
                    $elemMatch: {
                        $in: p.customerUser.employeeClass
                    }
                },
            })
                .exec(function(err, rootRanges) {
                    if (!!err) {
                        logger.error("probem loading definitions: " + util.inspect(err));
                    }
                    logger.silly('[wf status] found ' + rootRanges.length + ' def rangess to check');
                    p.rootRanges = rootRanges;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //get the containers for the root ranges
            models.definitions.workflow.workflowItemTypes.Root.find({
                rangedData: {
                    $elemMatch: {
                        $in: _.pluck(p.rootRanges, '_id')
                    }
                }
            })
                .exec(function(err, roots) {
                    p.workflowDefs = roots;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if ((p.workflowDefs || [])
                .length == 0) {
                cb(null, models, p);
            } else {
                definitionLoader(models, {
                    workflowItemDefinitions: p.workflowDefs,
                    flatten: true
                }, function(err, models, workflowDefs) {
                    p.workflowDefs = workflowDefs;
                    cb(err, models, p);
                });
            }
        },
        function(models, p, cb) {
            logger.silly('[wf status] loading wf instances by custuser');
            if ((p.customerUser.workflowInstances || [])
                .length == 0) {
                logger.silly('[wf status] no wf instances found, skipping instance load');
                p.roots = [];
                cb(null, models, p);
            } else {
                instanceLoader(models, {
                    workflowItemInstances: p.customerUser.workflowInstances,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: true
                }, function(err, models, workflowItemInstances) {
                    p.roots = workflowItemInstances;
                    cb(err, models, p);
                });
            }
        },
        function(models, p, cb) {
            //for each root, get the workflow items that are training or quizzes
            p.instances = _.map(p.roots, function(root) {
                logger.silly('[wf status] root: ' + util.inspect(root));
                var itemsToGrade = _.compact(_.filterDeep(root, {
                    __t: function(key, value) {
                        return /(?:workflowItemTypes\.(?:Training|Quiz))$/i.test(value);
                    }
                }));
                var completed = _.filter(itemsToGrade, function(i) {
                    return !!i && !!i.completedDate;
                });

                return {
                    definitionId: root.itemDefinition.toString(),
                    instanceId: root._id.toString(),
                    percentageComplete: parseFloat(((completed.length / itemsToGrade.length) * 100)
                        .toFixed(2)),
                    title: root.title,
                    description: root.description
                };
            });
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[wf status] backfilling wf definitions');
            //what workflows does the user not have?
            var definitions = _.chain(p.workflowDefs)
                .reject(function(d) {
                    //filter any defs that have an instance
                    return (_.any(p.instances, function(i) {
                        return i.definitionId == d._id.toString();
                    }))
                })
                .map(function(d) {
                    return {
                        definitionId: d._id.toString(),
                        instanceId: null,
                        percentageComplete: 0,
                        title: d.title,
                        description: d.description
                    };
                })
                .value();

            p.list = p.instances.concat(definitions);
            cb(null, models, p);
        },
    ], function(err, models, p) {
        logger.silly('[wf status] done.');
        cb(err, models, (p || {})
            .list);
    });
};
