var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {

    options = options || {};
    options = _.defaults(options, {
        flatten: false,
        omitItemDefinitionRange: false
    });

    var workflowItemInstanceIds = extractOptionIds(options, {
        single: 'workflowItemInstance',
        array: 'workflowItemInstances'
    });

    async.waterfall([

        function(cb) {
            if (!workflowItemInstanceIds || workflowItemInstanceIds.length == 0) {
                return cb('Bad item instance ID in wf instance loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemInstanceIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for wf instance ids ' + util.inspect(workflowItemInstanceId));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models, {});
        },
        function(models, p, cb) {
            var crawl = function(item, cb) {
                async.series({
                    ranges: function(cb) {
                        models.instances.workflow.WorkflowItem.populate(item, {
                            path: 'rangedData',
                            model: models.instances.workflow.ranged.WorkflowItem
                        }, function(err, item) {
                            cb(err, item);
                        });
                    },
                    rangeDefinitions: function(cb) {
                        if (!!options.omitItemDefinitionRange) {
                            cb(null, item);
                        } else {
                            models.instances.workflow.WorkflowItem.populate(item, {
                                path: 'rangedData.itemDefinitionRange',
                                model: models.definitions.workflow.ranged.WorkflowItem
                            }, function(err, item) {
                                cb(err, item);
                            });
                        }
                    },
                    /*
                    definition: function(cb) {
                        models.instances.workflow.WorkflowItem.populate(item, {
                            path: 'itemDefinition',
                            model: models.definitions.workflow.WorkflowItem
                        }, function(err, item) {
                            cb(err, item);
                        });
                    },
                    definitionRanges: function(cb){
                        models.instances.workflow.WorkflowItem.populate(item, {
                            path: 'itemDefinition.rangedData',
                            model: models.definitions.workflow.ranged.WorkflowItem
                        }, function(err, item){
                            cb(err, item);
                        });
                    },
                    */
                    sequence: function(cb) {
                        models.instances.workflow.WorkflowItem.populate(item, {
                            path: 'sequence',
                            model: models.instances.workflow.WorkflowItem
                        }, function(err, item) {
                            async.parallel(_.map(item.sequence, function(sequence) {
                                return function(cb) {
                                    crawl(sequence, cb);
                                };
                            }), function(err, r) {
                                cb(err, r);
                            });
                        });
                    },
                    items: function(cb) {
                        models.instances.workflow.WorkflowItem.populate(item, {
                            path: 'items',
                            model: models.instances.workflow.WorkflowItem
                        }, function(err, item) {
                            async.parallel(_.map(item.items, function(item) {
                                return function(cb) {
                                    crawl(item, cb);
                                };
                            }), function(err, r) {
                                cb(err, r);
                            });
                        });
                    }
                }, function(err) {
                    cb(err, item);
                });
            };

            async.parallel(_.map(workflowItemInstanceIds, function(workflowItemInstanceId) {
                return function(cb) {
                    logger.silly('[wf instance loader] loading ' + workflowItemInstanceId);
                    models.instances.workflow.WorkflowItem.findOne({
                        _id: workflowItemInstanceId.toObjectId()
                    })
                        .exec(function(err, item) {
                            crawl(item, function(err, item) {
                                cb(err, item);
                            });
                        });
                }
            }), function(err, r) {
                p.workflowItems = r || [];
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            var crawl = function(item) {
                item.sequence = _.map(item.sequence, function(item) {
                    return crawl(item);
                });
                item.items = _.map(item.items, function(item) {
                    return crawl(item);
                });
                item = datesplice.getByDateRangeSync(item, 'rangedData', options.startDate, options.endDate);

                if (!options.omitItemDefinitionRange) {
                    item = _.defaults(item, item.itemDefinitionRange);
                    item.itemDefinitionRange = item.itemDefinitionRange._id;
                }

                return item;
            };

            if (!!options.flatten) {
                logger.silly('[wf instance loader] flattening workflow!');
                p.workflowItems = _.map(p.workflowItems, function(workflowItem) {
                    return crawl(workflowItem.toObject());
                });
            }
            cb(null, models, p);
        }

    ], function(err, models, p) {
        if (!!err) {
            logger.error('[wf instance loader] problem loading instance: ' + util.inspect(err));
        }
        logger.silly('[wf instance loader] finished loading.');

        cb(err, models, (p || {})
            .workflowItems.length == 1 && !!options.workflowItemInstance ? p.workflowItems[0] : (p || {})
            .workflowItems);
    });
};
