var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId;
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) the model set to query against
    //options: {
    //  workflowItem: the obj or id to find the root for
    //}

    var crawler = function(models, workflowItem, cb) {
        logger.debug('[root for item] crawling ' + workflowItem.name + ' (' + workflowItem._id + ')');
        models.instances.workflow.WorkflowItem.findOne({
            $or: [{
                sequence: workflowItem._id
            }, {
                items: workflowItem._id
            }]
        })
            .exec(function(err, oneUp) {
                if (!oneUp) {
                    //its a root item
                    cb(err, workflowItem);
                } else {
                    crawler(models, oneUp, cb);
                }
            });
    };


    var workflowItemId = _.extractId(options, 'workflowItem');
    async.waterfall([

        function(cb) {
            if (!models) {
                query = {
                    collection: 'instances.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemId.toObjectId()
                    }
                };
                modeler.db(query, function(err, models) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    cb(err, models);
                });
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            //if the workflow item ID is passed in, find the workflow item to crawl
            models.instances.workflow.WorkflowItem.findOne({
                _id: workflowItemId.toObjectId()
            })
                .exec(function(err, wfItem) {
                    cb(err, models, wfItem);
                });
        }
    ], function(err, models, workflowItem) {
        crawler(models, workflowItem, function(err, rootItem) {
            if (!!rootItem.toObject) {
                cb(err, models, rootItem.toObject());
            } else {
                cb(err, models, rootItem);
            }
        });
    });
};
