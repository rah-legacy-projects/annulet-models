var modeler = require('../../../modeler'),
    fixCustomerUser = require('../../fixCustomerUser'),
    _ = require('lodash'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    async = require('async'),
    util = require('util');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var customerUserId = _.extractId(options, 'customerUser');
    options = fixCustomerUser(options);
    var crawler = function(models, workflowItem, cb) {
        //logger.debug('[item save] ' + workflowItem.name);
        var crawlerSequenceTasks = [];
        _.each(workflowItem.sequence, function(sequenceItem) {
            if (!!sequenceItem) {
                crawlerSequenceTasks.push(function(cb) {
                    crawler(models, sequenceItem, function(err, savedItem) {
                        cb(err, savedItem);
                    });
                });
            }
        });

        var crawlerItemTasks = [];
        _.each(workflowItem.items, function(item) {
            if (!!item) {
                crawlerItemTasks.push(function(cb) {
                    crawler(models, item, function(err, savedItem) {
                        cb(err, savedItem);
                    });
                });
            }
        });

        async.parallel({
            sequences: function(cb) {
                async.parallel(crawlerSequenceTasks, function(err, sequences) {
                    cb(err, sequences);
                });
            },
            items: function(cb) {
                async.parallel(crawlerItemTasks, cb);
            }
        }, function(err, r) {
            if (options.fromDefinition) {
                logger.silly('building workflow item from definition (option explicit)');
                var newItem = null;
                //make a clone of the wf item
                var copy = _.clone(workflowItem);
                //nix the collections - we want a bare item with no references
                var instanceType = (workflowItem._type || workflowItem.__t);
                var itemDefinitionId = workflowItem._id;
                delete copy._id;
                delete copy._type;
                delete copy.__t;
                delete copy.sequence;
                delete copy.items;
                delete copy.__v;

                if (!instanceType) {
                    //assume it's a vanilla workflow item?
                    //todo: verify it's a container item?
                    newItem = new models.instances.workflow.WorkflowItem(copy);
                } else {
                    //use the type to determine what kind of workflow item def to make
                    var instanceType = instanceType.replace('definition', 'instance');
                    logger.debug('[instance] new item type: ' + instanceType);
                    var instance = _.resolveObjectPath(models, instanceType);
                    var newItem = new instance(copy);
                }
                newItem.workflowItemDefinition = itemDefinitionId;
                newItem.sequence = _.map(r.sequences, function(sequence) {
                    return sequence._id;
                });
                newItem.items = _.map(r.items, function(item) {
                    return item.id;
                });
                newItem.createdBy = customerUserId;
                newItem.modifiedBy = customerUserId;
                newItem.save(function(err, item) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    cb(err, item);
                });
            } else if (!!workflowItem._id) {
                //find the existing workflow item
                models.instances.workflow.WorkflowItem.findOne({
                    _id: workflowItem._id
                })
                    .exec(function(err, toUpdate) {
                        toUpdate.sequence = _.map(r.sequences, function(sequence) {
                            return sequence._id;
                        });
                        toUpdate.items = _.map(r.items, function(item) {
                            return item._id;
                        });
                        toUpdate.completedDate = workflowItem.completedDate || (workflowItem.isCompleted ? new Date() : null);
                        toUpdate.save(function(err, item) {
                            cb(err, item);
                        });
                    });
            } else if (!!workflowItem.workflowItemDefinition) {
                logger.silly('building workflow item from definition (option implicit)')
                new models.instances.workflow.WorkflowItem({
                    name: workflowItem.name,
                    workflowItemDefinition: workflowItem.workflowItemDefinition,
                    completedDate: null,
                    sequence: _.map(r.sequences, function(sequence) {
                        return sequence._id
                    }),
                    items: _.map(r.items, function(item) {
                        return item._id;
                    })
                })
                    .save(function(err, item) {
                        if (!!err) {
                            logger.error(util.inspect(err));
                        }

                        cb(err, item);
                    });

            } else {
                //new...item...?
                logger.error(util.inspect(workflowItem))
                throw 'items must be from def or identifiable by id';
            }

        });

    };

    var query = {};
    if (options.fromDefinition) {
        query = {
            collection: 'definitions.workflow.WorkflowItem',
            query: {
                _id: options.workflowItem._id
            }
        };
    } else if (!!workflowItem._id) {
        query = {
            collection: 'instances.workflow.WorkflowItem',
            query: {
                _id: options.workflowItem._id
            }
        };
    }
    async.waterfall([

        function(cb) {
            if (!models) {
                modeler.db(query, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            crawler(models, options.workflowItem, function(err, rootItem) {
                if (!!err) {
                    logger.error(util.inspect(err));
                }
                //load saved workflow from the root item and call back with that?
                require('./workflowLoader')(models, {
                    workflowItem: rootItem
                }, function(err, models, instance) {
                    cb(err, models, instance);
                })
            });
        }
    ], function(err, models, p) {
        cb(err, models, p);
    })

}
