var _ = require('lodash'),
    util = require('util'),
    logger = require('winston');


var WorkflowItem = function(sequenceContainer, itemsContainer, data) {
    var self = {};

    //make items if they exist
    if (!!data.items && _.isArray(data.items) && data.items.length > 0) {
        self.items = [];
        _.each(data.items, function(item) {
            self.items.push(new WorkflowItem(null, self, item));
        });
    }
    //make sequence if they exist
    if (!!data.sequence && _.isArray(data.sequence) && data.sequence.length > 0) {
        self.sequence = [];
        _.each(data.sequence, function(sequence) {
            self.sequence.push(new WorkflowItem(self, null, sequence));
        });
    }

    self.updateComplete = function() {
        //update item completeness
        _.each(self.items, function(item) {
            if (!!item.updateComplete) {
                item.updateComplete();
            }
        });

        //update sequence completess
        _.each(self.sequence, function(sequence) {
            if (!!sequence.updateComplete) {
                sequence.updateComplete();
            }
        });

        //if this has items or sequence,
        if (!!self.items || !!self.sequence) {
            //use their completeness to determine my completeness
            self.isComplete = _.all(self.items, function(item) {
                return (!!item.completedDate) || item.isComplete;
            }) && _.all(self.sequence, function(sequence) {
                return (!!sequence.completedDate) || sequence.isComplete;
            })
        } else if (!self.isComplete) {
            //if this doesn't have completeness or completeness is false, set to false
            self.isComplete = false;
        }

        return !!self.isComplete;
    };

    self.updateAvailable = function() {
        //corner case: root: it's always available
        if (!sequenceContainer && !itemsContainer) {
            self.isAvailable = true;
        }

        if (!!sequenceContainer) {
            var ix = _.indexOf(sequenceContainer.sequence, self);
            if (ix == 0) {
                //if this is the first sequence, it's available if the sequence container is
                self.isAvailable = sequenceContainer.isAvailable;
            } else {
                //sequence must be concluded in order.  Is this' previous 'sibling' complete?
                self.isAvailable = sequenceContainer.sequence[ix - 1].isComplete || !!sequenceContainer.sequence[ix - 1].completedDate
            }
        }

        if (!!itemsContainer) {
            //if this is a contained item, it's available if the items container is
            self.isAvailable = itemsContainer.isAvailable;
        }

        //update items availability
        _.each(self.items, function(item) {
            item.updateAvailable();
        });
        //update sequence availability
        _.each(self.sequence, function(sequence) {
            sequence.updateAvailable();
        });

        return self.isAvailable;
    }

    self.update = function() {
        self.updateComplete();
        self.updateAvailable();
    };

    return _.defaults(self, data);

};

module.exports = exports = WorkflowItem;
