var _ = require('lodash');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    var actionCustomerUserId = _.extractId(options, 'actionCustomerUser');
    options.actionCustomerUser = options.actionCustomerUser || {};

    if (!options.actionCustomerUser || _.isString(options.actionCustomerUser) || !options.actionCustomerUser._id) {
        options.actionCustomerUser = {
            _id: actionCustomerUserId,
            user: {}
        }
    }

    options.actionCustomerUser.user = _.defaults(options.actionCustomerUser.user, {
        firstName: '[unknown]',
        lastName: '[unknown]',
        email: '[unknown]'
    });

    return options;
};
