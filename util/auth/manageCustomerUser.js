var _ = require('lodash'),
    async = require('async'),
    modeler = require('../../modeler'),
    logger = require('winston'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //options:
    //{
    //  user: ID of the weak reference user
    //  customer: id or object of the customer
    //  roles: [Owner, Administrator, User],
    //  customerUser:{
    //      object for customer user, not id-inclusive necessarily
    //  }
    //}
    //  models: (optional) the models set to query against
    var customerId = _.extractId(options, 'customer');
    var userId = _.extractId(options, 'user');
    logger.silly('[user manage] at customer ' + customerId);
    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('Bad customer ID for user management: ' + customerId);
            }
            if (!userId) {
                return cb('Bad uer ID for user management: ' + userId);
            }
            cb();
        },
        function(cb) {
            logger.silly('[user manage] getting models');
            if (!models) {
                //get the models for the customer
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset not found for customer ' + customerId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[user manage] getting customer');
            //get the customer
            models.Customer.findOne({
                _id: customerId.toObjectId()
            })
                .exec(function(err, customer) {
                    logger.silly('[user manage] customer: ' + !!customer);
                    cb(err, models, {
                        customer: customer
                    });
                });
        },
        function(models, p, cb) {
            if (!p.customer) {
                return cb('Customer not found for ID ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[user manage] getting customer user');
            models.auth.CustomerUser.findOne({
                customer: customerId.toObjectId(),
                user: userId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                p.isNew = true;
                (new models.auth.CustomerUser({
                    customer: p.customer,
                    user: userId.toObjectId(),
                    createdBy: options.createdBy,
                    modifiedBy: options.modifiedBy
                }))
                    .save(function(err, customerUser) {
                        p.customerUser = customerUser;
                        cb(err, models, p);
                    });
            } else {
                //hack: set activation up as a new customer user
                if (p.customerUser.active != options.customerUser.active && (!p.customerUser.active || options.customerUser.active == true)) {
                    p.isNew = true;
                }
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            logger.silly('[user manage] granting roles on customer');
            //grant the permissions to the user on the customer & save
            p.customer.setAccess(p.customerUser, options.roles);
            //update the active info
            p.customerUser.active = true;
            p.customerUser.deleted = false;
            p.customerUser.employeeClass = options.customerUser.employeeClass;
            p.modifiedBy = options.modifiedBy;
            p.customerUser.save(function(err, customerUser) {
                p.customerUser = customerUser.toObject();
                p.customerUser.isNew = p.isNew;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[user manage] issuemanaging user: ' + util.inspect(err));
            return cb(err, models, null);
        }
        logger.silly('[user manage] done');
        cb(err, models, p.customerUser);
    });
};
