var _ = require('lodash'),
    async = require('async'),
    modeler = require('../../modeler'),
    util = require('util'),
    logger = require('winston');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(options, cb) {
    //options:
    //{
    //  user: ID of the weak reference user
    //}
    //  models: (optional) the models set to query against

    var userId = _.extractId(options, 'user');
    logger.silly('[user customers] looking for: ' + userId);
    async.waterfall([

        function(cb) {
            //get collection names
            async.parallel(_.chain(modeler.dbhash)
                .keys()
                .map(function(key) {
                    return function(cb) {
                        modeler.dbhash[key].connection.db.listCollections()
                            .toArray(function(err, collections) {
                                cb(err, _.map(collections, function(collection) {
                                    return {
                                        name: collection.name,
                                        key: key
                                    }
                                }));
                            });
                    };
                })
                .value(), function(err, r) {
                    cb(err, {
                        namePairs: _.chain(r)
                            .compact()
                            .reduce(function(a, b) {
                                return a.concat(b);
                            }, [])
                            .value()
                    });
                });

        },
        function(p, cb) {
            //filter those names to just the collection(s) being looked for
            p.namePairs = _.filter(p.namePairs, function(namePair) {
                return new RegExp('CustomerUser', 'i')
                    .test(namePair.name) && !(new RegExp('Audit', 'i')
                        .test(namePair.name))
            });
            cb(null, p);
        },
        function(p, cb) {
            //issue the query against those collections
            async.parallel(_.map(p.namePairs, function(namePair) {
                return function(cb) {
                    //make a collection for that name
                    //nb: the slice takes off the database name
                    var dbname = modeler.dbhash[namePair.key].connection.name;
                    var modName = namePair.name;
                    if (modName.indexOf(dbname) >= 0) {
                        modName = modName.slice(dbname.length + 1);
                    } else {
                        //logger.silly(modName + ' in ' + namePair.key + ' did not have db prefix');
                    }
                    modeler.dbhash[namePair.key].connection.db.collection(modName, function(err, collection) {
                        //issue the query against that collection
                        collection.count({
                            user: userId.toObjectId()
                        }, function(err, result) {
                            logger.silly('result for ' + modName + ': ' + result);
                            //should that collection have a result, bubble back the collection
                            if (!!result) {
                                cb(err, modName);
                            } else {
                                cb(err, null);
                            }
                        });
                    });
                };
            }), function(err, r) {
                //r should contain the collections that matched, if any
                //filter out misses
                p.collectionNames = _.compact(r);
                if (p.collectionNames.length == 0) {
                    p.collectionNames = null;
                }
                cb(err, p);
            });
        },
        function(p, cb) {
            //get the customer IDs out of the names
            p.customerIds = _.map(p.collectionNames, function(collectionName) {
                return (/^C([0-9a-fA-F]{24})\./.exec(collectionName))[1];
            });

            cb(null, p);
        },
    ], function(err, p) {
        cb(err, p.customerIds);
    });
};
