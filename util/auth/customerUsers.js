var _ = require('lodash'),
    async = require('async'),
    modeler = require('../../modeler'),
    logger = require('winston'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //options:
    //{
    //}
    //  models: (optional) the models set to query against
    var customerId = _.extractId(options, 'customer');
    var actionCustomerUserId = _.extractId(options, 'actionCustomerUser');

    options = _.defaultsDeep(options || {}, {
        omitSystemUsers: true
    });

    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('Bad customer ID for getting customer users: ' + customerId);
            }
            cb();
        },
        function(cb) {
            if (!models) {
                //get the models for the customer
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset not found for customer ' + customerId);
            }
            cb(null, models);
        },
        function(models, cb) {
            //get the customer
            models.Customer.findOne({
                _id: customerId.toObjectId()
            })
                .exec(function(err, customer) {
                    cb(err, models, {
                        customer: customer
                    });
                });
        },
        function(models, p, cb) {
            if (!p.customer) {
                return cb('Customer not found for ID ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            models.auth.CustomerUser.find()
                .exec(function(err, customerUsers) {
                    p.customerUsers = _.map(customerUsers, function(customerUser) {
                        var access = p.customer.getAccess(customerUser);
                        var customerUserObj = customerUser.toObject();
                        customerUserObj.roles = access;
                        return customerUserObj
                    });
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (options.omitSystemUsers) {
                p.customerUsers = _.filter(p.customerUsers, function(customerUser) {
                    return !_.any(customerUser.roles, function(role) {
                        return role == 'System User';
                    });
                });
            }
            cb(null, models, p);
        }
    ], function(err, models, p) {
        logger.silly('[customer users] done');
        cb(err, models, p.customerUsers);
    });
};
