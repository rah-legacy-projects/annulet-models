module.exports = exports = {
	auth: require('./auth'),
	definition: require("./definition"),
	instance: require("./instance"),
	customer: require('./customer'),
	complaint: require('./complaint')
};
