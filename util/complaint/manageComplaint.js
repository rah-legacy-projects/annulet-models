var _ = require('lodash'),
    async = require('async'),
    modeler = require('../../modeler'),
    schema = require('../../schema'),
    logger = require('winston'),
    moment = require('moment'),
    fixActionCustomerUser = require('../fixActionCustomerUser'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //  models: (optional) the models set to query against
    //options:
    //{
    //  customerUser: id or object of the creating or modifying customer user
    //  complaint: body of the complaint
    //}
    var customerUserId = _.extractId(options, 'customerUser');
    var complaintId = _.extractId(options, 'complaint');
    logger.silly('[complaint manage] at customer user ' + customerUserId);
    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('Invalid customer user id.');
            }
            cb();
        },
        function(cb) {
            options = fixActionCustomerUser(options);
            logger.silly('[complaint manage] getting models');
            if (!models) {
                //get the models for the customer user
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: customerUserId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('customer modelset could not be determined from customer user ' + customerUserId);
            }
            cb(null,models, {});
        },
        function(models, p, cb){
            models.auth.CustomerUser.findOne({_id: customerUserId}).exec(function(err, customerUser){
                if(!customerUser){
                    return cb('customer user could not be identified from ' + customerUserId);
                }
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[complaint manage] looking for complaint: ' + options.complaint._id);
            models.complaints.Complaint.findOne({
                _id: (!complaintId ? complaintId : complaintId.toObjectId())
            })
                .exec(function(err, complaint) {
                    if (!complaint) {
                        logger.silly('[complaint manage] new complaint');
                        //new complaint
                        complaint = new models.complaints.Complaint(options.complaint);
                        complaint.createdBy = customerUserId;
                        complaint.modifiedBy = customerUserId;
                        if (!complaint.customer) {
                            complaint.customer = options.customerUser.customer;
                        }
                        if (!complaint.createdByUserId) {
                            complaint.createdByCustomerUser = customerUserId;
                        }
                        var changes = [new models.complaints.Change({
                            field: null,
                            changeType: 'created',
                            before: null,
                            after: null,
                            madeBy: customerUserId.toObjectId(),
                            createdBy: customerUserId,
                            modifiedBy: customerUserId
                        })];
                        async.waterfall([
                            //save change listing
                            function(cb) {
                                async.parallel(_.map(changes, function(change) {
                                    return function(cb) {
                                        change.save(function(err, change) {
                                            if (!!err) {
                                                logger.error('[complaint manage] problem saving change: ' + util.inspect(err));
                                            }
                                            cb(err, change);
                                        });
                                    };
                                }), function(err, changes) {
                                    logger.silly('[complaint manage] changes saved: ' + changes.length);
                                    cb(err, _.map(changes, function(change) {
                                        return change._id;
                                    }));
                                });
                            },
                            function(changeIds, cb) {
                                //put changes in new complaint
                                complaint.changes = (complaint.changes || [])
                                    .concat(changeIds);
                                //save complaint
                                complaint.save(function(err, complaint) {
                                    cb(err, complaint);
                                });
                            }
                        ], function(err, complaint) {
                            p.complaint = complaint;
                            cb(err, models, p);
                        });


                    } else {
                        //existing complaint
                        var changes = [];
                        var keys = _.keys(models.complaints.Complaint.schema.paths);
                        keys = _.difference(keys, [
                            'changes',
                            'comments',
                            'id',
                            '_id',
                            '__v',
                            'customer',
                            'createdByCustomerUser',
                            'created',
                            'active',
                            'deleted',
                            'modified',
                            'createdBy',
                            'modifiedBy'
                        ]);

                        _.each(keys, function(key) {
                            logger.silly('\t\t\t\t key: ' + key);
                            //build change listing
                            var lhs, rhs;
                            if (models.complaints.Complaint.schema.tree[key].type == Date) {
                                lhs = !!complaint[key] ? moment(complaint[key])
                                    .format('MM/DD/YYYY') : null;
                                rhs = !!options.complaint[key] ? moment(options.complaint[key])
                                    .format('MM/DD/YYYY') : null;
                            } else if (models.complaints.Complaint.schema.tree[key].type == Boolean) {
                                if (/true/i.test(complaint[key])) {
                                    lhs = true;
                                } else if (/false/i.test(complaint[key])) {
                                    lhs = false;
                                }

                                if (/true/i.test(options.complaint[key])) {
                                    rhs = true;
                                } else if (/false/i.test(options.complaint[key])) {
                                    rhs = false;
                                }
                            } else {
                                lhs = (complaint[key] || '')
                                    .toString();
                                rhs = (options.complaint[key] || '')
                                    .toString();
                            }
                            if (lhs !== rhs) {
                                logger.silly('[manage complaint] ' + key + ': ' + lhs + ' to ' + rhs);
                                changes.push(new models.complaints.Change({
                                    field: key,
                                    before: lhs || 'Not Set', //complaint[key],
                                    after: rhs || 'Not Set', //options.complaint[key],
                                    madeBy: customerUserId.toObjectId(),
                                    changeType: 'update',
                                    createdBy: customerUserId,
                                    modifiedBy: customerUserId
                                }));

                                complaint[key] = options.complaint[key];
                            }
                        });

                        async.waterfall([
                            //save change listing
                            function(cb) {
                                async.parallel(_.map(changes, function(change) {
                                    return function(cb) {
                                        change.save(function(err, change) {
                                            if (!!err) {
                                                logger.error('problem adding change to updated complaint: ' + util.inspect(err));
                                            }
                                            cb(err, change);
                                        });
                                    };
                                }), function(err, changes) {
                                    cb(err, _.map(changes, function(change) {
                                        return change._id;
                                    }));
                                });
                            },
                            function(changeIds, cb) {
                                //put changes in existing complaint
                                complaint.changes = (complaint.changes || [])
                                    .concat(changeIds);
                                //save complaint
                                complaint.save(function(err, complaint) {
                                    if (!!err) {
                                        logger.error('problem updating existing complaint: ' + util.inspect(err));
                                    }
                                    cb(err, complaint);
                                });
                            }
                        ], function(err, complaint) {
                            p.complaint = complaint;
                            cb(err, models, p);
                        });

                    }
                });
        }
    ], function(err, models, p) {
        logger.silly('[complaint manage] complaint management complete');
        cb(err, models, (p || {})
            .complaint);
    });
};
