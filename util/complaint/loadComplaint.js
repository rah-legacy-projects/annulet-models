var _ = require('lodash'),
    async = require('async'),
    modeler = require('../../modeler'),
    logger = require('winston'),
    moment = require('moment'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //  models: (optional) the models set to query against
    //options:
    //{
    //  complaint: object or id of the complaint to load
    //}
    var complaintId = _.extractId(options, 'complaint');
    logger.silly('[complaint load] complaint id: ' + complaintId);
    async.waterfall([

        function(cb) {
            if (!complaintId) {
                return cb('attempted to load with invalid complaint ID.')
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'complaints.Complaint',
                    query: {
                        _id: complaintId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('model set could not be identified for complaint ' + complaintId);
            }
            cb(null, models);
        },
        function(models, cb) {
            models.complaints.Complaint.findOne({
                _id: complaintId.toObjectId()
            })
                .exec(function(err, complaint) {

                    cb(err, models, {
                        complaint: complaint
                    });
                });
        },
        function(models, p, cb) {
            if (!p.complaint) {
                return cb('Complaint not found for ' + complaintId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load changes
            models.complaints.Complaint.populate(p.complaint, {
                path: 'changes',
                model: models.complaints.Change
            }, function(err, complaint) {
                p.complaint = complaint;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load comments
            models.complaints.Complaint.populate(p.complaint, {
                path: 'comments',
                model: models.complaints.Comment
            }, function(err, complaint) {
                p.complaint = complaint;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load customer
            models.complaints.Complaint.populate(p.complaint, {
                path: 'customer',
                model: models.Customer
            }, function(err, complaint) {
                p.complaint = complaint;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load creating customeruser
            models.complaints.Complaint.populate(p.complaint, {
                path: 'createdByCustomerUser',
                model: models.auth.CustomerUser
            }, function(err, complaint) {
                p.complaint = complaint;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            models.complaints.Complaint.populate(p.complaint, {
                path: 'changes.madeBy',
                model: models.auth.CustomerUser
            }, function(err, complaint) {
                p.complaint = complaint;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            models.complaints.Complaint.populate(p.complaint, {
                path: 'comments.madeBy',
                model: models.auth.CustomerUser
            }, function(err, complaint) {
                p.complaint = complaint;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        logger.silly('[load complaint] complete');
        cb(err, models, (p || {})
            .complaint);
    });
};
