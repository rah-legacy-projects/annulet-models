var _ = require('lodash');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options, members) {
    var ids;
    if (!!options[members.array] && _.isArray(options[members.array])) {
        ids = _.map(options[members.array], function(thing) {
            return _.extractId(thing, '_id');
        });
    } else if (!!options[members.single] && _.isObject(options[members.single])) {
        ids = [_.extractId(options[members.single], '_id')];
    } else if (!!options[members.single] && _.isString(options[members.single])) {
        ids = [_.extractId(options[members.single], '_id')];
    } else {
        throw new Error('invalid combination of options for options members');
    }
    return _.chain(ids)
        .compact()
        .map(function(id) {
            return id.toObjectId();
        })
        .value();
};
