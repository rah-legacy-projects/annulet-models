var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    loader = require('./loader'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var customerId = _.extractId(options, 'customer');

    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('Bad customer ID in operatingProcedure def loader by shortname.');
            }
            if (!options.shortName) {
                return cb('Bad shortname in operatingProcedure def loader by shortname.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for operatingProcedure def id ' + operatingProcedureDefinitionId);
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            cb(null, models);
        },
        function(models, cb) {
            models.definitions.operatingProcedure.OperatingProcedure.find({
                shortName: options.shortName,
                active: true,
                deleted: false
            })
                .exec(function(err, operatingProcedures) {
                    cb(err, models, {
                        operatingProcedures: operatingProcedures
                    });
                });
        },
        function(models, p, cb) {
            if (p.operatingProcedures.length == 0) {
                //no ops, defs, etc, skip load
                cb(null, models, p);
            } else {

                //load the operatingProcedures
                loader(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    operatingProcedureDefinitions: _.pluck(p.operatingProcedures, '_id')
                }, function(err, models, operatingProcedures) {
                    p.operatingProcedures = operatingProcedures;
                    cb(err, models, p);
                });
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[operatingProcedure def shortname loader] problem loading def: ' + util.inspect(err));
        }
        cb(err, models, (p || {})
            .operatingProcedures);
    });
};
