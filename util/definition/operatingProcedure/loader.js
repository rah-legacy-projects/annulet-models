var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var operatingProcedureDefinitionIds = extractOptionIds(options, {
        single: 'operatingProcedureDefinition',
        array: 'operatingProcedureDefinitions'
    });
    async.waterfall([

        function(cb) {
            if (!operatingProcedureDefinitionIds || operatingProcedureDefinitionIds.length == 0) {
                logger.error('no opdef ids for defloader');
                return cb('Bad operating procedure definition ID in OP def loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.operatingProcedure.OperatingProcedure',
                    query: {
                        _id: operatingProcedureDefinitionIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for op def ids ' + util.inspect(operatingProcedureDefinitionIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[operatingProcedure def loader] loading definition');
            models.definitions.operatingProcedure.OperatingProcedure.find({
                _id: {
                    $in: operatingProcedureDefinitionIds
                }
            })
                .exec(function(err, operatingProcedures) {
                    cb(err, models, {
                        operatingProcedures: operatingProcedures
                    });
                });
        },
        function(models, p, cb) {
            if (!p.operatingProcedures) {
                return cb('operating procedure def could not be found for id ' + util.inspect(operatingProcedureDefinitionIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the sections
            models.definitions.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'sections',
                model: models.definitions.operatingProcedure.Section
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data
            models.definitions.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'rangedData',
                model: models.definitions.operatingProcedure.ranged.OperatingProcedure
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op def loader] getting sections');
            //load the sections' ranged data
            models.definitions.operatingProcedure.OperatingProcedure.populate(p.operatingProcedures, {
                path: 'sections.rangedData',
                model: models.definitions.operatingProcedure.ranged.Section
            }, function(err, operatingProcedures) {
                p.operatingProcedures = operatingProcedures;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op def loader] op loaded.');
            if (!!options.flatten) {
                logger.silly('[op def loader] op is being flattened.');
                async.parallel(_.map(p.operatingProcedures, function(operatingProcedure) {
                    return function(cb) {
                        //objectify the OP for flattening
                        operatingProcedure = operatingProcedure.toObject();
                        var containerActive = operatingProcedure.active;
                        //flatten the sections and the OP
                        async.parallel({
                            OP: function(cb) {
                                datesplice.getByDateRange(operatingProcedure, 'rangedData', options.startDate, options.endDate, function(err, flatOP) {
                                    //hack: add range type
                                    flatOP._rangeType = 'definitions.operatingProcedure.ranged.OperatingProcedure';
                                    //hack: make sure active uses the container's, not the range's, activation value
                                    flatOP.active = containerActive;
                                    cb(err, flatOP);
                                });
                            },
                            sections: function(cb) {
                                async.parallel(_.map(operatingProcedure.sections, function(section) {
                                    return function(cb) {
                                        datesplice.getByDateRange(section, 'rangedData', options.startDate, options.endDate, function(err, flatSection) {
                                            cb(err, flatSection);
                                        });
                                    };
                                }), function(err, r) {
                                    cb(err, r);
                                });

                            }
                        }, function(err, r) {
                            operatingProcedure = r.OP;
                            operatingProcedure.sections = r.sections;
                            cb(err, operatingProcedure);
                        });
                    };
                }), function(err, r) {
                    p.operatingProcedures = r;
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[op def loader] problem loading def: ' + util.inspect(err));
        }
        logger.silly('[op def loader] finished loading.');
        cb(err, models, (p || {})
            .operatingProcedures.length == 1 && !!options.operatingProcedureDefinition ? p.operatingProcedures[0] : (p || {})
            .operatingProcedures);
    });
};
