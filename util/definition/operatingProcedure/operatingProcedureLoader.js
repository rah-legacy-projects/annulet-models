var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var operatingProcedureDefinitionId = _.extractId(options, 'operatingProcedureDefinition');
    logger.silly('[operatingProcedure def loader] td id: ' + operatingProcedureDefinitionId);
    async.waterfall([

        function(cb) {
            if (!operatingProcedureDefinitionId) {
                return cb('Bad operating procedure definition ID in OP def loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.operatingProcedure.OperatingProcedure',
                    query: {
                        _id: operatingProcedureDefinitionId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for op def id ' + operatingProcedureDefinitionId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[operatingProcedure def loader] loading definition');
            models.definitions.operatingProcedure.OperatingProcedure.findOne(operatingProcedureDefinitionId.toObjectId())
                .exec(function(err, operatingProcedure) {
                    cb(err, models, {
                        operatingProcedure: operatingProcedure
                    });
                });
        },
        function(models, p, cb) {
            if (!p.operatingProcedure) {
                return cb('operating procedure def could not be found for id ' + operatingProcedureDefinitionId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            models.definitions.operatingProcedure.OperatingProcedure.populate(p.operatingProcedure, {
                path: 'sections',
                model: models.definitions.operatingProcedure.Section
            }, function(err, operatingProcedure) {
                p.operatingProcedure = operatingProcedure;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        cb(err, models, (p || {})
            .operatingProcedure);
    });
};
