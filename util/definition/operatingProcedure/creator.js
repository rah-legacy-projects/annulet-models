var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    operatingProcedureLoader = require('./operatingProcedureLoader'),
    fixCustomerUser = require('../../fixCustomerUser');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against,
    //  customerUser: customer based user with the agnostic user loaded,
    //  operatingProcedureDefinition: object representing the operatingProcedure to create
    //}

    var customerId = _.extractId(options, 'customer'),
        customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to create OP.");
            }
            cb(null);
        },
        function(cb) {
            options = fixCustomerUser(options);
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[operatingProcedure creator] making section defs');
            //for each section,
            async.parallel(_.map(options.operatingProcedureDefinition.sections, function(section) {
                logger.silly('[operatingProcedure creator] ' + util.inspect(section));
                return function(cb) {
                    var typePath = section.__t;
                    delete section.__t;
                    section.createdBy = customerUserId,
                    section.modifiedBy = customerUserId,
                    //for each question, create with the model required and save
                    logger.silly('[operatingProcedure creator] creating new def ' + typePath);
                    (new _.resolveObjectPath(models, typePath)(section))
                        .save(function(err, section) {
                            if (!!err) {
                                logger.error('[operatingProcedure creator] problem saving: ' + util.inspect(err));
                            }
                            return cb(err, section);
                        });
                };
            }), function(err, sections) {
                cb(err, models, {
                    sections: sections
                });
            });
        },
        function(models, p, cb) {
            logger.silly('[operatingProcedure creator] making operatingProcedure def');
            var operatingProcedure = new models.definitions.operatingProcedure.OperatingProcedure({
                title: options.operatingProcedureDefinition.title,
                description: options.operatingProcedureDefinition.description,
                createdBy: customerUserId,
                modifiedBy: customerUserId,
                customer: customerId.toObjectId(),
            });
            operatingProcedure.sections = _.map(p.sections, function(section) {
                return section._id;
            });
            operatingProcedure.save(function(err, operatingProcedure) {
                p.operatingProcedure = operatingProcedure;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating operatingProcedure: ' + util.inspect(err));
        }
        logger.silly('[operatingProcedure creator] operatingProcedure created.');
        cb(err, models, (p || {})
            .operatingProcedure);
    });
};
