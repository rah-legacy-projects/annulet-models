var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    operatingProcedureLoader = require('../loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    operatingProcedureInstanceLoader = require('../../../instance/operatingProcedure/loader'),
    stamp = require('./stamper');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  customerUser: id or object of the customer user,
    //  actionCustomerUser
    //
    //}

    options = fixActionCustomerUser(options);
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('stamp op by customer user, bad customer user id: ' + options.customerUser);
            }
            cb();
        },
        function(cb) {
            logger.silly('[op stamp by customeruser] customer user = ' + customerUserId);
            if (!models) {
                logger.silly('[op stamp by customeruser] retrieving models ');
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: customerUserId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            //identify the customer user
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //get the OP instances for the customer user
            models.instances.operatingProcedure.OperatingProcedure.find({
                customerUser: p.customerUser._id,
                active:true,
                deleted:false
            })
                .exec(function(err, operatingProcedureInstances) {
                    p.instances = operatingProcedureInstances;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //get the OP definitions for the customer
            models.definitions.operatingProcedure.OperatingProcedure.find({
                active:true,
                deleted:false
            })
                .exec(function(err, operatingProcedureDefinitions) {
                    p.definitions = operatingProcedureDefinitions;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //reconcile stamped with not-stamped
            var defIds = _.map(p.definitions, function(d) {
                return d._id.toString();
            });
            var instanceDefIds = _.map(p.instances, function(i) {
                return i.operatingProcedureDefinition.toString();
            });
            var idsToStamp = _.difference(defIds, instanceDefIds);

            logger.silly('[op stamp by customeruser] to stamp: ' + idsToStamp.length);
            //stamp the OPs
            if (idsToStamp.length > 0) {
                async.parallel(_.map(idsToStamp, function(defId) {
                    return function(cb) {
                        logger.silly('[op stamp by customeruser] stamping ' + defId);
                        stamp(models, {
                            operatingProcedureDefinition: defId,
                            customerUser: p.customerUser._id,
                            actionCustomerUser: options.actionCustomerUser
                        }, function(err, models, operatingProcedureInstance) {
                            logger.silly('[op stamp by customeruser] stamped ' + defId);
                            cb(err, operatingProcedureInstance);
                        });
                    }
                }), function(err, stamped) {
                    logger.silly('[op stamp by customeruser] OPs stamped: ' + stamped.length);
                    p.stamped = stamped;
                    p.instances = p.instances.concat(stamped);
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb){
            operatingProcedureInstanceLoader(models,{
                actionCustomerUser: options.actionCustomerUser,
                operatingProcedureInstances: p.instances,
                flatten: options.flatten,
            }, function(err, models, opInstances){
                p.instances = opInstances;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, (p||{}).instances);
    });
};
