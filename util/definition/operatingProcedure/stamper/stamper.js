var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    definitionLoader = require('../loader'),
    instanceLoader = require('../../../instance/operatingProcedure/loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    stampClone = require('../../stampClone');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  operatingProcedureDefinition: the operatingProcedure def to stamp,
    //  customerUser: the customer user the instance belongs to,
    //  actionCustomerUser
    //}

    options = fixActionCustomerUser(options);
    var operatingProcedureDefinitionId = _.extractId(options, 'operatingProcedureDefinition');
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!operatingProcedureDefinitionId) {
                return cb('bad op def id when stamping: ' + options.operatingProcedureDefinition);
            }
            if (!customerUserId) {
                return cb('bad customer user id when stamping op: ' + options.customerUser);
            }

            cb();
        },
        function(cb) {
            logger.silly('[operatingProcedure stamper] op def id: ' + operatingProcedureDefinitionId);

            if (!models) {
                modeler.db({
                    collection: 'definitions.operatingProcedure.OperatingProcedure',
                    query: {
                        _id: operatingProcedureDefinitionId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, {});
                });
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('modelset could not be identified for stamping an op def: ' + operatingProcedureDefinitionId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //verify customer user exists
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                return cb('customer user could not be found for OP stamp: ' + customerUserId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.debug('[operatingProcedure stamper] loading definition ' + operatingProcedureDefinitionId);
            definitionLoader(models, {
                operatingProcedureDefinition: operatingProcedureDefinitionId.toObjectId(),
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, operatingProcedureDefinition) {
                cb(err, models, {
                    operatingProcedureDefinition: operatingProcedureDefinition
                });
            });
        },
        function(models, p, cb) {
            models.instances.operatingProcedure.OperatingProcedure.findOne({
                operatingProcedureDefinition: operatingProcedureDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, operatingProcedureInstance) {
                    if (!operatingProcedureInstance) {
                        p._state = 'Created';
                        operatingProcedureInstance = new models.instances.operatingProcedure.OperatingProcedure({
                            operatingProcedureDefinition: p.operatingProcedureDefinition._id,
                            customerUser: customerUserId.toObjectId(),
                            createdBy: options.actionCustomerUser._id,
                            modifiedBy: options.actionCustomerUser._id,
                            rangedData: [],
                            sections: [],
                            shortName: p.operatingProcedureDefinition.shortName
                        });
                        p.operatingProcedureInstance = operatingProcedureInstance;
                        cb(err, models, p);

                    } else {
                        p._state = 'Opened';
                        //load the OP instance.
                        instanceLoader(models, {
                            operatingProcedureInstance: operatingProcedureInstance,
                            flatten: false
                        }, function(err, models, operatingProcedureInstance) {
                            p.operatingProcedureInstance = operatingProcedureInstance;
                            cb(err, models, p);
                        });
                    }

                });
        },
        function(models, p, cb) {
            //determine parts to splice
            var opInstanceRangesToSplice = _.map(p.operatingProcedureDefinition.rangedData, function(opDefRange) {
                var range = _.find(p.operatingProcedureInstance.rangedData, function(opInstanceRange) {
                    return opInstanceRange.operatingProcedureDefinitionRange._id.toString() == opDefRange._id.toString();
                });

                //if the range doesn't exist or is otherwise different, splice it, otherwise return nothing
                if (!range ||
                    (moment(range.endDate)
                        .toString() != moment(opDefRange.endDate)
                        .toString()) ||
                    (moment(range.startDate)
                        .toString() != moment(opDefRange.startDate)
                        .toString())) {
                    //create a new range to splice
                    return new models.instances.operatingProcedure.ranged.OperatingProcedure({
                        title: opDefRange.title,
                        description: opDefRange.description,
                        completedDate: null,
                        operatingProcedureDefinitionRange: opDefRange._id,
                        startDate: opDefRange.startDate,
                        endDate: opDefRange.endDate,
                        rangeActive: opDefRange.rangeActive,
                        createdBy: options.actionCustomerUser._id
                    });
                }

                return null;
            });


            //crunch out the nulls
            opInstanceRangesToSplice = _.compact(opInstanceRangesToSplice);


            //splice the ranged data in series
            //note this has to be in series to ensure correct order of range operations
            var instanceRanges = p.operatingProcedureInstance.rangedData; //_.clone(p.operatingProcedureInstance.rangedData);
            async.series(_.map(opInstanceRangesToSplice, function(range) {
                return function(cb) {
                    datesplice.splice(instanceRanges, range, function(err, toSave) {
                        instanceRanges = toSave;
                        cb(err);
                    });
                };
            }), function(err) {
                logger.silly('[op stamper] splice-saving OP');
                datesplice.dateSpliceSave(options.actionCustomerUser._id, p.operatingProcedureInstance, instanceRanges, 'rangedData', function(err, savedOPInstance) {
                    //skip overwriting op, still need to save the sections
                    logger.silly('[op stamper] OP spliced.');
                    cb(err, models, p);
                });
            });

        },
        function(models, p, cb) {
            async.series(_.map(p.operatingProcedureDefinition.sections, function(sectionDefinition) {
                return function(cb) {
                    var sectionInstance = _.find(p.operatingProcedureInstance.sections, function(section) {
                        return section.sectionDefinition.toString() == sectionDefinition._id.toString();
                    });

                    if (!sectionInstance) {
                        sectionInstance = new(_.resolveObjectPath(models, sectionDefinition.__t.replace('definition', 'instance')))({
                            sectionDefinition: sectionDefinition._id,
                            createdBy: options.actionCustomerUser._id,
                            rangedData: []
                        });
                    }

                    var sectionInstanceRangesToSplice = _.map(sectionDefinition.rangedData, function(sectionDefRange) {
                        var range = _.find(sectionInstance.rangedData, function(sectionInstanceRange) {
                            return sectionInstanceRange.sectionDefinitionRange._id.toString() == sectionDefRange._id.toString();
                        });

                        if (!range ||
                            moment(range.endDate)
                            .toString() != moment(sectionDefRange.endDate)
                            .toString() ||
                            moment(range.startDate)
                            .toString() != moment(sectionDefRange.startDate)
                            .toString()) {
                            //create a new range to splice
                            var section = _.clone(sectionDefRange.toObject());
                            var sectionDefId = section._id;
                            delete section._id;
                            delete section.__t;
                            section.createdBy = options.actionCustomerUser._id;
                            section.modifiedBy = options.actionCustomerUser._id;
                            section.sectionDefinitionRange = sectionDefRange._id;
                            return new(_.resolveObjectPath(models, sectionDefRange.__t.replace('definition', 'instance')))(section);
                        }

                        return null;
                    });

                    //crunch out the nulls
                    sectionInstanceRangesToSplice = _.compact(sectionInstanceRangesToSplice);


                    if (sectionInstanceRangesToSplice.length == 0) {
                        //skip splicing.
                        logger.silly('[op stamper] skipping splice save, no ranges to splice');
                        cb(null, sectionInstance);
                    } else {
                        //splice the section ranged data in order
                        var instanceRanges = sectionInstance.rangedData.toObject();
                        _.each(instanceRanges, function(instanceRange) {
                            instanceRange.sectionDefinitionRange = instanceRange.sectionDefinitionRange._id;
                        });
                        async.series(_.map(sectionInstanceRangesToSplice, function(range) {
                            return function(cb) {
                                datesplice.splice(instanceRanges, range, function(err, toSave) {
                                    instanceRanges = toSave;
                                    cb(err);
                                });
                            };
                        }), function(err) {
                            logger.silly('[op stamper] splice-saving section');
                            datesplice.dateSpliceSave(options.actionCustomerUser._id, sectionInstance, instanceRanges, 'rangedData', function(err, savedSectionInstance) {
                                cb(err, savedSectionInstance);
                            });
                        });
                    }
                };
            }), function(err, r) {
                //r contains the saved section instances
                //put section instance IDs on the OP instance
                p.operatingProcedureInstance.sections = _.pluck(r, '_id');
                p.operatingProcedureInstance.save(function(err, opInstance) {
                    cb(err, models, p);
                });
            });
        },
        function(models, p, cb) {
            logger.debug('[operatingProcedure stamper] getting OP instance for def id ' + operatingProcedureDefinitionId);
            models.instances.operatingProcedure.OperatingProcedure.findOne({
                operatingProcedureDefinition: operatingProcedureDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, operatingProcedureInstance) {
                    p.operatingProcedureInstance = operatingProcedureInstance;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[operatingProcedure stamper] loading instance');
            instanceLoader(models, {
                operatingProcedureInstance: p.operatingProcedureInstance,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, operatingProcedureInstance) {
                logger.silly('[operatingProcedure stamper] unflat instance loaded.');
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            logger.silly('[operatingProcedure stamper] loading instance');
            instanceLoader(models, {
                operatingProcedureInstance: p.operatingProcedureInstance,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, operatingProcedureInstance) {
                logger.silly('[operatingProcedure stamper] instance loaded.');
                p.operatingProcedureInstance = operatingProcedureInstance;
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            (new models.tracking.ItemActivity({
                changedBy: options.actionCustomerUser._id,
                state: p._state,
                itemType: 'instances.operatingProcedure.OperatingProcedure',
                item: p.operatingProcedureInstance._id,
                message: {
                    firstName: options.actionCustomerUser.user.firstName,
                    lastName: options.actionCustomerUser.user.lastName,
                    email: options.actionCustomerUser.user.email,
                    itemTitle: p.operatingProcedureInstance.title
                }
            }))
                .save(function(err, activity) {
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        logger.silly('[operatingProcedure stamper] op stamped!');
        cb(err, models, p.operatingProcedureInstance);
    });
};
