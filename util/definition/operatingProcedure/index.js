module.exports = exports = {
	draft: require("./draft"),
	loadByShortName: require("./loadByShortName"),
	loader: require("./loader"),
	stamper: require("./stamper"),
};
