var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/operatingProcedure/loader'),
    closeDraft = require('./close');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  operatingProcedureDefinition: object representing the operatingProcedure to create,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        operatingProcedureDefinitionId = _.extractId(options, 'operatingProcedureDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!customerId) {
                    return cb("Bad customer ID when trying to publish draft of OP.");
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for OPs by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.operatingProcedureDefinition) {
                    return cb('Operating procedure definition not supplied to publish draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    operatingProcedureDefinition: operatingProcedureDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if (!!p.draft.isDraftOf && p.draft.isDraftOf === 'new') {
                    p.originalDraft = new models.definitions.operatingProcedure.OperatingProcedure({
                        rangedData: [],
                        sections: [],
                        customer: customerId,
                        shortName: p.draft.shortName
                    });
                    cb(null, models, p);
                } else {
                    definitionLoader(models, {
                        operatingProcedureDefinition: p.draft.isDraftOf,
                        actionCustomerUser: options.actionCustomerUser,
                        flatten: false
                    }, function(err, models, original) {
                        p.originalDraft = original;
                        cb(err, models, p);
                    })
                }
            },
            function(models, p, cb) {
                logger.silly('[op publish] about to publish sections and ranges');
                async.parallel({
                    sections: function(cb) {
                        async.series(_.map(p.draft.sections, function(newSection) {
                            return function(cb) {
                                async.waterfall([

                                    function(cb) {
                                        cb(null, {});
                                    },
                                    function(pp, cb) {
                                        //find original section
                                        var originalSection = _.find(p.originalDraft.sections, function(originalSection) {
                                            return originalSection._id.toString() == (newSection.isDraftOf || '')
                                                .toString();
                                        });

                                        //if the original section did not exist, create one
                                        if (!originalSection) {
                                            var copy = newSection.toObject();
                                            delete copy._id;
                                            delete copy.isDraftOf;
                                            delete copy.rangedData;
                                            (new(_.resolveObjectPath(models, newSection.__t))(copy))
                                                .save(function(err, section) {
                                                    pp.section = section;
                                                    cb(err, pp);
                                                });
                                        } else {
                                            pp.section = originalSection;
                                            cb(null, pp);
                                        }
                                    },
                                    function(pp, cb) {
                                        //splice the new section ranged data into the original section's ranged data
                                        var instancesToSave = pp.section.rangedData;
                                        async.series(_.map(newSection.rangedData, function(newRange) {
                                            return function(cb) {
                                                //see if there was an existing section range that is prexactly the same, skip the splice
                                                if (_.any(pp.section.rangedData, function(originalRange) {
                                                    var comparison = _.resolveObjectPath(models, originalRange.__t)
                                                        .compare(originalRange, newRange);
                                                    return comparison;
                                                })) {
                                                    cb();
                                                } else {
                                                    var copy = _.clone(newRange.toObject());
                                                    delete copy._id;
                                                    delete copy.isDraftOf;
                                                    var range = new(_.resolveObjectPath(models, copy.__t))(copy);
                                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                                        instancesToSave = partsToSave;
                                                        cb(err);
                                                    });
                                                }
                                            }
                                        }), function(err, r) {
                                            datesplice.dateSpliceSave(options.actionCustomerUser._id, pp.section, instancesToSave, 'rangedData', function(err, savedSection) {
                                                pp.section = savedSection;
                                                cb(err, pp);
                                            });
                                        });
                                    }
                                ], function(err, pp) {
                                    //bubble the section up
                                    cb(err, pp.section);
                                });
                            };
                        }), function(err, sections) {
                            cb(err, sections);
                        });
                    },
                    ranges: function(cb) {
                        var instancesToSave = p.originalDraft.rangedData;
                        async.series(_.map(p.draft.rangedData, function(rangeData) {
                            return function(cb) {

                                if (_.any(p.originalDraft.rangedData, function(originalRange) {
                                    return models.definitions.operatingProcedure.ranged.OperatingProcedure.compare(originalRange, rangeData);
                                })) {
                                    logger.silly('[op publish] ranges are the same');
                                    cb();
                                } else {

                                    var copy = _.clone(rangeData.toObject());
                                    delete copy._id;
                                    var range = new models.definitions.operatingProcedure.ranged.OperatingProcedure(copy);
                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                        instancesToSave = partsToSave;
                                        cb(err, partsToSave);
                                    });
                                }
                            };
                        }), function(err, r) {
                            cb(err, instancesToSave);
                        });
                    }

                }, function(err, r) {
                    logger.silly('[op publish] original prepared. Saving...');
                    p.originalDraft.sections = _.pluck(r.sections, '_id');
                    datesplice.dateSpliceSave(options.actionCustomerUser._id, p.originalDraft, r.ranges, 'rangedData', function(err, savedOperatingProcedure) {
                        p.operatingProcedure = savedOperatingProcedure;
                        cb(err, models, p);
                    });
                });
            },
            function(models, p, cb) {
                //reload the OP def as saved
                definitionLoader(models, {
                    operatingProcedureDefinition: p.operatingProcedure._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, operatingProcedure) {
                    //make the OP returned a regular object
                    p.operatingProcedure = operatingProcedure.toObject();
                    cb(err, models, p);
                });
            },
            //{{{ alter user data
            function(models, p, cb) {
                //find all instances of the operating procedure
                models.instances.operatingProcedure.OperatingProcedure.find({
                    operatingProcedureDefinition: p.operatingProcedure._id,
                    active: true,
                    deleted: false
                })
                    .exec(function(err, instances) {
                        p.operatingProcedureInstances = instances;
                        cb(err, models, p);
                    });

            },
            function(models, p, cb) {
                //load the sections for the instances
                models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedureInstances, {
                    path: 'sections',
                    model: models.instances.operatingProcedure.Section
                }, function(err, operatingProcedureInstances) {
                    p.operatingProcedureInstances = operatingProcedureInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //load the OP's ranged data for the instances
                models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedureInstances, {
                    path: 'rangedData',
                    model: models.instances.operatingProcedure.ranged.OperatingProcedure
                }, function(err, operatingProcedureInstances) {
                    p.operatingProcedureInstances = operatingProcedureInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //load the sections' ranged data for the instances
                models.instances.operatingProcedure.OperatingProcedure.populate(p.operatingProcedureInstances, {
                    path: 'sections.rangedData',
                    //model: models.instances.operatingProcedure.ranged.Section
                }, function(err, operatingProcedureInstances) {
                    p.operatingProcedureInstances = operatingProcedureInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for all of the loaded instances,
                async.parallel(_.map(p.operatingProcedureInstances, function(operatingProcedureInstance) {
                    return function(cb) {
                        //map the definition ranges on the existing data
                        async.parallel({
                            opRanges: function(cb) {
                                async.parallel(_.map(p.operatingProcedure.rangedData, function(opDefRange) {
                                    return function(cb) {
                                        var instanceRange = _.find(operatingProcedureInstance.rangedData, function(opInstanceRange) {
                                            return opInstanceRange.operatingProcedureDefinitionRange.toString() == opDefRange._id.toString()
                                        });

                                        if (!instanceRange) {
                                            return cb();
                                        }

                                        instanceRange.startDate = opDefRange.startDate;
                                        instanceRange.endDate = opDefRange.endDate;
                                        instanceRange.rangeActive = opDefRange.rangeActive;
                                        instanceRange.modifiedBy = options.actionCustomerUser._id;

                                        instanceRange.save(function(err, instanceRange) {
                                            cb(err, instanceRange);
                                        });
                                    }
                                }), cb);
                            },
                            sectionRanges: function(cb) {
                                async.parallel(_.map(p.operatingProcedure.sections, function(opDefSection) {
                                    return function(cb) {
                                        var instanceSection = _.find(operatingProcedureInstance.sections, function(opInstanceSection) {
                                            return opInstanceSection.sectionDefinition.toString() == opDefSection._id.toString();
                                        });

                                        if (!instanceSection) {
                                            return cb();
                                        }

                                        async.parallel(_.map(opDefSection.rangedData, function(sectionDefRange) {
                                            return function(cb) {
                                                var instanceRange = _.find(instanceSection.rangedData, function(range) {
                                                    return range.sectionDefinitionRange.toString() == sectionDefRange._id.toString();
                                                });

                                                if (!instanceRange) {
                                                    return cb();
                                                }

                                                instanceRange.startDate = sectionDefRange.startDate;
                                                instanceRange.endDate = sectionDefRange.endDate;
                                                instanceRange.rangeActive = sectionDefRange.rangeActive;
                                                instanceRange.modifiedBy = options.actionCustomerUser._id;

                                                instanceRange.save(function(err, instanceRange) {
                                                    cb(err, instanceRange);
                                                });
                                            };
                                        }), cb);
                                    };

                                }), cb);
                            }
                        }, cb);


                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            //}}}
            function(models, p, cb) {
                //close the draft that was published
                closeDraft(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    operatingProcedureDefinition: p.draft
                }, function(err, models, closed) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //reload the OP def as saved
                definitionLoader(models, {
                    operatingProcedureDefinition: p.operatingProcedure._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, operatingProcedure) {
                    //make the OP returned a regular object
                    operatingProcedure = operatingProcedure.toObject();
                    cb(err, models, p);
                });
            },
        ],
        function(err, models, p) {
            if (!!err) {
                logger.error('problem publishing draft of operatingProcedure: ' + util.inspect(err));
            }
            cb(err, models, p.operatingProcedure);
        });
};
