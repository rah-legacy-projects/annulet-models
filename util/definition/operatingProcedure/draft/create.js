var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    operatingProcedureLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  operatingProcedureDefinition: object representing the operatingProcedure to create,
    //  actionCustomerUser: the customer user with user for audit purposes
    //}

    var customerId = _.extractId(options, 'customer'),
        operatingProcedureDefinitionId = _.extractId(options, 'operatingProcedureDefinition');
        options = fixActionCustomerUser(options);

    logger.silly('[op create draft] opdef id: ' + operatingProcedureDefinitionId);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to create OP.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.operatingProcedureDefinition) {
                return cb('Operating procedure definition not supplied to create draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!!operatingProcedureDefinitionId && operatingProcedureDefinitionId !== 'new') {
                logger.silly('[op create draft] getting op definition: ' + operatingProcedureDefinitionId);
                //op exists, go get it
                operatingProcedureLoader(models, {
                    operatingProcedureDefinition: options.operatingProcedureDefinition,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, operatingProcedure) {
                    //operating procedure should be loaded but not flattened
                    p.operatingProcedure = operatingProcedure.toObject();
                    cb(err, models, p);
                });
            } else {
                logger.silly('[op create draft] op def new, making stub');
                //op does not exist, make one to stub off of
                p.operatingProcedure = {}; //new models.definitions.operatingProcedure.OperatingProcedure();
                p.operatingProcedure.rangedData = [];
                p.operatingProcedure.sections = [];
                p.operatingProcedure.customer = customerId;
                p.createdBy = options.actionCustomerUser._id;
                p.operatingProcedure._id = 'new';
                p.operatingProcedure.shortName = 'new';
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            logger.silly('[op create draft] copying ' + p.operatingProcedure.sections.length + ' sections');
            //make a copy of the sections
            async.parallel(_.map(p.operatingProcedure.sections, function(section) {
                return function(cb) {
                    async.waterfall([

                        function(cb) {
                            //make a copy of the currentmost date range
                            var s = datesplice.getByDateRangeSync(_.clone(section), 'rangedData', moment(), moment());
                            var srange = null;
                            if (!!s) {
                                srange = _.clone(_.find(section.rangedData, function(r) {
                                    return r._id == s._rangeId;
                                }));
                                delete srange._id;
                            } else {
                                srange = new(_.resolveObjectPath(models, s._rangeType))();
                            }

                            (new(_.resolveObjectPath(models, s._rangeType))(srange))
                                .save(function(err, sectionRange) {
                                    cb(err, {
                                        sectionRange: sectionRange
                                    });
                                });
                        },
                        function(pp, cb) {
                            //make a copy of the section with the ranged section inside
                            var sectionopts = _.clone(section);
                            delete sectionopts.rangedData;
                            delete sectionopts._id;
                            sectionopts.rangedData = [pp.sectionRange._id];
                            sectionopts.isDraftOf = section._id;
                            sectionopts.createdBy = options.actionCustomerUser._id;
                            (new(_.resolveObjectPath(models, section.__t))(sectionopts))
                                .save(function(err, section) {
                                    pp.section = section;
                                    cb(err, pp);
                                });
                        }
                    ], function(err, pp) {
                        //cb the section
                        cb(err, pp.section);
                    });
                };
            }), function(err, draftSections) {
                p.draftSections = draftSections;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op create draft] making copy of op range');
            //make a copy of the op range
            var f = datesplice.getByDateRangeSync(_.clone(p.operatingProcedure), 'rangedData', moment(), moment());
            var opRange = null;
            if (!!f) {
                opRange = _.find(p.operatingProcedure.rangedData, function(r) {
                    return r._id == f._rangeId;
                });
            } else {
                opRange = new models.definitions.operatingProcedure.ranged.OperatingProcedure();
            }

            (new models.definitions.operatingProcedure.ranged.OperatingProcedure({
                title: opRange.title,
                description: opRange.description,
                createdBy: options.actionCustomerUser._id,
                startDate: opRange.startDate,
                endDate: opRange.endDate
            }))
                .save(function(err, opRange) {
                    p.opRange = opRange;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[op create draft] saving draft copy');
            //save a draft copy of the op
            (new models.definitions.operatingProcedure.OperatingProcedure({
                customer: customerId,
                isDraftOf: p.operatingProcedure._id,
                sections: _.pluck(p.draftSections, '_id'),
                rangedData: [p.opRange._id],
                createdBy: options.actionCustomerUser._id,
                shortName: p.operatingProcedure.shortName
            }))
                .save(function(err, operatingProcedure) {
                    p.draft = operatingProcedure;
                    logger.silly('[op create draft] draft is draft of ' + p.draft.isDraftOf);
                    cb(err, models, p);
                });

        },
        function(models, p, cb) {
            logger.silly('[op create draft] reloading draft, unflattened');
            //reload the draft op unflattened
            operatingProcedureLoader(models, {
                operatingProcedureDefinition: p.draft._id,
                flatten: false,
                actionCustomerUser: options.actionCustomerUser
            }, function(err, models, draft) {
                p.draft = draft;
                logger.silly('[op create draft] draft is draft of ' + p.draft.isDraftOf);
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating draft of operatingProcedure: ' + util.inspect(err));
        }
        cb(err, models, p.draft);
    });
};
