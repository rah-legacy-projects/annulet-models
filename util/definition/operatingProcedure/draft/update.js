var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    moment = require('moment'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    operatingProcedureLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  operatingProcedureDefinition: flattened object of the draft OP,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        operatingProcedureDefinitionId = _.extractId(options, 'operatingProcedureDefinition');

    options = options || {};
    options = _.defaults(options, {
        flatten: false
    });

    options = fixActionCustomerUser(options);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to update OP draft.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.operatingProcedureDefinition) {
                return cb('Operating procedure definition not supplied to update draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.operatingProcedureDefinition.startDate || !options.operatingProcedureDefinition.endDate) {
                return cb('Operating procedure definition must supply start/end dates');
            }

            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[op update draft] loading op');
            operatingProcedureLoader(models, {
                operatingProcedureDefinition: operatingProcedureDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, operatingProcedure) {
                if (!operatingProcedure) {
                    return cb('Operating procedure could not be found: ' + operatingProcedureDefinitionId);
                }
                p.operatingProcedure = operatingProcedure;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op update draft] updating op range');
            var opRangeToUpdate = _.find(p.operatingProcedure.rangedData, function(range) {
                return range._id.toString() == (options.operatingProcedureDefinition._rangeId || 'new')
                    .toString();
            });

            if (!opRangeToUpdate) {
                //op range should be created by making a draft
                return cb('Invalid OP range id ' + options.operatingProcedureDefinition._rangeId);
            }

            //update op range
            opRangeToUpdate.title = options.operatingProcedureDefinition.title;
            opRangeToUpdate.description = options.operatingProcedureDefinition.description;
            opRangeToUpdate.startDate = options.operatingProcedureDefinition.startDate;
            opRangeToUpdate.endDate = options.operatingProcedureDefinition.endDate;
            opRangeToUpdate.modifiedBy = options.actionCustomerUser._id;

            opRangeToUpdate.save(function(err, opRangeToUpdate) {
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op update draft] updating op sections');
            async.series(_.map(options.operatingProcedureDefinition.sections, function(section) {
                return function(cb) {
                    var defSection = _.find(p.operatingProcedure.sections, function(ds) {
                        //nb, new sections - here, 'section' - will not have IDs
                        return ds._id.toString() == (section._id || '')
                            .toString();
                    });

                    if (!defSection) {
                        //new section!
                        logger.silly('[op update draft] section did not exist, creating');
                        defSection = new(_.resolveObjectPath(models, section.__t))(section);
                        defSection.createdBy = options.actionCustomerUser._id;
                    } else {
                        //todo: ... do nothing when saving section?
                        logger.silly('[op update draft] section existed.');
                    }

                    var defSectionRange = _.find(defSection.rangedData, function(range) {
                        return range._id.toString() == section._rangeId.toString();
                    });

                    var trimmedRange = _.clone(section);
                    delete trimmedRange.__t;
                    delete trimmedRange._id;
                    if (!trimmedRange.startDate) {
                        logger.warn('[op update draft] range start date missing, defaulting to OP range start date');
                        trimmedRange.startDate = options.operatingProcedureDefinition.startDate == 'forever' ? 'forever' : moment(options.operatingProcedureDefinition.startDate)
                            .toDate();
                    }
                    if (!trimmedRange.endDate) {
                        logger.warn('[op update draft] range end date missing, defaulting to OP range end date');
                        trimmedRange.endDate = options.operatingProcedureDefinition.endDate == 'forever' ? 'forever' : moment(options.operatingProcedureDefinition.endDate)
                            .toDate();
                    }
                    if (!defSectionRange) {
                        logger.silly('[op update draft] section range did not exist, creating');
                        //logger.silly('[op update draft] section type: ' + section._rangeType);
                        //new range
                        defSectionRange = new(_.resolveObjectPath(models, section._rangeType))(trimmedRange);
                        defSectionRange.createdBy = options.actionCustomerUser._id;
                    } else {
                        defSectionRange.modifiedBy = options.actionCustomerUser._id;
                        //defSectionRange = _.merge(defSectionRange, trimmedRange);
                        defSectionRange.merge(trimmedRange);
                    }

                    async.waterfall([

                        function(cb) {
                            //save the def section range
                            defSectionRange.save(function(err, defSectionRange) {
                                if (!!err) {
                                    logger.error('[op update draft] problem saving section range: ' + util.inspect(err));
                                }
                                cb(err, defSectionRange);
                            });
                        },
                        function(defSectionRange, cb) {
                            //save the def section
                            //drafts only have one range
                            //todo: consider splicing this instead?
                            defSection.rangedData = defSection.rangedData || [];
                            defSection.rangedData[0] = defSectionRange._id;
                            defSection.modifiedBy = options.actionCustomerUser._id;
                            defSection.save(function(err, defSectionSaved) {
                                if (!!err) {
                                    logger.error('[op update draft] problem saving section: ' + util.inspect(err));
                                }
                                cb(err, defSection);
                            });
                        },
                    ], function(err, defSection) {

                        cb(err, defSection._id);
                    });
                };

            }), function(err, defSectionIds) {
                p.defSectionIds = defSectionIds;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op update draft] updating op section IDs');
            //update the operating procedure's section IDs
            p.operatingProcedure.sections = p.defSectionIds;
            if (!!options.operatingProcedureDefinition.shortName) {
                p.operatingProcedure.shortName = options.operatingProcedureDefinition.shortName;
            }
            p.operatingProcedure.modifiedBy = options.actionCustomerUser._id;
            p.operatingProcedure.save(function(err, operatingProcedure) {
                p.operatingProcedure = operatingProcedure;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            operatingProcedureLoader(models, {
                operatingProcedureDefinition: operatingProcedureDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, operatingProcedure) {
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[op update draft] reloading op (flat)');
            operatingProcedureLoader(models, {
                operatingProcedureDefinition: operatingProcedureDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, operatingProcedure) {
                if (!operatingProcedure) {
                    return cb('Operating procedure could not be found: ' + operatingProcedureDefinitionId);
                }
                p.operatingProcedure = operatingProcedure;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem updating draft of operatingProcedure: ' + util.inspect(err));
        }

        cb(err, models, (p || {})
            .operatingProcedure);
    });
};
