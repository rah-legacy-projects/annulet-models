var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    extractOptionIds = require('../../extractOptionIds'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var quizDefinitionId = _.extractId(options, 'quizDefinition');
    var quizDefinitionIds = extractOptionIds(options, {
        single: 'quizDefinition',
        array: 'quizDefinitions'
    });

    async.waterfall([

        function(cb) {
            if (!quizDefinitionIds || quizDefinitionIds.length == 0) {
                return cb('Bad quiz definition ID in quiz def loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.quiz.Quiz',
                    query: {
                        _id: quizDefinitionIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for quiz def id ' + util.inspect(quizDefinitionIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[quiz def loader] loading definition');
            models.definitions.quiz.Quiz.find({
                _id: {
                    $in: quizDefinitionIds
                }
            })
                .exec(function(err, quizzes) {
                    cb(err, models, {
                        quizzes: quizzes
                    });
                });
        },
        function(models, p, cb) {
            if (!p.quizzes) {
                return cb('quiz def could not be found for id ' + util.inspect(quizDefinitionIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the questions
            models.definitions.quiz.Quiz.populate(p.quizzes, {
                path: 'questions',
                model: models.definitions.quiz.Question
            }, function(err, quizzes) {
                p.quizzes = quizzes;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data
            models.definitions.quiz.Quiz.populate(p.quizzes, {
                path: 'rangedData',
                model: models.definitions.quiz.ranged.Quiz
            }, function(err, quizzes) {
                p.quizzes = quizzes;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz def loader] getting questions');
            //load the questions' ranged data
            models.definitions.quiz.Quiz.populate(p.quizzes, {
                path: 'questions.rangedData',
                model: models.definitions.quiz.ranged.Question
            }, function(err, quizzes) {
                p.quizzes = quizzes;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz def loader] quiz loaded.');
            if (!!options.flatten) {
                async.series(_.map(p.quizzes, function(quiz) {
                    return function(cb) {
                        logger.silly('[quiz def loader] quiz is being flattened.');
                        //objectify the quiz for flattening
                        quiz = quiz.toObject();
                        //flatten the questions and the OP
                        async.parallel({
                            quiz: function(cb) {
                                datesplice.getByDateRange(quiz, 'rangedData', options.startDate, options.endDate, function(err, flatQuiz) {
                                    //hack: add range type
                                    flatQuiz._rangeType = 'definitions.quiz.ranged.Quiz';
                                    cb(err, flatQuiz);
                                });
                            },
                            questions: function(cb) {
                                async.parallel(_.map(quiz.questions, function(question) {
                                    return function(cb) {
                                        datesplice.getByDateRange(question, 'rangedData', options.startDate, options.endDate, function(err, flatQuestion) {
                                            cb(err, flatQuestion);
                                        });
                                    };
                                }), function(err, r) {
                                    cb(err, r);
                                });

                            }
                        }, function(err, r) {
                            quiz = r.quiz;
                            quiz.questions = r.questions;
                            cb(err, quiz);
                        });
                    };
                }), function(err, r) {
                    p.quizzes = r;
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[quiz def loader] problem loading def: ' + util.inspect(err));
        }
        logger.silly('[quiz def loader] finished loading.');
        cb(err, models, (p || {})
            .quizzes.length == 1 && !!options.quizDefinition ? p.quizzes[0] : (p || {})
            .quizzes)
    });
};
