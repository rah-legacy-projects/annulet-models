var _ = require('lodash'),
    models = require('../../../../../modeler');
module.exports = exports = {
    saveQuestion: function(customerId, question, cb) {
        modeler.db({
            collection: 'Customer',
            query: {
                _id: customerId
            }
        }, function(err, models) {
            this.saveAnswers(question.answers, function(err, answerDefinitions) {
                var questionDefinition = new models.Definitions.Quiz.MultipleChoice(question);
                questionDefinition.answers = _.map(answerDefinitions, function(answer) {
                    return {
                        left: answer.left._id,
                        right: answer.right._id,
                        isCorrect: answer.isCorrect
                    };
                });
                questionDefinition.createdBy = options.createdBy;
                questionDefinition.modifiedBy = options.modifiedBy;
                questionDefinition.save(function(err, questionDefinition) {
                    cb(err, questionDefinition);
                });
            });
        });
    }
};
