//hack: breaking naming convention to force model naming to match these
module.exports = exports = {
	Matching: require("./matching"),
	MultipleChoice: require("./multipleChoice"),
	TrueFalse: require('./trueFalse'),
	OneChoice: require('./oneChoice')
};
