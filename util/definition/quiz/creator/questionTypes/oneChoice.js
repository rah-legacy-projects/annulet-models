var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    util = require('util'),
    modeler = require('../../../../../modeler');
    _.mixin(require('annulet-util').lodashMixins);
module.exports = exports = {
    saveQuestion: function(models, options, cb) {
        //models: (optional) the modelset to query against
        //options:
        //  customer: obj or id of the customer
        //  question: the question to create
        //}

        var customerId = _.extractId(options, 'customer');
        async.waterfall([

            function(cb) {
                if (!models) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, cb);
                } else {
                    cb(null, models);
                }
            }
        ], function(err, models) {
            var questionDefinition = new models.definitions.quiz.OneChoice(options.question);
            questionDefinition.createdBy = options.createdBy;
            questionDefinition.modifiedBy = options.modifiedBy;
            questionDefinition.save(function(err, questionDefinition) {
                if (!!err) {
                    logger.error("problem saving question: " + util.inspect(err));
                }
                cb(err, models, questionDefinition);
            });
        });
    }
};
