var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    questionTypes = require('./questionTypes'),
    quizLoader = require('../quizLoader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:
    //{
    //  customer: customer id/object to create against
    //  quizDefinition: quiz def ID/object to create
    //}

    var customerId = _.extractId(options, 'customer');
    async.waterfall([

            function(cb) {
                if (!customerId) {
                    return cb('invalid customer id for creating quiz def: ' + options.customer);
                }
                cb(null);
            },
            function(cb) {
                if (!models) {
                    logger.silly('[quiz creator] getting models');
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, cb);
                } else {
                    logger.silly('[quiz creator] models passed in, skipping');
                    cb(null, models);
                }
            },
            function(models, cb) {
                if (!models) {
                    return cb('modelset could not be determined for quiz def creation for customer ' + customerId);
                }
                cb(null, models);
            },
            function(models, cb) {
                logger.debug('[quiz creator] making question instances');
                //for each question,
                async.parallel(_.map(options.quizDefinition.questions, function(questionWrap) {
                    return function(cb) {
                        logger.warn('[quiz creator] ' + questionWrap.questionType);
                        //for each question, call local save helper
                        questionTypes[questionWrap.questionType]
                            .saveQuestion(models, {
                                customer: customerId.toObjectId(),
                                question: questionWrap.question,
                                createdBy: options.createdBy,
                                modifiedBy: options.modifiedBy
                            }, function(err, models, questionInstance) {
                                cb(err, {
                                    question: questionInstance,
                                    questionType: questionWrap.questionType
                                });
                            });
                    };
                }), function(err, questionInstances) {
                    cb(err, models, {
                        questions: questionInstances
                    });
                });
            },
            function(models, p, cb) {
                logger.debug('[quiz creator] making quiz instance');
                var quizInstance = new models.definitions.quiz.Quiz(options.quizDefinition);
                quizInstance.questions = p.questions;
                quizInstance.customer = customerId.toObjectId();
                quizInstance.createdBy = options.createdBy;
                quizInstance.modifiedBy = options.modifiedBy;
                quizInstance.save(function(err, savedInstance) {
                    cb(err, models, _.defaults(p, {
                        quiz: savedInstance
                    }));
                });
            }
        ],
        function(err, models, p) {
            if (!!err) {
                logger.error('problem creating quiz');
                logger.error(util.inspect(err));
            }
            logger.debug('[quiz creator] quiz created.');
            cb(err, models, (p || {})
                .quiz);
        });
};
