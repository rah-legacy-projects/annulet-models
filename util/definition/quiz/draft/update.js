var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    moment = require('moment'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    quizLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  quizDefinition: flattened object of the draft OP,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        quizDefinitionId = _.extractId(options, 'quizDefinition');

    options = options || {};
    options = _.defaults(options, {
        flatten: false
    });

    options = fixActionCustomerUser(options);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to update quiz draft.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.quizDefinition) {
                return cb('Quiz definition not supplied to update draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.quizDefinition.startDate || !options.quizDefinition.endDate) {
                return cb('Quiz definition must supply start/end dates');
            }

            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[quiz update draft] loading quiz');
            quizLoader(models, {
                quizDefinition: quizDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, quiz) {
                if (!quiz) {
                    return cb('Quiz could not be found: ' + quizDefinitionId);
                }
                p.quiz = quiz;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz update draft] updating quiz range');
            var quizRangeToUpdate = _.find(p.quiz.rangedData, function(range) {
                return range._id.toString() == (options.quizDefinition._rangeId || 'new')
                    .toString();
            });

            if (!quizRangeToUpdate) {
                //quiz range should be created by making a draft
                return cb('Invalid quiz range id ' + options.quizDefinition._rangeId);
            }

            //update quiz range
            quizRangeToUpdate.title = options.quizDefinition.title;
            quizRangeToUpdate.description = options.quizDefinition.description;
            quizRangeToUpdate.directions = options.quizDefinition.directions;
            quizRangeToUpdate.startDate = options.quizDefinition.startDate;
            quizRangeToUpdate.endDate = options.quizDefinition.endDate;
            quizRangeToUpdate.modifiedBy = options.actionCustomerUser._id;
            quizRangeToUpdate.passingPercentage = options.quizDefinition.passingPercentage;
            quizRangeToUpdate.questionQuota = options.quizDefinition.questionQuota;

            quizRangeToUpdate.save(function(err, quizRangeToUpdate) {
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz update draft] updating quiz questions');
            async.parallel(_.map(options.quizDefinition.questions, function(question) {
                return function(cb) {
                    var defQuestion = _.find(p.quiz.questions, function(ds) {
                        return ds._id.toString() == (question._id || '')
                            .toString();
                    });

                    if (!defQuestion) {
                        //new question!
                        logger.silly('[quiz update draft] question did not exist, creating');
                        defQuestion = new(_.resolveObjectPath(models, question.__t))(question);
                        defQuestion.createdBy = options.actionCustomerUser._id;
                    } else {
                        logger.silly('[quiz update draft] question existed.');
                    }

                    var defQuestionRange = _.find(defQuestion.rangedData, function(range) {
                        return range._id.toString() == question._rangeId.toString();
                    });

                    var trimmedRange = _.clone(question);
                    delete trimmedRange.__t;
                    delete trimmedRange._id;
                    if (!trimmedRange.startDate) {
                        logger.warn('[quiz update draft] range start date missing, defaulting to quiz range start date');
                        trimmedRange.startDate = options.quizDefinition.startDate == 'forever' ? 'forever' : moment(options.quizDefinition.startDate)
                            .toDate();
                    }
                    if (!trimmedRange.endDate) {
                        logger.warn('[quiz update draft] range end date missing, defaulting to quiz range end date');
                        trimmedRange.endDate = options.quizDefinition.endDate == 'forever' ? 'forever' : moment(options.quizDefinition.endDate)
                            .toDate();
                    }
                    if (!defQuestionRange) {
                        logger.silly('[quiz update draft] question range did not exist, creating');
                        //new range
                        defQuestionRange = new(_.resolveObjectPath(models, question._rangeType))(trimmedRange);
                        defQuestionRange.createdBy = options.actionCustomerUser._id;
                    } else {
                        defQuestionRange.modifiedBy = options.actionCustomerUser._id;
                        defQuestionRange.merge(trimmedRange);
                    }

                    async.waterfall([

                        function(cb) {
                            //save the def question range
                            defQuestionRange.save(function(err, defQuestionRange) {
                                if (!!err) {
                                    logger.error('[quiz update draft] problem saving question range: ' + util.inspect(err));
                                }
                                cb(err, defQuestionRange);
                            });
                        },
                        function(defQuestionRange, cb) {
                            //save the def question
                            //drafts only have one range
                            //todo: consider splicing this instead?
                            defQuestion.rangedData = defQuestion.rangedData || [];
                            defQuestion.rangedData[0] = defQuestionRange._id;
                            defQuestion.modifiedBy = options.actionCustomerUser._id;
                            defQuestion.save(function(err, defQuestionSaved) {
                                if (!!err) {
                                    logger.error('[quiz update draft] problem saving question: ' + util.inspect(err));
                                }
                                cb(err, defQuestion);
                            });
                        },
                    ], function(err, defQuestion) {

                        cb(err, defQuestion._id);
                    });
                };

            }), function(err, defQuestionIds) {
                p.defQuestionIds = defQuestionIds;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz update draft] updating quiz question IDs');
            //update the operating procedure's question IDs
            p.quiz.questions = p.defQuestionIds;
            p.quiz.shortName = options.quizDefinition.shortName;
            p.quiz.modifiedBy = options.actionCustomerUser._id;
            p.quiz.save(function(err, quiz) {
                p.quiz = quiz;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz update draft] reloading quiz (flat)');
            quizLoader(models, {
                quizDefinition: quizDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, quiz) {
                if (!quiz) {
                    return cb('Quiz could not be found: ' + quizDefinitionId);
                }
                p.quiz = quiz;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem updating draft of quiz: ' + util.inspect(err));
        }

        cb(err, models, (p || {})
            .quiz);
    });
};
