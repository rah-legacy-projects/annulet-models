var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/quiz/loader'),
    closeDraft = require('./close');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  quizDefinition: object representing the quiz to create,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        quizDefinitionId = _.extractId(options, 'quizDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!customerId) {
                    return cb("Bad customer ID when trying to publish draft of OP.");
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for OPs by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.quizDefinition) {
                    return cb('Quiz definition not supplied to publish draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    quizDefinition: quizDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if (!!p.draft.isDraftOf && p.draft.isDraftOf === 'new') {
                    p.originalDraft = new models.definitions.quiz.Quiz({
                        rangedData: [],
                        questions: [],
                        customer: customerId
                    });
                    cb(null, models, p);
                } else {
                    definitionLoader(models, {
                        quizDefinition: p.draft.isDraftOf,
                        actionCustomerUser: options.actionCustomerUser,
                        flatten: false
                    }, function(err, models, original) {
                        p.originalDraft = original;
                        cb(err, models, p);
                    })
                }
            },
            function(models, p, cb) {
                logger.silly('[quiz publish] about to publish questions and ranges');
                async.parallel({
                    questions: function(cb) {
                        async.series(_.map(p.draft.questions, function(newquestion) {
                            return function(cb) {
                                async.waterfall([

                                    function(cb) {
                                        cb(null, {});
                                    },
                                    function(pp, cb) {
                                        //find original question
                                        var originalquestion = _.find(p.originalDraft.questions, function(originalquestion) {
                                            return originalquestion._id.toString() == (newquestion.isDraftOf || '')
                                                .toString();
                                        });

                                        //if the original question did not exist, create one
                                        if (!originalquestion) {
                                            var copy = newquestion.toObject();
                                            delete copy._id;
                                            delete copy.isDraftOf;
                                            delete copy.rangedData;
                                            (new(_.resolveObjectPath(models, newquestion.__t))(copy))
                                                .save(function(err, question) {
                                                    pp.question = question;
                                                    cb(err, pp);
                                                });
                                        } else {
                                            pp.question = originalquestion;
                                            cb(null, pp);
                                        }
                                    },
                                    function(pp, cb) {
                                        //splice the new question ranged data into the original question's ranged data
                                        var instancesToSave = pp.question.rangedData;
                                        async.series(_.map(newquestion.rangedData, function(newRange) {
                                            return function(cb) {
                                                //see if there was an existing question range that is prexactly the same, skip the splice
                                                if (_.any(pp.question.rangedData, function(originalRange) {
                                                    var comparison = _.resolveObjectPath(models, originalRange.__t)
                                                        .compare(originalRange, newRange);
                                                    return comparison;
                                                })) {
                                                    cb();
                                                } else {
                                                    var copy = _.clone(newRange.toObject());
                                                    delete copy._id;
                                                    delete copy.isDraftOf;
                                                    var range = new(_.resolveObjectPath(models, copy.__t))(copy);
                                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                                        instancesToSave = partsToSave;
                                                        cb(err);
                                                    });
                                                }
                                            }
                                        }), function(err, r) {
                                            datesplice.dateSpliceSave(options.actionCustomerUser._id, pp.question, instancesToSave, 'rangedData', function(err, savedquestion) {
                                                pp.question = savedquestion;
                                                cb(err, pp);
                                            });
                                        });
                                    }
                                ], function(err, pp) {
                                    //bubble the question up
                                    cb(err, pp.question);
                                });
                            };
                        }), function(err, questions) {
                            cb(err, questions);
                        });
                    },
                    ranges: function(cb) {
                        var instancesToSave = p.originalDraft.rangedData;
                        logger.silly('[quiz publish] parts before: ' + util.inspect(instancesToSave.toObject()));
                        async.series(_.map(p.draft.rangedData, function(rangeData) {
                            return function(cb) {

                                if (_.any(p.originalDraft.rangedData, function(originalRange) {
                                    return models.definitions.quiz.ranged.Quiz.compare(originalRange, rangeData);
                                })) {
                                    cb();
                                } else {

                                    var copy = _.clone(rangeData.toObject());
                                    delete copy._id;
                                    var range = new models.definitions.quiz.ranged.Quiz(copy);
                                    logger.silly('[quiz publish] splicing in ' + util.inspect(range));
                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                        if(err){
                                            logger.error('[quiz publish] splicing error: ' + util.inspect(err));
                                        }
                                        logger.silly('[quiz publish] parts after: ' + util.inspect(partsToSave));
                                        instancesToSave = partsToSave;
                                        cb(err, partsToSave);
                                    });
                                }
                            };
                        }), function(err, r) {
                            cb(err, instancesToSave);
                        });
                    }

                }, function(err, r) {
                    logger.silly('[quiz publish] original prepared. Saving...');
                    p.originalDraft.questions = _.pluck(r.questions, '_id');
                    p.originalDraft.shortName = p.draft.shortName;
                    //logger.silly('[quiz publish] original draft: ' + util.inspect(p.originalDraft));
                    logger.silly('[quiz publish] ranges: ' + util.inspect(r.ranges));
                    datesplice.dateSpliceSave(options.actionCustomerUser._id, p.originalDraft, r.ranges, 'rangedData', function(err, savedQuiz) {
                        p.quiz = savedQuiz;
                        cb(err, models, p);
                    });
                });
            },
            function(models, p, cb) {
                //reload the quiz def as saved
                definitionLoader(models, {
                    quizDefinition: p.quiz._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, quiz) {
                    //make the quiz returned a regular object
                    p.quiz = quiz.toObject();
                    logger.silly('[quiz publish] reloaded quiz: ' + JSON.stringify(p.quiz.rangedData,null,3));
                    cb(err, models, p);
                });
            },
            //{{{ alter user data
            function(models, p, cb) {
                //find all instances of the quiz
                models.instances.quiz.Quiz.find({
                    quizDefinition: p.quiz._id,
                    active: true,
                    deleted: false
                })
                    .exec(function(err, instances) {
                        p.instances = instances;
                        cb(err, models, p);
                    });

            },
            function(models, p, cb) {
                //load the instances
                if (p.instances.length == 0) {
                    return cb(null, models, p);
                }
                instanceLoader(models, {
                    quizInstances: p.instances,
                }, function(err, models, instances) {
                    p.instances = instances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //since the question instances are made just-in-time, no need to reconcile questions
                //do need to reconcile attempts with quiz definition range
                //pull attempts
                var attempts = _.chain(p.instances)
                    .pluck('attempts')
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .value();

                async.parallel(_.map(p.quiz.rangedData, function(definitionRange) {
                    return function(cb) {
                        var instanceRanges = _.filter(attempts, function(attempt) {
                            return attempt.quizDefinitionRange._id.toString() === definitionRange._id.toString();
                        });

                        if (instanceRanges.length == 0) {
                            return cb(null);
                        }
                        async.parallel(_.map(instanceRanges, function(instanceRange) {
                            return function(cb) {
                                instanceRange.startDate = definitionRange.startDate;
                                instanceRange.endDate = definitionRange.endDate;
                                instanceRange.rangeActive = definitionRange.rangeActive;
                                instanceRange.modifiedBy = options.actionCustomerUser._id;
                                instanceRange.save(function(err, instanceRange) {
                                    cb(err, instanceRange);
                                });
                            }
                        }), function(err, r) {
                            cb(err, r)
                        });
                    }
                }), function(err, r) {
                    cb(err, models, p);
                });

            },
            //}}}
            function(models, p, cb) {
                //close the draft that was published
                closeDraft(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    quizDefinition: p.draft
                }, function(err, models, closed) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //reload the quiz def as saved
                definitionLoader(models, {
                    quizDefinition: p.quiz._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, quiz) {
                    //make the quiz returned a regular object
                    quiz = quiz.toObject();
                    cb(err, models, p);
                });
            },
        ],
        function(err, models, p) {
            if (!!err) {
                logger.error('problem publishing draft of quiz: ' + util.inspect(err));
            }
            cb(err, models, p.quiz);
        });
};
