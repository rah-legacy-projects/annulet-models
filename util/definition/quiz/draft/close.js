var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/quiz/loader'),
    attemptLoader = require('../stamper/attempt/loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  quizDefinition: object representing the quiz to create,
    //  actionCustomerUser
    //}

    var quizDefinitionId = _.extractId(options, 'quizDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'definitions.quiz.Quiz',
                        query: {
                            _id: quizDefinitionId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for OPs by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.quizDefinition) {
                    return cb('Operating procedure definition not supplied to close draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    quizDefinition: quizDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //find the instances tied to the def
                models.instances.quiz.ranged.Attempt.find({
                    quizDefinitionRange: {
                        $in: _.pluck(p.draft.rangedData, '_id')
                    }
                })
                    .exec(function(err, attempts) {
                        async.parallel(_.map(attempts, function(attempt) {
                            return function(cb) {
                                attemptLoader(models, {
                                    attempt: attempt
                                }, function(err, models, loadedAttempt) {
                                    cb(err, loadedAttempt);
                                });
                            };
                        }), function(err, loadedAttempts) {
                            p.attempts = loadedAttempts;
                            cb(err, models, p);
                        });
                    });
            },
            function(models, p, cb) {
                //close the questions in the instances
                var questions = _.chain(p.attempts)
                    .pluck('questions')
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .value();

                logger.silly('[quiz def close] attempt questions: ' + util.inspect(questions));

                async.parallel(_.map(questions, function(question) {
                    return function(cb) {
                        question.active = false;
                        question.deleted = true;
                        question.modifiedBy = options.actionCustomerUser._id;
                        question.save(function(err, question) {
                            cb(err, question);
                        });
                    };
                }), function(err, r) {

                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the attempts
                async.parallel(_.map(p.attempts, function(attempt) {
                    return function(cb) {
                        attempt.active = false;
                        attempt.deleted = true;
                        attempt.modifiedBy = options.actionCustomerUser._id;
                        attempt.save(function(err, attempt) {
                            cb(err, attempt);
                        });
                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the ranges of the questions
                async.parallel(_.chain(p.draft.questions)
                    .map(function(question) {
                        return question.rangedData;
                    })
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .map(function(range) {
                        return function(cb) {
                            range.active = false;
                            range.deleted = true;
                            range.modifiedBy = options.actionCustomerUser._id;
                            range.save(function(err, range) {
                                cb(err, range);
                            });
                        }
                    })
                    .value(), function(err, r) {
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //close the questions
                async.parallel(_.map(p.draft.questions, function(question) {
                    return function(cb) {
                        question.active = false;
                        question.deleted = true;
                        question.modifiedBy = options.actionCustomerUser._id;
                        question.save(function(err, question) {
                            cb(err, question);
                        });
                    }
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the ranges of the OP
                async.parallel(_.map(p.draft.rangedData, function(range) {
                    return function(cb) {
                        range.active = false;
                        range.deleted = true;
                        range.modifiedBy = options.actionCustomerUser._id;
                        range.save(function(err, range) {
                            cb(err, range);
                        });
                    }
                }), function(err, r) {
                    logger.silly('[quiz draft close] ranges closed.');
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the op
                p.draft.active = false;
                p.draft.deleted = true;
                p.draft.modifiedBy = options.actionCustomerUser._id;
                p.draft.save(function(err, draft) {
                    logger.silly('[quiz draft close] quiz closed.');
                    cb(err, models, p);
                });
            }
        ],
        function(err, models, p) {
            logger.silly('[quiz draft close] close complete.');
            if (!!err) {
                logger.error('problem closing draft of quiz: ' + util.inspect(err));
            }
            cb(err, models, p.draft);
        });
};
