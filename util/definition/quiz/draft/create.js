var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    quizLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  quizDefinition,
    //  actionCustomerUser: the customer user with user for audit purposes
    //}

    var customerId = _.extractId(options, 'customer'),
        quizDefinitionId = _.extractId(options, 'quizDefinition');
        options = fixActionCustomerUser(options);

    logger.silly('[quiz create draft] quizdef id: ' + quizDefinitionId);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to create OP.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for quizzes by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.quizDefinition) {
                return cb('quiz definition not supplied to create draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!!quizDefinitionId && quizDefinitionId !== 'new') {
                logger.silly('[quiz create draft] getting quiz definition: ' + quizDefinitionId);
                //quiz exists, go get it
                quizLoader(models, {
                    quizDefinition: options.quizDefinition,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, quiz) {
                    //operating procedure should be loaded but not flattened
                    p.quiz = quiz.toObject();
                    cb(err, models, p);
                });
            } else {
                logger.silly('[quiz create draft] quiz def new, making stub');
                //quiz does not exist, make one to stub off of
                p.quiz = {}; //new models.definitions.quiz.Quiz();
                p.quiz.rangedData = [];
                p.quiz.questions = [];
                p.quiz.customer = customerId;
                p.quiz.shortName = 'new';

                p.createdBy = options.actionCustomerUser._id;
                p.quiz._id = 'new';
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            logger.silly('[quiz create draft] copying ' + p.quiz.questions.length + ' questions');
            //make a copy of the questions
            async.parallel(_.map(p.quiz.questions, function(question) {
                return function(cb) {
                    async.waterfall([

                        function(cb) {
                            //make a copy of the currentmost date range
                            var s = datesplice.getByDateRangeSync(_.clone(question), 'rangedData', moment(), moment());
                            var srange = null;
                            if (!!s) {
                                srange = _.clone(_.find(question.rangedData, function(r) {
                                    return r._id == s._rangeId;
                                }));
                                delete srange._id;
                            } else {
                                srange = new(_.resolveObjectPath(models, s._rangeType))();
                            }

                            (new(_.resolveObjectPath(models, s._rangeType))(srange))
                                .save(function(err, questionRange) {
                                    cb(err, {
                                        questionRange: questionRange
                                    });
                                });
                        },
                        function(pp, cb) {
                            //make a copy of the question with the ranged question inside
                            var questionopts = _.clone(question);
                            delete questionopts.rangedData;
                            delete questionopts._id;
                            questionopts.rangedData = [pp.questionRange._id];
                            questionopts.isDraftOf = question._id;
                            questionopts.createdBy = options.actionCustomerUser._id;
                            (new(_.resolveObjectPath(models, question.__t))(questionopts))
                                .save(function(err, question) {
                                    pp.question=question;
                                    cb(err, pp);
                                });
                        }
                    ], function(err, pp) {
                        cb(err, pp.question);
                    });
                };
            }), function(err, draftQuestions) {
                p.draftQuestions = draftQuestions;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz create draft] making copy of quiz range');
            //make a copy of the quiz range
            var f = datesplice.getByDateRangeSync(_.clone(p.quiz), 'rangedData', moment(), moment());
            var quizRange = null;
            if (!!f) {
                quizRange = _.find(p.quiz.rangedData, function(r) {
                    return r._id == f._rangeId;
                });
            } else {
                quizRange = new models.definitions.quiz.ranged.Quiz();
            }

            (new models.definitions.quiz.ranged.Quiz({
                title: quizRange.title,
                description: quizRange.description,
                directions:quizRange.directions,
                createdBy: options.actionCustomerUser._id,
                startDate: quizRange.startDate,
                endDate: quizRange.endDate,
                questionQuota: quizRange.questionQuota,
                passingPercentage: quizRange.passingPercentage
            }))
                .save(function(err, quizRange) {
                    p.quizRange = quizRange;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[quiz create draft] saving draft copy');
            //save a draft copy of the op
            (new models.definitions.quiz.Quiz({
                customer: customerId,
                isDraftOf: p.quiz._id,
                questions: _.pluck(p.draftQuestions, '_id'),
                rangedData: [p.quizRange._id],
                createdBy: options.actionCustomerUser._id,
                shortName: p.quiz.shortName
            }))
                .save(function(err, quiz) {
                    p.draft = quiz;
                    logger.silly('[quiz create draft] draft is draft of ' + p.draft.isDraftOf);
                    cb(err, models, p);
                });

        },
        function(models, p, cb) {
            logger.silly('[quiz create draft] reloading draft, unflattened');
            //reload the draft quiz unflattened
            quizLoader(models, {
                quizDefinition: p.draft._id,
                flatten: false,
                actionCustomerUser: options.actionCustomerUser
            }, function(err, models, draft) {
                p.draft = draft;
                logger.silly('[quiz create draft] draft is draft of ' + p.draft.isDraftOf);
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating draft of quiz: ' + util.inspect(err));
        }
        cb(err, models, p.draft);
    });
};
