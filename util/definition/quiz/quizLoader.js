var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //  models: (optional) the model set to load from
    //options:
    //{
    //  quizDefinition: the quiz def to load
    //}
    var quizDefId = _.extractId(options, 'quizDefinition');
    async.waterfall([

        function(cb) {
            if (!quizDefId) {
                return cb('invalid quiz def id for load: ' + options.quizDefinition);
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.quiz.Quiz',
                    query: {
                        _id: quizDefId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb){
            if(!models){
                return cb('modelset could not be found for quiz def id at load time: ' + quizDefId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.debug('[quiz loader] loading definition');
            models.definitions.quiz.Quiz.findOne(quizDefId.toObjectId())
                .lean()
                .exec(function(err, quiz) {
                    cb(err, models, {
                        quiz: quiz
                    });
                });
        },
        function(models, p, cb){
            if(!p.quiz){
                return cb('could not find quiz def for loading: ' + quizDefId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.debug('[quiz loader] getting question definitions');
            async.parallel(_.map(p.quiz.questions, function(questionWrap) {
                logger.silly('[quiz laoder] question type: ' + questionWrap.questionType);
                return function(cb) {
                    models.definitions.quiz[questionWrap.questionType].findOne({
                        _id: questionWrap.question
                    })
                        .exec(function(err, question) {
                            logger.silly('[quiz loader] question found: ' + util.inspect(question));
                            if(!!err){
                                logger.error('[quiz loader] problem loading qusetion: ' + util.inspect(err));
                            }
                            questionWrap.question = question;
                            cb(err, {
                                question: question,
                                questionType: questionWrap.questionType
                            });
                        });
                };

            }), function(err, questions) {
                cb(err, models, p);
            });
        },
    ], function(err, models, loadedQuizDefinition) {
        cb(err, models, loadedQuizDefinition);
    });
};
