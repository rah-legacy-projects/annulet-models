var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    QuizDefinitionLoader = require('../loader'),
    QuizInstanceLoader = require('../../../instance/quiz/loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    attempt = require('./attempt'),
    QuestionTypes = require('./questionTypes');


module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options: {
    //  quizDefinitition: object or ID of the quiz def to stamp,
    //  actionCustomerUser,
    //  customerUser
    //}
    var quizDefinitionId = _.extractId(options, 'quizDefinition'),
        customerUserId = _.extractId(options, 'customerUser');

    async.waterfall([

        function(cb) {
            if (!quizDefinitionId) {
                return cb('invalid quiz definition id for stamping: ' + options.quizDefinition);
            }
            cb(null);
        },
        function(cb) {
            options = fixActionCustomerUser(options);
            if (!models) {
                modeler.db({
                    collection: 'definitions.quiz.Quiz',
                    query: {
                        _id: quizDefinitionId
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for quiz def ' + quizDefinitionId);
            }
            cb(null, models, {});
        },
        function(models, p, cb) {
            //does this user have an instance of this quiz?
            models.instances.quiz.Quiz.findOne({
                customerUser: customerUserId,
                quizDefinition: quizDefinitionId
            })
                .exec(function(err, quizInstance) {
                    p.quizInstance = quizInstance;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //if the user does not have an instance of the quiz, make it
            if (!p.quizInstance) {
                (new models.instances.quiz.Quiz({
                    customerUser: customerUserId,
                    quizDefinition: quizDefinitionId,
                    attempts: []
                }))
                    .save(function(err, quizInstance) {
                        p.quizInstance = quizInstance;
                        cb(err, models, p);
                    });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            //use the instance loader to load the quiz
            QuizInstanceLoader(models, {
                quizInstance: p.quizInstance,
                flatten:true
            }, function(err, models, quizInstance) {
                p.quizInstance = quizInstance;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[quiz stamper] scoring attempts');
            //score the attempts
            async.parallel(_.map(p.quizInstance.attempts, function(attempt) {
                return function(cb) {
                    attempt.score(cb);
                };
            }), function(err, r) {
                if (!!err && !!err.error) {
                    logger.silly('[quiz stamper] attempt incomplete, using existing');
                    //scoring issue or not finalized issue,
                    //assign the failed attempt to be used again
                    p._state = 'Opened';
                    p.attempt = _.find(p.quizInstance.attempts, function(attempt) {
                        return attempt._id.toString() == err.attempt.toString();
                    });

                    //nb, ignoring the error purposefully
                    return cb(null, models, p);

                } else if (!err) {
                    //did any of the attempts pass?
                    var pass = _.any(r, function(score) {
                        return score.passing;
                    });

                    logger.silly('[quiz stamper] scores: ' + JSON.stringify(r, null, 2));

                    logger.silly('[quiz stamper] did attempts pass? ' + pass);
                    if (pass) {
                        //use the most recent passed attempt to bubble up
                        var mostRecent = _.chain(r)
                            .filter(function(attemptScore) {
                                return attemptScore.passing;
                            })
                            .sortBy(function(attemptScore) {
                                return attemptScore.finalizedDate;
                            })
                            .reverse()
                            .first()
                            .value()
                            .attempt;

                        p._state = 'Opened Completed';
                        p.attempt = _.find(p.quizInstance.attempts, function(attempt) {
                            return attempt._id.toString() == mostRecent.toString();
                        });
                        p.attempt.passed = true;
                    } else {
                        //no scoring error, quiz failed
                        //add a new attempt
                    };
                } else {
                    logger.silly('[quiz stamper] letting error bubble');
                    //let the error bubble up
                }
                return cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if (!p.attempt) {
                attempt.creator(models, {
                    quizDefinition: quizDefinitionId,
                    quizInstance: p.quizInstance,
                    customerUser: options.customerUser,
                    actionCustomerUser: options.actionCustomerUser
                }, function(err, models, attempt){
                    p.attempt = attempt;
                    p.quizInstance.attempts.push(attempt);
                    p.quizInstance.save(function(err, quizInstance){
                        p.quizInstance = quizInstance;
                        cb(err, models, p);
                    });
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb){
            //reload the attempt
            attempt.loader(models,{
                attempt: p.attempt
            },function(err, models, attempt){
                p.attempt = attempt.toObject();
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        //bubble the attempt back
        logger.debug('[quiz stamper] quiz stamped.');
        cb(err, models, (p || {})
            .attempt);
    });
};
