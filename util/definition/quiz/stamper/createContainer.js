var async = require('async'),
    _ = require('lodash'),
    logger = require('winston'),
    util = require('util'),
    modeler = require('../../../../modeler');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) models to query against
    //options: {
    //  workflowItem: object or id for the workflow item to create the container in,
    //  customerUser
    //}
    var workflowItemId = _.extractId(options, 'workflowItem'),
        customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!workflowItemId) {
                return cb('invalid workflow item id for quiz container creation: ' + options.workflowItem);
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'instances.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for quiz container creation for wf item ' + workflowItemId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[create quiz container] making the container');
            //make the quiz container
            new models.instances.quiz.QuizContainer({
                createdBy: customerUserId,
                modifiedBy: customerUserId
            })
                .save(function(err, quizContainer) {
                    cb(err, models, {
                        quizContainer: quizContainer
                    });
                });
        },
        function(models, p, cb) {
            //get the wf item instance
            logger.silly('[create quiz container] getting wf item ' + workflowItemId);
            models.instances.workflow.QuizWorkflowItem.findOne({
                _id: workflowItemId.toObjectId()
            })
                .exec(function(err, workflowItem) {
                    cb(err, models, _.defaults(p, {
                        workflowItem: workflowItem
                    }));
                });
        },
        function(models, p, cb){
            if(!p.workflowItem){
                return cb('workflow item could not be found for quiz container creation: ' + workflowItemId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //assign the workflow item to have the quiz container
            p.workflowItem.quizContainer = p.quizContainer._id;

            p.workflowItem.save(function(err, workflowItem) {
                p.workflowItem = workflowItem;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        cb(err, models, (p||{}).workflowItem);
    });
};
