module.exports = exports = {
	Matching: require("./matching"),
	MultipleChoice: require("./multipleChoice"),
	OneChoice: require("./oneChoice"),
	TrueFalse: require("./trueFalse"),
};
