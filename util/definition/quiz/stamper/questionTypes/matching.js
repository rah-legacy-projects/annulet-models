var _ = require('lodash'),
    models = require('../../../../../modeler');
module.exports = exports = {
    makeAnswers: function(question, cb) {
        //hack: assumes answers are loaded
        var answersToUse = _.filter(question.answers, function(answer) {
            return answer.left.isRequired || answer.right.isRequired;
        });

        var notRequiredAnswers = _.filter(question.answers, function(answer) {
            return !answer.left.isRequired && !answer.right.isRequired;
        });

        answersToUse = answersToUse.concat(_.sample(notRequiredAnswers, this.answerQuota));

        return _.shuffle(answersToUse);
    },
};
