var _ = require('lodash'),//{{{//}}}
    async = require('async'),
    logger = require('winston'),
    util = require('util'),
    modeler = require('../../../../../modeler');
module.exports = exports = {
    makeAnswers: function(question, cb) {
        //hack: assumes answers are loaded

        var answersToUse = _.filter(question.answers, function(answer) {
            return answer.isRequired;
        });

        var notRequiredAnswers = _.filter(question.answers, function(answer) {
            return !answer.isRequired;
        });
    
        logger.silly('[qtoc] answers to use before: ' + answersToUse.length);
        logger.silly('[qtoc] quota: ' + question.answerQuota);
        logger.silly('[qtoc] nonreqd answers: ' + notRequiredAnswers.length);
        answersToUse = answersToUse.concat(_.sample(notRequiredAnswers, question.answerQuota));
        logger.silly('[qtoc] answers to use: ' + util.inspect(answersToUse));

        return _.shuffle(answersToUse);
    },
};
