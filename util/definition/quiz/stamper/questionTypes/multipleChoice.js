var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    util = require('util'),
    modeler = require('../../../../../modeler');
module.exports = exports = {
    makeAnswers: function(question, cb) {
        //hack: assumes answers are loaded

        var answersToUse = _.filter(question.answers, function(answer) {
            return answer.isRequired;
        });

        var notRequiredAnswers = _.filter(question.answers, function(answer) {
            return !answer.isRequired;
        });

        answersToUse = answersToUse.concat(_.sample(notRequiredAnswers, question.answerQuota));

        return _.shuffle(answersToUse);
    },
};
