var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    QuizDefinitionLoader = require('../../loader'),
    QuizInstanceLoader = require('../../../../instance/quiz/loader'),
    fixActionCustomerUser = require('../../../../fixActionCustomerUser'),
    QuestionTypes = require('../questionTypes');


module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options: {
    //	quizDefinition,
    //  actionCustomerUser,
    //  customerUser
    //}
    var quizDefinitionId = _.extractId(options, 'quizDefinition'),
        customerUserId = _.extractId(options, 'customerUser');

    async.waterfall([

        function(cb) {
            if (!quizDefinitionId) {
                return cb('invalid quiz definition id for stamping: ' + options.quizDefinition);
            }
            cb(null);
        },
        function(cb) {
            options = fixActionCustomerUser(options);
            if (!models) {
                modeler.db({
                    collection: 'definitions.quiz.Quiz',
                    query: {
                        _id: quizDefinitionId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for quizDef ' + quizDefinitionId);
            }
            cb(null, models, {});
        },
        function(models, p, cb) {
            logger.debug('[quiz creator] loading definition');
            //load the quiz def flattened
            QuizDefinitionLoader(models, {
                quizDefinition: quizDefinitionId,
                customerUser: options.customerUser,
                flatten: true
            }, function(err, models, quizDefinition) {
                p.quizDefinition = quizDefinition;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.debug('[attempt creator] making question instances');
            //get the required questions
            var questionsToUse = _.filter(p.quizDefinition.questions, function(question) {
                return question.isRequired;
            });

            //get the nonrequired questions
            var notRequiredQuestions = _.filter(p.quizDefinition.questions, function(question) {
                return !question.isRequired;
            });

            //make up the quota difference with some random questions
            logger.silly('[attempt creator] question quota: ' + p.quizDefinition.questionQuota);
            questionsToUse = questionsToUse.concat(_.sample(notRequiredQuestions, p.quizDefinition.questionQuota));


            //shuffle the questions up
            questionsToUse = _.shuffle(questionsToUse);

            //for each question,
            _.each(questionsToUse, function(question) {
                //use random answers for that question
                question.answers = QuestionTypes[_.first(question.__t.split('.')
                    .reverse())]
                    .makeAnswers(question);
            });

            //for each question,
            async.parallel(_.map(questionsToUse, function(question) {
                return function(cb) {
                    var definitionId = question._id,
                        rangeId = question._rangeId,
                        type = question.__t.replace('definition', 'instance');
                    delete question._id;
                    delete question._rangeId;
                    delete question.__t;
                    //track what question and range def the question came from
                    question.questionDefinition = definitionId;
                    question.questionDefinitionRange = rangeId;
                    (new(_.resolveObjectPath(models, type))(question))
                        .save(function(err, questionInstance) {
                            cb(err, questionInstance);
                        });
                };
            }), function(err, questionInstances) {
                p.questions = questionInstances;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            (new models.instances.quiz.ranged.Attempt({
                questions: _.pluck(p.questions, '_id'),
                quizDefinitionRange: p.quizDefinition._rangeId,
                createdBy: options.actionCustomerUser._id
            }))
                .save(function(err, attempt) {
                    p.attempt = attempt;
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        logger.debug('[attempt creator] attempt stamped.');
        cb(err, models, (p || {})
            .attempt);
    });
};
