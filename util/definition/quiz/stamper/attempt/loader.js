var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    QuizDefinitionLoader = require('../../loader'),
    QuizInstanceLoader = require('../../../../instance/quiz/loader'),
    fixActionCustomerUser = require('../../../../fixActionCustomerUser'),
    QuestionTypes = require('../questionTypes');


module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options: {
    //  attempt,
    //  actionCustomerUser,
    //  customerUser
    //}
    var attemptId = _.extractId(options, 'attempt'),
        customerUserId = _.extractId(options, 'customerUser');

    async.waterfall([

        function(cb) {
            if (!attemptId) {
                return cb('invalid quiz definition id for stamping: ' + options.attempt);
            }
            cb(null);
        },
        function(cb) {
            options = fixActionCustomerUser(options);
            if (!models) {
                modeler.db({
                    collection: 'instances.quiz.ranged.Attempt',
                    query: {
                        _id: attemptId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for attempt ' + attemptId);
            }
            cb(null, models, {});
        },
        function(models, p, cb) {
            models.instances.quiz.ranged.Attempt.findOne({
                _id: attemptId.toObjectId()
            })
                .exec(function(err, attempt) {
                    p.attempt = attempt;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            models.instances.quiz.ranged.Attempt.populate(p.attempt, {
                path: 'quizDefinitionRange',
                model: models.definitions.quiz.ranged.Quiz
            }, function(err, attempt) {
                p.attempt = attempt;
               cb(err, models, p);
            });
        },
        function(models, p, cb){
            models.instances.quiz.ranged.Attempt.populate(p.attempt, {
                path:'questions',
                model: models.instances.quiz.Question
            }, function(err, attempt){
               p.attempt= attempt;
               cb(err, models, p);
            });
        }

    ], function(err, models, p) {
        logger.debug('[attempt loader] attempt loaded.');
        cb(err, models, (p || {})
            .attempt);
    });
};
