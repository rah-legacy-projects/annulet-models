module.exports = exports = {
	attempt: require("./attempt"),
	questionTypes: require("./questionTypes"),
	stamper: require("./stamper"),
};
