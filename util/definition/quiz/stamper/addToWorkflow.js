var async = require('async'),
    _ = require('lodash'),
    util = require('util'),
    logger = require('winston'),
    stamper = require('./stamper'),
    fixCustomerUser = require('../../../fixCustomerUser'),
    modeler = require('../../../../modeler');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options: {
    //  workflowItem: object or ID for the workflow item instance to stamp and add to,
    //  customerUser
    //}
    var workflowItemId = _.extractId(options, 'workflowItem'),
        customerUserId = _.extractId(options, 'customerUserId');
    async.waterfall([

        function(cb) {
            if (!workflowItemId) {
                return cb('invalid workflow item id for add to wf: ' + options.workflowItem);
            }
            cb();
        },
        function(cb) {
            options = fixCustomerUser(options);
            if (!models) {
                modeler.db({
                    collection: 'instances.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for ' + workflowItemId);
            }
            cb(null, models);
        },
        function(models, cb) {
            models.instances.workflow.WorkflowItem.findOne({
                _id: workflowItemId.toObjectId()
            })
                .populate({
                    path: 'workflowItemDefinition',
                    model: models.definitions.workflow.WorkflowItem
                })
                .exec(function(err, workflowItem) {
                    cb(err, models, {
                        workflowItem: workflowItem
                    });
                });
        },
        function(models, p, cb) {
            if (!p.workflowItem) {
                return cb('workflow item could not be found for add to wf ' + workflowItemId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            models.instances.quiz.QuizContainer.findOne({
                _id: p.workflowItem.quizContainer
            })
                .exec(function(err, quizContainer) {
                    cb(err, models, _.defaults(p, {
                        quizContainer: quizContainer
                    }));
                });
        },
        function(models, p, cb) {
            //the stamper will find, load, and stamp the quiz by quiz def id
            stamper(models, {
                quizDefinition: p.workflowItem.workflowItemDefinition.quiz,
                customerUser: options.customerUser
            }, function(err, models, stampedQuiz) {
                if(!!err){
                    return cb(err);
                }
                //add the stamped quiz id to the container
                p = _.defaults(p, {
                    stampedQuiz: stampedQuiz
                });
                p.quizContainer.modifiedBy = customerUserId;
                p.quizContainer.quizzes.push(stampedQuiz._id);
                //save container
                p.quizContainer.save(function(err, quizContainer) {
                    if (!!err) {
                        logger.error('error saving quiz container: ' + util.inspect(err));
                    }
                    p.quizContainer = quizContainer;
                    cb(err, models, p);
                });
            });

        },
        function(models, p, cb) {
            //assign the quiz container to the workflow item and save
            p.workflowItem.quizContainer = p.quizContainer._id;
            p.workflowItem.modifiedBy = customerUserId;
            p.workflowItem.save(function(err, item) {
                p.workflowItem = item;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, p);
    });
};
