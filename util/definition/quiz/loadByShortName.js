var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    loader = require('./loader'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var customerId = _.extractId(options, 'customer');


    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('Bad customer ID in quiz def loader by shortname.');
            }
            if (!options.shortName) {
                return cb('Bad shortname in quiz def loader by shortname.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for quiz def id ' + quizDefinitionId);
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[quiz def shortname loader] looking for shortname ' + options.shortName);
            models.definitions.quiz.Quiz.find({
                shortName: options.shortName,
                active: true,
                deleted: false
            })
                .exec(function(err, quizzes) {
                    cb(err, models, {
                        quizzes: quizzes
                    });
                });
        },
        function(models, p, cb) {
            if (p.quizzes.length == 0) {
                //no quizzes, defs, etc, skip load
                cb(null, models, p);
            } else {

                //load the quizzes
                loader(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    quizDefinitions: _.pluck(p.quizzes, '_id')
                }, function(err, models, quizzes) {
                    p.quizzes = quizzes;
                    cb(err, models, p);
                });
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[quiz def shortname loader] problem loading def: ' + util.inspect(err));
        }
        cb(err, models, (p || {})
            .quizzes);
    });
};
