var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var auxiliaryDocumentDefinitionIds = extractOptionIds(options, {
        single: 'auxiliaryDocumentDefinition',
        array: 'auxiliaryDocumentDefinitions'
    });

    logger.silly('[auxdoc loader] ids: ' + util.inspect(auxiliaryDocumentDefinitionIds));
    async.waterfall([

        function(cb) {
            if (!auxiliaryDocumentDefinitionIds || auxiliaryDocumentDefinitionIds.length == 0) {
                return cb('Bad operating procedure definition ID in auxdox def loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.auxiliaryDocument.AuxiliaryDocument',
                    query: {
                        _id: auxiliaryDocumentDefinitionIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for auxdox def id ' + util.inspect(auxiliaryDocumentDefinitionIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[auxiliaryDocument def loader] loading definition');
            models.definitions.auxiliaryDocument.AuxiliaryDocument.find({
                _id: {
                    $in: auxiliaryDocumentDefinitionIds
                }
            })
                .exec(function(err, auxiliaryDocuments) {
                    cb(err, models, {
                        auxiliaryDocuments: auxiliaryDocuments
                    });
                });
        },
        function(models, p, cb) {
            if (!p.auxiliaryDocuments) {
                return cb('operating procedure def could not be found for id ' + util.inspect(auxiliaryDocumentDefinitionIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the OP's ranged data
            models.definitions.auxiliaryDocument.AuxiliaryDocument.populate(p.auxiliaryDocuments, {
                path: 'rangedData',
                model: models.definitions.auxiliaryDocument.ranged.AuxiliaryDocument
            }, function(err, auxiliaryDocuments) {
                p.auxiliaryDocuments = auxiliaryDocuments;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[auxdox def loader] auxdox loaded.');
            if (!!options.flatten) {
                async.parallel(_.map(p.auxiliaryDocuments, function(auxiliaryDocument) {
                    return function(cb) {
                        logger.silly('[auxdox def loader] auxdox is being flattened.');
                        //objectify the auxdox for flattening
                        auxiliaryDocument = auxiliaryDocument.toObject();
                        //flatten the sections and the OP
                        async.parallel({
                            auxdox: function(cb) {
                                datesplice.getByDateRange(auxiliaryDocument, 'rangedData', options.startDate, options.endDate, function(err, flatAuxDoc) {
                                    //hack: add range type
                                    flatAuxDoc._rangeType = 'definitions.auxiliaryDocument.ranged.AuxiliaryDocument';
                                    cb(err, flatAuxDoc);
                                });
                            },
                        }, function(err, r) {
                            auxiliaryDocument = r.auxdox;
                            cb(err, auxiliaryDocument);
                        });
                    };
                }), function(err, auxiliaryDocuments) {
                    p.auxiliaryDocuments = auxiliaryDocuments;
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[auxdox def loader] problem loading def: ' + util.inspect(err));
        }
        logger.silly('[auxdox def loader] finished loading.');
        cb(err, models, (p || {})
            .auxiliaryDocuments.length == 1 && !!options.auxiliaryDocumentDefinition ? p.auxiliaryDocuments[0] : (p || {})
            .auxiliaryDocuments);
    });
};
