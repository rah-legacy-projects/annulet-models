var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/auxiliaryDocument/loader'),
    closeDraft = require('./close');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  auxiliaryDocumentDefinition: object representing the auxiliaryDocument to create,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        auxiliaryDocumentDefinitionId = _.extractId(options, 'auxiliaryDocumentDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!customerId) {
                    return cb("Bad customer ID when trying to publish draft of OP.");
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for auxdox by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.auxiliaryDocumentDefinition) {
                    return cb('auxdox definition not supplied to publish draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    auxiliaryDocumentDefinition: auxiliaryDocumentDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if (!!p.draft.isDraftOf && p.draft.isDraftOf === 'new') {
                    p.originalDraft = new models.definitions.auxiliaryDocument.AuxiliaryDocument({
                        rangedData: [],
                        customer: customerId
                    });
                    cb(null, models, p);
                } else {
                    definitionLoader(models, {
                        auxiliaryDocumentDefinition: p.draft.isDraftOf,
                        actionCustomerUser: options.actionCustomerUser,
                        flatten: false
                    }, function(err, models, original) {
                        p.originalDraft = original;
                        cb(err, models, p);
                    })
                }
            },
            function(models, p, cb) {
                logger.silly('[auxdox publish] about to publish ranges');
                async.parallel({
                    ranges: function(cb) {
                        var instancesToSave = p.originalDraft.rangedData;
                        async.series(_.map(p.draft.rangedData, function(rangeData) {
                            return function(cb) {

                                if (_.any(p.originalDraft.rangedData, function(originalRange) {
                                    return models.definitions.auxiliaryDocument.ranged.AuxiliaryDocument.compare(originalRange, rangeData);
                                })) {
                                    cb();
                                } else {

                                    var copy = _.clone(rangeData.toObject());
                                    delete copy._id;
                                    var range = new models.definitions.auxiliaryDocument.ranged.AuxiliaryDocument(copy);
                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                        instancesToSave = partsToSave;
                                        cb(err, partsToSave);
                                    });
                                }
                            };
                        }), function(err, r) {
                            cb(err, instancesToSave);
                        });
                    }

                }, function(err, r) {
                    logger.silly('[auxdox publish] original prepared. Saving...');
                    datesplice.dateSpliceSave(options.actionCustomerUser._id, p.originalDraft, r.ranges, 'rangedData', function(err, savedAuxiliaryDocument) {
                        p.auxiliaryDocument = savedAuxiliaryDocument;
                        cb(err, models, p);
                    });
                });
            },
            function(models, p, cb) {
                //reload the auxdox def as saved
                definitionLoader(models, {
                    auxiliaryDocumentDefinition: p.auxiliaryDocument._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, auxiliaryDocument) {
                    //make the auxdox returned a regular object
                    p.auxiliaryDocument = auxiliaryDocument.toObject();
                    cb(err, models, p);
                });
            },
            //{{{ alter user data
            function(models, p, cb) {
                //find all instances of the auxdox
                models.instances.auxiliaryDocument.AuxiliaryDocument.find({
                    auxiliaryDocumentDefinition: p.auxiliaryDocument._id,
                    active: true,
                    deleted: false
                })
                    .exec(function(err, instances) {
                        p.auxiliaryDocumentInstances = instances;
                        cb(err, models, p);
                    });

            },
            function(models, p, cb) {
                //load the OP's ranged data for the instances
                models.instances.auxiliaryDocument.AuxiliaryDocument.populate(p.auxiliaryDocumentInstances, {
                    path: 'rangedData',
                    model: models.instances.auxiliaryDocument.ranged.AuxiliaryDocument
                }, function(err, auxiliaryDocumentInstances) {
                    p.auxiliaryDocumentInstances = auxiliaryDocumentInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for all of the loaded instances,
                async.parallel(_.map(p.auxiliaryDocumentInstances, function(auxiliaryDocumentInstance) {
                    return function(cb) {
                        //map the definition ranges on the existing data
                        async.parallel({
                            auxiliaryDocumentRanges: function(cb) {
                                async.parallel(_.map(p.auxiliaryDocument.rangedData, function(opDefRange) {
                                    return function(cb) {
                                        var instanceRange = _.find(auxiliaryDocumentInstance.rangedData, function(opInstanceRange) {
                                            return opInstanceRange.auxiliaryDocumentDefinitionRange.toString() == opDefRange._id.toString()
                                        });

                                        if (!instanceRange) {
                                            return cb();
                                        }

                                        instanceRange.startDate = opDefRange.startDate;
                                        instanceRange.endDate = opDefRange.endDate;
                                        instanceRange.rangeActive = opDefRange.rangeActive;
                                        instanceRange.modifiedBy = options.actionCustomerUser._id;

                                        instanceRange.save(function(err, instanceRange) {
                                            cb(err, instanceRange);
                                        });
                                    }
                                }), cb);
                            },
                        }, cb);


                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            //}}}
            function(models, p, cb) {
                //close the draft that was published
                closeDraft(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    auxiliaryDocumentDefinition: p.draft
                }, function(err, models, closed) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //reload the auxdox def as saved
                definitionLoader(models, {
                    auxiliaryDocumentDefinition: p.auxiliaryDocument._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, auxiliaryDocument) {
                    //make the auxdox returned a regular object
                    auxiliaryDocument = auxiliaryDocument.toObject();
                    cb(err, models, p);
                });
            },
        ],
        function(err, models, p) {
            if (!!err) {
                logger.error('problem publishing draft of auxiliaryDocument: ' + util.inspect(err));
            }
            cb(err, models, p.auxiliaryDocument);
        });
};
