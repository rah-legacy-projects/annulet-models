var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    moment = require('moment'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    auxiliaryDocumentLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  auxiliaryDocumentDefinition: flattened object of the draft OP,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        auxiliaryDocumentDefinitionId = _.extractId(options, 'auxiliaryDocumentDefinition');

    options = options || {};
    options = _.defaults(options, {
        flatten: false
    });

    options = fixActionCustomerUser(options);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to update auxdox draft.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.auxiliaryDocumentDefinition) {
                return cb('Operating procedure definition not supplied to update draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.auxiliaryDocumentDefinition.startDate || !options.auxiliaryDocumentDefinition.endDate) {
                return cb('Operating procedure definition must supply start/end dates');
            }

            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[auxdox update draft] loading op');
            auxiliaryDocumentLoader(models, {
                auxiliaryDocumentDefinition: auxiliaryDocumentDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, auxiliaryDocument) {
                if (!auxiliaryDocument) {
                    return cb('Operating procedure could not be found: ' + auxiliaryDocumentDefinitionId);
                }
                p.auxiliaryDocument = auxiliaryDocument;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[auxdox update draft] updating auxdox range');
            var auxiliaryDocumentRangeToUpdate = _.find(p.auxiliaryDocument.rangedData, function(range) {
                return range._id.toString() == (options.auxiliaryDocumentDefinition._rangeId || 'new')
                    .toString();
            });

            if (!auxiliaryDocumentRangeToUpdate) {
                //auxdox range should be created by making a draft
                return cb('Invalid auxdox range id ' + options.auxiliaryDocumentDefinition._rangeId);
            }

            //update auxdox range
            auxiliaryDocumentRangeToUpdate.file = options.auxiliaryDocumentDefinition.file;
            auxiliaryDocumentRangeToUpdate.description = options.auxiliaryDocumentDefinition.description;
            auxiliaryDocumentRangeToUpdate.extension = options.auxiliaryDocumentDefinition.extension;
            auxiliaryDocumentRangeToUpdate.displayName = options.auxiliaryDocumentDefinition.displayName;
            auxiliaryDocumentRangeToUpdate.startDate = options.auxiliaryDocumentDefinition.startDate;
            auxiliaryDocumentRangeToUpdate.endDate = options.auxiliaryDocumentDefinition.endDate;
            auxiliaryDocumentRangeToUpdate.modifiedBy = options.actionCustomerUser._id;

            auxiliaryDocumentRangeToUpdate.save(function(err, auxiliaryDocumentRangeToUpdate) {
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[auxdox update draft] updating auxdox');
            //update the operating procedure's section IDs
            p.auxiliaryDocument.modifiedBy = options.actionCustomerUser._id;
            p.auxiliaryDocument.save(function(err, auxiliaryDocument) {
                p.auxiliaryDocument = auxiliaryDocument;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[auxdox update draft] reloading auxdox (flat)');
            auxiliaryDocumentLoader(models, {
                auxiliaryDocumentDefinition: auxiliaryDocumentDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, auxiliaryDocument) {
                if (!auxiliaryDocument) {
                    return cb('Operating procedure could not be found: ' + auxiliaryDocumentDefinitionId);
                }
                p.auxiliaryDocument = auxiliaryDocument;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem updating draft of auxiliaryDocument: ' + util.inspect(err));
        }

        cb(err, models, (p || {})
            .auxiliaryDocument);
    });
};
