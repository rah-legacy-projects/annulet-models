var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/auxiliaryDocument/loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  auxiliaryDocumentDefinition: object representing the auxiliaryDocument to create,
    //  actionCustomerUser
    //}

    var auxiliaryDocumentDefinitionId = _.extractId(options, 'auxiliaryDocumentDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'definitions.auxiliaryDocument.AuxiliaryDocument',
                        query: {
                            _id: auxiliaryDocumentDefinitionId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for OPs by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.auxiliaryDocumentDefinition) {
                    return cb('Operating procedure definition not supplied to close draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    auxiliaryDocumentDefinition: auxiliaryDocumentDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the ranges of the OP
                async.parallel(_.map(p.draft.rangedData, function(range) {
                    return function(cb) {
                        async.series([

                            function(cb) {
                                //close the ranges of the instances of the range
                                models.instances.auxiliaryDocument.ranged.AuxiliaryDocument.find({
                                    auxiliaryDocumentDefinitionRange: range._id
                                })
                                    .exec(function(err, instanceRanges) {
                                        async.parallel(_.map(instanceRanges, function(instanceRange) {
                                            return function(cb) {
                                                instanceRange.active = false;
                                                instanceRange.deleted = true;
                                                instanceRange.modifiedBy = options.actionCustomerUser._id;
                                                instanceRange.save(function(err, instanceRange) {
                                                    cb(err, instanceRange);
                                                });
                                            };
                                        }), function(err, r) {
                                            cb(err);
                                        });
                                    });
                            },
                            function(cb) {
                                range.active = false;
                                range.deleted = true;
                                range.modifiedBy = options.actionCustomerUser._id;
                                range.save(function(err, range) {
                                    cb(err, range);
                                });
                            }
                        ], function(err) {
                            cb(err);
                        });
                    }
                }), function(err, r) {
                    logger.silly('[auxdox draft close] ranges closed.');
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                async.series([

                    function(cb) {
                        //close the auxdox instances
                        models.instances.auxiliaryDocument.AuxiliaryDocument.find({
                            auxiliaryDocumentDefinition: p.draft._id
                        })
                            .exec(function(err, instances) {
                                async.parallel(_.map(instances, function(instance) {
                                    instance.active = false;
                                    instance.deleted = true;
                                    instance.modifiedBy = options.actionCustomerUser._id;
                                    instance.save(function(err, instance) {
                                        cb(err, instance);
                                    });
                                }), function(err, r) {
                                    cb(err, r);
                                });
                            });
                    },
                    function(cb) {
                        //close the auxdox
                        p.draft.active = false;
                        p.draft.deleted = true;
                        p.draft.modifiedBy = options.actionCustomerUser._id;
                        p.draft.save(function(err, draft) {
                            logger.silly('[auxdox draft close] auxdox closed.');
                            cb(err, draft);
                        });
                    }
                ], function(err, r) {
                    cb(err, models, p)
                });
            },
        ],
        function(err, models, p) {
            logger.silly('[auxdox draft close] close complete.');
            if (!!err) {
                logger.error('problem closing draft of auxiliaryDocument: ' + util.inspect(err));
            }
            cb(err, models, p.draft);
        });
};
