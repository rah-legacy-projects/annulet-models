var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    auxiliaryDocumentLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  auxiliaryDocumentDefinition: object representing the auxiliaryDocument to create,
    //  actionCustomerUser: the customer user with user for audit purposes
    //}

    var customerId = _.extractId(options, 'customer'),
        auxiliaryDocumentDefinitionId = _.extractId(options, 'auxiliaryDocumentDefinition');
        options = fixActionCustomerUser(options);

    logger.silly('[auxdox create draft] auxdoc id: ' + auxiliaryDocumentDefinitionId);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to create OP.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.auxiliaryDocumentDefinition) {
                return cb('Operating procedure definition not supplied to create draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!!auxiliaryDocumentDefinitionId && auxiliaryDocumentDefinitionId !== 'new') {
                logger.silly('[auxdox create draft] getting auxdox definition: ' + auxiliaryDocumentDefinitionId);
                //auxdox exists, go get it
                auxiliaryDocumentLoader(models, {
                    auxiliaryDocumentDefinition: options.auxiliaryDocumentDefinition,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, auxiliaryDocument) {
                    //operating procedure should be loaded but not flattened
                    p.auxiliaryDocument = auxiliaryDocument.toObject();
                    cb(err, models, p);
                });
            } else {
                logger.silly('[auxdox create draft] auxdox def new, making stub');
                //auxdox does not exist, make one to stub off of
                p.auxiliaryDocument = {}; //new models.definitions.auxiliaryDocument.AuxiliaryDocument();
                p.auxiliaryDocument.rangedData = [{
                    displayName:'',
                    description:'',
                    extension: '',
                    startDate: moment().subtract(1, 'seconds').toDate(),
                    endDate: moment().add(90, 'days').toDate(),
                    rangeActive:true
                }];
                p.auxiliaryDocument.sections = [];
                p.auxiliaryDocument.customer = customerId;
                p.createdBy = options.actionCustomerUser._id;
                p.auxiliaryDocument._id = 'new';
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            logger.silly('[auxdox create draft] making copy of auxdox range');
            //make a copy of the auxdox range
            var f = datesplice.getByDateRangeSync(_.clone(p.auxiliaryDocument), 'rangedData', moment(), moment());
    
            var auxiliaryDocumentRange = null;
            if (!!f) {
                auxiliaryDocumentRange = _.find(p.auxiliaryDocument.rangedData, function(r) {
                    return r._id == f._rangeId;
                });
            } else {
                auxiliaryDocumentRange = new models.definitions.auxiliaryDocument.ranged.AuxiliaryDocument();
            }

            (new models.definitions.auxiliaryDocument.ranged.AuxiliaryDocument({
                displayName: auxiliaryDocumentRange.displayName,
                description: auxiliaryDocumentRange.description,
                file: auxiliaryDocumentRange.file,
                extension: auxiliaryDocumentRange.extension,
                createdBy: options.actionCustomerUser._id,
                startDate: auxiliaryDocumentRange.startDate,
                endDate: auxiliaryDocumentRange.endDate
            }))
                .save(function(err, auxiliaryDocumentRange) {
                    p.auxiliaryDocumentRange = auxiliaryDocumentRange;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[auxdox create draft] saving draft copy');
            //save a draft copy of the op
            (new models.definitions.auxiliaryDocument.AuxiliaryDocument({
                customer: customerId,
                isDraftOf: p.auxiliaryDocument._id,
                rangedData: [p.auxiliaryDocumentRange._id],
                createdBy: options.actionCustomerUser._id
            }))
                .save(function(err, auxiliaryDocument) {
                    p.draft = auxiliaryDocument;
                    logger.silly('[auxdox create draft] draft is draft of ' + p.draft.isDraftOf);
                    cb(err, models, p);
                });

        },
        function(models, p, cb) {
            logger.silly('[auxdox create draft] reloading draft, unflattened');
            //reload the draft auxdox unflattened
            auxiliaryDocumentLoader(models, {
                auxiliaryDocumentDefinition: p.draft._id,
                flatten: false,
                actionCustomerUser: options.actionCustomerUser
            }, function(err, models, draft) {
                p.draft = draft;
                logger.silly('[auxdox create draft] draft is draft of ' + p.draft.isDraftOf);
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating draft of auxiliaryDocument: ' + util.inspect(err));
        }
        cb(err, models, p.draft);
    });
};
