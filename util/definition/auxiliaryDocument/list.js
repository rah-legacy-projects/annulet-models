var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    definitionLoader = require('./loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    options = fixActionCustomerUser(options);
    async.waterfall([

        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: options.actionCustomerUser._id
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for auxdox admin listing');
            }
            cb(null, models, {});
        },
        function(models, p, cb) {
            //get the active/undeleted auxiliaryDocuments
            models.definitions.auxiliaryDocument.AuxiliaryDocument.find({
                active: true,
                deleted: false
            }, function(err, auxiliaryDocuments) {
                p.auxiliaryDocuments = auxiliaryDocuments;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //separate drafts from published
            p.drafts = _.filter(p.auxiliaryDocuments, function(auxiliaryDocument) {
                return !!auxiliaryDocument.isDraftOf;
            });
            p.published = _.filter(p.auxiliaryDocuments, function(auxiliaryDocument) {
                return !auxiliaryDocument.isDraftOf;
            });
            logger.silly('[auxdox def list] drafts: ' + p.drafts.length);
            logger.silly('[auxdox def list] published: ' + p.published.length);

            cb(null, models, p);
        },
        function(models, p, cb) {
            if (p.published.length == 0) {
                return cb(null, models, p);
            }
            //load the published flattened
            definitionLoader(models, {
                actionCustomerUser: options.actionCustomerUser,
                auxiliaryDocumentDefinitions: p.published,
                flattened: true,
            }, function(err, models, auxiliaryDocuments) {
                p.published = auxiliaryDocuments;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if (p.drafts.length == 0) {
                logger.silly('[auxdox def list] no drafts found');
                return cb(null, models, p);
            }
            //load the drafts unflattened
            definitionLoader(models, {
                actionCustomerUser: options.actionCustomerUser,
                auxiliaryDocumentDefinitions: p.drafts,
                flatten: false,
            }, function(err, models, auxiliaryDocuments) {
                auxiliaryDocuments = _.map(auxiliaryDocuments, function(ad){return ad.toObject();});
                p.drafts = auxiliaryDocuments;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //make {published, draft} tuples

            var tuples = [];
            //take care of the new and draft tuples
            tuples = tuples.concat(_.map(p.drafts, function(draft) {
                return {
                    draft: draft,
                    published: _.find(p.published, function(published) {
                        return published._id.toString() == draft.isDraftOf.toString();
                    })
                };
            }));
            //filter the published to the undrafted published
            //map to tuples
            tuples = tuples.concat(_.chain(p.published)
                .filter(function(published) {
                    return !_.any(p.drafts, function(draft) {
                        return published._id.toString() == draft.isDraftOf.toString();
                    });
                })
                .map(function(published) {
                    return {
                        draft: null,
                        published: published
                    };
                })
                .value());

            p.tuples = tuples;
            cb(null, models, p);
        },
        function(models, p, cb) {
            //flatten the draft part of the tuples manually
            async.parallel(_.map(p.tuples, function(tuple) {
                return function(cb) {
                    if (!tuple.draft) {
                        return cb(null);
                    }
                    logger.silly('tuple draft: ' + util.inspect(tuple.draft));
                    datesplice.getByDateRange(tuple.draft, 'rangedData', tuple.draft.rangedData[0].startDate, tuple.draft.rangedData[0].endDate, function(err, draft) {
                        tuple.draft = draft;
                        logger.silly('tuple draft after: ' + util.inspect(tuple.draft));
                        cb(err);
                    });
                };
            }), function(err, r) {
                cb(err, models, p);
            });
        }

    ], function(err, models, p) {
        cb(err, models, p.tuples);
    });
};
