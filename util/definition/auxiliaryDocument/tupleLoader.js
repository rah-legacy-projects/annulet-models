var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    definitionLoader = require('./loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    options = fixActionCustomerUser(options);
    var auxiliaryDocumentId = _.extractId(options, 'auxiliaryDocument');
    logger.silly('[auxdox tuple loader] aux doc id: ' + auxiliaryDocumentId);
    logger.silly('[auxdox tuple loader] aux doc option: ' + util.inspect(options.auxiliaryDocument));
    async.waterfall([

        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: options.actionCustomerUser._id
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for auxdox admin listing');
            }
            cb(null, models, {});
        },
        function(models, p, cb) {

            //get the active/undeleted auxiliaryDocuments with the ID passed
            models.definitions.auxiliaryDocument.AuxiliaryDocument.findOne({
                active: true,
                deleted: false,
                _id: auxiliaryDocumentId.toObjectId()
            }, function(err, auxiliaryDocument) {
                p.auxiliaryDocument = auxiliaryDocument;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if (p.auxiliaryDocument.isDraftOf == 'new') {
                //new document, use this as the draft
                p.draft = p.auxiliaryDocument;
                return cb(null, models, p);
            } else if (!!p.auxiliaryDocument.isDraftOf) {
                //the doc loaded is a draft
                p.draft = p.auxiliaryDocument;
                models.definitions.auxiliaryDocument.AuxiliaryDocument.findOne({
                    _id: p.isDraftOf
                })
                    .exec(function(err, published) {
                        p.published = published;
                        return cb(err, models, p);
                    });
            } else {
                p.published = p.auxiliaryDocument;
                //does this have an open draft?
                models.definitions.auxiliaryDocument.AuxiliaryDocument.find({
                    active: true,
                    deleted: false,
                    isDraftOf: p.auxiliaryDocument._id
                })
                    .exec(function(err, drafts) {
                        if (drafts.length == 0) {
                            //not new, no drafts, it's published
                        } else if (drafts.length == 1) {
                            p.draft = drafts[0];
                        } else {
                            logger.warn('[aux dox tuple loader] more than one draft found for ' + p.auxiliaryDocument._id);
                            p.draft = drafts[0];
                        }
                        cb(err, models, p);
                    });
            }
        },
        function(models, p, cb) {
            //load the published, if any
            if (!p.published) {
                return cb(null, models, p);
            }

            definitionLoader(models, {
                auxiliaryDocumentDefinition: p.published,
                actionCustomerUser: options.actionCustomerUser,
                flatten: true
            }, function(err, models, published) {
                p.published = published;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the draft, if any
            if (!p.draft) {
                return cb(null, models, p);
            }

            definitionLoader(models, {
                auxiliaryDocumentDefinition: p.draft,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, draft) {
                //flatten by hand
                draft = draft.toObject();
                datesplice.getByDateRange(draft, 'rangedData', draft.rangedData[0].startDate, draft.rangedData[0].endDate, function(err, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            });
        },
        function(models, p, cb){
            //assemble the tuple
            p.tuple = {
                draft: p.draft,
                published: p.published
            };
            cb(null, models, p);
        }
    ], function(err, models, p) {
        logger.silly('[tuple loader] done.');
        cb(err, models, p.tuple);
    });
};
