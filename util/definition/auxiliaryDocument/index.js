module.exports = exports = {
	draft: require("./draft"),
	list: require("./list"),
	loader: require("./loader"),
	stamper: require("./stamper"),
	tupleLoader: require("./tupleLoader"),
};
