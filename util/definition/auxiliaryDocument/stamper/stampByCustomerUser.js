var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    auxiliaryDocumentLoader = require('../loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    auxiliaryDocumentInstanceLoader = require('../../../instance/auxiliaryDocument/loader'),
    stamp = require('./stamper');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  customerUser: id or object of the customer user,
    //  actionCustomerUser
    //  flatten
    //}

    options = fixActionCustomerUser(options);
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('stamp auxdoc by customer user, bad customer user id: ' + options.customerUser);
            }
            cb();
        },
        function(cb) {
            logger.silly('[auxdoc stamp by customeruser] customer user = ' + customerUserId);
            if (!models) {
                logger.silly('[auxdoc stamp by customeruser] retrieving models ');
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: customerUserId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            //identify the customer user
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //get the auxdoc definitions for the customer
            models.definitions.auxiliaryDocument.AuxiliaryDocument.find({
                active: true,
                deleted: false
            })
                .exec(function(err, auxiliaryDocumentDefinitions) {
                    p.definitions = auxiliaryDocumentDefinitions;
                    logger.silly('[auxdoc stamp by customeruser] definitions: ' + p.definitions.length);
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            var idsToStamp = _.pluck(p.definitions, '_id');
            async.parallel(_.map(idsToStamp, function(defId) {
                return function(cb) {
                    logger.silly('[auxdoc stamp by customeruser] stamping ' + defId);
                    stamp(models, {
                        createdBy: options.createdBy,
                        modifiedBy: options.modifiedBy,
                        auxiliaryDocumentDefinition: defId,
                        customerUser: p.customerUser._id,
                        actionCustomerUser: options.actionCustomerUser
                    }, function(err, models, auxiliaryDocumentInstance) {
                        logger.silly('[auxdoc stamp by customeruser] stamped ' + defId);
                        cb(err, auxiliaryDocumentInstance);
                    });
                }
            }), function(err, stamped) {
                logger.silly('[auxdoc stamp by customeruser] auxdoxs stamped: ' + stamped.length);
                p.instances = stamped;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if (p.instances.length == 0) {
                return cb(null, models, p);
            }

            auxiliaryDocumentInstanceLoader(models, {
                actionCustomerUser: options.actionCustomerUser,
                auxiliaryDocumentInstances: p.instances,
                flatten: options.flatten,
            }, function(err, models, auxDocInstances) {
                logger.silly('[auxdoc stamp by customeruser] after instance loader: ' + auxDocInstances.length);
                p.instances = auxDocInstances;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, (p || {})
            .instances);
    });
};
