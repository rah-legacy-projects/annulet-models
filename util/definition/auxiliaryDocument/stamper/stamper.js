var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    definitionLoader = require('../loader'),
    instanceLoader = require('../../../instance/auxiliaryDocument/loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    stampClone = require('../../stampClone');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  auxiliaryDocumentDefinition: the auxiliaryDocument def to stamp,
    //  customerUser: the customer user the instance belongs to,
    //  actionCustomerUser
    //}

    options = fixActionCustomerUser(options);
    var auxiliaryDocumentDefinitionId = _.extractId(options, 'auxiliaryDocumentDefinition');
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!auxiliaryDocumentDefinitionId) {
                return cb('bad auxdox def id when stamping: ' + options.auxiliaryDocumentDefinition);
            }
            if (!customerUserId) {
                return cb('bad customer user id when stamping op: ' + options.customerUser);
            }

            cb();
        },
        function(cb) {
            logger.silly('[auxiliaryDocument stamper] auxdox def id: ' + auxiliaryDocumentDefinitionId);

            if (!models) {
                modeler.db({
                    collection: 'definitions.auxiliaryDocument.AuxiliaryDocument',
                    query: {
                        _id: auxiliaryDocumentDefinitionId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, {});
                });
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('modelset could not be identified for stamping an auxdox def: ' + auxiliaryDocumentDefinitionId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //verify customer user exists
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                return cb('customer user could not be found for auxdox stamp: ' + customerUserId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.debug('[auxiliaryDocument stamper] loading definition ' + auxiliaryDocumentDefinitionId);
            definitionLoader(models, {
                auxiliaryDocumentDefinition: auxiliaryDocumentDefinitionId.toObjectId(),
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, auxiliaryDocumentDefinition) {
                logger.silly('[auxiliaryDocument stamper] doc def loaded.');
                cb(err, models, {
                    auxiliaryDocumentDefinition: auxiliaryDocumentDefinition
                });
            });
        },
        function(models, p, cb) {
            models.instances.auxiliaryDocument.AuxiliaryDocument.findOne({
                auxiliaryDocumentDefinition: auxiliaryDocumentDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, auxiliaryDocumentInstance) {
                    if (!auxiliaryDocumentInstance) {
                        p._state = 'Created';
                        auxiliaryDocumentInstance = new models.instances.auxiliaryDocument.AuxiliaryDocument({
                            auxiliaryDocumentDefinition: p.auxiliaryDocumentDefinition._id,
                            customerUser: customerUserId.toObjectId(),
                            createdBy: options.actionCustomerUser._id,
                            modifiedBy: options.actionCustomerUser._id,
                            rangedData: [],
                            sections: []
                        });
                        p.auxiliaryDocumentInstance = auxiliaryDocumentInstance;
                        cb(err, models, p);

                    } else {
                        p._state = 'Opened';
                        //load the auxdox instance.
                        instanceLoader(models, {
                            auxiliaryDocumentInstance: auxiliaryDocumentInstance,
                            flatten: false
                        }, function(err, models, auxiliaryDocumentInstance) {
                            p.auxiliaryDocumentInstance = auxiliaryDocumentInstance;
                            cb(err, models, p);
                        });
                    }

                });
        },
        function(models, p, cb) {
            //determine parts to splice
            var auxiliaryDocumentInstanceRangesToSplice = _.map(p.auxiliaryDocumentDefinition.rangedData, function(auxiliaryDocumentRange) {
                var range = _.find(p.auxiliaryDocumentInstance.rangedData, function(auxiliaryDocumentInstanceRange) {
                    return auxiliaryDocumentInstanceRange.auxiliaryDocumentDefinitionRange._id.toString() == auxiliaryDocumentRange._id.toString();
                });

                //if the range doesn't exist or is otherwise different, splice it, otherwise return nothing
                if (!range ||
                    !(moment(range.endDate)
                        .isSame(moment(auxiliaryDocumentRange.endDate))) ||
                    !(moment(range.startDate)
                        .isSame(moment(auxiliaryDocumentRange.startDate)))) {
                    //create a new range to splice
                    return new models.instances.auxiliaryDocument.ranged.AuxiliaryDocument({
                        title: auxiliaryDocumentRange.title,
                        description: auxiliaryDocumentRange.description,
                        completedDate: null,
                        auxiliaryDocumentDefinitionRange: auxiliaryDocumentRange._id,
                        startDate: auxiliaryDocumentRange.startDate,
                        endDate: auxiliaryDocumentRange.endDate,
                        rangeActive: auxiliaryDocumentRange.rangeActive,
                        createdBy: options.actionCustomerUser._id
                    });
                }

                return null;
            });

            //crunch out the nulls
            auxiliaryDocumentInstanceRangesToSplice = _.compact(auxiliaryDocumentInstanceRangesToSplice);


            //splice the ranged data in series
            //note this has to be in series to ensure correct order of range operations
            var instanceRanges = p.auxiliaryDocumentInstance.rangedData; //_.clone(p.auxiliaryDocumentInstance.rangedData);
            async.series(_.map(auxiliaryDocumentInstanceRangesToSplice, function(range) {
                return function(cb) {
                    datesplice.splice(instanceRanges, range, function(err, toSave) {
                        instanceRanges = toSave;
                        cb(err);
                    });
                };
            }), function(err) {
                logger.silly('[auxdox stamper] splice-saving auxiliary document');
                datesplice.dateSpliceSave(options.actionCustomerUser._id, p.auxiliaryDocumentInstance, instanceRanges, 'rangedData', function(err, savedOPInstance) {
                    //skip overwriting op, still need to save the sections
                    logger.silly('[auxdox stamper] auxdox spliced.');
                    cb(err, models, p);
                });
            });

        },
        function(models, p, cb) {
            logger.debug('[auxiliaryDocument stamper] getting auxdox instance for def id ' + auxiliaryDocumentDefinitionId);
            models.instances.auxiliaryDocument.AuxiliaryDocument.findOne({
                auxiliaryDocumentDefinition: auxiliaryDocumentDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, auxiliaryDocumentInstance) {
                    p.auxiliaryDocumentInstance = auxiliaryDocumentInstance;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[auxiliaryDocument stamper] loading instance');
            instanceLoader(models, {
                auxiliaryDocumentInstance: p.auxiliaryDocumentInstance,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, auxiliaryDocumentInstance) {
                logger.silly('[auxiliaryDocument stamper] instance loaded.');
                logger.silly('[auxdox stamper] loaded instance: ' + JSON.stringify(auxiliaryDocumentInstance, null, 2))
                p.auxiliaryDocumentInstance = auxiliaryDocumentInstance;
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            (new models.tracking.ItemActivity({
                changedBy: options.actionCustomerUser._id,
                state: p._state,
                itemType: 'instances.auxiliaryDocument.AuxiliaryDocument',
                item: p.auxiliaryDocumentInstance._id,
                message: {
                    firstName: options.actionCustomerUser.user.firstName,
                    lastName: options.actionCustomerUser.user.lastName,
                    email: options.actionCustomerUser.user.email,
                    itemTitle: p.auxiliaryDocumentInstance.title
                }
            }))
                .save(function(err, activity) {
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        if(!!err){
            logger.error('[auxiliaryDocument stamper] ' + util.inspect(err));
        }
        logger.silly('[auxiliaryDocument stamper] auxdox stamped!');
        cb(err, models, p.auxiliaryDocumentInstance);
    });
};
