var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    loader = require('./loader'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var customerId = _.extractId(options, 'customer');

    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('Bad customer ID in training def loader by shortname.');
            }
            if (!options.shortName) {
                return cb('Bad shortname in training def loader by shortname.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for training def id ' + trainingDefinitionId);
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            cb(null, models);
        },
        function(models, cb) {
            models.definitions.training.Training.find({
                shortName: options.shortName,
                active: true,
                deleted: false
            })
                .exec(function(err, trainings) {
                    cb(err, models, {
                        trainings: trainings
                    });
                });
        },
        function(models, p, cb) {
            if (p.trainings.length == 0) {
                //no trainings, defs, etc, skip load
                cb(null, models, p);
            } else {
                //load the trainings
                loader(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    trainingDefinitions: _.pluck(p.trainings, '_id')
                }, function(err, models, trainings) {
                    p.trainings = trainings;
                    cb(err, models, p);
                });
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[training def shortname loader] problem loading def: ' + util.inspect(err));
        }
        logger.silly('[trainingdef shortname loader] finished loading training.');
        cb(err, models, (p || {})
            .trainings);
    });
};
