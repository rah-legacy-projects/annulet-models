var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    extractOptionIds = require('../../extractOptionIds'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var trainingDefinitionIds = extractOptionIds(options, {
        single: 'trainingDefinition',
        array: 'trainingDefinitions'
    });

    async.waterfall([

        function(cb) {
            if (!trainingDefinitionIds || trainingDefinitionIds.length == 0) {
                return cb('Bad training definition ID in training def loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.training.Training',
                    query: {
                        _id: trainingDefinitionIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for training def id ' + util.inspect(trainingDefinitionIds));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment().toDate(),
                endDate: moment().toDate()
            });
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[training def loader] loading definition');
            models.definitions.training.Training.find({
                _id: {
                    $in: trainingDefinitionIds
                }
            })
                .exec(function(err, trainings) {
                    cb(err, models, {
                        trainings: trainings
                    });
                });
        },
        function(models, p, cb) {
            if (!p.trainings) {
                return cb('operating procedure def could not be found for id ' + util.inspect(trainingDefinitionIds));
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //load the sections
            models.definitions.training.Training.populate(p.trainings, {
                path: 'sections',
                model: models.definitions.training.Section
            }, function(err, trainings) {
                p.trainings = trainings;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the OP's ranged data
            models.definitions.training.Training.populate(p.trainings, {
                path: 'rangedData',
                model: models.definitions.training.ranged.Training
            }, function(err, trainings) {
                p.trainings = trainings;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[training def loader] getting sections');
            //load the sections' ranged data
            models.definitions.training.Training.populate(p.trainings, {
                path: 'sections.rangedData',
                model: models.definitions.training.ranged.Section
            }, function(err, trainings) {
                p.trainings = trainings;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[training def loader] training loaded.');
            if (!!options.flatten) {
                async.parallel(_.map(p.trainings, function(training) {
                    return function(cb) {
                        logger.silly('[training def loader] training is being flattened.');
                        //objectify the training for flattening
                        training = training.toObject();
                        //flatten the sections and the OP
                        async.parallel({
                            training: function(cb) {
                                datesplice.getByDateRange(training, 'rangedData', options.startDate, options.endDate, function(err, flatTraining) {
                                    logger.silly('[trainingdef loader] start/end: ' + options.startDate + ' - ' + options.endDate);
                                    //hack: add range type
                                    flatTraining._rangeType = 'definitions.training.ranged.Training';
                                    cb(err, flatTraining);
                                });
                            },
                            sections: function(cb) {
                                async.parallel(_.map(training.sections, function(section) {
                                    return function(cb) {
                                        datesplice.getByDateRange(section, 'rangedData', options.startDate, options.endDate, function(err, flatSection) {
                                            cb(err, flatSection);
                                        });
                                    };
                                }), function(err, r) {
                                    cb(err, r);
                                });

                            }
                        }, function(err, r) {
                            training = r.training;
                            training.sections = r.sections;
                            cb(err, training);
                        });
                    }
                }), function(err, r) {
                    p.trainings = r;
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[training def loader] problem loading def: ' + util.inspect(err));
        }
        logger.silly('[training def loader] finished loading.');

        cb(err, models, (p || {})
            .trainings.length == 1 && !!options.trainingDefinition ? p.trainings[0] : (p || {})
            .trainings);
    });
};
