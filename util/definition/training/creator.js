var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    fixCustomerUser = require('../../fixCustomerUser'),
    trainingLoader = require('./trainingLoader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against,
    //  customerUser,
    //  trainingDefinition: object representing the training to create
    //}

    var customerId = _.extractId(options, 'customer'),
        customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('invalid customer for training creation: ' + options.customer);
            }
            cb();
        },
        function(cb) {
            options = fixCustomerUser(options);
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for customer ' + customerId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[training creator] making section defs');
            //for each section,
            async.parallel(_.map(options.trainingDefinition.sections, function(section) {
                return function(cb) {
                    var typePath = section.__t;
                    delete section.__t;
                    section.createdBy = customerUserId;
                    section.modifiedBy = customerUserId;
                    //for each question, create with the model required and save
                    logger.silly('[training creator] creating new def ' + typePath);
                    (new _.resolveObjectPath(models, typePath)(section))
                        .save(function(err, section) {
                            if (!!err) {
                                logger.error('problem saving ' + typePath + ': ' + util.inspect(err));
                            }
                            return cb(err, section);
                        });
                };
            }), function(err, sections) {
                cb(err, models, {
                    sections: sections
                });
            });
        },
        function(models, p, cb) {
            logger.silly('[training creator] making training def');
            var training = new models.definitions.training.Training({
                title: options.trainingDefinition.title,
                createdBy: customerUserId,
                modifiedBy: customerUserId
            });
            training.sections = _.map(p.sections, function(section) {
                return section._id;
            });
            training.customer = customerId.toObjectId();
            training.save(function(err, training) {
                p.training = training;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating training: ' + util.inspect(err));
        }
        logger.silly('[training creator] training created.');
        cb(err, models, (p || {})
            .training);
    });
};
