var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    definitionLoader = require('../loader'),
    instanceLoader = require('../../../instance/training/loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    stampClone = require('../../stampClone');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  trainingDefinition: the training def to stamp,
    //  customerUser: the customer user the instance belongs to,
    //  actionCustomerUser
    //}

    options = fixActionCustomerUser(options);
    var trainingDefinitionId = _.extractId(options, 'trainingDefinition');
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!trainingDefinitionId) {
                return cb('bad training def id when stamping: ' + options.trainingDefinition);
            }
            if (!customerUserId) {
                return cb('bad customer user id when stamping op: ' + options.customerUser);
            }

            cb();
        },
        function(cb) {
            logger.silly('[training stamper] training def id: ' + trainingDefinitionId);

            if (!models) {
                modeler.db({
                    collection: 'definitions.training.Training',
                    query: {
                        _id: trainingDefinitionId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, {});
                });
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('modelset could not be identified for stamping an training def: ' + trainingDefinitionId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //verify customer user exists
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                return cb('customer user could not be found for training stamp: ' + customerUserId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.debug('[training stamper] loading definition ' + trainingDefinitionId);
            definitionLoader(models, {
                trainingDefinition: trainingDefinitionId.toObjectId(),
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, trainingDefinition) {
                cb(err, models, {
                    trainingDefinition: trainingDefinition
                });
            });
        },
        function(models, p, cb) {
            models.instances.training.Training.findOne({
                trainingDefinition: trainingDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, trainingInstance) {
                    if (!trainingInstance) {
                        p._state = 'Created';
                        trainingInstance = new models.instances.training.Training({
                            trainingDefinition: p.trainingDefinition._id,
                            customerUser: customerUserId.toObjectId(),
                            createdBy: options.actionCustomerUser._id,
                            modifiedBy: options.actionCustomerUser._id,
                            rangedData: [],
                            sections: []
                        });
                        p.trainingInstance = trainingInstance;
                        cb(err, models, p);

                    } else {
                        p._state = 'Opened';
                        //load the training instance.
                        instanceLoader(models, {
                            trainingInstance: trainingInstance,
                            flatten: false
                        }, function(err, models, trainingInstance) {
                            p.trainingInstance = trainingInstance;
                            cb(err, models, p);
                        });
                    }

                });
        },
        function(models, p, cb) {
            logger.silly('[training stamper] instance is an instance of: ' + p.trainingInstance.trainingDefinition);
            //determine parts to splice
            var trainingInstanceRangesToSplice = _.map(p.trainingDefinition.rangedData, function(trainingDefRange) {
                var range = _.find(p.trainingInstance.rangedData, function(trainingInstanceRange) {
                    return trainingInstanceRange.trainingDefinitionRange._id.toString() == trainingDefRange._id.toString();
                });

                //if the range doesn't exist or is otherwise different, splice it, otherwise return nothing
                if (!range ||
                    moment(trainingDefRange.endDate)
                    .toString() != moment(range.endDate)
                    .toString() ||
                    moment(trainingDefRange.startDate)
                    .toString() != moment(range.startDate)
                    .toString()) {
                    //create a new range to splice
                    return new models.instances.training.ranged.Training({
                        title: trainingDefRange.title,
                        description: trainingDefRange.description,
                        completedDate: null,
                        trainingDefinitionRange: trainingDefRange._id,
                        startDate: trainingDefRange.startDate,
                        endDate: trainingDefRange.endDate,
                        rangeActive: trainingDefRange.rangeActive,
                        createdBy: options.actionCustomerUser._id
                    });
                }

                return null;
            });

            //crunch out the nulls
            trainingInstanceRangesToSplice = _.compact(trainingInstanceRangesToSplice);


            //splice the ranged data in series
            //note this has to be in series to ensure correct order of range operations
            var instanceRanges = p.trainingInstance.rangedData; //_.clone(p.trainingInstance.rangedData);
            async.series(_.map(trainingInstanceRangesToSplice, function(range) {
                return function(cb) {
                    datesplice.splice(instanceRanges, range, function(err, toSave) {
                        instanceRanges = toSave;
                        cb(err);
                    });
                };
            }), function(err) {
                logger.silly('[training stamper] splice-saving OP');
                datesplice.dateSpliceSave(options.actionCustomerUser._id, p.trainingInstance, instanceRanges, 'rangedData', function(err, savedOPInstance) {
                    //skip overwriting op, still need to save the sections
                    logger.silly('[training stamper] training spliced.');
                    cb(err, models, p);
                });
            });

        },
        function(models, p, cb) {
            async.series(_.map(p.trainingDefinition.sections, function(sectionDefinition) {
                return function(cb) {
                    var sectionInstance = _.find(p.trainingInstance.sections, function(section) {
                        return section.sectionDefinition.toString() == sectionDefinition._id.toString();
                    });

                    if (!sectionInstance) {
                        logger.silly('setting section def to ' + sectionDefinition._id);
                        sectionInstance = new(_.resolveObjectPath(models, sectionDefinition.__t.replace('definition', 'instance')))({
                            sectionDefinition: sectionDefinition._id,
                            createdBy: options.actionCustomerUser._id,
                            rangedData: []
                        });
                    }

                    var sectionInstanceRangesToSplice = _.map(sectionDefinition.rangedData, function(sectionDefRange) {
                        var range = _.find(sectionInstance.rangedData, function(sectionInstanceRange) {
                            //logger.silly("[training stamper] " + sectionInstanceRange.sectionDefinitionRange._id.toString() + ' == ' + sectionDefRange._id.toString());
                            return sectionInstanceRange.sectionDefinitionRange._id.toString() == sectionDefRange._id.toString();
                        });

                        if (!range ||
                            moment(range.endDate)
                            .toString() != moment(sectionDefRange.endDate)
                            .toString() ||
                            moment(range.startDate)
                            .toString() != moment(sectionDefRange.startDate)
                            .toString()) {
                            //create a new range to splice
                            var section = _.clone(sectionDefRange.toObject());
                            var sectionDefId = section._id;
                            delete section._id;
                            delete section.__t;
                            section.createdBy = options.actionCustomerUser._id;
                            section.modifiedBy = options.actionCustomerUser._id;
                            section.sectionDefinitionRange = sectionDefRange._id;
                            return new(_.resolveObjectPath(models, sectionDefRange.__t.replace('definition', 'instance')))(section);
                        }

                        return null;
                    });

                    //crunch out the nulls
                    sectionInstanceRangesToSplice = _.compact(sectionInstanceRangesToSplice);


                    if (sectionInstanceRangesToSplice.length == 0) {
                        //skip splicing.
                        logger.silly('[training stamper] skipping splice save, no ranges to splice');
                        cb(null, sectionInstance);
                    } else {
                        //splice the section ranged data in order
                        var instanceRanges = sectionInstance.rangedData.toObject();
                        _.each(instanceRanges, function(instanceRange) {
                            instanceRange.sectionDefinitionRange = instanceRange.sectionDefinitionRange._id;
                        });
                        async.series(_.map(sectionInstanceRangesToSplice, function(range) {
                            return function(cb) {
                                datesplice.splice(instanceRanges, range, function(err, toSave) {
                                    instanceRanges = toSave;
                                    cb(err);
                                });
                            };
                        }), function(err) {
                            logger.silly('[training stamper] splice-saving section');
                            datesplice.dateSpliceSave(options.actionCustomerUser._id, sectionInstance, instanceRanges, 'rangedData', function(err, savedSectionInstance) {
                                if (!!err) {
                                    logger.error('problem saving section: ' + util.inspect(err));
                                }
                                cb(err, savedSectionInstance);
                            });
                        });
                    }
                };
            }), function(err, r) {
                //r contains the saved section instances
                //put section instance IDs on the training instance
                p.trainingInstance.sections = _.pluck(r, '_id');
                p.trainingInstance.save(function(err, opInstance) {
                    cb(err, models, p);
                });
            });
        },
        function(models, p, cb) {
            logger.debug('[training stamper] getting training instance for def id ' + trainingDefinitionId);
            logger.silly('[training stamper] getting training isntance for customeruser: ' + customerUserId);
            models.instances.training.Training.findOne({
                trainingDefinition: trainingDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, trainingInstance) {
                    p.trainingInstance = trainingInstance;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[training stamper] loading instance');
            instanceLoader(models, {
                trainingInstance: p.trainingInstance,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, trainingInstance) {
                logger.silly('[training stamper] instance loaded.');
                p.trainingInstance = trainingInstance;
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            (new models.tracking.ItemActivity({
                changedBy: options.actionCustomerUser._id,
                state: p._state,
                itemType: 'instances.training.Training',
                item: p.trainingInstance._id,
                message: {
                    firstName: options.actionCustomerUser.user.firstName,
                    lastName: options.actionCustomerUser.user.lastName,
                    email: options.actionCustomerUser.user.email,
                    itemTitle: p.trainingInstance.title
                }
            }))
                .save(function(err, activity) {
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        logger.silly('[training stamper] training stamped!');
        cb(err, models, p.trainingInstance);
    });
};
