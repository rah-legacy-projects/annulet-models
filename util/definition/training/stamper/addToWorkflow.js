var async = require('async'),
    _ = require('lodash'),
    util = require('util'),
    logger = require('winston'),
    stamper = require('./stamper'),
    fixCustomerUser = require('../../../fixCustomerUser'),
    modeler = require('../../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) models to query against
    //options:{
    //  workflowItem: object or ID of the workflow item to add training to,
    //  customerUser
    //}
    var workflowItemId = _.extractId(options, 'workflowItem'),
        customerUserId = _.extractId(options, 'customerUser');

    logger.silly('adding training to workflow item: ' + workflowItemId);
    async.waterfall([

        function(cb) {
            if (!workflowItemId) {
                return cb('workflow item for adding training to workflow invalid: ' + options.workflowItem);
            }
            cb(null);
        },

        function(cb) {
            options = fixCustomerUser(options);
            if (!models) {
                modeler.db({
                    collection: 'instances.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for workflow item for adding training to wf: ' + workflowItemId);
            }
            cb(null, models);
        },
        function(models, cb) {
            models.instances.workflow.TrainingWorkflowItem.findOne({
                _id: workflowItemId.toObjectId()
            })
                .populate({
                    path: 'workflowItemDefinition',
                    model: models.definitions.workflow.WorkflowItem
                })
                .exec(function(err, workflowItem) {
                    cb(err, models, {
                        workflowItem: workflowItem
                    });
                });
        },
        function(models, p, cb){
            if(!p.workflowItem){
                return cb('could not find workflow item for adding training to wf: ' + workflowItemId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //the stamper will find, load, and stamp the training by training def id
            stamper(models, {
                customerUser: options.customerUser,
                trainingDefinition: p.workflowItem.workflowItemDefinition.training
            }, function(err, models, stampedTraining) {
                //add the stamped training id to the container
                p = _.defaults(p, {
                    stampedTraining: stampedTraining
                });
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            //assign the training container to the workflow item and save
            p.workflowItem.training = p.stampedTraining._id;
            p.workflowItem.modifiedBy = customerUserId;
            p.workflowItem.save(function(err, item, affected) {
                if (!!err) {
                    logger.error('[training atwf] ' + util.inspect(err));
                }
                logger.silly('[training atwf] training saved, ' + affected);
                p.workflowItem = item;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, p);
    });
};
