var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/training/loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  trainingDefinition: object representing the training to create,
    //  actionCustomerUser
    //}

    var trainingDefinitionId = _.extractId(options, 'trainingDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'definitions.training.Training',
                        query: {
                            _id: trainingDefinitionId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for OPs by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.trainingDefinition) {
                    return cb('Training definition not supplied to close draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    trainingDefinition: trainingDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //find the instances of the OP
                models.instances.training.Training.find({
                    trainingDefinition: trainingDefinitionId
                })
                    .exec(function(err, instances) {
                        if (instances.length == 0) {
                            p.instances = [];
                            return cb(null, models, p);
                        }
                        instanceLoader(models, {
                            trainingInstances: instances,
                            flatten: false
                        }, function(err, models, instances) {
                            p.instances = instances;
                            cb(err, models, p);
                        });
                    });
            },
            function(models, p, cb) {
                //close the ranges of the sections
                var sectionRanges = _.chain(p.instances)
                    .pluck('sections')
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .map(function(section) {
                        return section.rangedData;
                    })
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .value();
                async.parallel(_.map(sectionRanges, function(sectionRange) {
                    return function(cb) {
                        sectionRange.active = false;
                        sectionRange.deleted = true;
                        sectionRange.modifiedBy = options.actionCustomerUser._id;
                        sectionRange.save(function(err, sectionRange) {
                            cb(err);
                        });
                    };
                }), function(err) {
                    cb(err, models, p);
                });

            },
            function(models, p, cb) {
                //close the instance sections
                var sections = _.chain(p.instances)
                    .pluck('sections')
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .value();
                async.parallel(_.map(sections, function(section) {
                    return function(cb) {
                        section.active = false;
                        section.deleted = true;
                        section.modifiedBy = options.actionCustomerUser._id;
                        section.save(function(err, section) {
                            cb(err);
                        });
                    };
                }), function(err) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the ranges of the instances
                var instanceRanges = _.chain(p.instances)
                    .pluck('rangedData')
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .value();
                async.parallel(_.map(instanceRanges, function(instanceRange) {
                    return function(cb) {
                        instanceRange.active = false;
                        instanceRange.deleted = true;
                        instanceRange.modifiedBy = options.actionCustomerUser._id;
                        instanceRange.save(function(err, instanceRange) {
                            cb(err);
                        });
                    };
                }), function(err) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the instances
                async.parallel(_.map(p.instances, function(instance) {
                    return function(cb) {
                        instance.active = false;
                        instance.deleted = true;
                        instance.modifiedBy = options.actionCustomerUser._id;
                        instance.save(function(err, instance) {
                            cb(err);
                        });
                    };
                }), function(err) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the ranges of the sections
                async.parallel(_.chain(p.draft.sections)
                    .map(function(section) {
                        return section.rangedData;
                    })
                    .reduce(function(a, b) {
                        return a.concat(b);
                    }, [])
                    .map(function(range) {
                        return function(cb) {
                            range.active = false;
                            range.deleted = true;
                            range.modifiedBy = options.actionCustomerUser._id;
                            range.save(function(err, range) {
                                cb(err, range);
                            });
                        }
                    })
                    .value(), function(err, r) {
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //close the sections
                async.parallel(_.map(p.draft.sections, function(section) {
                    return function(cb) {
                        section.active = false;
                        section.deleted = true;
                        section.modifiedBy = options.actionCustomerUser._id;
                        section.save(function(err, section) {
                            cb(err, section);
                        });
                    }
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the ranges of the OP
                async.parallel(_.map(p.draft.rangedData, function(range) {
                    return function(cb) {
                        range.active = false;
                        range.deleted = true;
                        range.modifiedBy = options.actionCustomerUser._id;
                        range.save(function(err, range) {
                            cb(err, range);
                        });
                    }
                }), function(err, r) {
                    logger.silly('[training draft close] ranges closed.');
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //close the op
                p.draft.active = false;
                p.draft.deleted = true;
                p.draft.modifiedBy = options.actionCustomerUser._id;
                p.draft.save(function(err, draft) {
                    logger.silly('[training draft close] training closed.');
                    cb(err, models, p);
                });
            }
        ],
        function(err, models, p) {
            logger.silly('[training draft close] close complete.');
            if (!!err) {
                logger.error('problem closing draft of training: ' + util.inspect(err));
            }
            cb(err, models, p.draft);
        });
};
