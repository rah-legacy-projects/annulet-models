var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/training/loader'),
    closeDraft = require('./close');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  trainingDefinition: object representing the training to create,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        trainingDefinitionId = _.extractId(options, 'trainingDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!customerId) {
                    return cb("Bad customer ID when trying to publish draft of training.");
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for training by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.trainingDefinition) {
                    return cb('Operating procedure definition not supplied to publish draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    trainingDefinition: trainingDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if (!!p.draft.isDraftOf && p.draft.isDraftOf === 'new') {
                    p.originalDraft = new models.definitions.training.Training({
                        rangedData: [],
                        sections: [],
                        customer: customerId,
                        shortName: p.draft.shortName
                    });
                    cb(null, models, p);
                } else {
                    definitionLoader(models, {
                        trainingDefinition: p.draft.isDraftOf,
                        actionCustomerUser: options.actionCustomerUser,
                        flatten: false
                    }, function(err, models, original) {
                        p.originalDraft = original;
                        cb(err, models, p);
                    })
                }
            },
            function(models, p, cb) {
                logger.silly('[training publish] about to publish sections and ranges');
                async.parallel({
                    sections: function(cb) {
                        async.series(_.map(p.draft.sections, function(newSection) {
                            return function(cb) {
                                async.waterfall([

                                    function(cb) {
                                        cb(null, {});
                                    },
                                    function(pp, cb) {
                                        //find original section
                                        var originalSection = _.find(p.originalDraft.sections, function(originalSection) {
                                            return originalSection._id.toString() == (newSection.isDraftOf || '')
                                                .toString();
                                        });

                                        //if the original section did not exist, create one
                                        if (!originalSection) {
                                            var copy = newSection.toObject();
                                            delete copy._id;
                                            delete copy.isDraftOf;
                                            delete copy.rangedData;
                                            (new(_.resolveObjectPath(models, newSection.__t))(copy))
                                                .save(function(err, section) {
                                                    pp.section = section;
                                                    cb(err, pp);
                                                });
                                        } else {
                                            pp.section = originalSection;
                                            cb(null, pp);
                                        }
                                    },
                                    function(pp, cb) {
                                        //splice the new section ranged data into the original section's ranged data
                                        var instancesToSave = pp.section.rangedData;
                                        async.series(_.map(newSection.rangedData, function(newRange) {
                                            return function(cb) {
                                                //see if there was an existing section range that is prexactly the same, skip the splice
                                                if (_.any(pp.section.rangedData, function(originalRange) {
                                                    var comparison = _.resolveObjectPath(models, originalRange.__t)
                                                        .compare(originalRange, newRange);
                                                    return comparison;
                                                })) {
                                                    cb();
                                                } else {
                                                    var copy = _.clone(newRange.toObject());
                                                    delete copy._id;
                                                    delete copy.isDraftOf;
                                                    var range = new(_.resolveObjectPath(models, copy.__t))(copy);
                                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                                        instancesToSave = partsToSave;
                                                        cb(err);
                                                    });
                                                }
                                            }
                                        }), function(err, r) {
                                            datesplice.dateSpliceSave(options.actionCustomerUser._id, pp.section, instancesToSave, 'rangedData', function(err, savedSection) {
                                                pp.section = savedSection;
                                                cb(err, pp);
                                            });
                                        });
                                    }
                                ], function(err, pp) {
                                    //bubble the section up
                                    cb(err, pp.section);
                                });
                            };
                        }), function(err, sections) {
                            cb(err, sections);
                        });
                    },
                    ranges: function(cb) {
                        var instancesToSave = p.originalDraft.rangedData;
                        async.series(_.map(p.draft.rangedData, function(rangeData) {
                            return function(cb) {

                                if (_.any(p.originalDraft.rangedData, function(originalRange) {
                                    return models.definitions.training.ranged.Training.compare(originalRange, rangeData);
                                })) {
                                    cb();
                                } else {

                                    var copy = _.clone(rangeData.toObject());
                                    delete copy._id;
                                    var range = new models.definitions.training.ranged.Training(copy);
                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                        instancesToSave = partsToSave;
                                        cb(err, partsToSave);
                                    });
                                }
                            };
                        }), function(err, r) {
                            cb(err, instancesToSave);
                        });
                    }

                }, function(err, r) {
                    logger.silly('[training publish] original prepared. Saving...');
                    p.originalDraft.sections = _.pluck(r.sections, '_id');
                    datesplice.dateSpliceSave(options.actionCustomerUser._id, p.originalDraft, r.ranges, 'rangedData', function(err, savedTraining) {
                        p.training = savedTraining;
                        cb(err, models, p);
                    });
                });
            },
            function(models, p, cb) {
                //reload the training def as saved
                definitionLoader(models, {
                    trainingDefinition: p.training._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, training) {
                    //make the training returned a regular object
                    p.training = training.toObject();
                    cb(err, models, p);
                });
            },
            //{{{ alter user data
            function(models, p, cb) {
                //find all instances of the operating procedure
                models.instances.training.Training.find({
                    trainingDefinition: p.training._id,
                    active: true,
                    deleted: false
                })
                    .exec(function(err, instances) {
                        p.trainingInstances = instances;
                        cb(err, models, p);
                    });

            },
            function(models, p, cb) {
                //load the sections for the instances
                models.instances.training.Training.populate(p.trainingInstances, {
                    path: 'sections',
                    model: models.instances.training.Section
                }, function(err, trainingInstances) {
                    p.trainingInstances = trainingInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //load the OP's ranged data for the instances
                models.instances.training.Training.populate(p.trainingInstances, {
                    path: 'rangedData',
                    model: models.instances.training.ranged.Training
                }, function(err, trainingInstances) {
                    p.trainingInstances = trainingInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //load the sections' ranged data for the instances
                models.instances.training.Training.populate(p.trainingInstances, {
                    path: 'sections.rangedData',
                    //model: models.instances.training.ranged.Section
                }, function(err, trainingInstances) {
                    p.trainingInstances = trainingInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for all of the loaded instances,
                async.parallel(_.map(p.trainingInstances, function(trainingInstance) {
                    return function(cb) {
                        //map the definition ranges on the existing data
                        async.parallel({
                            trainingRanges: function(cb) {
                                async.parallel(_.map(p.training.rangedData, function(trainingDefRange) {
                                    return function(cb) {
                                        var instanceRange = _.find(trainingInstance.rangedData, function(trainingInstanceRange) {
                                            return trainingInstanceRange.trainingDefinitionRange.toString() == trainingDefRange._id.toString()
                                        });

                                        if (!instanceRange) {
                                            return cb();
                                        }

                                        instanceRange.startDate = trainingDefRange.startDate;
                                        instanceRange.endDate = trainingDefRange.endDate;
                                        instanceRange.rangeActive = trainingDefRange.rangeActive;
                                        instanceRange.modifiedBy = options.actionCustomerUser._id;

                                        instanceRange.save(function(err, instanceRange) {
                                            cb(err, instanceRange);
                                        });
                                    }
                                }), cb);
                            },
                            sectionRanges: function(cb) {
                                async.parallel(_.map(p.training.sections, function(trainingDefSection) {
                                    return function(cb) {
                                        var instanceSection = _.find(trainingInstance.sections, function(trainingInstanceSection) {
                                            return trainingInstanceSection.sectionDefinition.toString() == trainingDefSection._id.toString();
                                        });

                                        if (!instanceSection) {
                                            return cb();
                                        }

                                        async.parallel(_.map(trainingDefSection.rangedData, function(sectionDefRange) {
                                            return function(cb) {
                                                var instanceRange = _.find(instanceSection.rangedData, function(range) {
                                                    return range.sectionDefinitionRange.toString() == sectionDefRange._id.toString();
                                                });

                                                if (!instanceRange) {
                                                    return cb();
                                                }

                                                instanceRange.startDate = sectionDefRange.startDate;
                                                instanceRange.endDate = sectionDefRange.endDate;
                                                instanceRange.rangeActive = sectionDefRange.rangeActive;
                                                instanceRange.modifiedBy = options.actionCustomerUser._id;

                                                instanceRange.save(function(err, instanceRange) {
                                                    cb(err, instanceRange);
                                                });
                                            };
                                        }), cb);
                                    };

                                }), cb);
                            }
                        }, cb);


                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            //}}}
            function(models, p, cb) {
                //close the draft that was published
                closeDraft(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    trainingDefinition: p.draft
                }, function(err, models, closed) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //reload the training def as saved
                definitionLoader(models, {
                    trainingDefinition: p.training._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, training) {
                    //make the training returned a regular object
                    training = training.toObject();
                    cb(err, models, p);
                });
            },
        ],
        function(err, models, p) {
            if (!!err) {
                logger.error('problem publishing draft of training: ' + util.inspect(err));
            }
            cb(err, models, p.training);
        });
};
