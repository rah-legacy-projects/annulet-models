var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    trainingLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  trainingDefinition: object representing the training to create,
    //  actionCustomerUser: the customer user with user for audit purposes
    //}

    var customerId = _.extractId(options, 'customer'),
        trainingDefinitionId = _.extractId(options, 'trainingDefinition');
        options = fixActionCustomerUser(options);

    logger.silly('[training create draft] training def id: ' + trainingDefinitionId);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to create training.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.trainingDefinition) {
                return cb('Training definition not supplied to create draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!!trainingDefinitionId && trainingDefinitionId !== 'new') {
                logger.silly('[training create draft] getting training definition: ' + trainingDefinitionId);
                //training exists, go get it
                trainingLoader(models, {
                    trainingDefinition: options.trainingDefinition,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, training) {
                    //tranining should be loaded but not flattened
                    p.training = training.toObject();
                    cb(err, models, p);
                });
            } else {
                logger.silly('[training create draft] training def new, making stub');
                //training does not exist, make one to stub off of
                p.training = {}; //new models.definitions.training.Training();
                p.training.rangedData = [];
                p.training.sections = [];
                p.training.customer = customerId;
                p.createdBy = options.actionCustomerUser._id;
                p.training._id = 'new';
                p.training.shortName = 'new';
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            logger.silly('[training create draft] copying ' + p.training.sections.length + ' sections');
            //make a copy of the sections
            async.parallel(_.map(p.training.sections, function(section) {
                return function(cb) {
                    async.waterfall([

                        function(cb) {
                            //make a copy of the currentmost date range
                            var s = datesplice.getByDateRangeSync(_.clone(section), 'rangedData', moment(), moment());
                            var srange = null;
                            if (!!s) {
                                srange = _.clone(_.find(section.rangedData, function(r) {
                                    return r._id == s._rangeId;
                                }));
                                delete srange._id;
                            } else {
                                srange = new(_.resolveObjectPath(models, s._rangeType))();
                            }

                            (new(_.resolveObjectPath(models, s._rangeType))(srange))
                                .save(function(err, sectionRange) {
                                    cb(err, {
                                        sectionRange: sectionRange
                                    });
                                });
                        },
                        function(pp, cb) {
                            //make a copy of the section with the ranged section inside
                            var sectionopts = _.clone(section);
                            delete sectionopts.rangedData;
                            delete sectionopts._id;
                            sectionopts.rangedData = [pp.sectionRange._id];
                            sectionopts.isDraftOf = section._id;
                            sectionopts.createdBy = options.actionCustomerUser._id;
                            (new(_.resolveObjectPath(models, section.__t))(sectionopts))
                                .save(function(err, section) {
                                    pp.section = section;
                                    cb(err, pp);
                                });
                        }
                    ], function(err, pp) {
                        //cb the section
                        cb(err, pp.section);
                    });
                };
            }), function(err, draftSections) {
                p.draftSections = draftSections;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[training create draft] making copy of training range');
            //make a copy of the training range
            var f = datesplice.getByDateRangeSync(_.clone(p.training), 'rangedData', moment(), moment());
            var trainingRange = null;
            if (!!f) {
                trainingRange = _.find(p.training.rangedData, function(r) {
                    return r._id == f._rangeId;
                });
            } else {
                trainingRange = new models.definitions.training.ranged.Training();
            }

            (new models.definitions.training.ranged.Training({
                title: trainingRange.title,
                description: trainingRange.description,
                createdBy: options.actionCustomerUser._id,
                startDate: trainingRange.startDate,
                endDate: trainingRange.endDate
            }))
                .save(function(err, trainingRange) {
                    p.trainingRange = trainingRange;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[training create draft] saving draft copy');
            logger.silly('[training create draft] drafted id: ' + p.training._id);
            //save a draft copy of the op
            (new models.definitions.training.Training({
                customer: customerId,
                isDraftOf: p.training._id,
                sections: _.pluck(p.draftSections, '_id'),
                rangedData: [p.trainingRange._id],
                createdBy: options.actionCustomerUser._id,
                shortName: p.training.shortName
            }))
                .save(function(err, training) {
                    p.draft = training;
                    logger.silly('[training create draft] draft is draft of ' + p.draft.isDraftOf);
                    cb(err, models, p);
                });

        },
        function(models, p, cb) {
            logger.silly('[training create draft] reloading draft, unflattened');
            //reload the draft training unflattened
            trainingLoader(models, {
                trainingDefinition: p.draft._id,
                flatten: false,
                actionCustomerUser: options.actionCustomerUser
            }, function(err, models, draft) {
                p.draft = draft;
                logger.silly('[training create draft] draft is draft of ' + p.draft.isDraftOf);
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating draft of training: ' + util.inspect(err));
        }
        cb(err, models, p.draft);
    });
};
