var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    moment = require('moment'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    trainingLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  trainingDefinition: flattened object of the draft OP,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        trainingDefinitionId = _.extractId(options, 'trainingDefinition');

    options = options || {};
    options = _.defaults(options, {
        flatten: false
    });

    options = fixActionCustomerUser(options);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to update training draft.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for OPs by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.trainingDefinition) {
                return cb('training definition not supplied to update draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.trainingDefinition.startDate || !options.trainingDefinition.endDate) {
                return cb('training definition must supply start/end dates');
            }

            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[training update draft] loading training');
            trainingLoader(models, {
                trainingDefinition: trainingDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, training) {
                if (!training) {
                    return cb('training could not be found: ' + trainingDefinitionId);
                }
                p.training = training;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[training update draft] updating training range');
            var trainingRangeToUpdate = _.find(p.training.rangedData, function(range) {
                return range._id.toString() == (options.trainingDefinition._rangeId || 'new')
                    .toString();
            });

            if (!trainingRangeToUpdate) {
                //training range should be created by making a draft
                return cb('Invalid training range id ' + options.trainingDefinition._rangeId);
            }

            //update training range
            trainingRangeToUpdate.title = options.trainingDefinition.title;
            trainingRangeToUpdate.description = options.trainingDefinition.description;
            trainingRangeToUpdate.startDate = options.trainingDefinition.startDate;
            trainingRangeToUpdate.endDate = options.trainingDefinition.endDate;
            trainingRangeToUpdate.modifiedBy = options.actionCustomerUser._id;

            trainingRangeToUpdate.save(function(err, trainingRangeToUpdate) {
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[training update draft] updating training sections');
            async.parallel(_.map(options.trainingDefinition.sections, function(section) {
                return function(cb) {
                    var defSection = _.find(p.training.sections, function(ds) {
                        //nb, new sections - here, 'section' - will not have IDs
                        return ds._id.toString() == (section._id || '')
                            .toString();
                    });

                    if (!defSection) {
                        //new section!
                        logger.silly('[training update draft] section did not exist, creating');
                        defSection = new(_.resolveObjectPath(models, section.__t))(section);
                        defSection.createdBy = options.actionCustomerUser._id;
                    } else {
                        //todo: ... do nothing when saving section?
                        logger.silly('[training update draft] section existed.');
                    }

                    var defSectionRange = _.find(defSection.rangedData, function(range) {
                        return range._id.toString() == section._rangeId.toString();
                    });

                    var trimmedRange = _.clone(section);
                    delete trimmedRange.__t;
                    delete trimmedRange._id;
                    if (!trimmedRange.startDate) {
                        logger.warn('[training update draft] range start date missing, defaulting to training range start date');
                        trimmedRange.startDate = options.trainingDefinition.startDate == 'forever' ? 'forever' : moment(options.trainingDefinition.startDate)
                            .toDate();
                    }
                    if (!trimmedRange.endDate) {
                        logger.warn('[training update draft] range end date missing, defaulting to training range end date');
                        trimmedRange.endDate = options.trainingDefinition.endDate == 'forever' ? 'forever' : moment(options.trainingDefinition.endDate)
                            .toDate();
                    }
                    if (!defSectionRange) {
                        logger.silly('[training update draft] section range did not exist, creating');
                        //logger.silly('[training update draft] section type: ' + section._rangeType);
                        //new range
                        defSectionRange = new(_.resolveObjectPath(models, section._rangeType))(trimmedRange);
                        defSectionRange.createdBy = options.actionCustomerUser._id;
                    } else {
                        defSectionRange.modifiedBy = options.actionCustomerUser._id;
                        //defSectionRange = _.merge(defSectionRange, trimmedRange);
                        defSectionRange.merge(trimmedRange);
                    }

                    async.waterfall([

                        function(cb) {
                            //save the def section range
                            defSectionRange.save(function(err, defSectionRange) {
                                if (!!err) {
                                    logger.error('[training update draft] problem saving section range: ' + util.inspect(err));
                                }
                                cb(err, defSectionRange);
                            });
                        },
                        function(defSectionRange, cb) {
                            //save the def section
                            //drafts only have one range
                            //todo: consider splicing this instead?
                            defSection.rangedData = defSection.rangedData || [];
                            defSection.rangedData[0] = defSectionRange._id;
                            defSection.modifiedBy = options.actionCustomerUser._id;
                            defSection.save(function(err, defSectionSaved) {
                                if (!!err) {
                                    logger.error('[training update draft] problem saving section: ' + util.inspect(err));
                                }
                                cb(err, defSection);
                            });
                        },
                    ], function(err, defSection) {

                        cb(err, defSection._id);
                    });
                };

            }), function(err, defSectionIds) {
                p.defSectionIds = defSectionIds;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[training update draft] updating training section IDs');
            //update the training's section IDs
            p.training.sections = p.defSectionIds;
            if (!!options.trainingDefinition.shortName) {
                p.training.shortName = options.trainingDefinition.shortName;
            }
            p.training.modifiedBy = options.actionCustomerUser._id;
            p.training.save(function(err, training) {
                if (!!err) {
                    logger.error('problem saving training: ' + util.inspect(err))
                }
                p.training = training;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[training update draft] reloading training (flat)');
            trainingLoader(models, {
                trainingDefinition: trainingDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, training) {
                if (!training) {
                    return cb('training could not be found: ' + trainingDefinitionId);
                }
                p.training = training;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem updating draft of training: ' + util.inspect(err));
        }

        logger.silly('[training update draft] loaded.');
        cb(err, models, (p || {})
            .training);
    });
};
