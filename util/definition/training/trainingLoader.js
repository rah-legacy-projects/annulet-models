var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    var trainingDefinitionId = _.extractId(options, 'trainingDefinition');
    logger.silly('[training def loader] td id: ' + trainingDefinitionId);
    async.waterfall([

        function(cb) {
            if (!trainingDefinitionId) {
                return cb('invalid training definition id passed to training def loader: ' + options.traniningDefinition);
            }
            cb(null);
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.training.Training',
                    query: {
                        _id: trainingDefinitionId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for loading training def ' + trainingDefinitionId);
            }
            cb(null, models);
        },
        function(models, cb) {
            logger.silly('[training def loader] loading definition');
            models.definitions.training.Training.findOne(trainingDefinitionId.toObjectId())
                .populate({
                    path: 'sections',
                    model: models.definitions.training.Section
                })
                .lean()
                .exec(function(err, training) {
                    cb(err, models, {
                        training: training
                    });
                });
        },
    ], function(err, models, p) {
        cb(err, models, (p || {})
            .training);
    });
};
