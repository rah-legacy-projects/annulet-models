var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    alertLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  alertDefinition: object representing the alert to create,
    //  actionCustomerUser: the customer user with user for audit purposes
    //}

    var customerId = _.extractId(options, 'customer'),
        alertDefinitionId = _.extractId(options, 'alertDefinition');
        options = fixActionCustomerUser(options);

    logger.silly('[alert create draft] alertdef id: ' + alertDefinitionId);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to create Alert.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for Alerts by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.alertDefinition) {
                return cb('Operating procedure definition not supplied to create draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!!alertDefinitionId && alertDefinitionId !== 'new') {
                logger.silly('[alert create draft] getting alert definition: ' + alertDefinitionId);
                //alert exists, go get it
                alertLoader(models, {
                    alertDefinition: options.alertDefinition,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, alert) {
                    //operating procedure should be loaded but not flattened
                    p.alert = alert.toObject();
                    cb(err, models, p);
                });
            } else {
                logger.silly('[alert create draft] alert def new, making stub');
                //alert does not exist, make one to stub off of
                p.alert = {}; //new models.definitions.alert.Alert();
                p.alert.rangedData = [{
                    alertLevel: 'info',
                    markdown: 'new',
                    startDate: moment().subtract(1, 'minute').toDate(),
                    endDate: moment().add(7, 'days').toDate(),
                    rangeActive: true
                }];
                p.alert.customer = customerId;
                p.createdBy = options.actionCustomerUser._id;
                p.alert._id = 'new';
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            logger.silly('[alert create draft] making copy of alert range');
            //make a copy of the alert range
            var f = datesplice.getByDateRangeSync(_.clone(p.alert), 'rangedData', moment(), moment());
            var alertRange = null;
            if (!!f) {
                alertRange = _.find(p.alert.rangedData, function(r) {
                    return r._id == f._rangeId;
                });
            } else {
                alertRange = new models.definitions.alert.ranged.Alert();
            }

            (new models.definitions.alert.ranged.Alert({
                alertLevel: alertRange.alertLevel,
                markdown: alertRange.markdown,
                createdBy: options.actionCustomerUser._id,
                startDate: alertRange.startDate,
                endDate: alertRange.endDate
            }))
                .save(function(err, alertRange) {
                    p.alertRange = alertRange;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[alert create draft] saving draft copy');
            //save a draft copy of the op
            (new models.definitions.alert.Alert({
                customer: customerId,
                isDraftOf: p.alert._id,
                rangedData: [p.alertRange._id],
                createdBy: options.actionCustomerUser._id
            }))
                .save(function(err, alert) {
                    p.draft = alert;
                    logger.silly('[alert create draft] draft is draft of ' + p.draft.isDraftOf);
                    cb(err, models, p);
                });

        },
        function(models, p, cb) {
            logger.silly('[alert create draft] reloading draft, unflattened');
            //reload the draft alert unflattened
            alertLoader(models, {
                alertDefinition: p.draft._id,
                flatten: false,
                actionCustomerUser: options.actionCustomerUser
            }, function(err, models, draft) {
                p.draft = draft;
                logger.silly('[alert create draft] draft is draft of ' + p.draft.isDraftOf);
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating draft of alert: ' + util.inspect(err));
        }
        cb(err, models, p.draft);
    });
};
