var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/alert/loader'),
    closeDraft = require('./close');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  alertDefinition: object representing the alert to create,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        alertDefinitionId = _.extractId(options, 'alertDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!customerId) {
                    return cb("Bad customer ID when trying to publish draft of Alert.");
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for Alerts by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.alertDefinition) {
                    return cb('Alert definition not supplied to publish draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    alertDefinition: alertDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                if (!!p.draft.isDraftOf && p.draft.isDraftOf === 'new') {
                    p.originalDraft = new models.definitions.alert.Alert({
                        rangedData: [],
                        sections: [],
                        customer: customerId
                    });
                    cb(null, models, p);
                } else {
                    definitionLoader(models, {
                        alertDefinition: p.draft.isDraftOf,
                        actionCustomerUser: options.actionCustomerUser,
                        flatten: false
                    }, function(err, models, original) {
                        p.originalDraft = original;
                        cb(err, models, p);
                    })
                }
            },
            function(models, p, cb) {
                logger.silly('[alert publish] about to publish ranges');
                async.parallel({
                    ranges: function(cb) {
                        var instancesToSave = p.originalDraft.rangedData;
                        async.series(_.map(p.draft.rangedData, function(rangeData) {
                            return function(cb) {

                                if (_.any(p.originalDraft.rangedData, function(originalRange) {
                                    return models.definitions.alert.ranged.Alert.compare(originalRange, rangeData);
                                })) {
                                    cb();
                                } else {

                                    var copy = _.clone(rangeData.toObject());
                                    delete copy._id;
                                    var range = new models.definitions.alert.ranged.Alert(copy);
                                    datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                        instancesToSave = partsToSave;
                                        cb(err, partsToSave);
                                    });
                                }
                            };
                        }), function(err, r) {
                            cb(err, instancesToSave);
                        });
                    }

                }, function(err, r) {
                    logger.silly('[alert publish] original prepared. Saving...');
                    datesplice.dateSpliceSave(options.actionCustomerUser._id, p.originalDraft, r.ranges, 'rangedData', function(err, savedAlert) {
                        p.alert = savedAlert;
                        cb(err, models, p);
                    });
                });
            },
            function(models, p, cb) {
                //reload the Alert def as saved
                definitionLoader(models, {
                    alertDefinition: p.alert._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, alert) {
                    //make the Alert returned a regular object
                    p.alert = alert.toObject();
                    cb(err, models, p);
                });
            },
            //{{{ alter user data
            function(models, p, cb) {
                //find all instances of the operating procedure
                models.instances.alert.Alert.find({
                    alertDefinition: p.alert._id,
                    active: true,
                    deleted: false
                })
                    .exec(function(err, instances) {
                        p.alertInstances = instances;
                        cb(err, models, p);
                    });

            },
            function(models, p, cb) {
                //load the Alert's ranged data for the instances
                models.instances.alert.Alert.populate(p.alertInstances, {
                    path: 'rangedData',
                    model: models.instances.alert.ranged.Alert
                }, function(err, alertInstances) {
                    p.alertInstances = alertInstances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for all of the loaded instances,
                async.parallel(_.map(p.alertInstances, function(alertInstance) {
                    return function(cb) {
                        //map the definition ranges on the existing data
                        async.parallel({
                            alertRanges: function(cb) {
                                async.parallel(_.map(p.alert.rangedData, function(alertdefRange) {
                                    return function(cb) {
                                        var instanceRange = _.find(alertInstance.rangedData, function(opInstanceRange) {
                                            return opInstanceRange.alertDefinitionRange.toString() == alertdefRange._id.toString()
                                        });

                                        if (!instanceRange) {
                                            return cb();
                                        }

                                        instanceRange.startDate = alertdefRange.startDate;
                                        instanceRange.endDate = alertdefRange.endDate;
                                        instanceRange.rangeActive = alertdefRange.rangeActive;
                                        instanceRange.modifiedBy = options.actionCustomerUser._id;

                                        instanceRange.save(function(err, instanceRange) {
                                            cb(err, instanceRange);
                                        });
                                    }
                                }), cb);
                            },

                        }, cb);


                    };
                }), function(err, r) {
                    cb(err, models, p);
                });
            },
            //}}}
            function(models, p, cb) {
                //close the draft that was published
                closeDraft(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    alertDefinition: p.draft
                }, function(err, models, closed) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //reload the Alert def as saved
                definitionLoader(models, {
                    alertDefinition: p.alert._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, alert) {
                    //make the Alert returned a regular object
                    alert = alert.toObject();
                    logger.silly('[alert publish] ' + JSON.stringify(alert, null, 2));
                    cb(err, models, p);
                });
            },
        ],
        function(err, models, p) {
            if (!!err) {
                logger.error('problem publishing draft of alert: ' + util.inspect(err));
            }
            logger.silly('[alert publish] done publishing.');
            cb(err, models, p.alert);
        });
};
