var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    moment = require('moment'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    alertLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  alertDefinition: flattened object of the draft Alert,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        alertDefinitionId = _.extractId(options, 'alertDefinition');

    options = options || {};
    options = _.defaults(options, {
        flatten: false
    });

    options = fixActionCustomerUser(options);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to update alert draft.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for Alerts by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.alertDefinition) {
                return cb('alert definition not supplied to update draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.alertDefinition.startDate || !options.alertDefinition.endDate) {
                return cb('alert definition must supply start/end dates');
            }

            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[alert update draft] loading op');
            alertLoader(models, {
                alertDefinition: alertDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, alert) {
                if (!alert) {
                    return cb('alert could not be found: ' + alertDefinitionId);
                }
                p.alert = alert;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[alert update draft] updating alert range');
            var alertRangeToUpdate = _.find(p.alert.rangedData, function(range) {
                return range._id.toString() == (options.alertDefinition._rangeId || 'new')
                    .toString();
            });

            if (!alertRangeToUpdate) {
                //alert range should be created by making a draft
                return cb('Invalid alert range id ' + options.alertDefinition._rangeId);
            }

            logger.silly('[alert update draft] update pre changes: ' + util.inspect(alertRangeToUpdate));

            //update alert range
            alertRangeToUpdate.alertLevel = options.alertDefinition.alertLevel;
            alertRangeToUpdate.markdown = options.alertDefinition.markdown;
            alertRangeToUpdate.startDate = options.alertDefinition.startDate;
            alertRangeToUpdate.endDate = options.alertDefinition.endDate;
            alertRangeToUpdate.modifiedBy = options.actionCustomerUser._id;

            logger.silly('[alert update draft] update before: ' + util.inspect(alertRangeToUpdate));
            alertRangeToUpdate.save(function(err, alertRangeToUpdate) {
                logger.silly('[alert update draft] update after: ' + util.inspect(alertRangeToUpdate));
                p.alertRange = alertRangeToUpdate;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[alert update draft] updating alert section IDs');
            p.alert.modifiedBy = options.actionCustomerUser._id;
            p.alert.save(function(err, alert) {
                p.alert = alert;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[alert update draft] reloading alert (flat)');
            alertLoader(models, {
                alertDefinition: alertDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, alert) {
                if (!alert) {
                    return cb('alert could not be found: ' + alertDefinitionId);
                }
                logger.silly('[alert update draft] reloaded: ' + util.inspect(alert));
                p.alert = alert;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem updating draft of alert: ' + util.inspect(err));
        }

        cb(err, models, (p || {})
            .alert);
    });
};
