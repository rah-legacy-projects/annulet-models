module.exports = exports = {
	"draft": require("./draft"),
	"loader": require("./loader"),
	"stamper": require("./stamper"),
	"tupleLoader": require("./tupleLoader"),
};
