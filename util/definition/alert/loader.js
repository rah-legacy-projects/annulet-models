var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var alertDefinitionIds = extractOptionIds(options, {
        single: 'alertDefinition',
        array: 'alertDefinitions'
    });

    async.waterfall([

            function(cb) {
                if (!alertDefinitionIds || alertDefinitionIds.length == 0) {
                    return cb('Bad alert definition ID in alert loader.');
                }
                cb();
            },
            function(cb) {
                if (!models) {
                    modeler.db({
                        collection: 'definitions.alert.Alert',
                        query: {
                            _id: alertDefinitionIds[0].toObjectId()
                        }
                    }, cb);
                } else {
                    cb(null, models);
                }
            },
            function(models, cb) {
                if (!models) {
                    return cb('Modelset could not be identified for alert id ' + util.inspect(alertDefinitionIds));
                }
                cb(null, models);
            },
            function(models, cb) {
                //set up default options
                options = fixActionCustomerUser(options);
                options = _.defaults(options, {
                    flatten: true,
                    startDate: moment(),
                    endDate: moment()
                });
                cb(null, models);
            },
            function(models, cb) {
                logger.silly('[alert def loader] loading definition');
                models.definitions.alert.Alert.find({
                    _id: {
                        $in: alertDefinitionIds
                    }
                })
                    .exec(function(err, alerts) {
                        cb(err, models, {
                            alerts: alerts
                        });
                    });
            },
            function(models, p, cb) {
                if (!p.alerts) {
                    return cb('alert def could not be found for id ' + util.inspect(alertDefinitionIds));
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                //load the alert's ranged data
                models.definitions.alert.Alert.populate(p.alerts, {
                    path: 'rangedData',
                    model: models.definitions.alert.ranged.Alert
                }, function(err, alerts) {
                    p.alerts = alerts;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[alert loader] alert loaded.');
                if (!!options.flatten) {
                    async.parallel(_.map(p.alerts, function(alert) {
                        return function(cb) {
                            logger.silly('[alert loader] alert is being flattened.');
                            //objectify the OP for flattening
                            alert = alert.toObject();
                            //flatten the sections and the OP
                            async.parallel({
                                alert: function(cb) {
                                    datesplice.getByDateRange(alert, 'rangedData', options.startDate, options.endDate, function(err, flatAlert) {
                                        //hack: add range type
                                        flatAlert._rangeType = 'definitions.alert.ranged.Alert';
                                        cb(err, flatAlert);
                                    });
                                },
                            }, function(err, r) {
                                alert = r.alert;
                                cb(err, alert);
                            });
                        };
                    }), function(err, r) {
                        p.alerts = r;
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
        ],
        function(err, models, p) {
            if (!!err) {
                logger.error('[alert loader] problem loading def: ' + util.inspect(err));
            }
            logger.silly('[alert loader] finished loading.');

            cb(err, models, (p || {})
                .alerts.length == 1 && !!options.alertDefinition ? p.alerts[0] : (p || {})
                .alerts);
        });
};
