var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    definitionLoader = require('../loader'),
    instanceLoader = require('../../../instance/alert/loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    stampClone = require('../../stampClone');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  alertDefinition: the alert def to stamp,
    //  customerUser: the customer user the instance belongs to,
    //  actionCustomerUser,
    //  flatten
    //}

    options = fixActionCustomerUser(options);
    var alertDefinitionId = _.extractId(options, 'alertDefinition');
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!alertDefinitionId) {
                return cb('bad alertdef id when stamping: ' + options.alertDefinition);
            }
            if (!customerUserId) {
                return cb('bad customer user id when stamping op: ' + options.customerUser);
            }

            cb();
        },
        function(cb) {
            logger.silly('[alert stamper] alertdef id: ' + alertDefinitionId);

            if (!models) {
                modeler.db({
                    collection: 'definitions.alert.Alert',
                    query: {
                        _id: alertDefinitionId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, {});
                });
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('modelset could not be identified for stamping an alertdef: ' + alertDefinitionId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //verify customer user exists
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                return cb('customer user could not be found for Alert stamp: ' + customerUserId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.debug('[alert stamper] loading definition ' + alertDefinitionId);
            definitionLoader(models, {
                alertDefinition: alertDefinitionId.toObjectId(),
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, alertDefinition) {
                cb(err, models, {
                    alertDefinition: alertDefinition
                });
            });
        },
        function(models, p, cb) {
            models.instances.alert.alertTypes.Alert.findOne({
                alertDefinition: alertDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, alertInstance) {
                    if (!alertInstance) {
                        p._state = 'Created';
                        alertInstance = new models.instances.alert.alertTypes.Alert({
                            alertDefinition: p.alertDefinition._id,
                            customerUser: customerUserId.toObjectId(),
                            createdBy: options.actionCustomerUser._id,
                            modifiedBy: options.actionCustomerUser._id,
                            rangedData: [],
                            sections: []
                        });
                        p.alertInstance = alertInstance;
                        cb(err, models, p);

                    } else {
                        p._state = 'Opened';
                        //load the Alert instance.
                        instanceLoader(models, {
                            alertInstance: alertInstance,
                            flatten: false
                        }, function(err, models, alertInstance) {
                            p.alertInstance = alertInstance;
                            cb(err, models, p);
                        });
                    }

                });
        },
        function(models, p, cb) {
            //determine parts to splice
            var alertInstanceRangesToSplice = _.map(p.alertDefinition.rangedData, function(alertRange) {
                var range = _.find(p.alertInstance.rangedData, function(alertInstanceRange) {
                    return alertInstanceRange.alertDefinitionRange._id.toString() == alertRange._id.toString();
                });

                //if the range doesn't exist or is otherwise different, splice it, otherwise return nothing
                if (!range ||
                    moment(range.endDate)
                    .toString() != moment(alertRange.endDate)
                    .toString() ||
                    moment(range.startDate)
                    .toString() != moment(alertRange.startDate)
                    .toString()) {
                    //create a new range to splice
                    return new models.instances.alert.ranged.alertTypes.Alert({
                        markdown: alertRange.markdown,
                        alertLevel: alertRange.alertLevel,
                        completedDate: null,
                        alertDefinitionRange: alertRange._id,
                        startDate: alertRange.startDate,
                        endDate: alertRange.endDate,
                        rangeActive: alertRange.rangeActive,
                        createdBy: options.actionCustomerUser._id
                    });
                }

                return null;
            });

            //crunch out the nulls
            alertInstanceRangesToSplice = _.compact(alertInstanceRangesToSplice);


            //splice the ranged data in series
            //note this has to be in series to ensure correct order of range operations
            var instanceRanges = p.alertInstance.rangedData; //_.clone(p.alertInstance.rangedData);
            async.series(_.map(alertInstanceRangesToSplice, function(range) {
                return function(cb) {
                    datesplice.splice(instanceRanges, range, function(err, toSave) {
                        instanceRanges = toSave;
                        cb(err);
                    });
                };
            }), function(err) {
                logger.silly('[alertstamper] splice-saving Alert');
                datesplice.dateSpliceSave(options.actionCustomerUser._id, p.alertInstance, instanceRanges, 'rangedData', function(err, savedAlertInstance) {
                    //skip overwriting op, still need to save the sections
                    logger.silly('[alertstamper] Alert spliced.');
                    cb(err, models, p);
                });
            });

        },
        function(models, p, cb) {
            logger.debug('[alert stamper] getting Alert instance for def id ' + alertDefinitionId);
            models.instances.alert.alertTypes.Alert.findOne({
                alertDefinition: alertDefinitionId.toObjectId(),
            })
                .exec(function(err, alertInstance) {
                    p.alertInstance = alertInstance;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[alert stamper] loading instance');
            logger.silly('[alert stamper] flatten: ' + options.flatten);
            instanceLoader(models, {
                alertInstance: p.alertInstance,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, alertInstance) {
                logger.silly('[alert stamper] instance loaded.');
                logger.silly('[alertstamper] loaded instance: ' + JSON.stringify(alertInstance, null, 2))
                p.alertInstance = alertInstance;
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            (new models.tracking.ItemActivity({
                changedBy: options.actionCustomerUser._id,
                state: p._state,
                itemType: 'instances.alertTypes.Alert',
                item: p.alertInstance._id,
                message: {
                    firstName: options.actionCustomerUser.user.firstName,
                    lastName: options.actionCustomerUser.user.lastName,
                    email: options.actionCustomerUser.user.email,
                    itemTitle: p.alertInstance.title
                }
            }))
                .save(function(err, activity) {
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        logger.silly('[alert stamper] alertstamped!');
        cb(err, models, p.alertInstance);
    });
};
