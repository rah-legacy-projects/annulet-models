var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    datesplice = require('mongoose-date-splice'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    alertDefinitionLoader = require('../loader'),
    alertInstanceLoader = require('../../../instance/alert/loader'),
    moment = require('moment'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    stamp = require('./stamper');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  customerUser: id or object of the customer user,
    //  actionCustomerUser,
    //  includeDismissed,
    //  flatten
    //
    //}

    options = options || {};
    options = _.defaults(options, {
        includeDismissed: true,
        flatten: false
    });

    options = fixActionCustomerUser(options);
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('stamp alert by customer user, bad customer user id: ' + options.customerUser);
            }
            cb();
        },
        function(cb) {
            logger.silly('[alert stamp by customeruser] customer user = ' + customerUserId);
            if (!models) {
                logger.silly('[alert stamp by customeruser] retrieving models ');
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: customerUserId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            //identify the customer user
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //get the alert definitions for the customer
            models.definitions.alert.Alert.find({
                active: true,
                deleted: false
            })
                .exec(function(err, alertDefinitions) {
                    p.definitions = alertDefinitions;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            var idsToStamp = _.pluck(p.definitions, '_id');
            async.parallel(_.map(idsToStamp, function(defId) {
                return function(cb) {
                    logger.silly('[alert stamp by customeruser] stamping ' + defId);
                    stamp(models, {
                        createdBy: options.createdBy,
                        modifiedBy: options.modifiedBy,
                        alertDefinition: defId,
                        customerUser: p.customerUser._id,
                        actionCustomerUser: options.actionCustomerUser,
                        flatten: options.flatten
                    }, function(err, models, alertInstance) {
                        logger.silly('[alert stamp by customeruser] stamped ' + defId);
                        cb(err, alertInstance);
                    });
                }
            }), function(err, stamped) {
                logger.silly('[alert stamp by customeruser] stamped: ' + stamped.length);
                p.instances = stamped;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if ((p.instances || [])
                .length == 0) {
                return cb(null, models, p);
            }

            alertInstanceLoader(models, {
                actionCustomerUser: options.actionCustomerUser,
                alertInstances: p.instances,
                flatten: options.flatten
            }, function(err, models, alertInstances) {
                p.instances = alertInstances;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[alert stamp by customer user] flatten: ' + !!options.flatten + ', includeDismissed: ' + !!options.includeDismissed);
            if (!!options.flatten && !!options.includeDismissed) {
                //skip, dismissed are already included
            } else if (!options.flatten && !!options.includeDismissed) {
                //skip, dismissed are already included
            } else if (!!options.flatten && !options.includeDismissed) {
                //filter out the dismissed
                p.instances = _.filter(p.instances, function(instance) {
                    logger.silly('[alert stamp by custuser] instance: ' + util.inspect(instance));
                    return !instance.dismissedDate;
                });
            } else if (!options.flatten && !options.includeDismissed) {
                //alerts must be flattened to filter out dismissed
                return cb('alerts must be flattened to filter out dismissed records.');
            }

            cb(null, models, p);
        },
        function(models, p, cb) {
            //sort the alerts
            if (!!options.flatten) {
                p.instances = _.sortBy(p.instances, function(instance) {
                    return moment(instance.startDate)
                        .format('YYYYMMDDhhmmss') + '_' + moment(instance.endDate)
                        .format('YYYYMMDDhhmmss');
                });
            } else {
                //todo: sort by... modified? i guess?
            }
            cb(null, models, p);
        }
    ], function(err, models, p) {
        cb(err, models, (p || {})
            .instances);
    });
};
