var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    definitionLoader = require('./loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    options = fixActionCustomerUser(options);
    var alertId = _.extractId(options, 'alert');
    logger.silly('[alert tuple loader] alert id: ' + alertId);
    logger.silly('[alert tuple loader] alert option: ' + util.inspect(options.alert));
    async.waterfall([

        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: options.actionCustomerUser._id
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for alert admin listing');
            }
            cb(null, models, {});
        },
        function(models, p, cb) {

            //get the active/undeleted alerts with the ID passed
            models.definitions.alert.Alert.findOne({
                active: true,
                deleted: false,
                _id: alertId.toObjectId()
            }, function(err, alert) {
                p.alert = alert;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            if (p.alert.isDraftOf == 'new') {
                //new document, use this as the draft
                p.draft = p.alert;
                return cb(null, models, p);
            } else if (!!p.alert.isDraftOf) {
                //the doc loaded is a draft
                p.draft = p.alert;
                models.definitions.alert.Alert.findOne({
                    _id: p.isDraftOf
                })
                    .exec(function(err, published) {
                        p.published = published;
                        return cb(err, models, p);
                    });
            } else {
                p.published = p.alert;
                //does this have an open draft?
                models.definitions.alert.Alert.find({
                    active: true,
                    deleted: false,
                    isDraftOf: p.alert._id
                })
                    .exec(function(err, drafts) {
                        if (drafts.length == 0) {
                            //not new, no drafts, it's published
                        } else if (drafts.length == 1) {
                            p.draft = drafts[0];
                        } else {
                            logger.warn('[alert tuple loader] more than one draft found for ' + p.alert._id);
                            p.draft = drafts[0];
                        }
                        cb(err, models, p);
                    });
            }
        },
        function(models, p, cb) {
            //load the published, if any
            if (!p.published) {
                return cb(null, models, p);
            }

            definitionLoader(models, {
                alertDefinition: p.published,
                actionCustomerUser: options.actionCustomerUser,
                flatten: true
            }, function(err, models, published) {
                p.published = published;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            //load the draft, if any
            if (!p.draft) {
                return cb(null, models, p);
            }

            definitionLoader(models, {
                alertDefinition: p.draft,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, draft) {
                //flatten by hand
                draft = draft.toObject();
                datesplice.getByDateRange(draft, 'rangedData', draft.rangedData[0].startDate, draft.rangedData[0].endDate, function(err, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            });
        },
        function(models, p, cb){
            //assemble the tuple
            p.tuple = {
                draft: p.draft,
                published: p.published
            };
            cb(null, models, p);
        }
    ], function(err, models, p) {
        logger.silly('[tuple loader] done.');
        cb(err, models, p.tuple);
    });
};
