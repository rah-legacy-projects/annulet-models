var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    modeler = require('../../../modeler'),
    stamper = require('./stamper');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('invalid customer user for wf stamp: ' + options.customerUser);
            }
            cb();
        },
        function(cb) {
            modeler.db({
                collection: 'auth.CustomerUser',
                query: {
                    _id: customerUserId.toObjectId()
                }
            }, cb);
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for customer user ' + customerUserId + ' for wf stamp by user.');
            }
            cb(null, models);
        },
        function(models, cb) {
            //get user
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    logger.silly('[metastamp] user: ' + util.inspect(customerUser));
                    cb(err, models, {
                        customerUser: customerUser
                    });
                });
        },
        function(models, p, cb){
            if(!p.customerUser){
                return cb('customer user not found for workflow stamp: ' + customerUserId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            async.waterfall([

                function(cb) {
                    //populate the workflow instances
                    models.auth.CustomerUser.populate(p.customerUser, {
                        path: 'workflowInstances',
                        model: models.instances.workflow.Workflow
                    }, function(err, customerUser) {
                        if (!!err) {
                            logger.error(util.inspect(err));
                        }
                        cb(err, customerUser);
                    });
                }
            ], function(err, customerUser) {
                p.customerUser = customerUser;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            var customers = [p.customerUser.customer];
            logger.silly('employee classes: ' + util.inspect(p.customerUser.employeeClass));
            logger.silly('customers: ' + util.inspect(customers));

            //get the workflow definitions for the employee classes
            models.definitions.workflow.Workflow.find({
                employeeClass: {
                    $elemMatch: {
                        $in: p.customerUser.employeeClass
                    }
                },
                customer: {
                    $in: customers
                }
            })
                .exec(function(err, workflowDefs) {
                    if (!!err) {
                        logger.error("probem loading definitions: " + util.inspect(err));
                    }
                    logger.silly('wf defs: ' + workflowDefs.length)
                    p.workflowDefs = workflowDefs;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //what workflows does the user not have?
            var defIds = _.map(p.workflowDefs, function(wd) {
                return wd._id.toString();
            });
            var instanceDefIds = _.map(p.customerUser.workflowInstances, function(wi) {
                return wi.workflowDefinition.toString();
            });
            var idsToStamp = _.difference(defIds, instanceDefIds);

            //stamp each wf
            //save wf instances on user
            if (idsToStamp.length > 0) {
                async.parallel(_.map(idsToStamp, function(id) {
                    return function(cb) {
                        logger.silly('stamping ' + id);
                        stamper(models, {
                            customerUser: options.customerUser,
                            workflowDefinition: id
                        }, function(err, models, stampedWorkflow, stampedRoot) {
                            cb(err, stampedWorkflow);
                        });
                    };
                }), function(err, stamped) {
                    if(!!err){
                        return cb(err);
                    }
                    logger.silly('stamped ' + stamped.length);
                    p.customerUser.workflowInstances = p.customerUser.workflowInstances.concat(_.pluck(stamped, '_id'));
                    logger.silly('wf instances to save: ' + util.inspect(p.customerUser.workflowInstances));
                    p.customerUser.modifiedBy = options.modifiedBy;
                    p.customerUser.save(function(err, customerUser) {
                        logger.silly('saved customer user: ' + util.inspect(customerUser));
                        p.customerUser = customerUser;

                        cb(err, models, p);
                    });
                });
            } else {
                cb(null, models, p);
            }


        },
        function(models, p, cb) {
            //load the workflows roots
            models.instances.workflow.Workflow.find({
                _id: {
                    $in: p.customerUser.workflowInstances
                }
            })
                .exec(function(err, workflows) {
                    logger.silly('wfs found: ' + workflows.length);
                    p.workflows = workflows;
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        cb(err, models, (p||{}).workflows);
        //hack - mark p as null for the gc
        p = null;
    });
};
