var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    workflowItemLoader = require('../loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    workflowItemInstanceLoader = require('../../../instance/workflow/loader'),
    stamp = require('./stamper');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  customerUser: id or object of the customer user,
    //  actionCustomerUser
    //
    //}

    options = fixActionCustomerUser(options);
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!customerUserId) {
                return cb('stamp wf item by customer user, bad customer user id: ' + options.customerUser);
            }
            cb();
        },
        function(cb) {
            logger.silly('[wf item stamp by customeruser] customer user = ' + customerUserId);
            if (!models) {
                logger.silly('[wf item stamp by customeruser] retrieving models ');
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: customerUserId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            //identify the customer user
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //get the wf item instances for the customer user
            models.instances.workflow.workflowItemTypes.Root.find({
                customerUser: p.customerUser._id,
                active:true,
                deleted:false
            })
                .exec(function(err, workflowItemInstances) {
                    p.instances = workflowItemInstances;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //get the wf item definitions for the customer
            models.definitions.workflow.workflowItemTypes.Root.find({
                active:true,
                deleted:false
            })
                .exec(function(err, workflowItemDefinitions) {
                    p.definitions = workflowItemDefinitions;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            //reconcile stamped with not-stamped
            var defIds = _.map(p.definitions, function(d) {
                return d._id.toString();
            });
            var instanceDefIds = _.map(p.instances, function(i) {
                return i.workflowItemDefinition.toString();
            });
            var idsToStamp = _.difference(defIds, instanceDefIds);

            logger.silly('[wf item stamp by customeruser] to stamp: ' + idsToStamp.length);
            //stamp the wfs
            if (idsToStamp.length > 0) {
                async.parallel(_.map(idsToStamp, function(defId) {
                    return function(cb) {
                        logger.silly('[wf item stamp by customeruser] stamping ' + defId);
                        stamp(models, {
                            workflowItemDefinition: defId,
                            customerUser: p.customerUser._id,
                            actionCustomerUser: options.actionCustomerUser
                        }, function(err, models, workflowItemInstance) {
                            logger.silly('[wf item stamp by customeruser] stamped ' + defId);
                            cb(err, workflowItemInstance);
                        });
                    }
                }), function(err, stamped) {
                    logger.silly('[wf item stamp by customeruser] item stamped: ' + stamped.length);
                    p.stamped = stamped;
                    p.instances = p.instances.concat(stamped);
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb){
            workflowItemInstanceLoader(models,{
                actionCustomerUser: options.actionCustomerUser,
                workflowItemInstances: p.instances,
                flatten: options.flatten,
            }, function(err, models, wfItemInstances){
                p.instances = wfItemInstances;
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, (p||{}).instances);
    });
};
