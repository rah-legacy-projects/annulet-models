var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    definitionLoader = require('../loader'),
    instanceLoader = require('../../../instance/workflow/loader'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    stampClone = require('../../stampClone');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  workflowItemDefinition: the workflowItem def to stamp,
    //  customerUser: the customer user the instance belongs to,
    //  actionCustomerUser
    //}

    options = fixActionCustomerUser(options);
    var workflowItemDefinitionId = _.extractId(options, 'workflowItemDefinition');
    var customerUserId = _.extractId(options, 'customerUser');
    async.waterfall([

        function(cb) {
            if (!workflowItemDefinitionId) {
                return cb('bad wf def id when stamping: ' + options.workflowItemDefinition);
            }
            if (!customerUserId) {
                return cb('bad customer user id when stamping op: ' + options.customerUser);
            }

            cb();
        },
        function(cb) {
            logger.silly('[workflowItem stamper] wf def id: ' + workflowItemDefinitionId);

            if (!models) {
                modeler.db({
                    collection: 'definitions.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemDefinitionId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, {});
                });
            } else {
                cb(null, models, {});
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('modelset could not be identified for stamping an wf def: ' + workflowItemDefinitionId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            //verify customer user exists
            models.auth.CustomerUser.findOne({
                _id: customerUserId.toObjectId()
            })
                .exec(function(err, customerUser) {
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            if (!p.customerUser) {
                return cb('customer user could not be found for wf stamp: ' + customerUserId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.debug('[workflowItem stamper] loading definition ' + workflowItemDefinitionId);
            definitionLoader(models, {
                workflowItemDefinition: workflowItemDefinitionId.toObjectId(),
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, workflowItemDefinition) {
                p.workflowItemDefinition = workflowItemDefinition;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            models.instances.workflow.WorkflowItem.findOne({
                itemDefinition: workflowItemDefinitionId.toObjectId(),
                customerUser: customerUserId.toObjectId()
            })
                .exec(function(err, workflowItemInstance) {
                    if (!workflowItemInstance) {
                        p._state = 'Created';
                        logger.silly('[wf stamp] wf item instance not found, creating');
                        cb(null, models, p);
                    } else {
                        p._state = 'Opened';
                        logger.silly('[wf stamp] wf item instance found, loading ' + workflowItemInstance._id);
                        //load the wf instance.
                        instanceLoader(models, {
                            workflowItemInstance: workflowItemInstance,
                            flatten: false,
                            omitItemDefinitionRange: true
                        }, function(err, models, workflowItemInstance) {
                            p.workflowItemInstance = workflowItemInstance;
                            cb(err, models, p);
                        });
                    }
                });
        },

        function(models, p, cb) {
            logger.silly('[wf stamp] about to crawl');
            var crawl = function(definition, instance, cb) {
                async.waterfall([

                    function(cb) {
                        cb(null, {});
                    },
                    function(pp, cb) {
                        async.series(_.map(definition.items, function(definitionItem) {
                            return function(cb) {
                                var instanceItem = _.find((instance || {})
                                    .items, function(item) {
                                        return item.itemDefinition.toString() == definitionItem._id.toString();
                                    });
                                crawl(definitionItem, instanceItem, cb);
                            };
                        }), function(err, items) {
                            pp.items = items;
                            cb(err, pp);
                        });
                    },
                    function(pp, cb) {
                        async.series(_.map(definition.sequence, function(definitionItem) {
                            return function(cb) {
                                var instanceItem = _.find((instance || {})
                                    .sequence, function(item) {
                                        return item.itemDefinition.toString() == definitionItem._id.toString();
                                    });
                                crawl(definitionItem, instanceItem, cb);
                            };
                        }), function(err, sequence) {
                            pp.sequence = sequence;
                            cb(err, pp);
                        });
                    },
                    function(pp, cb) {
                        //if the instance does not exist, create it
                        if (!instance) {
                            logger.silly('[wf stamp] instance is new, adding');
                            logger.silly('[wf stamp] instance type: ' + definition.__t.replace('definition', 'instance'));
                            instance = new((_.resolveObjectPath(models, definition.__t.replace('definition', 'instance'))))({
                                itemDefinition: definition._id,
                                items: [],
                                sequence: [],
                                rangedData: [],
                                createdBy: options.actionCustomerUser._id,
                                modifiedBy: options.actionCustomerUser._id,
                                customerUser: customerUserId
                            });
                        } else {}
                        cb(null, pp);
                    },
                    function(pp, cb) {

                        //determine parts to splice
                        var wfItemInstanceRangesToSplice = _.map(definition.rangedData, function(defRange) {
                            var range = _.find(instance.rangedData, function(instanceRange) {
                                //hack: purposefully don't load item def range to get the bare ID
                                return instanceRange.itemDefinitionRange.toString() == defRange._id.toString();
                            });

                            //if the range doesn't exist or is otherwise different, splice it, otherwise return nothing
                            if (!range ||
                                moment(range.endDate)
                                .toString() != moment(defRange.endDate)
                                .toString() ||
                                moment(range.startDate)
                                .toString() != moment(defRange.startDate)
                                .toString()) {

                                //create a new range to splice
                                return new(_.resolveObjectPath(models, defRange.__t.replace('definition', 'instance')))({
                                    completedDate: null,
                                    itemDefinitionRange: defRange._id,
                                    startDate: defRange.startDate,
                                    endDate: defRange.endDate,
                                    rangeActive: defRange.rangeActive,
                                    createdBy: options.actionCustomerUser._id
                                });
                            }

                            return null;
                        });

                        //crunch out the nulls
                        wfItemInstanceRangesToSplice = _.compact(wfItemInstanceRangesToSplice);


                        //splice the ranged data in series
                        //note this has to be in series to ensure correct order of range operations
                        var instanceRanges = ((instance || {})
                            .rangedData || []);; //_.clone(p.workflowItemInstance.rangedData);
                        async.series(_.map(wfItemInstanceRangesToSplice, function(range) {
                            return function(cb) {
                                datesplice.splice(instanceRanges, range, function(err, toSave) {
                                    instanceRanges = toSave;
                                    cb(err);
                                });
                            };
                        }), function(err) {
                            logger.silly('[wf stamper] splice-saving item');
                            instance.items = pp.items;
                            instance.sequence = pp.sequence;
                            datesplice.dateSpliceSave(options.actionCustomerUser._id, instance, instanceRanges, 'rangedData', function(err, savedWFInstance) {
                                logger.silly('[wf stamper] wf spliced.');
                                if (!!err) {
                                    logger.silly('[wf stamper] error stamping sequence: ' + util.inspect(err));
                                }
                                cb(err, savedWFInstance);
                            });
                        });
                    }
                ], function(err, savedInstance) {
                    cb(err, savedInstance);
                });
            };

            crawl(p.workflowItemDefinition, p.workflowItemInstance, function(err, r) {
                if (!!err) {
                    logger.error('[wf stamper] error during crawl: ' + util.inspect(err));
                }
                logger.silly('[wf stamper] crawl complete');

                logger.silly('[wf stamper] ' + workflowItemDefinitionId + ' == ' + r.itemDefinition);
                logger.silly('[wf stamper] ' + customerUserId + ' == ' + r.customerUser)

                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            logger.debug('[workflowItem stamper] getting wf instance for def id ' + workflowItemDefinitionId);
            _.resolveObjectPath(models, p.workflowItemDefinition.__t.replace('definition', 'instance'))
                .findOne({
                    itemDefinition: workflowItemDefinitionId.toObjectId(),
                    customerUser: customerUserId.toObjectId()
                })
                .exec(function(err, workflowItemInstance) {
                    p.workflowItemInstance = workflowItemInstance;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly('[workflowItem stamper] loading instance ' + p.workflowItemInstance._id);
            instanceLoader(models, {
                workflowItemInstance: p.workflowItemInstance,
                actionCustomerUser: options.actionCustomerUser,
                flatten: options.flatten
            }, function(err, models, workflowItemInstance) {
                logger.silly('[workflowItem stamper] instance loaded.');
                p.workflowItemInstance = workflowItemInstance;
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            //add the wf instance root to the user
            if (!p.customerUser.workflowInstances) {
                p.customerUser.workflowInstances = [];
            }

            //check to see if wf item instance is already in the customer user, and if not, add
            if (!_.any(p.customerUser.workflowInstances, function(wfi) {
                return wfi.toString() == p.workflowItemInstance._id.toString();
            })) {
                p.customerUser.workflowInstances.push(p.workflowItemInstance._id);
            }
            p.customerUser.save(function(err, customerUser) {
                p.customerUser = customerUser;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            (new models.tracking.ItemActivity({
                changedBy: options.actionCustomerUser._id,
                state: p._state,
                itemType: 'instances.workflowItem.WorkflowItem',
                item: p.workflowItemInstance._id,
                message: {
                    firstName: options.actionCustomerUser.user.firstName,
                    lastName: options.actionCustomerUser.user.lastName,
                    email: options.actionCustomerUser.user.email,
                    itemTitle: p.workflowItemInstance.title
                }
            }))
                .save(function(err, activity) {
                    cb(err, models, p);
                });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[wf stamp] issue stamping: ' + util.inspect(err));
        }
        logger.silly('[workflowItem stamper] wf stamped!');
        cb(err, models, p.workflowItemInstance);
    });
};
