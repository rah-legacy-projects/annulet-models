var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    extractOptionIds = require('../../extractOptionIds'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var workflowItemDefinitionIds = extractOptionIds(options, {
        single: 'workflowItemDefinition',
        array: 'workflowItemDefinitions'
    });


    async.waterfall([

        function(cb) {
            if (!workflowItemDefinitionIds || workflowItemDefinitionIds.length == 0) {
                return cb('Bad item definition ID in wf def loader.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemDefinitionIds[0].toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for wf def id ' + util.inspect(workflowItemDefinitionId));
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            options = _.defaults(options, {
                flatten: true,
                startDate: moment(),
                endDate: moment()
            });
            cb(null, models, {});
        },
        function(models, p, cb) {
            var crawl = function(item, cb) {
                async.parallel({
                    ranges: function(cb) {
                        models.definitions.workflow.WorkflowItem.populate(item, {
                            path: 'rangedData',
                            model: models.definitions.workflow.ranged.WorkflowItem
                        }, function(err, item) {
                            cb(err, item);
                        });
                    },
                    sequence: function(cb) {
                        models.definitions.workflow.WorkflowItem.populate(item, {
                            path: 'sequence',
                            model: models.definitions.workflow.WorkflowItem
                        }, function(err, item) {
                            async.parallel(_.map(item.sequence, function(sequence) {
                                return function(cb) {
                                    crawl(sequence, cb);
                                };
                            }), function(err, r) {
                                cb(err, r);
                            });
                        });
                    },
                    items: function(cb) {
                        models.definitions.workflow.WorkflowItem.populate(item, {
                            path: 'items',
                            model: models.definitions.workflow.WorkflowItem
                        }, function(err, item) {
                            async.parallel(_.map(item.items, function(item) {
                                return function(cb) {
                                    crawl(item, cb);
                                };
                            }), function(err, r) {
                                cb(err, r);
                            });
                        });
                    }
                }, function(err) {
                    cb(err, item);
                });
            };

            async.parallel(_.map(workflowItemDefinitionIds, function(workflowItemDefinitionId) {
                return function(cb) {
                    models.definitions.workflow.WorkflowItem.findOne({
                        _id: workflowItemDefinitionId.toObjectId()
                    })
                        .exec(function(err, item) {
                            logger.silly('[wf def loader] about to crawl ' + workflowItemDefinitionId);
                            logger.silly('[wf def loader] about to crawl ' + util.inspect(item));
                            crawl(item, function(err, item) {
                                cb(err, item);
                            });
                        });
                };
            }), function(err, r) {
                p.workflowItems = r;
                cb(err, models, p);
            });

        },
        function(models, p, cb) {
            var crawl = function(item) {
                item.sequence = _.map(item.sequence, function(item) {
                    return crawl(item);
                });
                item.items = _.map(item.items, function(item) {
                    return crawl(item);
                });
                item = datesplice.getByDateRangeSync(item, 'rangedData', options.startDate, options.endDate);
                return item;
            };

            if (!!options.flatten) {
                logger.silly('[wf def loader] flattening workflow!');

                p.workflowItems = _.map(p.workflowItems, function(workflowItem) {
                    return crawl(workflowItem.toObject());
                });
            }
            cb(null, models, p);
        }

    ], function(err, models, p) {
        if (!!err) {
            logger.error('[wf def loader] problem loading def: ' + util.inspect(err));
        }
        logger.silly('[wf def loader] finished loading.');
        cb(err, models, (p || {})
            .workflowItems.length == 1 && !!options.workflowItemDefinition ? p.workflowItems[0] : (p || {})
            .workflowItems);
    });
};
