var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util'),
    moment = require('moment'),
    datesplice = require('mongoose-date-splice'),
    loader = require('./loader'),
    fixActionCustomerUser = require('../../fixActionCustomerUser'),
    modeler = require('../../../modeler');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    var customerId = _.extractId(options, 'customer');

    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('Bad customer ID in workflow def loader by shortname.');
            }
            if (!options.shortName) {
                return cb('Bad shortname in workflow def loader by shortname.');
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('Modelset could not be identified for workflow def id ' + workflowDefinitionId);
            }
            cb(null, models);
        },
        function(models, cb) {
            //set up default options
            options = fixActionCustomerUser(options);
            cb(null, models);
        },
        function(models, cb) {
            models.definitions.workflow.workflowItemTypes.Root.find({
                shortName: options.shortName,
                active: true,
                deleted: false
            })
                .exec(function(err, workflows) {
                    cb(err, models, {
                        workflows: workflows
                    });
                });
        },
        function(models, p, cb) {
            if (p.workflows.length == 0) {
                //no wfs, defs, etc, skip load
                cb(null, models, p);
            } else {
                //load the workflows
                loader(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    workflowItemDefinitions: _.pluck(p.workflows, '_id')
                }, function(err, models, workflows) {
                    p.workflows = workflows;
                    cb(err, models, p);
                });
            }
        },
    ], function(err, models, p) {
        if (!!err) {
            logger.error('[workflow def shortname loader] problem loading def: ' + util.inspect(err));
        }
        cb(err, models, (p || {})
            .workflows);
    });
};
