var modeler = require('../../../modeler'),
    _ = require('lodash'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    fixCustomerUser = require('../../fixCustomerUser'),
    util = require('util'),
    async = require('async');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  customer: object or id of the customer,
    //  customerUser,
    //  workflowItemDef: the item def to save
    //}
    var customerId = _.extractId(options, 'customer'),
        customerUserId = _.extractId(options, 'customerUser');
    logger.silly('customer id: ' + customerId);

    async.waterfall([

        function(cb) {
            if (!customerId) {
                return cb('invalid customer id passed into wf def save: ' + options.customer);
            }
            cb();
        },
        function(cb) {
            options = fixCustomerUser(options);
            if (!models) {
                logger.silly('[wf item def save] models not passed, retrieving');
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, cb);
            } else {
                logger.silly('[wf item def save] models passed, skipping');
                cb(null, models);
            }
        },
        function(models, cb){
            if(!models){
                return cb('modelset could not be identified in wf def item save for customer ' + customerId);
            }
            cb(null, models);
        },
        function(models, cb) {
            var crawler = function(workflowItem, cb) {

                var crawlerSequenceTasks = [];
                _.each(workflowItem.sequence, function(sequenceItem) {
                    crawlerSequenceTasks.push(function(cb) {
                        crawler(sequenceItem, function(err, savedItem) {
                            cb(err, savedItem);
                        });
                    });
                });

                var crawlerItemTasks = [];
                _.each(workflowItem.items, function(item) {
                    crawlerItemTasks.push(function(cb) {
                        crawler(item, function(err, savedItem) {
                            cb(err, savedItem);
                        });
                    });
                });

                async.parallel({
                    sequences: function(cb) {
                        async.parallel(crawlerSequenceTasks, function(err, sequences) {
                            cb(err, sequences);
                        });
                    },
                    items: function(cb) {
                        async.parallel(crawlerItemTasks, cb);
                    }
                }, function(err, r) {
                    var newItem = null;
                    //make a clone of the wf item
                    var copy = _.clone(workflowItem);
                    //nix the collections - we want a bare item with no references
                    delete copy.sequence;
                    delete copy.items;
                    if (!workflowItem._type) {
                        //assume it's a vanilla workflow item?
                        //todo: verify it's a container item?
                        newItem = new models.definitions.workflow.WorkflowItem(copy);
                    } else {
                        //use the type to determine what kind of workflow item def to make
                        logger.debug('[wf item def save] new item type: ' + workflowItem._type);
                        var definition = _.resolveObjectPath(models, workflowItem._type);
                        var newItem = new definition(copy);
                    }
                    newItem.sequence = _.map(r.sequences, function(sequence) {
                        return sequence._id;
                    });
                    newItem.items = _.map(r.items, function(item) {
                        return item.id;
                    });
                    newItem.createdBy = customerUserId;
                    newItem.modifiedBy = customerUserId;
                    newItem.save(function(err, item) {
                        if (!!err) {
                            logger.error(util.inspect(err));
                        }
                        cb(err, item);
                    });
                });

            };

            crawler(options.workflowItemDef, function(err, rootItem) {
                if (!!err) {
                    logger.error(util.inspect(err));
                }
                require('./workflowLoader')(models, {
                    workflowItem: rootItem._id
                }, function(err, models, rootItem) {
                    cb(err, models, rootItem);
                });
            });
        }
    ], function(err, models, p) {
        cb(err, models, p);
    })
}
