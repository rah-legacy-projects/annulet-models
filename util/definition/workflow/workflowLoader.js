var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId;

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to query against
    //options:{
    //  workflowItem: the workflow item to load items under,
    //}

    var crawler = function(models, workflowItem, cb) {
        async.parallel({
            items: function(cb) {
                models.definitions.workflow.WorkflowItem.populate(workflowItem, {
                    path: 'sequence',
                    model: models.definitions.workflow.WorkflowItem
                }, function(err, workflowItem) {
                    cb(err, workflowItem);
                });
            },
            sequence: function(cb) {
                models.definitions.workflow.WorkflowItem.populate(workflowItem, {
                    path: 'items',
                    model: models.definitions.workflow.WorkflowItem
                }, function(err, workflowItem) {
                    cb(err, workflowItem);
                });
            }
        }, function(err, wf) {
            var itemsPopulate = [];
            _.each(workflowItem.items, function(item) {
                itemsPopulate.push(function(cb) {
                    crawler(models, item, cb);
                });
            });

            var sequencePopulate = [];
            _.each(workflowItem.sequence, function(seq) {
                sequencePopulate.push(function(cb) {
                    crawler(models, seq, cb);
                });
            });

            async.parallel({
                sequences: function(cb) {
                    async.parallel(sequencePopulate, cb);
                    sequencePopulate = null;
                },
                items: function(cb) {
                    async.parallel(itemsPopulate, cb);
                    itemsPopulate = null;
                }
            }, function(err, r) {
                cb(err, r);
            });

        });
    };

    var workflowItemId = _.extractId(options, 'workflowItem');
    async.waterfall([

        function(cb) {
            if (!workflowItemId) {
                return cb('workflow item id invalid for def loading: ' + options.workflowItem);
            }
            cb();
        },
        function(cb) {
            if (!models) {
                modeler.db({
                    collection: 'definitions.workflow.WorkflowItem',
                    query: {
                        _id: workflowItemId.toObjectId()
                    }
                }, cb);
            } else {
                cb(null, models);
            }
        },
        function(models, cb) {
            if (!models) {
                return cb('modelset could not be identified for workflow item ' + workflowItemId + ' for def load');
            }
            cb(null, models);
        },
        function(models, cb) {
            models.definitions.workflow.WorkflowItem.findOne({
                _id: workflowItemId.toObjectId()
            })
                .exec(function(err, workflowItem) {
                    cb(err, models, workflowItem)
                });
        },
        function(models, workflowItem, cb) {
            if (!workflowItem) {
                return cb('workflow item could not be found for def loader: ' + workflowItemId);
            }
            cb(null, models, workflowItem);
        },
        function(models, workflowItem, cb) {

            crawler(models, workflowItem, function(err, rootItem) {
                if (!!workflowItem.toObject) {
                    cb(err, models, workflowItem.toObject());
                } else {
                    logger.warn('workflow item does not have toObject, something is wrong');
                    cb(err, models, workflowItem);
                }
            });
        }
    ], function(err, models, workflowItem) {
        if (!!err) {
            logger.error(util.inspect(err));
        }
        cb(err, models, workflowItem);
    });
};
