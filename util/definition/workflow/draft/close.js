var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/workflow/loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  workflowItemDefinition: object representing the workflow to create,
    //  actionCustomerUser
    //}

    logger.silly('[wf def close] options: ' + util.inspect(options));
    var workflowItemDefinitionId = _.extractId(options, 'workflowItemDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'definitions.workflow.WorkflowItem',
                        query: {
                            _id: workflowItemDefinitionId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for workflowItems by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.workflowItemDefinition) {
                    return cb('workflow item definition not supplied to close draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('[wf def close] loading def: ' + workflowItemDefinitionId);
                definitionLoader(models, {
                    workflowItemDefinition: workflowItemDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                var crawl = function(close, cb) {
                    logger.silly('closing ' + util.inspect(close));
                    async.parallel({
                        ranges: function(cb) {
                            async.parallel({
                                definitionRanges: function(cb) {
                                    async.parallel(_.map(close.rangedData, function(range) {
                                        return function(cb){
                                            range.active = false;
                                            range.deleted = true;
                                            range.modifiedBy = options.actionCustomerUser._id;
                                            range.save(function(err, range) {
                                                cb(err);
                                            });
                                        };
                                    }), function(err) {
                                        cb(err);
                                    });
                                },
                                instanceRanges: function(cb) {
                                    models.instances.workflow.ranged.WorkflowItem.find({
                                        itemDefinitionRange: {
                                            $in: _.pluck(close.rangedData, '_id')
                                        }
                                    })
                                        .exec(function(err, instanceRanges) {
                                            async.parallel(_.map(instanceRanges, function(instanceRange) {
                                                return function(cb){
                                                    instanceRange.active = false;
                                                    instanceRange.deleted = true;
                                                    instanceRange.modifiedBy = options.actionCustomerUser._id;
                                                    instanceRange.save(function(err, instanceRange) {
                                                        cb(err);
                                                    });
                                                };
                                            }), function(err) {
                                                cb(err);
                                            });
                                        });
                                }
                            }, function(err) {
                                cb(err);
                            });
                        },
                        self: function(cb) {
                            async.parallel({
                                definition: function(cb) {
                                    close.active = false;
                                    close.deleted = true;
                                    close.modifiedBy = options.actionCustomerUser._id;
                                    close.save(function(err, close) {
                                        cb(err);
                                    });
                                },
                                instances: function(cb) {
                                    models.instances.workflow.WorkflowItem.find({
                                        itemDefinition:close._id
                                    }).exec(function(err, instances){
                                        async.parallel(_.map(instances, function(instance){
                                            instance.active=false;
                                            instance.deleted=true;
                                            instance.modifiedBy = options.actionCustomerUser._id;
                                            instance.save(function(err, instance){
                                                cb(err);
                                            });
                                        }), function(err){
                                            cb(err);
                                        });
                                    });
                                }
                            }, function(err) {
                                cb(err);
                            });
                        },

                        items: function(cb) {
                            async.parallel(_.map(close.items, function(wfi) {
                                return function(cb) {
                                    crawl(wfi, cb);
                                }
                            }), function(err, r) {
                                cb(err);
                            });
                        },
                        sequence: function(cb) {
                            async.parallel(_.map(close.sequence, function(wfi) {
                                return function(cb) {
                                    crawl(wfi, cb);
                                }
                            }), function(err, r) {
                                cb(err);
                            });
                        }

                    }, function(err, r) {
                        cb(err, r);
                    });
                };

                crawl(p.draft, function(err) {
                    cb(err, models, p);
                });
            }
        ],
        function(err, models, p) {
            logger.silly('[workflow draft close] close complete.');
            if (!!err) {
                logger.error('problem closing draft of workflow: ' + util.inspect(err));
            }
            cb(err, models, p.draft);
        });
};
