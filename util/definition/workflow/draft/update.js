var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    moment = require('moment'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    workflowItemDefinitionLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  workflowDefinition: flattened object of the draft OP,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        workflowItemDefinitionId = _.extractId(options, 'workflowItemDefinition');

    options = options || {};
    options = _.defaults(options, {
        flatten: false
    });

    options = fixActionCustomerUser(options);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to update workflow draft.");
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!models) {
                logger.silly('[wf update draft] deriving models...');
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for workflow items by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.workflowItemDefinition) {
                return cb('workflow item definition not supplied to update draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.workflowItemDefinition.startDate || !options.workflowItemDefinition.endDate) {
                return cb('workflow item definition must supply start/end dates');
            }

            cb(null, models, p);
        },
        function(models, p, cb) {
            logger.silly('[workflow update draft] loading wfi');
            workflowItemDefinitionLoader(models, {
                workflowItemDefinition: workflowItemDefinitionId,
                actionCustomerUser: options.actionCustomerUser,
                flatten: false
            }, function(err, models, workflowItem) {
                if (!workflowItem) {
                    return cb('workflow item definition could not be found: ' + workflowItemDefinitionId);
                }
                p.workflowItem = workflowItem;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[workflow update draft] crawling...');

            var rootStart, rootEnd;
            //crawl the update
            var crawl = function(update, original, cb) {
                var range = _.find((original || {})
                    .rangedData, function(range) {
                        return range._id.toString() == (update._rangeId || '')
                            .toString();
                    });

                var trimmedRange = _.clone(update);
                delete trimmedRange.__t;
                delete trimmedRange._id;
                delete trimmedRange.sequence;
                delete trimmedRange.items;

                trimmedRange.startDate = trimmedRange.startDate || rootStart;
                trimmedRange.endDate = trimmedRange.endDate || rootEnd;
                logger.silly('[wf def update] range goes from ' + trimmedRange.startDate + ' to ' + trimmedRange.endDate)

                if (!range) {
                    logger.silly('[wf def update] range is new, creating ' + trimmedRange.title);
                    logger.silly('[wf def update] range type: ' + update._rangeType);
                    range = new(_.resolveObjectPath(models, update._rangeType))(trimmedRange);
                    range.createdBy = options.actionCustomerUser._id;
                } else {
                    logger.silly('[wf def update] range exists, updating ' + trimmedRange.title);
                    range.modifiedBy = options.actionCustomerUser._id;
                    range.merge(trimmedRange);
                    range.startDate = trimmedRange.startDate;
                    range.endDate = trimmedRange.endDate;
                }

                async.series({
                    sequences: function(cb) {
                        //crawl the sequence tiems
                        async.series(_.map(update.sequence, function(updateSequence) {
                            return function(cb) {
                                //find the original, if any
                                var originalSequence = _.find((original || {})
                                    .sequence, function(originalSequence) {
                                        return originalSequence._id.toString() == updateSequence._id.toString();
                                    });
                                crawl(updateSequence, originalSequence, function(err, sequenceId) {
                                    cb(err, sequenceId);
                                });
                            }
                        }), function(err, sequenceIds) {
                            cb(err, sequenceIds);
                        });
                    },
                    items: function(cb) {
                        //crawl the update items
                        async.series(_.map(update.items, function(updateItem) {
                            return function(cb) {
                                //find the original, if any
                                var originalItem = _.find(original.items, function(originalItem) {
                                    return originalItem._id.toString() == (updateItem._id || '')
                                        .toString();
                                });
                                crawl(updateItem, originalItem, function(err, itemId) {
                                    cb(err, itemId);
                                });
                            };
                        }), function(err, itemIds) {
                            cb(err, itemIds);
                        });
                    },
                    range: function(cb) {
                        range.save(function(err, range) {
                            if (!!err) {
                                logger.error('[wf draft update] problem saving range: ' + util.inspect(err));
                            }
                            cb(err, range._id);
                        });
                    },
                    original: function(cb){
                        (_.resolveObjectPath(models, update.__t)).findOne({_id: (original||{})._id}).exec(function(err, wfItems){
                            cb(err, wfItems);
                        });
                    },
                }, function(err, r) {
                    if (!(r.original || {})
                        ._id) {

                        logger.silly('[wf draft update] original new, creating new ' + update.__t);
                        r.original = new(_.resolveObjectPath(models, update.__t))({
                            rangedData: [r.range]
                        });
                    }

                    r.original.rangedData[0] = r.range;
                    //hack: for some reason, ranges weren't being accepted in 4.0.7.  Forcing marked as modified.
                    r.original.markModified('rangedData');
                    r.original.modifiedBy = options.actionCustomerUser._id;
                    r.original.sequence = r.sequences;
                    r.original.items = r.items;
                    r.original.shortName = update.shortName;
                    r.original.save(function(err, original) {
                        if (!!err) {
                            logger.error('problem saving original: ' + util.inspect(err));
                            logger.error('options customer: ' + customerId);
                            logger.error('set cusomer: ' + models.$customerId);
                        }
                            cb(err, original);
                    });

                });

            };

            rootStart = options.workflowItemDefinition.startDate;
            rootEnd = options.workflowItemDefinition.endDate;
            crawl(options.workflowItemDefinition, p.workflowItem, function(err, r) {
                logger.silly('[workflow update draft] crawl complete.');
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[workflow update draft] reloading wf draft');
            //reload the workflow item
            workflowItemDefinitionLoader(models, {
                actionCustomerUser: options.actionCustomerUser,
                workflowItemDefinition: p.workflowItem,
                flatten:options.flatten
            }, function(err, models, workflowItemDefinition) {
                p.workflowItem = workflowItemDefinition;
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        logger.silly('[workflow update draft] finished');
        if (!!err) {
            logger.error('problem updating draft of workflow: ' + util.inspect(err));
        }

        cb(err, models, (p || {})
            .workflowItem);
    });
};
