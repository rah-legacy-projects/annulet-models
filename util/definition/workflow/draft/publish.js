var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    definitionLoader = require('../loader'),
    datesplice = require('mongoose-date-splice'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    instanceLoader = require('../../../instance/workflow/loader'),
    closeDraft = require('./close');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  workflowDefinition: object representing the workflow to create,
    //  actionCustomerUser
    //}

    var customerId = _.extractId(options, 'customer'),
        workflowItemDefinitionId = _.extractId(options, 'workflowItemDefinition');

    options = fixActionCustomerUser(options);
    async.waterfall([

            function(cb) {
                cb(null, models, {});
            },
            function(models, p, cb) {
                if (!customerId) {
                    return cb("Bad customer ID when trying to publish draft of OP.");
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!models) {
                    modeler.db({
                        collection: 'Customer',
                        query: {
                            _id: customerId.toObjectId()
                        }
                    }, function(err, models) {
                        cb(err, models, p);
                    });
                } else {
                    cb(null, models, p);
                }
            },
            function(models, p, cb) {
                if (!models) {
                    return cb('Models could not be identified for OPs by customer ' + customerId);
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                if (!options.workflowItemDefinition) {
                    return cb('workflow item definition not supplied to publish draft.');
                }
                cb(null, models, p);
            },
            function(models, p, cb) {
                definitionLoader(models, {
                    workflowItemDefinition: workflowItemDefinitionId,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, draft) {
                    p.draft = draft;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[wf publish] determining draft: ' + p.draft.isDraftOf);
                if (!!p.draft.isDraftOf && p.draft.isDraftOf === 'new') {
                    logger.silly('[wf publish] draft is new.  Stubbing original root.');
                    p.originalDraft = new models.definitions.workflow.workflowItemTypes.Root({
                        rangedData: [],
                        sections: [],
                        customer: customerId,
                        shortName: p.draft.shortName
                    });
                    cb(null, models, p);
                } else {
                    definitionLoader(models, {
                        workflowItemDefinition: p.draft.isDraftOf,
                        actionCustomerUser: options.actionCustomerUser,
                        flatten: false
                    }, function(err, models, original) {
                        p.originalDraft = original;
                        cb(err, models, p);
                    })
                }
            },
            function(models, p, cb) {
                logger.silly('[wf publish] about to pub crawl');
                var crawl = function(update, original, cb) {
                    async.waterfall([

                        function(cb) {
                            cb(null, {});
                        },
                        function(pp, cb) {
                            //recurse down on children first
                            async.series(_.map(update.sequence, function(updateSequence) {
                                return function(cb) {
                                    var originalSequence = _.find((original || {})
                                        .sequence, function(originalSequence) {
                                            //logger.silly('[wf def publish]\t\t' + originalSequence._id + ' == ' + updateSequence.isDraftOf);
                                            return originalSequence._id.toString == updateSequence.isDraftOf.toString();;
                                        });
                                    crawl(updateSequence, originalSequence, function(err, item) {
                                        cb(err, item._id);
                                    });
                                };
                            }), function(err, sequenceIds) {
                                pp.sequences = sequenceIds;
                                cb(err, pp);
                            });
                        },
                        function(pp, cb) {
                            //recurse down on children first
                            async.series(_.map(update.items, function(updateitem) {
                                return function(cb) {
                                    var originalitem = _.find((original || {})
                                        .item, function(originalitem) {
                                            return originalitem._id.toString == updateitem.isDraftOf.toString();;
                                        });
                                    crawl(updateitem, originalitem, function(err, item) {
                                        cb(err, item._id);
                                    });
                                };
                            }), function(err, itemIds) {
                                pp.items = itemIds;
                                cb(err, pp);
                            });
                        },

                        function(pp, cb) {
                            if (!original) {
                                logger.silly('[wf def publish] original did not exist, creating');
                                var copy = update.toObject();
                                delete copy._id;
                                delete copy.isDraftOf;
                                delete copy.rangedData;
                                delete copy.sequence;
                                delete copy.items;
                                (new(_.resolveObjectPath(models, update.__t))(copy))
                                    .save(function(err, workflowItem) {
                                        pp.workflowItem = workflowItem;
                                        cb(err, pp);
                                    });
                            } else {
                                logger.silly('[wf def publish] original existed')
                                pp.workflowItem = original;
                                cb(null, pp);
                            }
                        },
                        function(pp, cb) {
                            //splice the new item ranged data into the original item's ranged data
                            var instancesToSave = pp.workflowItem.rangedData || [];
                            async.series(_.map(update.rangedData, function(newRange) {
                                return function(cb) {
                                    //see if there was an existing item range that is prexactly the same, skip the splice
                                    if (_.any(pp.workflowItem.rangedData, function(originalRange) {
                                        logger.silly('[wf def publish] comparing on type ' + originalRange.__t);
                                        var comparison = _.resolveObjectPath(models, originalRange.__t)
                                            .compare(originalRange, newRange);
                                        return comparison;
                                    })) {
                                        logger.silly('[wf def publish] skipping splice of item');
                                        cb();
                                    } else {
                                        var copy = _.clone(newRange.toObject());
                                        delete copy._id;
                                        delete copy.isDraftOf;
                                        var range = new(_.resolveObjectPath(models, copy.__t))(copy);
                                        datesplice.splice(instancesToSave, range, function(err, partsToSave) {
                                            instancesToSave = partsToSave;
                                            cb(err);
                                        });
                                    }
                                }
                            }), function(err, r) {
                                pp.workflowItem.sequence = pp.sequences;
                                pp.workflowItem.items = pp.items;
                                //hack: set up ranged data to be empty if it doesn't exist
                                pp.workflowItem.rangedData = pp.workflowItem.rangedData || [];
                                datesplice.dateSpliceSave(options.actionCustomerUser._id, pp.workflowItem, instancesToSave, 'rangedData', function(err, saveditem) {
                                    pp.workflowItem = saveditem;
                                    cb(err, pp);
                                });
                            });
                        },

                    ], function(err, pp) {
                        cb(err, pp.workflowItem);
                    });
                };

                crawl(p.draft, p.originalDraft, function(err, workflowItem) {
                    p.workflowItem = workflowItem;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //reload the workflow def as saved
                definitionLoader(models, {
                    workflowItemDefinition: p.workflowItem._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, workflow) {
                    //make the workflow returned a regular object
                    p.workflow = workflow.toObject();
                    cb(err, models, p);
                });
            },
            //{{{ alter user data
            function(models, p, cb) {
                //crawl wf def to get def ids
                var items = [];
                var crawl = function(wfitem) {
                    _.each(wfitem.sequence, crawl);
                    _.each(wfitem.items, crawl);
                    items.push(wfitem);
                };
                crawl(p.workflow);
                p.workflowDefinitionItems = items;
                cb(null, models, p);
            },
            function(models, p, cb) {
                //get all instances for those items
                models.instances.workflow.WorkflowItem.find({
                    itemDefinition: {
                        $in: _.pluck(p.workflowDefinitionItems, '_id')
                    }
                })
                    .exec(function(err, instances) {
                        p.workflowInstanceItems = instances;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                //populate the ranges for those instances
                models.instances.workflow.WorkflowItem.populate(p.workflowInstanceItems, {
                    path: 'rangedData',
                    model: models.instances.workflow.ranged.WorkflowItem
                }, function(err, instances) {
                    p.workflowInstanceItems = instances;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //for the instance items,
                async.parallel(_.map(p.workflowInstanceitems, function(instance) {
                    return function(cb) {
                        //get the definition item for this instance
                        var definition = _.find(p.workflowDefinitionItems, function(d) {
                            return d._id.toString() == instance.itemDefinition.toString();
                        });

                        //for the definition's ranged data,
                        async.parallel(_.map(definition.rangedData, function(definitionRange) {
                            var instanceRange = _.find(instance.rangedData, function(instanceRange) {
                                return instanceRange.itemDefinitionRange.toString() == definitionRange._id.toString();
                            });
                            if (!instanceRange) {
                                return cb();
                            };

                            //if the instance exists, update it to look like the definition
                            instanceRange.startDate = definitionRange.startDate;
                            instanceRange.endDate = definitionRange.endDate;
                            instanceRange.rangeActive = definitionRange.rangeActive;
                            instanceRange.modifiedBy = options.actionCustomerUser._id;
                            instanceRange.save(function(err, instanceRange) {
                                cb(err, instanceRange);
                            });
                        }), function(err, r) {
                            cb(err);
                        });
                    }
                }), function(err, r) {
                    cb(err, models, p);
                });
            },

            //}}}
            function(models, p, cb) {
                //close the draft that was published
                closeDraft(models, {
                    actionCustomerUser: options.actionCustomerUser,
                    workflowItemDefinition: p.draft
                }, function(err, models, closed) {
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //reload the workflow def as saved
                definitionLoader(models, {
                    workflowItemDefinition: p.workflow._id,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, workflow) {
                    //make the workflow returned a regular object
                    workflow = workflow.toObject();
                    cb(err, models, p);
                });
            },
        ],
        function(err, models, p) {
            logger.silly('[workflow publish] finished.');
            if (!!err) {
                logger.error('problem publishing draft of workflow: ' + util.inspect(err));
            }
            cb(err, models, p.workflow);
        });
};
