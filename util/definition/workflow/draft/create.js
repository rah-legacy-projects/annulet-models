var async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    mongoose = require('mongoose'),
    modeler = require('../../../../modeler'),
    ObjectId = mongoose.Schema.Types.ObjectId,
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    fixActionCustomerUser = require('../../../fixActionCustomerUser'),
    workflowItemLoader = require('../loader');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(models, options, cb) {
    //models: (optional) model set to use
    //options: {
    //  customer: customer object or id to create against
    //  workflowItemDefinition: object representing the workflowItem to create,
    //  actionCustomerUser: the customer user with user for audit purposes
    //}

    var customerId = _.extractId(options, 'customer'),
        workflowItemDefinitionId = _.extractId(options, 'workflowItemDefinition');
    options = fixActionCustomerUser(options);

    logger.silly('[wf create draft] wfdef id: ' + workflowItemDefinitionId);
    async.waterfall([

        function(cb) {
            cb(null, models, {});
        },
        function(models, p, cb) {
            if (!customerId) {
                return cb("Bad customer ID when trying to create workflow.");
            }
            cb(null, models, p);
            __t: 'definitions.workflow.WorkflowItem'
        },
        function(models, p, cb) {
            if (!models) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: customerId.toObjectId()
                    }
                }, function(err, models) {
                    cb(err, models, p);
                });
            } else {
                cb(null, models, p);
            }
        },
        function(models, p, cb) {
            if (!models) {
                return cb('Models could not be identified for workflow items by customer ' + customerId);
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!options.workflowItemDefinition) {
                return cb('workflow item definition not supplied to create draft.');
            }
            cb(null, models, p);
        },
        function(models, p, cb) {
            if (!!workflowItemDefinitionId && workflowItemDefinitionId !== 'new') {
                logger.silly('[wf create draft] getting wf definition: ' + workflowItemDefinitionId);
                //wf exists, go get it
                workflowItemLoader(models, {
                    workflowItemDefinition: options.workflowItemDefinition,
                    actionCustomerUser: options.actionCustomerUser,
                    flatten: false
                }, function(err, models, workflowItem) {
                    //operating procedure should be loaded but not flattened
                    p.workflowItem = workflowItem.toObject();
                    cb(err, models, p);
                });
            } else {
                logger.silly('[wf create draft] wf def new, making stub');
                //wf does not exist, make one to stub off of
                p.workflowItem = {}; //new models.definitions.workflowItem.WorkflowItem();
                p.workflowItem.rangedData = [{
                    name: '',
                    description: '',
                    startDate: moment()
                        .subtract(1, 'seconds')
                        .toDate(),
                    endDate: moment()
                        .add(90, 'days')
                        .toDate(),
                    __t: 'definitions.workflow.ranged.workflowItemTypes.Root',
                    rangeActive: true
                }];
                p.workflowItem.__t = 'definitions.workflow.workflowItemTypes.Root'
                p.workflowItem.sequence = [];
                p.workflowItem.items = [];
                p.workflowItem.customer = customerId;
                p.createdBy = options.actionCustomerUser._id;
                p.workflowItem._id = 'new';
                p.workflowItem.shortName = 'new';
                cb(null, models, p);
            }
        },
        function(models, p, cb) {

            var crawl = function(item, cb) {
                async.waterfall([

                    function(cb) {
                        cb(null, {});
                    },
                    function(pp, cb) {
                        //crawl the sequence
                        async.series(_.map(item.sequence, function(item) {
                            return function(cb) {
                                crawl(item, cb);
                            };
                        }), function(err, r) {
                            pp.sequence = r;
                            cb(err, pp);
                        });
                    },
                    function(pp, cb) {
                        //crawl the items
                        async.series(_.map(item.items, function(item) {
                            return function(cb) {
                                crawl(item, cb);
                            };
                        }), function(err, r) {
                            pp.items = r;
                            cb(err, pp);
                        });
                    },
                    function(pp, cb) {
                        //make a copy of the currentmost date range
                        var s = datesplice.getByDateRangeSync(_.clone(item), 'rangedData', moment(), moment());
                        var srange = null;
                        if (!!s) {
                            srange = _.clone(_.find(item.rangedData, function(r) {
                                return r._id == s._rangeId;
                            }));
                            delete srange._id;
                        } else {
                            srange = new(_.resolveObjectPath(models, s._rangeType))();
                        }

                        (new(_.resolveObjectPath(models, s._rangeType))(srange))
                            .save(function(err, sectionRange) {
                                pp.sectionRange = sectionRange;
                                cb(err, pp);
                            });
                    },
                    function(pp, cb) {
                        //make a copy of the section with the ranged section inside
                        var itemopts = _.clone(item);
                        delete itemopts.rangedData;
                        delete itemopts._id;
                        delete itemopts.__t;
                        delete itemopts.items;
                        delete itemopts.sequence;
                        itemopts.rangedData = [pp.sectionRange._id];
                        itemopts.isDraftOf = item._id;
                        itemopts.createdBy = options.actionCustomerUser._id;
                        itemopts.sequence = _.pluck(pp.sequence, '_id');
                        itemopts.items = _.pluck(pp.items, '_id');
                        logger.silly('resolving type: ' + item.__t);
                        (new(_.resolveObjectPath(models, item.__t))(itemopts))
                            .save(function(err, item) {
                                pp.item = item;
                                cb(err, pp);
                            });
                    }
                ], function(err, pp) {
                    cb(err, pp.item);
                });

            };

            crawl(p.workflowItem, function(err, item) {
                p.draft = item;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('[wf create draft] reloading draft, unflattened');
            //reload the draft wf unflattened
            workflowItemLoader(models, {
                workflowItemDefinition: p.draft._id,
                flatten: false,
                actionCustomerUser: options.actionCustomerUser
            }, function(err, models, draft) {
                p.draft = draft;
                logger.silly('[wf create draft] draft is draft of ' + p.draft.isDraftOf);
                cb(err, models, p);
            });
        }
    ], function(err, models, p) {
        if (!!err) {
            logger.error('problem creating draft of workflowItem: ' + util.inspect(err));
        }
        cb(err, models, p.draft);
    });
};
