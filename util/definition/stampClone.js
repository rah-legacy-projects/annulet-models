var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    util = require('util');

module.exports = exports = function(object) {
    var clone = _.clone(object);
    var refs = [];

    var scrub = function(object) {
        if (!_.contains(refs, object)) {
            refs.push(object);
            if (_.isObject(object)) {
                delete object._id;
                delete object.__t;
                _.chain(object)
                    .keys()
                    .each(function(key) {
                        //logger.silly('recursing on ' + key);
                        scrub(object[key]);
                    });
            }
        }
    };

    scrub(clone);
    return clone;
};
