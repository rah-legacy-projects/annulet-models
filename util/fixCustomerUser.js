var _ = require('lodash');
_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    var customerUserId = _.extractId(options, 'customerUser');
    options.customerUser = options.customerUser || {};

    if (!options.customerUser || _.isString(options.customerUser) || !options.customerUser._id) {
        options.customerUser = {
            _id: customerUserId,
            user: {}
        }
    }

    options.customerUser.user = _.defaults(options.customerUser.user, {
        firstName: '[unknown]',
        lastName: '[unknown]',
        email: '[unknown]'
    });

    return options;
};
