module.exports = exports = {
	draft: require("./draft"),
	loadByShortName: require("./loadByShortName"),
	modifyAndPublish: require("./modifyAndPublish"),
	publish: require("./publish"),
	sandbox: require("./sandbox"),
	stampAllByCustomerUser: require("./stampAllByCustomerUser"),
	stampEditStamp: require("./stampEditStamp"),
	stamp: require("./stamp"),
	stampTwice: require("./stampTwice"),
	status: require("./status"),
};
