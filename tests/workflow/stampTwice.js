var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp twice': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.workflowItem.createDraft(),
            testUtility.parts.workflowItem.updateDraft(),
            testUtility.parts.workflowItem.publishDraft(),
            testUtility.parts.workflowItem.stamper({
                flatten: true
            }),
            function(models, p, cb) {
                testUtility.parts.workflowItem.stamper({
                    flatten: true,
                    member: 'stampAgain'
                })(models, p, cb);
            },
            function(models, p, cb) {
                //reload the flattened definition
                projectUtility.definition.workflow.loader(models, {
                    workflowItemDefinition: p.published._id
                }, function(err, models, reloaded) {
                    p.reloaded = reloaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                    //finally, get the status of the user
                    projectUtility.instance.workflow.status(models, {
                        customerUser: p.customerUser,
                        actionCustomerUser: p.customerUser
                    }, function(err, models, statusList) {
                        p.statusList = statusList;
                        cb(err, models, p);
                    });
                }

        ], function(err, models, p) {
            test.ok(p.stampAgain);
            test.equal(p.statusList.length, 1);
            test.done();
        });
    },
};
