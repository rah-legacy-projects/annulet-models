var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'status - none': function(test) {
        async.waterfall([

                function(cb) {
                    cb(null, null, {});
                },
                testUtility.parts.customer(),
                testUtility.parts.customerUser(),
                function(models, p, cb) {
                    //finally, get the status of the user
                    projectUtility.instance.workflow.status(models, {
                        customerUser: p.customerUser,
                        actionCustomerUser: p.customerUser
                    }, function(err, models, statusList) {
                        p.statusList = statusList;
                        cb(err, models, p);
                    });
                }
            ],
            function(err, models, p) {

                test.ifError(err);
                test.ok(p);

                if (!!p.statusList) {
                    test.equal(p.statusList.length, 0);
                }

                test.done();
            });
    },
    'status - one, no stamp': function(test) {
        async.waterfall([

                function(cb) {
                    cb(null, null, {});
                },
                testUtility.parts.customer(),
                testUtility.parts.customerUser(),

                testUtility.parts.workflowItem.createDraft({
                    member: 'draftOne'
                }),
                function(models, p, cb) {
                    testUtility.parts.workflowItem.updateDraft({
                        member: 'updateOne',
                        workflowItemDefinition: testUtility.parts.workflowItem.definition({
                            rangeId: p.draftOne.rangedData[0]._id,
                            draftId: p.draftOne._id
                        })
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.workflowItem.publishDraft({
                        member: 'publishOne',
                        draftId: p.draftOne._id
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    cb(null, models, p);
                },

                function(models, p, cb) {
                    //finally, get the status of the user
                    projectUtility.instance.workflow.status(models, {
                        customerUser: p.customerUser,
                        actionCustomerUser: p.customerUser
                    }, function(err, models, statusList) {
                        p.statusList = statusList;
                        cb(err, models, p);
                    });
                }
            ],
            function(err, models, p) {

                test.ifError(err);
                test.ok(p);

                if (!!p.statusList) {
                    test.equal(p.statusList.length, 1);

                    test.equal(_.find(p.statusList, function(s) {
                            return s.definitionId == p.publishOne._id.toString()
                        })
                        .percentageComplete, 0)
                }

                test.done();
            });
    },
    'status - mixed': function(test) {
        async.waterfall([

                function(cb) {
                    cb(null, null, {});
                },
                testUtility.parts.customer(),
                testUtility.parts.customerUser(),

                testUtility.parts.workflowItem.createDraft({
                    member: 'draftOne'
                }),
                function(models, p, cb) {
                    testUtility.parts.workflowItem.updateDraft({
                        member: 'updateOne',
                        workflowItemDefinition: testUtility.parts.workflowItem.definition({
                            rangeId: p.draftOne.rangedData[0]._id,
                            draftId: p.draftOne._id
                        })
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.workflowItem.publishDraft({
                        member: 'publishOne',
                        draftId: p.draftOne._id
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    cb(null, models, p);
                },

                testUtility.parts.workflowItem.createDraft({
                    member: 'draftTwo'
                }),
                function(models, p, cb) {
                    testUtility.parts.workflowItem.updateDraft({
                        member: 'updateTwo',
                        workflowItemDefinition: testUtility.parts.workflowItem.definition({
                            rangeId: p.draftTwo.rangedData[0]._id,
                            draftId: p.draftTwo._id
                        })
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.workflowItem.publishDraft({
                        member: 'publishTwo',
                        draftId: p.draftTwo._id
                    })(models, p, cb);
                },


                testUtility.parts.workflowItem.createDraft({
                    member: 'draftThree'
                }),
                function(models, p, cb) {
                    testUtility.parts.workflowItem.updateDraft({
                        member: 'updateThree',
                        workflowItemDefinition: testUtility.parts.workflowItem.definition({
                            rangeId: p.draftThree.rangedData[0]._id,
                            draftId: p.draftThree._id
                        })
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.workflowItem.publishDraft({
                        member: 'publishThree',
                        draftId: p.draftThree._id
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.workflowItem.stamper({
                        member: 'stampOne',
                        workflowItemDefinition: p.publishOne._id,
                        flatten: true
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.workflowItem.stamper({
                        member: 'stampTwo',
                        workflowItemDefinition: p.publishTwo._id,
                        flatten: true
                    })(models, p, cb);
                },

                function(models, p, cb) {
                    logger.silly('[top] about to finish instance one');
                    //finish everything in the first instance
                    var wfItemsToFinish = _.compact(_.filterDeep(p.stampOne, {
                        __t: function(key, value) {
                            return /workflowItemTypes\.(?:Training|Quiz)$/.test(value);
                        }
                    }));
                    logger.silly('[top] items to finish: ' + util.inspect(wfItemsToFinish));

                    async.parallel(_.map(wfItemsToFinish, function(f) {
                        return function(cb) {
                            f.completedDate = new Date();
                            projectUtility.instance.workflow.updateFlat(models, {
                                workflowItemInstance: f
                            }, function(err, models, updated) {
                                cb(err, updated);
                            })
                        };
                    }), function(err, r) {
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    //finish half of the second instance
                    var wfItemsToFinish = _.compact(_.filterDeep(p.stampTwo, {
                        __t: function(key, value) {
                            return /workflowItemTypes\.(?:Training)$/.test(value);
                        }
                    }));

                    logger.silly('[top] finishing: ' + util.inspect(wfItemsToFinish));

                    async.parallel(_.map(wfItemsToFinish, function(f) {
                        return function(cb) {
                            f.completedDate = new Date();
                            projectUtility.instance.workflow.updateFlat(models, {
                                workflowItemInstance: f
                            }, function(err, models, updated) {
                                cb(err, updated);
                            })
                        };
                    }), function(err, r) {
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    //finally, get the status of the user
                    projectUtility.instance.workflow.status(models, {
                        customerUser: p.customerUser,
                        actionCustomerUser: p.customerUser
                    }, function(err, models, statusList) {
                        p.statusList = statusList;
                        cb(err, models, p);
                    });
                }
            ],
            function(err, models, p) {

                test.ifError(err);
                test.ok(p);

                logger.silly('[top] ' + util.inspect(p.statusList));
                if (!!p.statusList) {
                    test.equal(p.statusList.length, 3);

                    test.equal(_.find(p.statusList, function(s) {
                            return s.definitionId == p.publishOne._id.toString()
                        })
                        .percentageComplete, 100);
                    test.equal(_.find(p.statusList, function(s) {
                            return s.definitionId == p.publishTwo._id.toString()
                        })
                        .percentageComplete, 50);
                    test.equal(_.find(p.statusList, function(s) {
                            return s.definitionId == p.publishThree._id.toString()
                        })
                        .percentageComplete, 0)
                }

                test.done();
            });
    },
};
