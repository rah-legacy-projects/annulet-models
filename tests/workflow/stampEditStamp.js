var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

var stamp = function(editStep, cb) {
    async.waterfall([

        function(cb) {
            cb(null, null, {});
        },
        testUtility.parts.customer(),
        testUtility.parts.customerUser(),
        testUtility.parts.workflowItem.createDraft(),
        testUtility.parts.workflowItem.updateDraft(),
        testUtility.parts.workflowItem.publishDraft(),
        testUtility.parts.workflowItem.stamper({
            flatten: true,
            member: 'instanceOne'
        }),
        function(models, p, cb) {
            projectUtility.definition.workflow.loader(models, {
                workflowItemDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.definitionOne = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            testUtility.parts.workflowItem.createDraft({
                workflowItemDefinitionId: p.published._id,
                member: 'createdSecondDraft'
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.workflow.loader(models, {
                workflowItemDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.secondDraft = loaded;
                cb(err, models, p);
            });
        },
        editStep,
        function(models, p, cb) {
            testUtility.parts.workflowItem.updateDraft({
                member: 'secondDraftUpdate',
                workflowItem: p.secondDraft
            })(models, p, cb);
        },
        function(models, p, cb) {
            testUtility.parts.workflowItem.publishDraft({
                member: 'secondPublishUnflat',
                draftId: p.secondDraft._id
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.workflow.loader(models, {
                workflowItemDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.flatDefinition = loaded;
                cb(err, models, p);
            });
        },
        testUtility.parts.workflowItem.stamper({
            flatten: true,
            member: 'instanceTwo'
        }),
        function(models, p, cb) {
            projectUtility.definition.workflow.loader(models, {
                workflowItemDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.definitionTwo = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            projectUtility.definition.workflow.loader(models, {
                workflowItemDefinition: p.published._id,
                flatten: false
            }, function(err, models, loaded) {
                p.fullDefinitionTwo = loaded.toObject();
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, p);
    });
};

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'edit nothing': function(test) {
        stamp(function(models, p, cb) {
                cb(null, models, p);
            },
            function(err, models, p) {

                test.ifError(err);
                test.ok(p);

                test.ok(p.definitionOne);
                test.ok(p.instanceOne);
                test.ok(p.definitionTwo);
                test.ok(p.instanceTwo);

                testUtility.workflowItemCompare(test, p.definitionOne, p.instanceOne, 'instance one');
                testUtility.workflowItemCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');
                test.done();
            });
    },
    'edit - add container, change root title and first container title': function(test) {
        stamp(function(models, p, cb) {
                p.secondDraft.title = 'altered title';
                p.secondDraft.items[0].title = 'altered container title';
                p.secondDraft.items.push({
                    __t: 'definitions.workflow.workflowItemTypes.Container',
                    _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Container',
                    title: '*** new content'
                });
                cb(null, models, p);
            },
            function(err, models, p) {

                test.ifError(err);
                test.ok(p);

                test.ok(p.definitionOne);
                test.ok(p.instanceOne);
                test.ok(p.definitionTwo);
                test.ok(p.instanceTwo);

                testUtility.workflowItemCompare(test, p.definitionOne, p.instanceOne, 'instance one');
                testUtility.workflowItemCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');
                test.done();
            });
    },
};
