var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    comparator = require('../util/workflowComparator'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();
    },
    'root for item': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            function(models, p, cb) {
                projectUtility.definition.workflow.itemSave(models, {
                    createdBy: 'test',
                    modifiedBy: 'test',
                    customer: p.customer._id,
                    workflowItemDef: {
                        name: 'root',
                        sequence: [{
                            name: 'item 1',
                        }, {
                            name: 'item 2',
                            sequence: [{
                                name: 'subitem 1'
                            }, {
                                name: 'subitem 2'
                            }]
                        }]
                    }
                }, function(err, models, workflowItemDefinitionRoot) {
                    logger.silly('saved wf def');
                    cb(err, models, _.defaults(p, {
                        workflowItemDefinitionRoot: workflowItemDefinitionRoot
                    }));
                });
            },
            testUtility.parts.workflow.makeWorkflow(),
            testUtility.parts.workflow.stamp(),
            function(models, stuff, cb) {
                projectUtility.instance.workflow.rootForItem(models, {
                    workflowItem: stuff.stampedRoot.sequence[1].sequence[1]._id
                }, function(err, models, rootItem) {
                    stuff.rootItem = rootItem;
                    cb(err, models, stuff);
                });
            },
        ], function(err, models, stuff) {
            test.ifError(err);
            test.ok(stuff);
            test.equal(stuff.rootItem._id.toString(), stuff.stampedRoot._id.toString());
            test.done();
        });
    },
};
