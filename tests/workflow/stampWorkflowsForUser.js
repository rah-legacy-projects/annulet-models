var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    heapdiffStart = require('../util/heapdiffStart'),
    heapdiffEnd = require('../util/heapdiffEnd'),
    heapdiffAnalyze = require('../util/heapdiffAnalyze'),
    fs = require('fs'),
    comparator = require('../util/workflowComparator'),
    projectUtility = require('../../util'),
    testUtility = require('../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();
    },
    'Stamp workflows for user': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            function(models, p, cb) {
                projectUtility.definition.workflow.itemSave(models, {
                    createdBy: 'test',
                    modifiedBy: 'test',
                    customer: p.customer._id,
                    workflowItemDef: {
                        name: 'root',
                        sequence: [{
                            name: 'item 1',
                        }, {
                            name: 'item 2',
                            sequence: [{
                                name: 'subitem 1'
                            }, {
                                name: 'subitem 2'
                            }]
                        }]
                    }
                }, function(err, models, workflowItemDefinitionRoot) {
                    logger.silly('saved wf def');
                    cb(err, models, _.defaults(p, {
                        workflowItemDefinitionRoot: workflowItemDefinitionRoot
                    }));
                });
            },
            testUtility.parts.workflow.makeWorkflow(),
            testUtility.parts.workflow.makeWorkflow({
                name: 'test workflow 2',
                member: 'workflowDefinition2'
            }),
            function(models, p, cb) {
                logger.silly('stamping all');
                //instantiate an instance of v1
                projectUtility.definition.workflow.stampWorkflowsByCustomerUser(models, {
                    createdBy: 'test',
                    modifiedBy: 'test',
                    customerUser: p.customerUser
                }, function(err, models, stampedWorkflows) {
                    cb(err, models, _.defaults(p, {
                        stampedWorkflows: stampedWorkflows,
                    }));
                });
            },
        ], function(err, models, stuff) {
            logger.silly('done propping');
            logger.silly(util.inspect(stuff.stampedWorkflows, null, 4))
            test.ifError(err);
            test.ok(stuff);
            test.ok(stuff.stampedWorkflows);
            test.equal(2, stuff.stampedWorkflows.length);
            test.done();
        });
    },
};
