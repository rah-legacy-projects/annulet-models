var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.workflowItem.createDraft(),
            testUtility.parts.workflowItem.updateDraft(),
            testUtility.parts.workflowItem.publishDraft(),
            testUtility.parts.workflowItem.stamper({
                flatten: true
            }),
            function(models, p, cb){
                //reload the flattened definition
                projectUtility.definition.workflow.loader(models,{
                    workflowItemDefinition: p.published._id
                }, function(err, models, reloaded){
                    p.reloaded = reloaded;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {

            logger.silly('[top] ' + JSON.stringify(p.stampedWorkflowItem, null, 3));
            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedWorkflowItem);
            test.equal(p.stampedWorkflowItem.items.length, 1);
            test.ok(_.all(p.stampedWorkflowItem.sections, function(section) {
                return /instance/.test(section.__t);
            }));
            test.ok(_.chain(p.stampedWorkflowItem.sections, function(section) {
                return /instance/.test(section._rangeType);
            }));

            test.ok(moment()
                .startOf('day')
                .subtract(1, 'day')
                .isSame(moment(p.stampedWorkflowItem.startDate)));

            test.equal(p.stampedWorkflowItem.endDate, 'forever');


            test.ok(_.all(p.stampedWorkflowItem.sections, function(section) {
                return moment()
                    .startOf('day')
                    .subtract(1, 'day')
                    .isSame(moment(section.startDate));
            }), 'not all section start dates are correct');

            test.ok(_.all(p.stampedWorkflowItem.sections, function(section) {
                return moment()
                    .startOf('day')
                    .add(30, 'day')
                    .isSame(moment(section.endDate));
            }), 'not all section end dates are correct');

            test.ok(_.all(p.stampedWorkflowItem.sections, function(section) {
                return !!section.title;
            }));

            logger.silly('[top] titles: ' + p.reloaded.title + ', ' + p.stampedWorkflowItem.title);
            logger.silly('[top] descriptions: ' + p.reloaded.description + ', ' + p.stampedWorkflowItem.description);

            testUtility.workflowItemCompare(test, p.reloaded, p.stampedWorkflowItem);



            test.done();
        });
    },
};
