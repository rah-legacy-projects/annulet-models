var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'close': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.workflowItem.createDraft(),
            testUtility.parts.workflowItem.updateDraft(),
            testUtility.parts.workflowItem.publishDraft(),
            testUtility.parts.workflowItem.stamper({
                flatten: true
            }),
            function(models, p, cb){
                projectUtility.definition.workflow.loader(models,{
                    workflowItemDefinition: p.published._id
                },function(err, models, root){
                    p.loaded = root;
                    cb(err, models, p);
                });
            },
            function(models, p, cb){
                logger.silly(util.inspect(p.loaded));
                projectUtility.definition.workflow.draft.close(models,{
                    workflowItemDefinition: p.loaded.items[0].sequence[1]._id
                },function(err, models, closed){
                    logger.silly('closed: ' + util.inspect(closed));
                    p.closed = closed;
                    cb(err, models, p);
                });
            },
            function(models, p, cb){
                projectUtility.definition.workflow.loader(models,{
                    workflowItemDefinition: p.published._id
                },function(err, models, root){
                    p.reloaded = root;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            test.ok(p.closed);
            test.done();
        });
    },
};
