var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'draft and update an workflowItem': function(test) {
        async.waterfall([

            function(cb) {
                logger.silly('about to create customer!');
                cb(null, {}, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.workflowItem.createDraft(),
            testUtility.parts.workflowItem.updateDraft({member: 'updatedWorkflowItem', flatten:true}),
        ], function(err, models, p) {
            //logger.silly('[top] ' + JSON.stringify(p, null, 2));
            logger.silly('[top] ' + JSON.stringify(p.updatedWorkflowItem, null, 2));
            test.ifError(err);
            test.ok(p);
            test.ok(p.updatedWorkflowItem, 'wf item did not exist');
            //test.equal(p.updatedWorkflowItem.title, 'test workflowItem', 'title update failed');
            //test.equal(p.updatedWorkflowItem.description, 'test description', 'description update failed');

            logger.silly('start date: ' + p.updatedWorkflowItem.startDate + ' vs ' + moment().startOf('day').subtract(1, 'day').toDate());
            logger.silly('end date: ' + p.updatedWorkflowItem.endDate);

            test.ok(moment(p.updatedWorkflowItem.startDate)
                .isSame(moment()
                    .startOf('day')
                    .subtract(1, 'day')), 'start date update failed');
            test.ok(p.updatedWorkflowItem.endDate == 'forever');
            test.equal(p.updatedWorkflowItem.items.length, 1, 'more or less than one item exists: ' + p.updatedWorkflowItem.items.length);

            _.each(p.original.items, function(originalItem) {
                var updatedItem = _.find(p.updatedWorkflowItem.items, function(flatItem) {
                    return flatItem._rangeType == originalItem._rangeType;
                });

                test.ok(updatedItem);
                if (!updatedItem) {
                    return;
                }

                test.equal(updatedItem.title, originalItem.title);
                test.equal(updatedItem.content, originalItem.content);
                test.equal(updatedItem._rangeType, originalItem._rangeType);
                
            });

            _.each(p.original.sequence, function(originalItem) {
                var updatedItem = _.find(p.updatedWorkflowItem.items, function(flatItem) {
                    return flatItem._rangeType == originalItem._rangeType;
                });

                test.ok(updatedItem);
                if (!updatedItem) {
                    return;
                }

                test.equal(updatedItem.title, originalItem.title);
                test.equal(updatedItem.content, originalItem.content);
                test.equal(updatedItem._rangeType, originalItem._rangeType);
                
            });
            test.done();
        });
    },
};
