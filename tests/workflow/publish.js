var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'publish an operating procedure def': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.workflowItem.createDraft(),
            function(models, p, cb){
                testUtility.parts.workflowItem.updateDraft()(null, p, cb);
            },
            function(models, p, cb){
                projectUtility.definition.workflow.loader(models, {
                    workflowItemDefinition: p.workflowItemDefinition._id,
                    flatten: false
                }, function(err, models, workflow){
                    logger.silly('[top] updated: ' + JSON.stringify(workflow, null, 2));
                cb(null, models, p);
                });
            },
            testUtility.parts.workflowItem.publishDraft(),
            function(models, p, cb) {
                logger.silly('[top] published: ' + util.inspect(p.published));
                projectUtility.definition.workflow.loader(models, {
                    workflowItemDefinition: p.published._id
                }, function(err, models, loaded) {
                    logger.silly('[top] reloaded: ' + util.inspect(loaded));
                    p.reloaded = loaded;
                    cb(err, models, p);
                });
            }

        ], function(err, models, p) {
            test.ifError(err);
            test.ok(p);
            test.ok(p.published);
            test.ok(!p.published.isDraftOf);

            test.equal(p.reloaded.items.length, 1);
            test.ok(p.reloaded.employeeClass);
            test.equal(p.reloaded.employeeClass.length, 1);
            test.equal(p.reloaded.employeeClass[0], 'test');
            test.ok(p.reloaded._rangeType);
            test.equal(p.reloaded._rangeType, 'definitions.workflow.ranged.workflowItemTypes.Root');
            _.each(p.reloaded.sections, function(section) {
                test.ok(/definitions\.workflowItem\.ranged\.sectionTypes\.(\w+)/.test(section._rangeType));
            });

            test.done();
        });
    },
};
