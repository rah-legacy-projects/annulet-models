var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp all by customer': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.workflowItem.createDraft(),
            testUtility.parts.workflowItem.updateDraft(),
            testUtility.parts.workflowItem.publishDraft(),
            testUtility.parts.workflowItem.createDraft({
                member: 'draftTwo'
            }),
            function(models, p, cb) {
                var definition = testUtility.parts.workflowItem.definition({
                        rangeId: p.draftTwo.rangedData[0]._id,
                        draftId: p.draftTwo._id
                    });
                definition.title = 'definition two!'
                testUtility.parts.workflowItem.updateDraft({
                    member: 'updateTwo',
                    workflowItem: definition
                })(models, p, cb);
            },
            function(models, p, cb){
                testUtility.parts.workflowItem.publishDraft({
                    member:'publishTwo',
                    draftId: p.draftTwo._id
                })(models, p, cb);
            },
            function(models, p, cb){
                projectUtility.definition.workflow.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser
                },function(err, models, instances){
                    logger.silly('[top] instances: ' + util.inspect(instances));
                    p.instances = instances;
                    cb(err, models, p);
                });

            }
        ], function(err, models, p) {
            test.ifError(err);
            test.ok(p);
            test.ok(p.instances);
            test.equal(p.instances.length, 2);

            var wfItemDefinitions = _.map(p.instances, function(instance) {
                logger.silly('[top] instance: ' + util.inspect(instance));
                return instance.itemDefinition.toString();
            })
            test.ok(_.any(wfItemDefinitions, function(d) {
                return d == p.published._id.toString();
            }));
            test.ok(_.any(wfItemDefinitions, function(d) {
                return d == p.publishTwo._id.toString()
            }));
            logger.silly('draft id: ' + p.draft._id);
            logger.silly('draft two id: ' + p.draftTwo._id);
            logger.silly('published id: ' + p.published._id);
            logger.silly('publushed two id: ' + p.publishTwo._id);
            logger.silly('instance def ids: ' + util.inspect(_.pluck(p.instances, 'workflowItemDefinition')));

            test.done();
        });
    },
};
