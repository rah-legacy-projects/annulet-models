var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'publish an operating procedure def': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.workflowItem.createDraft(),
            testUtility.parts.workflowItem.updateDraft(),
            testUtility.parts.workflowItem.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.workflow.loader(models, {
                    workflowItemDefinition: p.published._id
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                testUtility.parts.workflowItem.createDraft({
                    workflowItemDefinitionId: p.published._id,
                    member: 'createdSecondDraft'
                })(models, p, cb);
            },
            function(models, p, cb) {
                projectUtility.definition.workflow.loader(models, {
                    workflowItemDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.secondDraft = loaded;
                    cb(err, models, p)
                });
            },
            function(models, p, cb) {
                p.secondDraft.title = 'altered title';
                p.secondDraft.items[0].title = 'altered container title';
                p.secondDraft.items.push({
                    __t: 'definitions.workflow.workflowItemTypes.Container',
                    _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Container',
                    title: '*** new content'
                });
                logger.silly('[top] second update: ' + JSON.stringify(p.secondDraft, null, 2));
                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('[top] updating second draft');
                testUtility.parts.workflowItem.updateDraft({
                    member: 'secondDraftUpdate',
                    workflowItemDefinition: p.secondDraft
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('[top] updated second draft: ' + util.inspect(p.secondDraftUpdate));
                logger.silly('[top] publishing second draft');
                testUtility.parts.workflowItem.publishDraft({
                    member: 'secondPublishUnflat',
                    draftId: p.secondDraft._id
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('[top] reloading second publish');
                logger.silly('[top] second publish draft id: ' + p.secondPublishUnflat._id);
                projectUtility.definition.workflow.loader(models, {
                    workflowItemDefinition: p.secondPublishUnflat._id, 
                    flatten:true
                }, function(err, models, loaded) {
                    logger.silly('[top] second publish: ' + JSON.stringify(loaded, null, 2));
                    p.secondPublish = loaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[top] reloadign second draft');
                projectUtility.definition.workflow.loader(models, {
                    workflowItemDefinition: p.createdSecondDraft,
                    flatten: false
                }, function(err, models, loaded) {
                    p.loadedDraft = loaded;
                    cb(err, models, p);
                });
            },

        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.published, ' first publish not published');
            test.ok(!p.published.isDraftOf, 'first publish is still marked as draft');
            test.ok(p.secondPublish, ' second publish not published');
            test.ok(!p.secondPublish.isDraftOf, 'second publish is still marked as draft');

            //make sure second publish has updates
            test.equal(p.secondPublish.title, 'altered title', 'title not updated');
            test.ok(p.secondPublish.items, ' no items found');
            test.ok(p.secondPublish.items[0], 'container not loaded');
            test.equal(p.secondPublish.items[0].title, 'altered container title', 'container title not updated');
            var newSection = _.find(p.secondPublish.items, function(section) {
                return section.title == '*** new content';
            });
            test.ok(newSection, 'new section not found');


            //make sure draft is closed
            test.ok(p.loadedDraft.deleted);
            test.ok(_.all(p.loadedDraft.rangedData, function(range) {
                return range.deleted;
            }));
            test.ok(_.all(p.loadedDraft.sections, function(section) {
                return section.deleted;
            }));
            test.ok(_.chain(p.loadedDraft.items)
                .map(function(item) {
                    return item.rangedData;
                })
                .reduce(function(a, b) {
                    return a.concat(b);
                }, [])
                .all(function(range) {
                    return range.deleted;
                })
                .value())

            test.done();
        });
    },
};
