module.exports = exports = {
	Alert: require("./alert"),
	Auth: require("./auth"),
	Complaint: require("./complaint"),
	Customer: require("./customer"),
	Modeler: require("./modeler"),
	OperatingProcedure: require("./operatingProcedure"),
	Quiz: require("./quiz"),
	Training: require("./training"),
	Workflow: require("./workflow"),
};
