var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    projectUtility = require('../../util');
require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'manage with bad customer user id, valid object id': function(test) {
        async.waterfall([

            function(cb) {
                //make a customer
                projectUtility.customer.create({
                    customer: {
                        name: 'test customer',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 1,
                        numberOfEmployees: 1,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, customer) {
                    logger.silly(util.inspect(customer));
                    logger.silly('customer made');
                    cb(err, models, {
                        customer: customer
                    });
                });
            },
            function(models, p, cb) {
                projectUtility.auth.manageCustomerUser(models, {
                    user: new mongoose.Types.ObjectId(),
                    customer: p.customer,
                    roles: ['Owner'],
                    createdBy: 'test',
                    modifiedBy: 'test',
                    customerUser: {
                        employeeClass: ['test']
                    }
                }, function(err, models, customerUser) {
                    logger.silly('user: ' + util.inspect(customerUser));
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.manageComplaint(models, {
                    customerUser: new mongoose.Types.ObjectId(),
                    complaint: {
                        text: 'test complaint',
                        state: 'test',
                        product: 'test',
                        issueType: 'test',
                        state: 'TT',
                        status: 'open',
                        zip: 'xxxxx',
                        submittedVia: 'Unit Test',
                        dateReceived: moment()
                            .toDate(),
                        customer: p.customer._id,
                        createdByCustomerUser: p.customerUser._id,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, complaint) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    p.complaint = complaint;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.loadComplaint(models, {
                    complaint: p.complaint._id
                }, function(err, models, complaint) {
                    p.reloadedComplaint = complaint;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            test.ok(err);
            test.done();
        });
    },
    'manage with bad customer user id, invalid object id': function(test) {
        async.waterfall([

            function(cb) {
                //make a customer
                projectUtility.customer.create({
                    customer: {
                        name: 'test customer',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 1,
                        numberOfEmployees: 1,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, customer) {
                    logger.silly(util.inspect(customer));
                    logger.silly('customer made');
                    cb(err, models, {
                        customer: customer
                    });
                });
            },
            function(models, p, cb) {
                projectUtility.auth.manageCustomerUser(models, {
                    user: new mongoose.Types.ObjectId(),
                    customer: p.customer,
                    roles: ['Owner'],
                    createdBy: 'test',
                    modifiedBy: 'test',
                    customerUser: {
                        employeeClass: ['test']
                    }
                }, function(err, models, customerUser) {
                    logger.silly('user: ' + util.inspect(customerUser));
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.manageComplaint(models, {
                    customerUser: 'junk',
                    complaint: {
                        text: 'test complaint',
                        state: 'test',
                        product: 'test',
                        issueType: 'test',
                        state: 'TT',
                        status: 'open',
                        zip: 'xxxxx',
                        submittedVia: 'Unit Test',
                        dateReceived: moment()
                            .toDate(),
                        customer: p.customer._id,
                        createdByCustomerUser: 'junk'
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, complaint) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    p.complaint = complaint;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.loadComplaint(models, {
                    complaint: p.complaint._id
                }, function(err, models, complaint) {
                    p.reloadedComplaint = complaint;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            test.ok(err);
            test.done();
        });
    },
    'manage with good customer user id, load with bad id, valid object id': function(test) {
        async.waterfall([

            function(cb) {
                //make a customer
                projectUtility.customer.create({
                    customer: {
                        name: 'test customer',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 1,
                        numberOfEmployees: 1,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, customer) {
                    logger.silly(util.inspect(customer));
                    logger.silly('customer made');
                    cb(err, models, {
                        customer: customer
                    });
                });
            },
            function(models, p, cb) {
                projectUtility.auth.manageCustomerUser(models, {
                    user: new mongoose.Types.ObjectId(),
                    customer: p.customer,
                    roles: ['Owner'],
                    createdBy: 'test',
                    modifiedBy: 'test',
                    customerUser: {
                        employeeClass: ['test']
                    }
                }, function(err, models, customerUser) {
                    logger.silly('user: ' + util.inspect(customerUser));
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.manageComplaint(models, {
                    customerUser: p.customerUser._id,
                    complaint: {
                        text: 'test complaint',
                        state: 'test',
                        product: 'test',
                        issueType: 'test',
                        state: 'TT',
                        status: 'open',
                        zip: 'xxxxx',
                        submittedVia: 'Unit Test',
                        dateReceived: moment()
                            .toDate(),
                        customer: p.customer._id,
                        createdByCustomerUser: p.customerUser._id,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, complaint) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    p.complaint = complaint;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.loadComplaint(models, {
                    complaint: new mongoose.Types.ObjectId()
                }, function(err, models, complaint) {
                    p.reloadedComplaint = complaint;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            test.ok(err);
            test.done();
        });
    },
    'manage with good customer user id, load with bad id, invalid object id': function(test) {
        async.waterfall([

            function(cb) {
                //make a customer
                projectUtility.customer.create({
                    customer: {
                        name: 'test customer',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 1,
                        numberOfEmployees: 1,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, customer) {
                    logger.silly(util.inspect(customer));
                    logger.silly('customer made');
                    cb(err, models, {
                        customer: customer
                    });
                });
            },
            function(models, p, cb) {
                projectUtility.auth.manageCustomerUser(models, {
                    user: new mongoose.Types.ObjectId(),
                    customer: p.customer,
                    roles: ['Owner'],
                    createdBy: 'test',
                    modifiedBy: 'test',
                    customerUser: {
                        employeeClass: ['test']
                    }
                }, function(err, models, customerUser) {
                    logger.silly('user: ' + util.inspect(customerUser));
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.manageComplaint(models, {
                    customerUser: p.customerUser._id,
                    complaint: {
                        text: 'test complaint',
                        state: 'test',
                        product: 'test',
                        issueType: 'test',
                        state: 'TT',
                        status: 'open',
                        zip: 'xxxxx',
                        submittedVia: 'Unit Test',
                        dateReceived: moment()
                            .toDate(),
                        customer: p.customer._id,
                        createdByCustomerUser: p.customerUser._id,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test',

                }, function(err, models, complaint) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    p.complaint = complaint;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.complaint.loadComplaint(models, {
                    complaint: 'junk'
                }, function(err, models, complaint) {
                    p.reloadedComplaint = complaint;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            test.ok(err);
            test.done();
        });
    },
};
