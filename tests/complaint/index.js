module.exports = exports = {
	"CreateAndLoadComplaint": require("./createAndLoadComplaint"),
	"CreateComplaint": require("./createComplaint"),
	"Fault": require("./fault"),
	"UpdateComplaint": require("./updateComplaint"),
};
