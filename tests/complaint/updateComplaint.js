var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');


require('../test-config');
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'update a complaint': function(test) {
        async.waterfall([
            function(cb){
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.complaints.manageComplaint(),
            testUtility.parts.complaints.loadComplaint(),
            function(models, p, cb) {
                p.reloadedComplaint.issueType = 'retest';
                p.reloadedComplaint.status = 'openish';
                logger.silly('managing complaint update');
                projectUtility.complaint.manageComplaint(models, {
                    customerUser: p.customerUser._id,
                    customer: p.customer._id,
                    complaint: p.reloadedComplaint,
                    modifiedBy: 'test',
                }, function(err, models, complaint) {
                    p.updatedComplaint = complaint;
                    cb(err, models, p);
                });
            },
            function(models, p, cb){
                testUtility.parts.complaints.loadComplaint({
                    complaint: p.updatedComplaint._id,
                    member: 'reloadedUpdatedComplaint'
                })(models, p, cb);
            }
        ], function(err, models, p) {
            test.ok(p);
            test.ok(p.customer);
            test.ok(p.customerUser);
            test.ok(p.complaint);
            test.ok(p.reloadedComplaint);
            test.equal(p.complaint.text, p.reloadedComplaint.text);
            test.equal(p.complaint.text, p.updatedComplaint.text);
            test.equal(p.updatedComplaint.changes.length, 3);
            logger.silly('before done: ' + util.inspect(p.reloadedUpdatedComplaint));
            test.done();
        });
    },
};
