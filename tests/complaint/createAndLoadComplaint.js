var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');
require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Make a complaint, then reload the complaint': function(test) {
        async.waterfall([
            function(cb){
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.complaints.manageComplaint(),
            testUtility.parts.complaints.loadComplaint(),
        ], function(err, models, p) {
            test.ok(p);
            test.ok(p.customer);
            test.ok(p.customerUser);
            test.ok(p.complaint);
            test.ok(p.reloadedComplaint);
            test.equal(p.complaint.text, p.reloadedComplaint.text);
            test.equal(p.complaint.status, p.reloadedComplaint.status);
            test.equal(p.complaint.changes.length, p.reloadedComplaint.changes.length);
            logger.silly('before done: ' + util.inspect(p.complaint));
            logger.silly('before done: ' + util.inspect(p.reloadedComplaint));
            test.done();
        });
    },
};
