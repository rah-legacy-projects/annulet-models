var memwatch = require('memwatch-next');
module.exports = exports = function(){
    var models, p, cb;
    if(arguments.length == 1){
        cb = arguments[0];
        p = {heapdiff: new memwatch.HeapDiff()};
        return cb(null, p);
    }else if(arguments.length ==2){
        p = arguments[0];
        cb = arguments[1];
    }else if (arguments.length ==3){
        models = arguments[0];
        p = arguments[1];
        cb = arguments[2];
    }
    p.heapdiff = new memwatch.HeapDiff();
    cb(null, models, p);
};
