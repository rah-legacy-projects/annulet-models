module.exports = exports = {
	alert: require("./alert"),
	auxiliaryDocument: require("./auxiliaryDocument"),
	complaints: require("./complaints"),
	customer: require("./customer"),
	customerUser: require("./customerUser"),
	itemActivity: require("./itemActivity"),
	operatingProcedures: require("./operatingProcedures"),
	quiz: require("./quiz"),
	training: require("./training"),
	userCustomers: require("./userCustomers"),
	workflowItem: require("./workflowItem"),
};
