var logger = require('winston'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    util = require('util'),
    projectUtility = require('../../../util');

module.exports = exports = function(options) {
    options = options || {};

    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'customerUser',
            user: new mongoose.Types.ObjectId(),
            customer: p.customer._id,
            useModels: true
        });
        projectUtility.auth.manageCustomerUser(options.useModels ? models : null, {
            user: options.user,
            customer: options.customer,
            roles: ['Owner'],
            modifiedBy: 'test',
            createdBy: 'test',
            customerUser: {
                employeeClass: ['test']
            }
        }, function(err, models, customerUser) {
            p[options.member] = customerUser;
            cb(err, models, p);
        });
    };
};
