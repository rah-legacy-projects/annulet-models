var logger = require('winston'),
    util = require('util'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
            customerUser: p.customerUser._id,
            customer: p.customer,
            includeDismissed:true,
            flatten:true
        }, function(err, models, alerts) {
            logger.silly('stamped alerts: ' + util.inspect(alerts));
            p.alertInstances = alerts;
            cb(err, models, p);
        });

    };
};
