var logger = require('winston'),
    util = require('util'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        projectUtility.instance.alert.dismiss(models, {
            alertRange: p.alertInstances[0]._rangeId,
            actionCustomerUser: p.actionCustomerUser
        }, function(err, models, alert) {
            p.dismissed = alert;
            cb(err, models, p);
        })
    };
};
