var logger = require('winston'),
    projectUtility = require('../../../../util'),
    _ = require('lodash'),
    definition = require('./definition'),
    moment = require('moment');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'flatAlert',
            alert: definition({rangeId: p.draft.rangedData[0]._id, draftId: p.draft._id}),
            actionCustomerUser: p.customerUser,
            flatten: false

        });
        p.original = options.alert;
        projectUtility.definition.alert.draft.update(models, {
            customer: p.customer._id,
            alertDefinition: options.alert,
            actionCustomerUser: options.actionCustomerUser,
            flatten: options.flatten
        }, function(err, models, flatOP) {
            logger.silly('[test - alert] draft updated.');
            p[options.member] = flatOP;
            cb(err, models, p);
        });
    };
};
