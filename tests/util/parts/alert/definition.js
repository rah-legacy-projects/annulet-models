var moment = require('moment');


module.exports = exports = function(options) {

    return {
        markdown: 'test alert',
        alertLevel: 'info',
        _id: options.draftId, //p.draft._id,
        _rangeId: options.rangeId, //
        startDate: moment()
            .startOf('day')
            .subtract(1, 'day'),
        //endDate: moment().startOf('day').add(30, 'day'),
        endDate: 'forever'

    };
};
