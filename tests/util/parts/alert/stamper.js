var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedAlert',
            alertDefinition: p.published._id,
            actionCustomerUser: p.customerUser._id,
            customerUser: p.customerUser._id,
            flatten: false
        });
        logger.silly('stamping instance alert');
        projectUtility.definition.alert.stamper.stamper(models, {
            alertDefinition: options.alertDefinition,
            actionCustomerUser: options.actionCustomerUser,
            customerUser: options.customerUser,
            flatten: options.flatten
        }, function(err, models, stampedAlert) {
            p[options.member] = stampedAlert;
            cb(err, models, p);
        });
    };
};
