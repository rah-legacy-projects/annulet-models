var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../util');
module.exports = exports = function(options) {
    options = options || {};

    return function(ignore, p, cb) {
        logger.silly('[test - customer] about to make customer...');
        options = _.defaults(options, {
            member: 'customer',
            name: 'test customer',
            street1: 'one',
            city: 'city',
            state: 'state',
            zip: 'zip',
            phone: 'phone',
            numberOfLocations: 1,
            numberOfEmployees: 1
        });
        //make a customer
        projectUtility.customer.create({
            customer: {
                name: options.name,
                address: {
                    street1: options.street1,
                    city: options.city,
                    state: options.state,
                    zip: options.zip
                },
                phone: options.zip,
                numberOfLocations: options.numberOfLocations,
                numberOfEmployees: options.numberOfEmployees,
            },
            createdBy: 'test',
            modifiedBy: 'test'

        }, function(err, models, customer) {
            logger.silly('[test - customer] customer made');
            p[options.member] = customer;
            cb(err, models, p);
        });
    };
};
