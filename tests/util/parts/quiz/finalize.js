var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            toFinishMember: 'toFinish'
        });
        logger.silly('finalizing quiz');
        models.instances.quiz.ranged.Attempt.findOne({
            _id: p[options.toFinishMember]._id
        })
            .exec(function(err, q) {
                q.finalizedDate = new Date();
                q.save(function(err, q) {
                    cb(err, models, p);
                });

            });
    };
};
