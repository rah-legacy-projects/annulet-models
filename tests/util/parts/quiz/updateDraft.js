var logger = require('winston'),
    projectUtility = require('../../../../util'),
    _ = require('lodash'),
    definition = require('./definition'),
    moment = require('moment');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'flatOperatingProcedure',
            quiz: definition({rangeId: p.draft.rangedData[0]._id, draftId: p.draft._id}),
            actionCustomerUser: p.customerUser,
            flatten: false

        });
        p.original = options.quiz;
        projectUtility.definition.quiz.draft.update(models, {
            customer: p.customer._id,
            quizDefinition: options.quiz,
            actionCustomerUser: options.actionCustomerUser,
            flatten: options.flatten
        }, function(err, models, flatOP) {
            logger.silly('[test - quiz] draft updated.');
            p[options.member] = flatOP;
            cb(err, models, p);
        });
    };
};
