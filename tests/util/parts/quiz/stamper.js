var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedQuiz',
            quizDefinition: p.published._id,
            actionCustomerUser: p.customerUser._id,
            customerUser: p.customerUser._id,
            flatten: false
        });
        logger.silly('stamping instance quiz');
        projectUtility.definition.quiz.stamper.stamper(models, {
            quizDefinition: options.quizDefinition,
            actionCustomerUser: options.actionCustomerUser,
            customerUser: options.customerUser,
        }, function(err, models, stampedQuiz) {
            p[options.member] = stampedQuiz;
            cb(err, models, p);
        });
    };
};
