var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        logger.silly('scoring quiz');
        projectUtility.instance.quiz.scorer(models, {
            customerUser: p.customerUser,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test'
            },
            quizInstance: p.stampedQuiz._id
        }, function(err, models, scores) {
            p.scores = scores;
            cb(err, models, p);
        });
    };
};
