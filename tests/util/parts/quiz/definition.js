var moment = require('moment');


module.exports = exports = function(options) {


    return {
        _id: options.draftId, //p.draft._id,
        _rangeId: options.rangeId, //       questionQuota: 5,
        startDate: moment()
            .startOf('day')
            .subtract(1, 'day'),
        //endDate: moment().startOf('day').add(30, 'day'),
        endDate: 'forever',
        directions: 'do some p',
        title: 'test quiz',
        passingPercentage: 70,
        questionQuota: 5,
        shortName: 'testQuiz',
        questions: [{
            __t: 'definitions.quiz.questionTypes.OneChoice',
            _rangeType: 'definitions.quiz.ranged.questionTypes.OneChoice',
            questionText: '1',
            answerQuota: 4,
            required: false,
            answers: [{
                answerText: '1 one',
                isCorrect: true,
            }, {
                answerText: '1 two',
                isCorrect: false
            }, {
                answerText: '1 three',
                isCorrect: false
            }, {
                answerText: '1 four',
                isCorrect: false
            }, ]
        }, {
            __t: 'definitions.quiz.questionTypes.OneChoice',
            _rangeType: 'definitions.quiz.ranged.questionTypes.OneChoice',
            questionText: '2',
            answerQuota: 4,
            required: false,
            answers: [{
                answerText: '2 one',
                isCorrect: true
            }, {
                answerText: '2 two',
                isCorrect: false
            }, {
                answerText: '2 three',
                isCorrect: false
            }, {
                answerText: '2 four',
                isCorrect: false
            }, ]
        }, {
            __t: 'definitions.quiz.questionTypes.MultipleChoice',
            _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
            questionText: '3',
            answerQuota: 4,
            required: false,
            answers: [{
                answerText: '3 one',
                isCorrect: true
            }, {
                answerText: '3 two',
                isCorrect: false
            }, {
                answerText: '3 three',
                isCorrect: false
            }, {
                answerText: '3 four',
                isCorrect: false
            }, ]
        }, {
            __t: 'definitions.quiz.questionTypes.MultipleChoice',
            _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
            required: false,
            questionText: '4',
            answerQuota: 4,
            answers: [{
                answerText: '4 one',
                isCorrect: true
            }, {
                answerText: '4 two',
                isCorrect: false
            }, {
                answerText: '4 three',
                isCorrect: false
            }, {
                answerText: '4 four',
                isCorrect: false
            }, ]
        }, {
            __t: 'definitions.quiz.questionTypes.MultipleChoice',
            _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
            required: false,
            questionText: '5',
            answerQuota: 4,
            answers: [{
                answerText: '5 one',
                isCorrect: true
            }, {
                answerText: '5 two',
                isCorrect: false
            }, {
                answerText: '5 three',
                isCorrect: false
            }, {
                answerText: '5 four',
                isCorrect: false
            }, ]
        }, {
            __t: 'definitions.quiz.questionTypes.MultipleChoice',
            _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
            required: false,
            questionText: '6',
            answerQuota: 4,
            answers: [{
                answerText: '6 one',
                isCorrect: true
            }, {
                answerText: '6 two',
                isCorrect: false
            }, {
                answerText: '6 three',
                isCorrect: false
            }, {
                answerText: '6 four',
                isCorrect: false
            }, ]
        }]
    };
};
