var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        var quiz = {
            questionQuota: 5,
            directions: 'do some p',
            title: 'test quiz',
            passingPercentage: 70,
            questions: [{
                questionType: 'MultipleChoice',
                question: {
                    questionText: '1',
                    answerQuota: 4,
                    required: false,
                    answers: [{
                        answerText: '1 one',
                        isCorrect: true,
                    }, {
                        answerText: '1 two',
                        isCorrect: false
                    }, {
                        answerText: '1 three',
                        isCorrect: false
                    }, {
                        answerText: '1 four',
                        isCorrect: false
                    }, ]
                }
            }, {
                questionType: 'MultipleChoice',
                question: {
                    questionText: '2',
                    answerQuota: 4,
                    required: false,
                    answers: [{
                        answerText: '2 one',
                        isCorrect: true
                    }, {
                        answerText: '2 two',
                        isCorrect: false
                    }, {
                        answerText: '2 three',
                        isCorrect: false
                    }, {
                        answerText: '2 four',
                        isCorrect: false
                    }, ]
                }
            }, {
                questionType: 'MultipleChoice',
                question: {
                    questionText: '3',
                    answerQuota: 4,
                    required: false,
                    answers: [{
                        answerText: '3 one',
                        isCorrect: true
                    }, {
                        answerText: '3 two',
                        isCorrect: false
                    }, {
                        answerText: '3 three',
                        isCorrect: false
                    }, {
                        answerText: '3 four',
                        isCorrect: false
                    }, ]
                }
            }, {
                questionType: 'MultipleChoice',
                question: {
                    required: false,
                    questionText: '4',
                    answerQuota: 4,
                    answers: [{
                        answerText: '4 one',
                        isCorrect: true
                    }, {
                        answerText: '4 two',
                        isCorrect: false
                    }, {
                        answerText: '4 three',
                        isCorrect: false
                    }, {
                        answerText: '4 four',
                        isCorrect: false
                    }, ]
                }
            }, {
                questionType: 'MultipleChoice',
                question: {
                    required: false,
                    questionText: '5',
                    answerQuota: 4,
                    answers: [{
                        answerText: '5 one',
                        isCorrect: true
                    }, {
                        answerText: '5 two',
                        isCorrect: false
                    }, {
                        answerText: '5 three',
                        isCorrect: false
                    }, {
                        answerText: '5 four',
                        isCorrect: false
                    }, ]
                }
            }, {
                questionType: 'MultipleChoice',
                question: {
                    required: false,
                    questionText: '6',
                    answerQuota: 4,
                    answers: [{
                        answerText: '6 one',
                        isCorrect: true
                    }, {
                        answerText: '6 two',
                        isCorrect: false
                    }, {
                        answerText: '6 three',
                        isCorrect: false
                    }, {
                        answerText: '6 four',
                        isCorrect: false
                    }, ]
                }
            }]
        };

        projectUtility.definition.quiz.creator(models, {
            customer: p.customer._id,
            customerUser: p.customerUser,
            quizDefinition: quiz
        }, function(err, models, quizDefinition) {
            p.quizDefinition = quizDefinition;
            cb(err, models, p);
        });
    };
};
