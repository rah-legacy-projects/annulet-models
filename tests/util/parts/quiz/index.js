module.exports = exports = {
	createDraft: require("./createDraft"),
	definition: require("./definition"),
	finalize: require("./finalize"),
	makeInstanceAnswers: require("./makeInstanceAnswers"),
	publishDraft: require("./publishDraft"),
	stamper: require("./stamper"),
	updateDraft: require("./updateDraft"),
};
