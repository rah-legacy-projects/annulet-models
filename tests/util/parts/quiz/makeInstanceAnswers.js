var logger = require('winston'),
    util = require('util'),
    async = require('async'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            passFail: 'pass',
            toFinishMember: 'toFinish'
        });
        logger.silly('making answers on the quiz for all questions');
        async.parallel(_.map(p[options.toFinishMember].questions, function(qw) {
            return function(cb) {
                logger.silly('finishing ' + qw.questionText);
                var correctAnswer = _.find(qw.answers, function(answer) {
                    return /one/i.test(answer);
                });
                var incorrectAnswer = _.find(qw.answers, function(answer) {
                    return !/one/i.test(answer);
                });

                logger.silly('correct: ' + util.inspect(correctAnswer));
                logger.silly('incorrect: ' + util.inspect(incorrectAnswer));

                logger.silly('qw: ' + util.inspect(qw));
                if (/OneChoice/.test(qw.__t)) {
                    qw.saveAnswers({
                        _id: (options.passFail == 'pass' ? correctAnswer : incorrectAnswer)
                            ._id.toString()
                    }, function(err, a) {
                        cb(err, a);
                    });
                } else if (/MultipleChoice/.test(qw.__t)) {
                    qw.saveAnswers([{
                        _id: (options.passFail == 'pass' ? correctAnswer : incorrectAnswer)
                            ._id.toString()
                    }], function(err, a) {
                        cb(err, a);
                    });
                }else{
                    logger.error("tf not supported in test yet");
                }
            }
        }), function(err, as) {
            cb(err, models, p);
        });

    };
};
