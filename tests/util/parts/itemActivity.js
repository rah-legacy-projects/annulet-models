var _ = require('lodash');
module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {

        });
        models.tracking.ItemActivity.find(options.query)
            .exec(function(err, activities) {
                p.activities = activities;
                cb(err, models, p);
            });
    };
};
