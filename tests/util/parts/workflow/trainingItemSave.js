var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {

        //make a workflow with that training in it
        projectUtility.definition.workflow.itemSave(models, {
            customerUser: p.customerUser,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test@test.test'
            },
            customer: p.customer._id,
            workflowItemDef: {
                name: 'root',
                sequence: [{
                    name: 'training 1',
                    _type: 'definitions.workflow.TrainingWorkflowItem',
                    training: p.trainingDefinition._id
                }]
            }
        }, function(err, models, workflowItemDefinitionRoot) {
            logger.silly('saved wf def');
            cb(err, models, _.defaults(p, {
                workflowItemDefinitionRoot: workflowItemDefinitionRoot
            }));
        });

    }
};
