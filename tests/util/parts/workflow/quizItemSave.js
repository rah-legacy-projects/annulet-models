var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        projectUtility.definition.workflow.itemSave(models, {
            customer: p.customer._id,
            customerUser: p.customerUser,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test'
            },
            workflowItemDef: {
                name: 'root',
                sequence: [{
                    name: 'quiz 1',
                    _type: 'definitions.workflow.QuizWorkflowItem',
                    quiz: p.quizDefinition._id
                }]
            }
        }, function(err, models, workflowItemDefinitionRoot) {
            logger.silly('workflow item def root: ' + util.inspect(workflowItemDefinitionRoot));
            cb(err, models, _.defaults(p, {
                workflowItemDefinitionRoot: workflowItemDefinitionRoot
            }));
        });
    };
};
