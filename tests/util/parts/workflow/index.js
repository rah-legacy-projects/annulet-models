module.exports = exports = {
	loadDefinition: require("./loadDefinition"),
	loadInstance: require("./loadInstance"),
	makeWorkflow: require("./makeWorkflow"),
	quizItemSave: require("./quizItemSave"),
	stamp: require("./stamp"),
	trainingItemSave: require("./trainingItemSave"),
};
