var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        logger.silly('stamping wf one');
        //instantiate an instance with the quiz
        projectUtility.definition.workflow.stamper(models, {
            customerUser: p.customerUser,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test'
            },
            workflowDefinition: p.workflowDefinition
        }, function(err, models, stampedWorkflow, stampedRoot) {
            logger.silly('stamped root: ' + util.inspect(stampedRoot));
            cb(err, models, _.defaults(p, {
                stampedWorkflow: stampedWorkflow,
                stampedRoot: stampedRoot
            }));
        });
    };
};
