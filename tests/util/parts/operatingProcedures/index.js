module.exports = exports = {
	createDraft: require("./createDraft"),
	definition: require("./definition"),
	instanceLoader: require("./instanceLoader"),
	publishDraft: require("./publishDraft"),
	stamper: require("./stamper"),
	stampOperatingProceduresByCustomerUser: require("./stampOperatingProceduresByCustomerUser"),
	status: require("./status"),
	updateDraft: require("./updateDraft"),
};
