var moment = require('moment');


module.exports = exports = function(options) {
    return {
        title: 'test operatingProcedure',
        description: 'test description',
        shortName: 'test OP',
        _id: options.draftId, //p.draft._id,
        _rangeId:options.rangeId, //
        startDate: moment()
            .startOf('day')
            .subtract(1, 'day'),
        endDate: 'forever',
        sections: [{
                __t: 'definitions.operatingProcedure.sectionTypes.Overview',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Overview',
                title: 'Introduction',
                content: 'This is an introduction!'
            }, {
                __t: 'definitions.operatingProcedure.sectionTypes.RelatedProcedures',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.RelatedProcedures',
                title: 'Related Procedures!',
                content: 'No related procedures.'
            }, {
                __t: 'definitions.operatingProcedure.sectionTypes.Definitions',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Definitions',
                title: 'Definitions',
                content: 'Some definitions... for your enjoyment',
                terms: [{
                    term: 'test',
                    meaning: 'test meaning'
                }, {
                    term: 'test again',
                    meaning: ' test meaning again'
                }]
            }, {
                __t: 'definitions.operatingProcedure.sectionTypes.Content',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content',
                title: 'Some Content',
                content: 'This is some content on this test.'
            }, {
                __t: 'definitions.operatingProcedure.sectionTypes.SectionedContent',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.SectionedContent',
                title: 'Some _sectioned_ content!',
                subSections: [{
                    title: 'subsection 1',
                    content: 'pursuant to something'
                }, {
                    title: 'subsection 2',
                    content: 'another subsection'
                }]
            }

        ]
    }
};
