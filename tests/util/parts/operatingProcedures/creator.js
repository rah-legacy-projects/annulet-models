var projectUtility = require('../../../../util'),
    _ = require('lodash');
module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            title: 'test operating procedure',
            description: 'test description',
            customer: p.customer._id,
            member: 'operatingProcedureDefinition'
        });
        var operatingProcedure = {
            title: options.title,
            customer: options.customer,
            description: options.description,
            sections: [{
                    __t: 'definitions.operatingProcedure.sectionTypes.Overview',
                    title: 'Introduction',
                    content: 'This is an introduction!'
                }, {
                    __t: 'definitions.operatingProcedure.sectionTypes.RelatedProcedures',
                    title: 'Related Procedures!',
                    content: 'No related procedures.'
                }, {
                    __t: 'definitions.operatingProcedure.sectionTypes.Definitions',
                    title: 'Definitions',
                    content: 'Some definitions... for your enjoyment',
                    terms: [{
                        term: 'test',
                        meaning: 'test meaning'
                    }, {
                        term: 'test again',
                        meaning: ' test meaning again'
                    }]
                }, {
                    __t: 'definitions.operatingProcedure.sectionTypes.Content',
                    title: 'Some Content',
                    content: 'This is some content on this test.'
                }, {
                    __t: 'definitions.operatingProcedure.sectionTypes.SectionedContent',
                    title: 'Some _sectioned_ content!',
                    subSections: [{
                        title: 'subsection 1',
                        content: 'pursuant to something'
                    }, {
                        title: 'subsection 2',
                        content: 'another subsection'
                    }]
                }

            ]
        };
        projectUtility.definition.operatingProcedure.creator(models, {
            customer: p.customer._id,
            operatingProcedureDefinition: operatingProcedure,
            customerUser: p.customerUser,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test@test.test'
            }
        }, function(err, models, operatingProcedureDefinition) {
            p[options.member] = operatingProcedureDefinition;
            cb(err, models, p);
        });
    }
}
