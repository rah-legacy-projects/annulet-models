var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedOperatingProcedure'
        });
        logger.silly(util.inspect(_.keys(models)));
        projectUtility.definition.operatingProcedure.stamper.stampOperatingProceduresByCustomerUser(models, {
            customerUser: p.customerUser._id,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test@test.test'
            }
        }, function(err, models, operatingProcedureInstances) {
            cb(err, models, _.defaults(p, {
                operatingProcedureInstances: operatingProcedureInstances
            }));
        });
    };

};
