var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            instance: p.operatingProcedureInstance
        });
        projectUtility.instance.operatingProcedure.loader(models, {
            customerUser: p.customerUser._id,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test@test.test'
            },
            operatingProcedureInstance: options.instance
        }, function(err, models, loaded) {
            p.loadedInstance = loaded;
            cb(err, models, p);
        });
    };

};
