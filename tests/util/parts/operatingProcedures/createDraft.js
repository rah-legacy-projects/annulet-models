var logger = require('winston'),
    _ =require('lodash'),
    util = require('util'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {

    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'draft',
            operatingProcedureDefinitionId: 'new'
        });
        //make a draft of the new thing
        projectUtility.definition.operatingProcedure.draft.create(models, {
            customer: p.customer._id,
            operatingProcedureDefinition: options.operatingProcedureDefinitionId,
            actionCustomerUser: p.customerUser
        }, function(err, models, draft) {
            p[options.member] = draft;
            logger.silly('[test - create draft op] draft created.');
            cb(err, models, p);
        });
    };
};
