var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedOperatingProcedure',
            operatingProcedureDefinition: p.published._id,
            actionCustomerUser: p.customerUser._id,
            customerUser: p.customerUser._id,
            flatten: false
        });
        logger.silly('stamping instance operatingProcedure');
        projectUtility.definition.operatingProcedure.stamper.stamper(models, {
            operatingProcedureDefinition: options.operatingProcedureDefinition,
            actionCustomerUser: options.actionCustomerUser,
            customerUser: options.customerUser,
            flatten: options.flatten
        }, function(err, models, stampedOperatingProcedure) {
            p[options.member] = stampedOperatingProcedure;
            cb(err, models, p);
        });
    };
};
