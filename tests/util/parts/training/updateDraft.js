var logger = require('winston'),
    projectUtility = require('../../../../util'),
    _ = require('lodash'),
    definition = require('./definition'),
    moment = require('moment');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'flatTraining',
            training: definition({rangeId: p.draft.rangedData[0]._id, draftId: p.draft._id}),
            actionCustomerUser: p.customerUser,
            flatten: false

        });
        p.original = options.training;
        projectUtility.definition.training.draft.update(models, {
            customer: p.customer._id,
            trainingDefinition: options.training,
            actionCustomerUser: options.actionCustomerUser,
            flatten: options.flatten
        }, function(err, models, flatOP) {
            logger.silly('[test - op] draft updated.');
            p[options.member] = flatOP;
            cb(err, models, p);
        });
    };
};
