var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedQuiz'
        });
        var training = {
            title: 'test training',
            customer: p.customer._id,
            sections: [{
                __t: 'definitions.training.sectionTypes.Content',
                title: 'Introduction',
                content: 'Here is some content!'
            }, {
                __t: 'definitions.training.sectionTypes.MarkdownContent',
                title: 'Related Procedures!',
                markdown: '#hi.'
            }]
        };
        require('../../../../util/definition/training/creator')(models, {
            customerUser: p.customerUser,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test@test.test'
            },
            customer: p.customer._id,
            trainingDefinition: training
        }, function(err, models, trainingDefinition) {
            p.trainingDefinition = trainingDefinition;
            cb(err, models, p);
        });
    };
};
