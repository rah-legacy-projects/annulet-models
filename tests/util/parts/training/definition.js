var moment = require('moment');


module.exports = exports = function(options) {
    return {
        title: 'test training',
        description: 'test description',
        startDate: moment()
            .startOf('day')
            .subtract(1, 'day'),
        //endDate: moment().startOf('day').add(30, 'day'),
        endDate: 'forever',
        _id: options.draftId, //p.draft._id,
        _rangeId: options.rangeId, //
        shortName: 'test',
        sections: [{
            __t: 'definitions.training.sectionTypes.Content',
            _rangeType: 'definitions.training.ranged.sectionTypes.Content',
            title: 'Introduction',
            content: 'Here is some content!'
        }, {
            __t: 'definitions.training.sectionTypes.MarkdownContent',
            _rangeType: 'definitions.training.ranged.sectionTypes.MarkdownContent',
            title: 'Related Procedures!',
            markdown: '#hi.'
        }]
    };
};
