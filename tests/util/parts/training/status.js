var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {});
        projectUtility.instance.training.status(models, {
                customerUser: p.customerUser,
            },
            function(err, models, statusList) {
                logger.silly('status list: ' + util.inspect(statusList));
                p.statusList = statusList;
                cb(err, models, p);
            });
    }

};
