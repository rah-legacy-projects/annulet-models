var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedTraining',
            trainingDefinition: p.published._id,
            actionCustomerUser: p.customerUser._id,
            customerUser: p.customerUser._id,
            flatten: false
        });
        logger.silly('stamping instance training');
        projectUtility.definition.training.stamper.stamper(models, {
            trainingDefinition: options.trainingDefinition,
            actionCustomerUser: options.actionCustomerUser,
            customerUser: options.customerUser,
            flatten: options.flatten
        }, function(err, models, stampedTraining) {
            p[options.member] = stampedTraining;
            cb(err, models, p);
        });
    };
};
