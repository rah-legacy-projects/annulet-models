module.exports = exports = {
	createDraft: require("./createDraft"),
	definition: require("./definition"),
	instanceLoader: require("./instanceLoader"),
	publishDraft: require("./publishDraft"),
	stamper: require("./stamper"),
	status: require("./status"),
	updateDraft: require("./updateDraft"),
};
