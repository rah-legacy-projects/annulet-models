var logger = require('winston'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    util = require('util'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        _.defaults(options, {
            complaint: p.complaint._id,
            member: 'reloadedComplaint'
        });
        projectUtility.complaint.loadComplaint(models, {
            complaint: options.complaint
        }, function(err, models, complaint) {
            p[options.member] = complaint;
            cb(err, models, p);
        });
    }
}
