var logger = require('winston'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    moment = require('moment'),
    util = require('util'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {

        });
        projectUtility.complaint.manageComplaint(models, {
            customerUser: p.customerUser._id,
            complaint: {
                text: 'test complaint',
                state: 'test',
                product: 'test',
                issueType: 'test',
                state: 'TT',
                status: 'open',
                zip: 'xxxxx',
                submittedVia: 'Unit Test',
                dateReceived: moment()
                    .toDate(),
                customer: p.customer._id,
                createdByCustomerUser: p.customerUser._id,
            },
            createdBy: 'test',
            modifiedBy: 'test',

        }, function(err, models, complaint) {
            if (!!err) {
                logger.error(util.inspect(err));
            }
            p.complaint = complaint;
            cb(err, models, p);
        });
    };
}
