var logger = require('winston'),
    projectUtility = require('../../../../util'),
    _ = require('lodash'),
    definition = require('./definition'),
    util = require('util'),
    moment = require('moment');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'workflowItemDefinition',
            workflowItemDefinition: !!p.draft ? definition({rangeId: p.draft.rangedData[0]._id, draftId: p.draft._id}) : null,
            actionCustomerUser: p.customerUser,
            flatten: false

        });

        p.original = options.workflowItemDefinition;
        projectUtility.definition.workflow.draft.update(models, {
            customer: p.customer._id,
            workflowItemDefinition: options.workflowItemDefinition,
            actionCustomerUser: options.actionCustomerUser,
            flatten: options.flatten
        }, function(err, models, workflowItemDefinition) {
            logger.silly('[test - wf] draft updated.');
            p[options.member] = workflowItemDefinition;
            cb(err, models, p);
        });
    };
};
