var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'workflowDefinition',
            name: 'test workflow'
        });
        logger.silly('making workflow');
        new models.definitions.workflow.Workflow({
            name: options.name,
            customerUser: p.customerUser,
            user: {
                firstName: 'test',
                lastName: 'test',
                email: 'test'
            },
            rootItem: p.workflowItemDefinitionRoot,
            employeeClass: ['test'],
            customer: p.customer._id
        })
            .save(function(err, workflowDefinition) {
                p[options.member] = workflowDefinition;
                cb(err, models, p);
            });
    };
};
