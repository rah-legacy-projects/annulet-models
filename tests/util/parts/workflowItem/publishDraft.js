var logger = require('winston'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');


module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'published',
            draftId: !!p.draft ? p.draft._id:null
        });
        //publish the draft
        projectUtility.definition.workflow.draft.publish(models, {
            workflowItemDefinition: options.draftId,
            customer: p.customer._id
        }, function(err, models, published) {
            p[options.member] = published;
            cb(err, models, p);
        });
    };
};
