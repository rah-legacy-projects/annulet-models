var moment = require('moment'),
    _ = require('lodash'),
    ObjectId = require('mongoose')
    .Types.ObjectId;


module.exports = exports = function(options) {
    options = options || {};
    options = _.defaults(options, {
        trainingId: new ObjectId(),
        quizId: new ObjectId(),
        title: 'test workflowItem'
    });
    return {
        startDate: moment()
            .startOf('day')
            .subtract(1, 'day')
            .toDate(),
            
        //endDate: moment().startOf('day').add(30, 'day').toDate(),
        endDate: 'forever',
        employeeClass: ['test'],
        _id: options.draftId, //p.draft._id,
        _rangeId: options.rangeId, //
        title: '$$#H$IUHIH#$IUH ROOT', //options.title,
        description: '###!!!! ROOT',
        shortName: 'rootShort',
        __t: 'definitions.workflow.workflowItemTypes.Root',
        _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Root',
        items: [{
            title: 'container',
            __t: 'definitions.workflow.workflowItemTypes.Container',
            _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Container',
            sequence: [{
                title: 'training 1',
                __t: 'definitions.workflow.workflowItemTypes.Training',
                _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Training',
                training: options.trainingId
            }, {
                title: 'quiz 1',
                __t: 'definitions.workflow.workflowItemTypes.Quiz',
                _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Quiz',
                quiz: options.quizId
            }]
        }]
    };
};
