var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedWorkflowItem',
            workflowItemDefinition: !!p.published ? p.published._id : null,
            actionCustomerUser: p.customerUser._id,
            customerUser: p.customerUser._id,
            flatten: false
        });
        logger.silly('stamping instance workflowItem');
        projectUtility.definition.workflow.stamper.stamper(models, {
            workflowItemDefinition: options.workflowItemDefinition,
            actionCustomerUser: options.actionCustomerUser,
            customerUser: options.customerUser,
            flatten: options.flatten
        }, function(err, models, stampedWorkflowItem) {
            p[options.member] = stampedWorkflowItem;
            cb(err, models, p);
        });
    };
};
