module.exports = exports = {
	createDraft: require("./createDraft"),
	definition: require("./definition"),
	loadDefinition: require("./loadDefinition"),
	loadInstance: require("./loadInstance"),
	makeWorkflow: require("./makeWorkflow"),
	publishDraft: require("./publishDraft"),
	stamper: require("./stamper"),
	updateDraft: require("./updateDraft"),
};
