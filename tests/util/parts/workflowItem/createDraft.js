var logger = require('winston'),
    _ =require('lodash'),
    util = require('util'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {

    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'draft',
            workflowItemDefinitionId: 'new'
        });
        //make a draft of the new thing
        projectUtility.definition.workflow.draft.create(models, {
            customer: p.customer._id,
            workflowItemDefinition: options.workflowItemDefinitionId,
            actionCustomerUser: p.customerUser
        }, function(err, models, draft) {
            p[options.member] = draft;
            logger.silly('[test - create draft wf] draft created.');
            cb(err, models, p);
        });
    };
};
