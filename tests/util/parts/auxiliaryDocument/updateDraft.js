var logger = require('winston'),
    projectUtility = require('../../../../util'),
    _ = require('lodash'),
    util = require('util'),
    definition = require('./definition'),
    moment = require('moment');

module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'flatAuxiliaryDocument',
            auxiliaryDocument: definition({rangeId: p.draft.rangedData[0]._id, draftId: p.draft._id}),
            actionCustomerUser: p.customerUser,
            flatten: false

        });
        p.original = options.auxiliaryDocument;
        projectUtility.definition.auxiliaryDocument.draft.update(models, {
            customer: p.customer._id,
            auxiliaryDocumentDefinition: options.auxiliaryDocument,
            actionCustomerUser: options.actionCustomerUser,
            flatten: options.flatten
        }, function(err, models, flatAD) {
            p[options.member] = flatAD;
            cb(err, models, p);
        });
    };
};
