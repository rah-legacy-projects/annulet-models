module.exports = exports = {
	adminList: require("./adminList"),
	createDraft: require("./createDraft"),
	definition: require("./definition"),
	publishDraft: require("./publishDraft"),
	stamper: require("./stamper"),
	updateDraft: require("./updateDraft"),
};
