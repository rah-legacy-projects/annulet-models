var moment = require('moment'),
    _ = require('lodash'),
    ObjectId = require('mongoose')
    .Types.ObjectId;


module.exports = exports = function(options) {

    options = _.defaults(options || {}, {
        displayName: 'test auxiliaryDocument',
        description: 'test description',
        extension: 'pdf'
    });

    return {
        displayName: options.displayName,
        file: new ObjectId(),
        description: options.description,
        extension: options.extension,
        _id: options.draftId,
        _rangeId: options.rangeId,
        startDate: moment()
            .startOf('day')
            .subtract(1, 'day'),
        endDate: 'forever'
    }
};
