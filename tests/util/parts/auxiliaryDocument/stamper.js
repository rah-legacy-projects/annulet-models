var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'stampedAuxiliaryDocument',
            auxiliaryDocumentDefinition: p.published._id,
            actionCustomerUser: p.customerUser._id,
            customerUser: p.customerUser._id,
            flatten: false
        });
        logger.silly('stamping instance auxiliaryDocument');
        projectUtility.definition.auxiliaryDocument.stamper.stamper(models, {
            auxiliaryDocumentDefinition: options.auxiliaryDocumentDefinition,
            actionCustomerUser: options.actionCustomerUser,
            customerUser: options.customerUser,
            flatten: options.flatten
        }, function(err, models, stampedAuxiliaryDocument) {
            p[options.member] = stampedAuxiliaryDocument;
            cb(err, models, p);
        });
    };
};
