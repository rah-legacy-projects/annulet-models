var logger = require('winston'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');


module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'adminList',
            actionCustomerUser: p.customerUser
        });
        //publish the draft
        projectUtility.definition.auxiliaryDocument.list(models, {
            actionCustomerUser: options.actionCustomerUser
        }, function(err, models, tuples) {
            p[options.member] = tuples;
            cb(err, models, p);
        });
    };
};
