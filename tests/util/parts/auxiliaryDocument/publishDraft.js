var logger = require('winston'),
    _ = require('lodash'),
    projectUtility = require('../../../../util');


module.exports = exports = function(options) {
    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'published',
            draftId: p.draft._id
        });
        //publish the draft
        projectUtility.definition.auxiliaryDocument.draft.publish(models, {
            auxiliaryDocumentDefinition: options.draftId,
            customer: p.customer._id
        }, function(err, models, published) {
            p[options.member] = published;
            cb(err, models, p);
        });
    };
};
