var logger = require('winston'),
    _ =require('lodash'),
    util = require('util'),
    projectUtility = require('../../../../util');

module.exports = exports = function(options) {

    return function(models, p, cb) {
        options = options || {};
        options = _.defaults(options, {
            member: 'draft',
            auxiliaryDocumentDefinitionId: 'new'
        });
        //make a draft of the new thing
        projectUtility.definition.auxiliaryDocument.draft.create(models, {
            customer: p.customer._id,
            auxiliaryDocumentDefinition: options.auxiliaryDocumentDefinitionId,
            actionCustomerUser: p.customerUser
        }, function(err, models, draft) {
            p[options.member] = draft;
            cb(err, models, p);
        });
    };
};
