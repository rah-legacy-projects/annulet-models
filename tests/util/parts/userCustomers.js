var logger = require('winston'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    util = require('util'),
    projectUtility = require('../../../util');


module.exports = exports = function(options) {
    options = options || {};
    return function(models, p, cb) {
        options = _.defaults(options, {
            member: 'customers',
            user: p.user,
        });
        projectUtility.auth.userCustomers({
            user: options.user
        }, function(err, customerIds) {
            p.customers = customerIds;
            cb(err, models, p);
        });
    }
};
