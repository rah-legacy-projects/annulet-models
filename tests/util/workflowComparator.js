var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(test, workflowItemDefinition, workflowItemInstance) {
    logger.debug('[comparator] comparing...');
    var crawler = function(cid) {
        if (cid != workflowItemDefinition) {
            var path = _.getObjectPath(workflowItemDefinition, cid);
            var cii = _.resolveObjectPath(workflowItemInstance, path);
            test.equal(cii.workflowItemDefinition.toString(), cid._id.toString(), 'child ids do not match');
        } else {
            test.equal(workflowItemInstance.workflowItemDefinition.toString(), cid._id.toString(), 'root ids do not match');
        }

        _.each(cid.sequence, function(s) {
            crawler(s);
        });

        _.each(cid.items, function(i) {
            crawler(i);
        });
    };

    crawler(workflowItemDefinition);

}
