module.exports = exports = {
	errorHandler: require("./errorHandler"),
	heapdiffAnalyze: require("./heapdiffAnalyze"),
	heapdiffEnd: require("./heapdiffEnd"),
	heapdiffStart: require("./heapdiffStart"),
	operatingProcedureCompare: require("./operatingProcedureCompare"),
	parts: require("./parts"),
	quizCompare: require("./quizCompare"),
	trainingCompare: require("./trainingCompare"),
	workflowItemCompare: require("./workflowItemCompare"),
};
