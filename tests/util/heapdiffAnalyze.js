var memwatch = require('memwatch-next'),
    logger = require('winston'),
    sizeof = require('sizeof').sizeof,
    prettyBytes = require('pretty-bytes'),
    _ = require('lodash'),
    util = require('util');

module.exports = exports = function() {
    var models, p, cb;
    if (arguments.length == 1) {
        //this can't happen
        throw ('heap diff analyze called without beginning');
    } else if (arguments.length == 2) {
        p = arguments[0];
        cb = arguments[1];
    } else if (arguments.length == 3) {
        models = arguments[0];
        p = arguments[1];
        cb = arguments[2];
    }

    if (!p.diffs) {
        cb('no diffs to analyze');
    }

    var sizeChanges = _.map(p.diffs, function(diff) {
        return diff.change.size;
    });

    var overallChange = _.chain(p.diffs)
        .map(function(diff) {
            return diff.change.size_bytes;
        })
        .reduce(function(a, b) {
            return a + b;
        }, 0)
        .value();

    logger.debug(util.inspect(p.diffs,null, 10))
    logger.info('size changes: ' + util.inspect(sizeChanges));
    logger.info('size of diffs: ' + prettyBytes(sizeof(p.diffs)));
    logger.info('overall change: ' + prettyBytes(overallChange));
    logger.info('avgchange: ' + prettyBytes(overallChange/sizeChanges.length));

    p.diffReport = {
        diffsize: sizeof(p.diffs),
        overallChange: overallChange
    };

    if (!models) {
        cb(null, p);
    } else {
        cb(null, models, p);
    }

};
