var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    moment = require('moment'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(test, definition, instance, testMessagePrefix) {
    logger.debug('[op comparator] comparing...');
    testMessagePrefix = !!testMessagePrefix ? testMessagePrefix + ': ' : null;
    var crawler = function(path) {
        logger.silly('checking path ' + path.join('.'));

        var r = _.resolveObjectPath(definition, path.join('.')) || (path.length == 0 ? definition : null);
        _.chain(r)
            .keys()
            .each(function(key) {
                path.push(key);
                var definitionPart = _.resolveObjectPath(definition, path.join('.'));
                var instancePart = _.resolveObjectPath(instance, path.join('.'));

                if (moment.isMoment(definitionPart)) {
                    test.ok(moment(definitionPart)
                        .isSame(moment(instancePart)), testMessagePrefix + 'dates at ' + path.join('.') + ' were not equal.');
                } else if (!definitionPart) {
                    //logger.warn('definition for ' + path.join('.') + ' did not exist.');
                } else if (!instancePart) {
                    //logger.warn('instance for ' + path.join('.') + ' did not exist.');
                } else if (key == '_id' || key == '_rangeId' || key == '__v') {
                    //skip
                } else if (key == '__t' || key == '_rangeType' || key == '__mt_type') {
                    test.equal(definitionPart.replace('definition', 'instance'), instancePart, testMessagePrefix + 'path ' + path.join('.') + ' had a problem with types');
                } else if (!!definitionPart && !definitionPart.__t && !!_.extractId({
                    part: definitionPart
                }, 'part')) {
                    logger.silly(path.join('.') + ' was determined to be an ID: ' + _.extractId({
                        part: definitionPart
                    }, 'part'));
                    test.equal(definitionPart.toString(), instancePart.toString(), testMessagePrefix + 'path ' + path.join('.') + ' was not equal.');
                } else if (_.isObject(definitionPart) || _.isArray(definitionPart)) {
                    crawler(path);
                } else {
                    test.equal(definitionPart.toString(), instancePart.toString(), testMessagePrefix + 'path ' + path.join('.') + ' was not equal.');
                }

                path.pop();
            })
            .value();
    };

    crawler([]);

}
