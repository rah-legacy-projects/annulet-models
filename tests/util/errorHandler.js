
var logger = require('winston');
process.on('uncaughtException', function(x){
    logger.error('uncaught: ' + x);
    logger.error(x.stack);
});
