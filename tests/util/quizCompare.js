var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    moment = require('moment'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(test, definition, instance, testMessagePrefix) {
    logger.debug('[quiz compare] comparing...');
    testMessagePrefix = !!testMessagePrefix ? testMessagePrefix + ': ' : null;

    //def must be flattened
    
    _.each(instance.questions, function(instanceQuestion){
        var definitionQuestion = _.find(definition.questions, function(definitionQuestion){
            logger.silly('[quiz compare] ' + definitionQuestion._id + ' == ' + instanceQuestion.questionDefinition);
            return definitionQuestion._id.toString() == instanceQuestion.questionDefinition.toString();
        });

        test.ok(definitionQuestion, 'no definition found for question ' + instanceQuestion.questionText);
        if(!!definitionQuestion){
            test.equal(definitionQuestion._rangeId.toString(), instanceQuestion.questionDefinitionRange.toString(), testMessagePrefix+ ': definition and instance to not have the same range ID for question "'+instanceQuestion.questionText+'"');
        }
    });

}
