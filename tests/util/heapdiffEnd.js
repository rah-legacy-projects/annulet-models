var memwatch = require('memwatch-next'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util');

module.exports = exports = function() {
    var models, p, cb;
    if (arguments.length == 1) {
        //this can't happen
        throw ('heap diff end called without beginning');
    } else if (arguments.length == 2) {
        p = arguments[0];
        cb = arguments[1];
    } else if (arguments.length == 3) {
        models = arguments[0];
        p = arguments[1];
        cb = arguments[2];
    }

    var diff = p.heapdiff.end();
    p.diffs = p.diffs || [];
    p.diffs.push(diff);
    if (!models) {
        cb(null, p);
    } else {
        cb(null, models, p);
    }

};
