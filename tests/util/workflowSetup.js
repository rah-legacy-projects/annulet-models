var async = require('async'),
    _ = require('lodash'),
    logger = require('winston'),
    util = require('util'),
    projectUtility = require('../../util');

module.exports = function(models, options, cb) {
    async.waterfall([

        function(cb) {
            models.definitions.workflow.Workflow.findOne({
                name: options.name || 'test workflow'
            })
                .exec(function(err, wf) {
                    if (!!wf) {
                        cb('workflow already exists');
                    } else {
                        cb(err);
                    }
                });
        },
        function(cb) {
            // {{{ make a quiz def
            var quiz = {
                questionQuota: 5,
                directions: 'do some stuff',
                title: 'test quiz',
                passingPercentage: 70,
                questions: [{
                    questionType: 'MultipleChoice',
                    question: {
                        questionText: '1',
                        answerQuota: 4,
                        required: false,
                        answers: [{
                            answerText: '1 one (correct)',
                            isCorrect: true,
                        }, {
                            answerText: '1 two',
                            isCorrect: false
                        }, {
                            answerText: '1 three',
                            isCorrect: false
                        }, {
                            answerText: '1 four',
                            isCorrect: false
                        }, ]
                    }
                }, {
                    questionType: 'MultipleChoice',
                    question: {
                        questionText: '2',
                        answerQuota: 4,
                        required: false,
                        answers: [{
                            answerText: '2 one (correct)',
                            isCorrect: true
                        }, {
                            answerText: '2 two',
                            isCorrect: false
                        }, {
                            answerText: '2 three',
                            isCorrect: false
                        }, {
                            answerText: '2 four',
                            isCorrect: false
                        }, ]
                    }
                }, {
                    questionType: 'MultipleChoice',
                    question: {
                        questionText: '3',
                        answerQuota: 4,
                        required: false,
                        answers: [{
                            answerText: '3 one (correct)',
                            isCorrect: true
                        }, {
                            answerText: '3 two',
                            isCorrect: false
                        }, {
                            answerText: '3 three',
                            isCorrect: false
                        }, {
                            answerText: '3 four',
                            isCorrect: false
                        }, ]
                    }
                }, {
                    questionType: 'MultipleChoice',
                    question: {
                        required: false,
                        questionText: '4',
                        answerQuota: 4,
                        answers: [{
                            answerText: '4 one (correct)',
                            isCorrect: true
                        }, {
                            answerText: '4 two',
                            isCorrect: false
                        }, {
                            answerText: '4 three',
                            isCorrect: false
                        }, {
                            answerText: '4 four',
                            isCorrect: false
                        }, ]
                    }
                }, {
                    questionType: 'MultipleChoice',
                    question: {
                        required: false,
                        questionText: '5',
                        answerQuota: 4,
                        answers: [{
                            answerText: '5 one (correct)',
                            isCorrect: true
                        }, {
                            answerText: '5 two',
                            isCorrect: false
                        }, {
                            answerText: '5 three',
                            isCorrect: false
                        }, {
                            answerText: '5 four',
                            isCorrect: false
                        }, ]
                    }
                }, {
                    questionType: 'MultipleChoice',
                    question: {
                        required: false,
                        questionText: '6',
                        answerQuota: 4,
                        answers: [{
                            answerText: '6 one (correct)',
                            isCorrect: true
                        }, {
                            answerText: '6 two',
                            isCorrect: false
                        }, {
                            answerText: '6 three',
                            isCorrect: false
                        }, {
                            answerText: '6 four',
                            isCorrect: false
                        }, ]
                    }
                }]
            };
            //}}}
            projectUtility.definition.quiz.creator(models, {
                createdBy: 'test',
                modifiedBy: 'test',
                customer: options.customer._id,
                quizDefinition: quiz
            }, function(err, models, quizDefinition) {
                cb(err, {
                    quizDefinition: quizDefinition
                });
            });
        },
        function(p, cb) {
            //{{{ make a training def
            var training = {
                title: 'test training',
                customer: options.customer._id,
                sections: [{
                    __t: 'definitions.training.sectionTypes.MarkdownContent',
                    title: 'Introduction',
                    markdown: 'stuff'
                }, {
                    __t: 'definitions.training.sectionTypes.MarkdownContent',
                    title: 'Overview',
                    markdown: 'stuff'
                }, {
                    __t: 'definitions.training.sectionTypes.MarkdownContent',
                    title: 'Rules of Application',
                    markdown: 'stuff'
                }, {
                    __t: 'definitions.training.sectionTypes.MarkdownContent',
                    title: 'Thou Shalt Not Rules',
                    markdown: 'stuff'
                }, {
                    __t: 'definitions.training.sectionTypes.MarkdownContent',
                    title: 'FAQs and Examples',
                    markdown: 'stuff'
                }]
            };
            //}}}
            projectUtility.definition.training.creator(models, {
                createdBy: 'test',
                modifiedBy: 'test',
                customer: options.customer._id,
                trainingDefinition: training
            }, function(err, models, training) {
                p.training = training;
                cb(err, p);
            });
        },
        function(p, cb) {
            logger.silly('about to save - wf setup with models: ' + util.inspect(_.keys(models.definitions)));
            //save the workflow
            projectUtility.definition.workflow.itemSave(models, {
                createdBy: 'test',
                modifiedBy: 'test',
                customer: options.customer._id,
                workflowItemDef: {
                    name: options.name || 'root',
                    description: 'sample description',
                    _type: 'definitions.workflow.RootWorkflowItem',
                    items: [{
                        name: 'container',
                        _type: 'definitions.workflow.ContainerWorkflowItem',

                        sequence: [{
                            name: 'training 1',
                            _type: 'definitions.workflow.TrainingWorkflowItem',
                            training: p.training._id
                        }, {
                            name: 'quiz 1',
                            _type: 'definitions.workflow.QuizWorkflowItem',
                            quiz: p.quizDefinition._id
                        }]
                    }]
                }
            }, function(err, models, workflowItemDefinitionRoot) {
                logger.silly('saved wf def');
                cb(err, _.defaults(p, {
                    workflowItemDefinitionRoot: workflowItemDefinitionRoot
                }));
            });
        },
        function(p, cb) {
            logger.silly('making workflow');
            new models.definitions.workflow.Workflow({
                createdBy: 'test',
                modifiedBy: 'test',
                name: options.name || 'test',
                description: 'sample descripption',
                rootItem: p.workflowItemDefinitionRoot,
                employeeClass: options.employeeClasses || ['demo'],
                customer: options.customer._id
            })
                .save(function(err, workflowDefinition) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    logger.silly('wf def: ' + util.inspect(workflowDefinition));
                    cb(err, _.defaults(p, {
                        workflowDefinition: workflowDefinition
                    }));
                });
        }
    ], function(err, r) {
        logger.silly('[wf setup] def: ' + util.inspect(r.workflowDefinition));
        cb(null, models, r);
    });
}
