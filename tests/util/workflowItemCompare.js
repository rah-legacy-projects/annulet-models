var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    moment = require('moment'),
    util = require('util');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(test, definition, instance, testMessagePrefix) {
    logger.debug('[wf comparator] comparing...');
    testMessagePrefix = !!testMessagePrefix ? testMessagePrefix + ': ' : null;

    var crawl = function(definition, instance){
        test.equal((definition.items||[]).length, (instance.items||[]).length, testMessagePrefix + ': lengths of items not equal');
        test.equal((definition.sequence||[]).length, (instance.sequence||[]).length, testMessagePrefix + ': lengths of sequences not equal');
        _.each(definition.items, function(defItem){
            var instanceItem = _.find(instance.items, function(instanceItem){
                return instanceItem.itemDefinition.toString() ==  defItem._id.toString();
            });

            test.ok(instanceItem);
            crawl(defItem, instanceItem);
        });

        _.each(definition.sequence, function(defItem){
            var instanceItem = _.find(instance.sequence, function(instanceItem){
                return instanceItem.itemDefinition.toString() ==  defItem._id.toString();
            });

            test.ok(instanceItem);
            crawl(defItem, instanceItem);
        });

        test.equal(definition.description, instance.description);
        test.equal(definition.title, instance.title);
    };

    crawl(definition, instance);

}
