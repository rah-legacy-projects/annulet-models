var dblist = require('./test-databases.json'),
    logger = require('winston'),
    _ = require('lodash');
module.exports = exports = function(filename, cb) {
    logger.silly('destroying database');
    //build test database names
    var criteria = [];
    _.map(dblist, function(dbentry) {
        dbname = /\/([\w\-]+)$/.exec(dbentry.uri)[1];
        criteria.push('i.indexOf("' + dbname + '") >= 0');
    });
    var joinedCriteria = criteria.join(' || ');

    var exec = require('child_process')
        .exec;
    exec("mongo --eval 'db.getMongo().getDBNames().forEach(function(i){if(" + joinedCriteria + "){db.getSiblingDB(i).dropDatabase()}})'", function(err, stdout, stderr) {
        setTimeout(function() {

            logger.silly('database destoyed');
            cb(err);
        }, 1000);
    });
};
