var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../modeler'),
    projectUtility = require('../util'),
    path = require('path'),
    fs = require('fs');

require('./test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                logger.silly(util.inspect(path.resolve(__dirname, './test-databases.json')));
                require('./test-drop')(path.resolve(__dirname, './test-databases.json'), function(){
                    logger.silly('drop complete');
                    cb();
                });
            },
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'modeler setup': function(test) {
        async.waterfall([

            function(cb) {
                modeler.setup(path.resolve(__dirname, './test-databases.json'), cb);
            },
            function(cb){
                logger.silly('attempting to model for a nonexistent customer');
                modeler.db({
                    collection: 'Customer',
                    query: {name: 'test customer'}
                }, function(err, models){
                    logger.silly('models: ' + util.inspect(models));
                    cb(err);
                });
            },
            function(cb){
                //make a customer
                projectUtility.customer.create({
                    customer: {
                        name: 'test customer',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 1,
                        numberOfEmployees: 1
                    }
                }, function(err, models, customer) {
                    logger.silly(util.inspect(customer));
                    logger.silly('customer made');
                    cb(err);
                });
            },
            function(cb){
                modeler.dbhash.mongooseSplit1.Customer.find().exec(function(err, r){
                    logger.silly('customers found: ' + r.length);
                    cb(err);
                });
            },
            function(cb){
                require('./test-drop')(path.resolve(__dirname, './test-databases.json'), function(){
                    logger.silly('drop complete');
                    cb();
                });
            },
            function(cb){
                modeler.setup(path.resolve(__dirname, './test-databases.json'), cb);
            },
            function(cb){
                //make a customer
                projectUtility.customer.create({
                    customer: {
                        name: 'test customer two',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 2,
                        numberOfEmployees: 2
                    }
                }, function(err, models, customer) {
                    logger.silly(util.inspect(models));
                    logger.silly(util.inspect(customer));
                    logger.silly('customer two made');
                    cb(err);
                });
            }
        ], function(err) {
            test.ifError(err);
            test.done();
        });
    },
};
