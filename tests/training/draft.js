var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'draft and update an training': function(test) {
        async.waterfall([

            function(cb) {
                logger.silly('about to create customer!');
                cb(null, {}, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.training.createDraft(),
            testUtility.parts.training.updateDraft({member: 'updatedTraining', flatten:true}),
            //testUtility.parts.training.updateDraft({member: 'updatedTraining', flatten:false}),
        ], function(err, models, p) {
            logger.silly('[top] ' + JSON.stringify(p.updatedTraining, null, 3));
            test.ifError(err);
            test.ok(p);
            test.ok(p.updatedTraining);
            test.equal(p.updatedTraining.title, 'test training');
            test.equal(p.updatedTraining.description, 'test description');
            test.ok(moment(p.updatedTraining.startDate)
                .isSame(moment()
                    .startOf('day')
                    .subtract(1, 'day')));
            test.equal(p.updatedTraining.endDate, 'forever');
            test.equal(p.updatedTraining.sections.length, 2);

            _.each(p.original.sections, function(originalSection) {
                var updatedSection = _.find(p.updatedTraining.sections, function(flatSection) {
                    return flatSection._rangeType == originalSection._rangeType;
                });

                test.ok(updatedSection);
                if (!updatedSection) {
                    return;
                }

                logger.silly(util.inspect(updatedSection));

                test.equal(updatedSection.title, originalSection.title);
                test.equal(updatedSection.content, originalSection.content);
                test.equal(updatedSection._rangeType, originalSection._rangeType);
                
                if(!!originalSection.terms){
                    test.ok(_.all(updatedSection.terms, function(updatedTerm){
                        var originalTerm = _.find(originalSection.terms, function(originalTerm){
                            return originalTerm.term == updatedTerm.term;
                        });

                        return !!originalTerm && originalTerm.meaning == updatedTerm.meaning;
                    }));
                }

                if(!!originalSection.subSections){
                    test.ok(_.all(updatedSection.subSections, function(updatedSubSection){
                        var originalSubSection = _.find(originalSection.subSections, function(originalSubSection){
                            return updatedSubSection.title == originalSubSection.title;
                        });

                        return !!originalSubSection && originalSubSection.content == updatedSubSection.content;
                    }));
                }

            });
            test.done();
        });
    },
};
