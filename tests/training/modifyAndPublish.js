var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'publish an operating procedure def': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.training.createDraft(),
            function(models, p, cb){
                logger.silly('draft created');
                cb(null,models, p);
            },
            testUtility.parts.training.updateDraft(),
            function(models, p, cb){
                logger.silly('draft updated');
                cb(null,models, p);
            },
            testUtility.parts.training.publishDraft(),
            function(models, p, cb){
                logger.silly('draft published');
                cb(null,models, p);
            },
            function(models, p, cb) {
                logger.silly('loading first publish');
                projectUtility.definition.training.loader(models, {
                    trainingDefinition: p.published._id,
                    flatten: true
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    logger.silly('p.reloaded: ' + util.inspect(p.reloaded));
                    cb(err, models, p);
                });
            },
            
            function(models, p, cb) {
                logger.silly('creating second draft');
                testUtility.parts.training.createDraft({
                    trainingDefinitionId: p.published._id,
                    member: 'createdSecondDraft'
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('createdSecondDraft: ' + util.inspect(p.createdSecondDraft));
                projectUtility.definition.training.loader(models, {
                    trainingDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.secondDraft = loaded;

                    logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //change something on the training itself
                p.secondDraft.title = 'altered title';
                //change something on a section
                p.secondDraft.sections[0].title = 'altered introduction';
                //add a section
                p.secondDraft.sections.push({
                    __t: 'definitions.training.sectionTypes.Content',
                    _rangeType: 'definitions.training.ranged.sectionTypes.Content',
                    title: '*** new content',
                    content: 'this is new content for unit testing'
                });

                //logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('--- updating draft!');
                testUtility.parts.training.updateDraft({
                    member: 'secondDraftUpdate',
                    training: p.secondDraft
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('publishing second draft');
                testUtility.parts.training.publishDraft({
                    member: 'secondPublishUnflat',
                    draftId: p.secondDraft._id
                })(models, p, cb);
            },

            function(models, p, cb) {
                logger.silly('loading second publish');
                projectUtility.definition.training.loader(models, {
                    trainingDefinition: p.secondPublishUnflat._id
                }, function(err, models, loaded) {
                    p.secondPublish = loaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('loading unflattened second draft')
                projectUtility.definition.training.loader(models, {
                    trainingDefinition: p.createdSecondDraft,
                    flatten: false
                }, function(err, models, loaded) {
                    p.loadedDraft = loaded;
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.published, ' first publish not published');
            test.ok(!p.published.isDraftOf, 'first publish is still marked as draft');
            test.ok(p.secondPublish, ' second publish not published');
            test.ok(!p.secondPublish.isDraftOf, 'second publish is still marked as draft');

            //make sure second publish has updates
            test.equal(p.secondPublish.title, 'altered title', 'title not updated');
            test.ok(p.secondPublish.sections, ' no sections found');
            test.ok(p.secondPublish.sections[0], 'introduction not loaded');
            test.equal(p.secondPublish.sections[0].title, 'altered introduction', 'introduction not updated');
            var newSection = _.find(p.secondPublish.sections, function(section) {
                return section.title == '*** new content';
            });
            test.ok(newSection, 'new section not found');


            //make sure draft is closed
            test.ok(p.loadedDraft.deleted);
            test.ok(_.all(p.loadedDraft.rangedData, function(range) {
                return range.deleted;
            }));
            test.ok(_.all(p.loadedDraft.sections, function(section) {
                return section.deleted;
            }));
            test.ok(_.chain(p.loadedDraft.sections)
                .map(function(section) {
                    return section.rangedData;
                })
                .reduce(function(a, b) {
                    return a.concat(b);
                }, [])
                .all(function(range) {
                    return range.deleted;
                })
                .value())

            test.done();
        });
    },
};
