var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'create, edit, publish, edit, publish, stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.training.createDraft(),
            testUtility.parts.training.updateDraft(),
            testUtility.parts.training.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.training.loader(models, {
                    trainingDefinition: p.published._id,
                    flatten: true
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {

                testUtility.parts.training.createDraft({
                    trainingDefinitionId: p.published._id,
                    member: 'createdSecondDraft'
                })(models, p, cb);
            },
            function(models, p, cb) {
                projectUtility.definition.training.loader(models, {
                    trainingDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.secondDraft = loaded;

                    logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //change something on the training itself
                p.secondDraft.title = 'altered title';
                //change something on a section
                p.secondDraft.sections[0].title = 'altered introduction';
                //add a section
                p.secondDraft.sections.push({
                    __t: 'definitions.training.sectionTypes.Content',
                    _rangeType: 'definitions.training.ranged.sectionTypes.Content',
                    title: '*** new content',
                    content: 'this is new content for unit testing'
                });

                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.info('--- updating draft!');
                testUtility.parts.training.updateDraft({
                    member: 'secondDraftUpdate',
                    training: p.secondDraft
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.info('publishing update');
                testUtility.parts.training.publishDraft({
                    member: 'secondPublishUnflat',
                    draftId: p.secondDraft._id
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.info('loading published update');
                projectUtility.definition.training.loader(models, {
                    trainingDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    logger.silly('published update: ' + JSON.stringify(loaded,null,2));
                    p.flatDefinition = loaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.info('stamping.');
                testUtility.parts.training.stamper({
                    flatten: true
                })(models, p, cb)
            }
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedTraining);
            test.equal(p.stampedTraining.title, 'altered title');
            test.equal(p.stampedTraining.sections.length, 3);
            test.ok(_.all(p.stampedTraining.sections, function(section) {
                return /instance/.test(section.__t);
            }));
            test.ok(_.chain(p.stampedTraining.sections, function(section) {
                return /instance/.test(section._rangeType);
            }));

            testUtility.trainingCompare(test, p.flatDefinition, p.stampedTraining);



            logger.silly('[top] loaded def: ' + util.inspect(p.flatDefinition));
            test.done();
        });
    },
};
