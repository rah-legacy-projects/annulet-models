module.exports = exports = {
	draft: require("./draft"),
	modifyAndPublish: require("./modifyAndPublish"),
	publish: require("./publish"),
	stampAfterTwoEdits: require("./stampAfterTwoEdits"),
	stampEditStamp: require("./stampEditStamp"),
	stamp: require("./stamp"),
};
