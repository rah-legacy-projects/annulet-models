var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'continue training': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.training.creator(),
            testUtility.parts.workflow.trainingItemSave(),
            testUtility.parts.workflow.makeWorkflow(),
            testUtility.parts.workflow.stamp(),
            testUtility.parts.training.stamper(),
            testUtility.parts.training.stamper({
                member: 'stampedTrainingAgain'
            }),
        ], function(err, models, p) {
            //logger.silly(util.inspect(p, null, 10));
            test.ok(p);
            test.ok(p.stampedTraining);
            test.ok(p.stampedTrainingAgain);
            logger.silly(p.stampedTraining._id.toString() + ' == ' + p.stampedTrainingAgain._id.toString());
            test.equal(p.stampedTraining._id.toString(), p.stampedTrainingAgain._id.toString());

            test.areEqual
            test.done();
        });
    },
};
