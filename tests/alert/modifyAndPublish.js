var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'publish an operating procedure def': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.alert.createDraft(),
            testUtility.parts.alert.updateDraft(),
            testUtility.parts.alert.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.alert.loader(models, {
                    alertDefinition: p.published._id,
                    flatten: true
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    logger.silly('p.reloaded: ' + util.inspect(p.reloaded));
                    cb(err, models, p);
                });
            },
            
            function(models, p, cb) {

                testUtility.parts.alert.createDraft({
                    alertDefinitionId: p.published._id,
                    member: 'createdSecondDraft'
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('createdSecondDraft: ' + util.inspect(p.createdSecondDraft));
                projectUtility.definition.alert.loader(models, {
                    alertDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.secondDraft = loaded;

                    logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //change something on the alert itself
                p.secondDraft.markdown = 'altered title';
                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('--- updating draft!');
                testUtility.parts.alert.updateDraft({
                    member: 'secondDraftUpdate',
                    alert: p.secondDraft
                })(models, p, cb);
            },
            function(models, p, cb) {
                testUtility.parts.alert.publishDraft({
                    member: 'secondPublishUnflat',
                    draftId: p.secondDraft._id
                })(models, p, cb);
            },

            function(models, p, cb) {
                projectUtility.definition.alert.loader(models, {
                    alertDefinition: p.secondPublishUnflat._id
                }, function(err, models, loaded) {
                    p.secondPublish = loaded;
                    logger.silly('[top] loaded second publish');
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.definition.alert.loader(models, {
                    alertDefinition: p.createdSecondDraft,
                    flatten: false
                }, function(err, models, loaded) {
                    p.loadedDraft = loaded;
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.published, ' first publish not published');
            test.ok(!p.published.isDraftOf, 'first publish is still marked as draft');
            test.ok(p.secondPublish, ' second publish not published');
            test.ok(!p.secondPublish.isDraftOf, 'second publish is still marked as draft');

            //make sure second publish has updates
            test.equal(p.secondPublish.markdown, 'altered title', 'title not updated');

            //make sure draft is closed
            test.ok(p.loadedDraft.deleted);

            test.done();
        });
    },
};
