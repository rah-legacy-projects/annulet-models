var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp, none to stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            function(models, p, cb) {
                projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser,
                    includeDismissed: true,
                    flatten: true
                }, function(err, models, instances) {
                    p.instances = instances;
                    cb(err, models, p);
                });

            }
        ], function(err, models, p) {
            if (err) {
                logger.error(util.inspect(err));
            }
            test.ifError(err);
            test.ok(p);
            test.ok(p.instances);
            test.equal(p.instances.length, 0);

            test.done();
        });
    },
    'stamp, dismiss': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.alert.createDraft(),
            testUtility.parts.alert.updateDraft(),
            testUtility.parts.alert.publishDraft(),
            testUtility.parts.alert.createDraft({
                member: 'draftTwo'
            }),
            function(models, p, cb) {
                var definition = testUtility.parts.alert.definition({
                    rangeId: p.draftTwo.rangedData[0]._id,
                    draftId: p.draftTwo._id
                });
                definition.title = 'definition two!'
                testUtility.parts.alert.updateDraft({
                    member: 'updateTwo',
                    alert: definition
                })(models, p, cb);
            },
            function(models, p, cb) {
                testUtility.parts.alert.publishDraft({
                    member: 'publishTwo',
                    draftId: p.draftTwo._id
                })(models, p, cb);
            },
            function(models, p, cb) {
                projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser,
                    includeDismissed: true,
                    flatten: true
                }, function(err, models, instances) {
                    p.instances = instances;
                    cb(err, models, p);
                });

            },
            function(models, p, cb) {
                projectUtility.instance.alert.dismiss(models, {
                    alertRange: p.instances[0]._rangeId,
                    actionCustomerUser: p.customerUser,
                }, function(err, models, dismissedRange) {
                    p.dismissedRange = dismissedRange;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser,
                    includeDismissed: true,
                    flatten: true
                }, function(err, models, instances) {
                    logger.silly('[top] flattened instances: ' + util.inspect(instances));
                    p.instances = instances;
                    cb(err, models, p);
                });

            },
        ], function(err, models, p) {
            if (err) {
                logger.error(util.inspect(err));
            }
            test.ifError(err);
            test.ok(p);
            test.ok(p.instances);
            test.equal(p.instances.length, 2);

            var alertDefinitions = _.map(p.instances, function(instance) {
                return instance.alertDefinition.toString();
            })
            test.ok(_.any(alertDefinitions, function(d) {
                return d == p.published._id.toString();
            }));
            test.ok(_.any(alertDefinitions, function(d) {
                return d == p.publishTwo._id.toString()
            }));

            var dismissedAlert = _.find(p.instances, function(instance) {
                return !!instance.dismissedDate;
            });
            test.equal(p.dismissedRange._id.toString(), dismissedAlert._rangeId.toString());

            test.done();
        });
    },
    'stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.alert.createDraft(),
            testUtility.parts.alert.updateDraft(),
            testUtility.parts.alert.publishDraft(),
            testUtility.parts.alert.createDraft({
                member: 'draftTwo'
            }),
            function(models, p, cb) {
                var definition = testUtility.parts.alert.definition({
                    rangeId: p.draftTwo.rangedData[0]._id,
                    draftId: p.draftTwo._id
                });
                definition.title = 'definition two!'
                testUtility.parts.alert.updateDraft({
                    member: 'updateTwo',
                    alert: definition
                })(models, p, cb);
            },
            function(models, p, cb) {
                testUtility.parts.alert.publishDraft({
                    member: 'publishTwo',
                    draftId: p.draftTwo._id
                })(models, p, cb);
            },
            function(models, p, cb) {
                projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser,
                    includeDismissed: true,
                    flatten: true
                }, function(err, models, instances) {
                    p.instances = instances;
                    cb(err, models, p);
                });

            }
        ], function(err, models, p) {
            if (err) {
                logger.error(util.inspect(err));
            }
            test.ifError(err);
            test.ok(p);
            test.ok(p.instances);
            test.equal(p.instances.length, 2);

            var alertDefinitions = _.map(p.instances, function(instance) {
                return instance.alertDefinition.toString();
            })
            test.ok(_.any(alertDefinitions, function(d) {
                return d == p.published._id.toString();
            }));
            test.ok(_.any(alertDefinitions, function(d) {
                return d == p.publishTwo._id.toString()
            }));

            test.done();
        });
    },
    'stamp, dismiss': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.alert.createDraft(),
            testUtility.parts.alert.updateDraft(),
            testUtility.parts.alert.publishDraft(),
            testUtility.parts.alert.createDraft({
                member: 'draftTwo'
            }),
            function(models, p, cb) {
                var definition = testUtility.parts.alert.definition({
                    rangeId: p.draftTwo.rangedData[0]._id,
                    draftId: p.draftTwo._id
                });
                definition.title = 'definition two!'
                testUtility.parts.alert.updateDraft({
                    member: 'updateTwo',
                    alert: definition
                })(models, p, cb);
            },
            function(models, p, cb) {
                testUtility.parts.alert.publishDraft({
                    member: 'publishTwo',
                    draftId: p.draftTwo._id
                })(models, p, cb);
            },
            function(models, p, cb) {
                projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser,
                    includeDismissed: true,
                    flatten: true
                }, function(err, models, instances) {
                    p.instances = instances;
                    cb(err, models, p);
                });

            },
            function(models, p, cb) {
                projectUtility.instance.alert.dismiss(models, {
                    alertRange: p.instances[0]._rangeId,
                    actionCustomerUser: p.customerUser,
                }, function(err, models, dismissedRange) {
                    p.dismissedRange = dismissedRange;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.definition.alert.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser,
                    includeDismissed: true,
                    flatten: true
                }, function(err, models, instances) {
                    logger.silly('[top] flattened instances: ' + util.inspect(instances));
                    p.instances = instances;
                    cb(err, models, p);
                });

            },
        ], function(err, models, p) {
            if (err) {
                logger.error(util.inspect(err));
            }
            test.ifError(err);
            test.ok(p);
            test.ok(p.instances);
            test.equal(p.instances.length, 2);

            var alertDefinitions = _.map(p.instances, function(instance) {
                return instance.alertDefinition.toString();
            })
            test.ok(_.any(alertDefinitions, function(d) {
                return d == p.published._id.toString();
            }));
            test.ok(_.any(alertDefinitions, function(d) {
                return d == p.publishTwo._id.toString()
            }));

            var dismissedAlert = _.find(p.instances, function(instance) {
                return !!instance.dismissedDate;
            });
            test.equal(p.dismissedRange._id.toString(), dismissedAlert._rangeId.toString());

            test.done();
        });
    },
};
