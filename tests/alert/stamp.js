var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.alert.createDraft(),
            testUtility.parts.alert.updateDraft(),
            testUtility.parts.alert.publishDraft(),
            testUtility.parts.alert.stamper({
                flatten: true
            }),
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedAlert);
            test.equal(p.stampedAlert.markdown, 'test alert');
            test.ok(moment()
                .startOf('day')
                .subtract(1, 'day')
                .isSame(moment(p.stampedAlert.startDate)));

            test.equal(p.stampedAlert.endDate, 'forever');

            test.done();
        });
    },
};
