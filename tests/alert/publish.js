var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'publish an operating procedure def': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.alert.createDraft(),
            testUtility.parts.alert.updateDraft(),
            testUtility.parts.alert.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.alert.loader(models, {
                    alertDefinition: p.published._id
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    cb(err, models, p);
                });
            }

        ], function(err, models, p) {
            logger.silly('--- ' + util.inspect(p.reloaded));
            test.ifError(err);
            test.ok(p);
            test.ok(p.published);
            test.ok(!p.published.isDraftOf);

            test.ok(p.reloaded._rangeType);
            test.equal(p.reloaded._rangeType, 'definitions.alert.ranged.Alert');

            test.done();
        });
    },
};
