module.exports = exports = {
	draft: require("./draft"),
	modifyAndPublish: require("./modifyAndPublish"),
	publish: require("./publish"),
	stampAllByCustomerUser: require("./stampAllByCustomerUser"),
	stampEditStamp: require("./stampEditStamp"),
	stamp: require("./stamp"),
};
