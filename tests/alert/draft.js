var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'draft and update an alert': function(test) {
        async.waterfall([

            function(cb) {
                logger.silly('about to create customer!');
                cb(null, {}, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.alert.createDraft(),
            testUtility.parts.alert.updateDraft({member: 'updatedAlert', flatten:true}),
        ], function(err, models, p) {
            test.ifError(err);
            test.ok(p);
            test.ok(p.updatedAlert);
            test.equal(p.updatedAlert.markdown, 'test alert');
            test.equal(p.updatedAlert.alertLevel, 'info');
            test.ok(moment(p.updatedAlert.startDate)
                .isSame(moment()
                    .startOf('day')
                    .subtract(1, 'day')));

            test.equal(p.updatedAlert.endDate, 'forever');
            test.done();
        });
    },
};
