var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'manage a user across two customers': function(test) {
        var userId = new mongoose.Types.ObjectId()
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customer({
                member: 'customerTwo',
                name: 'test customer two'
            }),
            testUtility.parts.customerUser({
                useModels:false,
                user: userId
            }),
            function(models, p, cb){
                testUtility.parts.customerUser({
                    useModels: false,
                    customer: p.customerTwo._id,
                    member: 'customerUserTwo'
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('p cust user one: ' + p.customerUser._id);
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: p.customerUser._id
                    }
                }, function(err, models) {
                    p.modelsOne = models;
                    cb(err, p);
                });
            },
            function(p, cb) {
                modeler.db({
                    collection: 'auth.CustomerUser',
                    query: {
                        _id: p.customerUserTwo._id
                    }
                }, function(err, models) {
                    p.modelsTwo = models;
                    cb(err, p);
                });
            }
        ], function(err, stuff) {
            test.ok(stuff);
            test.ok(stuff.modelsOne);
            test.ok(stuff.customerUser);
            test.ok(stuff.customerUserTwo);

            test.done();
        });
    },
};
