var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            logger.silly('setup complete');
            cb(err);
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();
    },
    'manage a user': function(test) {
        async.waterfall([
            function(cb){
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
        ], function(err, models, stuff) {
            test.ifError(err);
            test.ok(stuff);
            test.ok(stuff.customer);
            test.ok(stuff.customerUser);
            test.ok(stuff.customerUser.employeeClass);
            test.done();
        });
    },
};
