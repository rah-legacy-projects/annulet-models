module.exports = exports = {
	"manage": require("./manage"),
	"manage-sameCustomer": require("./manage-sameCustomer"),
	"manage-twoCustomers": require("./manage-twoCustomers"),
	"userCustomers": require("./userCustomers"),
};
