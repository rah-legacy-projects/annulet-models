var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'manage a user on same customer': function(test) {
        var userId = new mongoose.Types.ObjectId()
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser({
                user: userId
            }),
            testUtility.parts.customerUser({
                member: 'customerUserTwo',
                user: userId
            }),
        ], function(err, models, stuff) {
            logger.silly(util.inspect(stuff));
            test.ifError(err);
            test.ok(stuff);
            test.ok(stuff.customer);
            test.ok(stuff.customerUser);
            test.ok(stuff.customerUserTwo);
            test.equal(stuff.customerUser._id.toString(), stuff.customerUserTwo._id.toString());

            test.done();
        });
    },
};
