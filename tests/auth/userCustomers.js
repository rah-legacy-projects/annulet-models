var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    testUtility = require('../util'),
    path = require('path'),
    fs = require('fs'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'getting customers by user': function(test) {
        var userId = new mongoose.Types.ObjectId()
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customer({
                member: 'customerTwo',
                name: 'test customer two'
            }),
            testUtility.parts.customerUser({
                useModels: false,
                user: userId
            }),
            function(models, p, cb) {
                testUtility.parts.customerUser({
                    useModels: false,
                    user: userId,
                    customer: p.customerTwo._id
                })(models, p, cb);
            },
            testUtility.parts.userCustomers({
                user: userId
            }),

        ], function(err, models, stuff) {
            test.ok(stuff);
            test.equal(stuff.customers.length, 2);
            test.done();
        });
    },
};
