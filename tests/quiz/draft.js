var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'draft and update an quiz': function(test) {
        async.waterfall([

            function(cb) {
                logger.silly('about to create customer!');
                cb(null, {}, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.quiz.createDraft(),
            testUtility.parts.quiz.updateDraft({member: 'updatedQuiz', flatten:true}),
            //testUtility.parts.quiz.updateDraft({member: 'updatedQuiz', flatten:false}),
        ], function(err, models, p) {
            logger.silly('[top] ' + JSON.stringify(p.updatedQuiz, null, 3));
            test.ifError(err);
            test.ok(p);
            test.ok(p.updatedQuiz);
            test.equal(p.updatedQuiz.title, 'test quiz');
            test.ok(moment(p.updatedQuiz.startDate)
                .isSame(moment()
                    .startOf('day')
                    .subtract(1, 'day')));
            test.equal(p.updatedQuiz.endDate, 'forever');
            test.equal(p.updatedQuiz.questions.length, 6);

            logger.silly('[quiz draft test] updated quiz: ' + util.inspect(p.updatedQuiz));
            _.each(p.original.questions, function(originalQuestion) {
                logger.silly('[quiz draft test] original question: ' + util.inspect(originalQuestion));
                var updatedQuestion = _.find(p.updatedQuiz.questions, function(flatQuestion) {
                    logger.silly('[quiz draft test] flat question: ' + util.inspect(flatQuestion));
                    return flatQuestion._rangeType == originalQuestion._rangeType;
                });

                test.ok(updatedQuestion);
                if (!updatedQuestion) {
                    return;
                }

                test.equal(updatedQuestion.title, originalQuestion.title);
                test.equal(updatedQuestion.content, originalQuestion.content);
                test.equal(updatedQuestion._rangeType, originalQuestion._rangeType);
            });
            test.done();
        });
    },
};
