var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

//{{{ stamp process
var stamp = function(editStep, cb) {
    async.waterfall([

        function(cb) {
            cb(null, null, {});
        },
        testUtility.parts.customer(),
        testUtility.parts.customerUser(),
        testUtility.parts.quiz.createDraft(),
        testUtility.parts.quiz.updateDraft(),
        testUtility.parts.quiz.publishDraft(),
        testUtility.parts.quiz.stamper({
            flatten: true,
            member: 'instanceOne'
        }),
        function(models,p, cb){
            projectUtility.definition.quiz.stamper.attempt.loader(models, {
                attempt: p.instanceOne,
                customerUser: p.customerUser
            }, function(err, models, attempt){
                p.toFinish = attempt;
                cb(err, models, p);
            });
        },
        testUtility.parts.quiz.makeInstanceAnswers(),
        testUtility.parts.quiz.finalize(),
        function(models, p, cb) {
            projectUtility.definition.quiz.loader(models, {
                quizDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.definitionOne = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            projectUtility.definition.quiz.loader(models, {
                quizDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.reloaded = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {

            testUtility.parts.quiz.createDraft({
                quizDefinitionId: p.published._id,
                member: 'createdSecondDraft'
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.quiz.loader(models, {
                quizDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.secondDraft = loaded;

                cb(err, models, p);
            });
        },
        editStep,
        function(models, p, cb) {
            logger.silly('--- updating draft!');
            testUtility.parts.quiz.updateDraft({
                member: 'secondDraftUpdate',
                quiz: p.secondDraft
            })(models, p, cb);
        },
        function(models, p, cb) {
            testUtility.parts.quiz.publishDraft({
                member: 'secondPublishUnflat',
                draftId: p.secondDraft._id
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.quiz.loader(models, {
                quizDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.flatDefinition = loaded;
                cb(err, models, p);
            });
        },
        testUtility.parts.quiz.stamper({
            flatten: true,
            member: 'instanceTwo'
        }),
        function(models, p, cb) {
            projectUtility.definition.quiz.loader(models, {
                quizDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                //logger.silly('[top] ' + JSON.stringify(loaded, null, 2));
                p.definitionTwo = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('test: loading OP def');
            projectUtility.definition.quiz.loader(models, {
                quizDefinition: p.published._id,
                flatten: false
            }, function(err, models, loaded) {
                p.fullDefinitionTwo = loaded.toObject();
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        if(!!err){
            logger.error('[top] ' + util.inspect(err));
        }
        cb(err, models, p);
    });
};
//}}}

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'edit nothing': function(test) {

        stamp(function(models, p, cb) {
            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            logger.silly('def one: ' + util.inspect(p.definitionOne));
            logger.silly('def two: ' + util.inspect(p.definitionTwo));
            logger.silly('ins one: ' + util.inspect(p.instanceOne));
            logger.silly('ins two: ' + util.inspect(p.instanceTwo));

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.quizCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.quizCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[4].rangedData.length, 1);

            test.done();
        });
    },
    'edit title and introduction, add multichoice question': function(test) {

        stamp(function(models, p, cb) {
            //change something on the OP itself
            p.secondDraft.title = 'altered title';
            //change something on a question
            p.secondDraft.questions[0].questionText= 'altered question';
            p.secondDraft.questions[0].isRequired = true;
            //add a question
            p.secondDraft.questions.push({
                    __t: 'definitions.quiz.questionTypes.MultipleChoice',
                    _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
                    questionText: 'new multiple choice',
                    answerQuota: 4,
                    isRequired: true,
                    answers: [{
                        answerText: 'new one',
                        isCorrect: true,
                    }, {
                        answerText: 'new two',
                        isCorrect: false
                    }, {
                        answerText: 'new three',
                        isCorrect: false
                    }, {
                        answerText: 'new four',
                        isCorrect: false
                    }, ]
                });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.quizCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.quizCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.questions[0].rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.questions[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.questions[5].rangedData.length, 1);

            var newQuestion = _.find(p.instanceTwo.questions, function(question){
                logger.silly('\t\t' + question.questionText);
                return question.questionText == 'new multiple choice';
            });
            test.ok(newQuestion);

            test.done();
        });
    },
};
