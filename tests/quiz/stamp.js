var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.quiz.createDraft(),
            testUtility.parts.quiz.updateDraft(),
            testUtility.parts.quiz.publishDraft(),
            testUtility.parts.quiz.stamper(),
            function(models, p, cb) {
                //reload the quiz def, flattened
                projectUtility.definition.quiz.loader(models, {
                    flatten: true,
                    quizDefinition: p.published._id
                }, function(err, models, quizDefinition) {
                    p.reloadedDefinition = quizDefinition;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            logger.silly('[top] ' + JSON.stringify(p.stampedQuiz, null, 3));
            logger.silly('[top] ' + JSON.stringify(p.reloadedDefinition, null, 3));
            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedQuiz);
            test.equal(p.stampedQuiz.quizDefinitionRange.title, 'test quiz');
            test.equal(p.stampedQuiz.questions.length, 5);
            test.ok(_.all(p.stampedQuiz.questions, function(question) {
                logger.silly('\t\tquestion type: ' + question.__t);
                return /instance/.test(question.__t);
            }), 'not all questions are instances');

            /*
            test.ok(moment()
                .startOf('day')
                .subtract(1, 'day')
                .isSame(moment(p.stampedQuiz.startDate)));

            test.ok(moment()
                .startOf('day')
                .add(30, 'day')
                .isSame(moment(p.stampedQuiz.endDate)));


            test.ok(_.all(p.stampedQuiz.questions, function(question) {
                return moment()
                    .startOf('day')
                    .subtract(1, 'day')
                    .isSame(moment(question.startDate));
            }), 'not all question start dates are correct');

            test.ok(_.all(p.stampedQuiz.questions, function(question) {
                return moment()
                    .startOf('day')
                    .add(30, 'day')
                    .isSame(moment(question.endDate));
            }), 'not all question end dates are correct');
            */
            test.ok(_.all(p.stampedQuiz.questions, function(question) {
                return !!question.questionText;
            }), 'not all questions have question text');

            testUtility.quizCompare(test, p.reloadedDefinition, p.stampedQuiz);



            test.done();
        });
    },
};
