var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');
require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Complete a quiz': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.quiz.creator(),
            testUtility.parts.workflow.itemSave(),
            testUtility.parts.workflow.makeWorkflow(),
            testUtility.parts.workflow.stamp(),
            testUtility.parts.quiz.stamper(),
            testUtility.parts.quiz.makeInstanceAnswers(),
            testUtility.parts.quiz.scorer(),
        ], function(err, models, p) {
            logger.silly(' --- about to reload def and stamp');
            async.parallel({
                workflowDefinition: function(cb) {
                    projectUtility.definition.workflow.workflowLoader(models, {
                        workflowItem: p.workflowItemDefinitionRoot
                    }, function(err, models, r) {
                        cb(err, r);
                    });
                },
                stampedWorkflow: function(cb) {
                    //logger.silly('wf item def root: ' + util.inspect(p.stampedRoot));
                    projectUtility.instance.workflow.workflowLoader(models, {
                        workflowItem: p.stampedRoot
                    }, function(err, models, r) {
                        cb(err, r);
                    });
                }
            }, function(err, r) {
                //logger.debug(util.inspect(r,null,10));
                test.ifError(err);;
                test.ok(r);
                test.ok(r.stampedWorkflow);
                test.ok(r.stampedWorkflow.sequence);
                test.ok(r.stampedWorkflow.sequence[0]);
                test.ok(r.stampedWorkflow.sequence[0].quizContainer);
                test.ok(p.stampedQuiz);
                test.done();
            });

        });
    },
};
