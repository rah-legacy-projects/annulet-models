var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Stamp a workflow with a quiz item': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.quiz.creator(),
            testUtility.parts.workflow.itemSave(),
            testUtility.parts.workflow.makeWorkflow(),
            testUtility.parts.workflow.stamp(),
            function(models, p, cb) {
                logger.silly('stamping wf one');
                //instantiate an instance with the quiz
                projectUtility.definition.workflow.stamper(models, {
                    customerUser: p.customerUser,
                    user: {
                        firstName: 'test',
                        lastName: 'test',
                        email: 'test'
                    },
                    workflowDefinition: p.workflowDefinition
                }, function(err, stampedWorkflow, stampedRoot) {
                    cb(err, _.defaults(p, {
                        stampedWorkflow: stampedWorkflow,
                        stampedRoot: stampedRoot
                    }));
                });
            },
        ], function(err, p) {
            //logger.silly(util.inspect(p, null, 10));
            test.done();
        });
    },
};
