var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'complete': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.quiz.createDraft(),
            testUtility.parts.quiz.updateDraft(),
            testUtility.parts.quiz.publishDraft(),
            testUtility.parts.quiz.stamper(),
            function(models, p, cb) {
                projectUtility.definition.quiz.stamper.attempt.loader(models, {
                    attempt: p.stampedQuiz,
                    customerUser: p.customerUser
                }, function(err, models, attempt) {
                    p.toFinish = attempt;
                    cb(err, models, p);
                });
            },
            testUtility.parts.quiz.makeInstanceAnswers(),
            testUtility.parts.quiz.finalize(),
            testUtility.parts.quiz.stamper({
                member: 'stampedQuizAgain'
            }),
            function(models, p, cb) {
                projectUtility.definition.quiz.stamper.attempt.loader(models, {
                    attempt: p.stampedQuiz,
                    customerUser: p.customerUser
                }, function(err, models, attempt) {
                    p.finished = attempt;
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {
            if (!!err) {
                logger.error('problem during test: ' + util.inspect(err));
            }
            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedQuiz, 'first stamping failed');
            test.ok(p.stampedQuizAgain, 'second stamping failed');
            test.ok(p.finished.finalizedDate);
            test.equal(p.stampedQuiz._id.toString(), p.stampedQuizAgain._id.toString(), 'completed quizzes should have the same ID');
            test.done();
        });
    },
};
