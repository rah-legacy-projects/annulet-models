var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Stamp a workflow with a quiz item, then start the quiz': function(test) {
        async.waterfall([
            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.quiz.creator(),
            testUtility.parts.workflow.itemSave(),
            testUtility.parts.workflow.makeWorkflow(),
            testUtility.parts.workflow.stamp(),
            testUtility.parts.quiz.stamper(),
        ], function(err, models, p) {
            logger.silly(' --- about to reload def and stamp');
            async.parallel({
                workflowDefinition: function(cb) {
                    projectUtility.definition.workflow.workflowLoader(models, {
                        workflowItem: p.workflowItemDefinitionRoot
                    }, function(err, models, r) {
                        cb(err, r);
                    });
                },
                stampedWorkflow: function(cb) {
                    //logger.silly('wf item def root: ' + util.inspect(p.stampedRoot));
                    projectUtility.instance.workflow.workflowLoader(models, {
                        workflowItem: p.stampedRoot
                    }, function(err, models, r) {
                        cb(err, r);
                    });
                }
            }, function(err, r) {
                if(!!err){
                    logger.error(util.inspect(err));
                }
                //logger.debug(util.inspect(r,null,10));
                test.ifError(err);;
                test.ok(r, 'r not ok');
                test.ok(r.stampedWorkflow, 'stamped wf not ok');
                test.ok(r.stampedWorkflow.sequence, 'stamped wf seq not ok');
                test.ok(r.stampedWorkflow.sequence[0], 'stamped wf seq[0] not ok');
                test.ok(r.stampedWorkflow.sequence[0].quizContainer, 'quiz container not ok');
                test.done();
            });

        });
    },
};
