var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'quiz load by shortname': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.quiz.createDraft(),
            testUtility.parts.quiz.updateDraft(),
            testUtility.parts.quiz.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.quiz.loadByShortName(models, {
                    shortName: p.published.shortName,
                    customer: p.customer
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    cb(err, models, p);
                });
            }

        ], function(err, models, p) {
            logger.silly('--- ' + util.inspect(p.reloaded));
            test.ifError(err);
            test.ok(p);
            test.ok(p.published);
            test.ok(!p.published.isDraftOf);

            //length should be one as the draft is closed
            test.equal(p.reloaded.length, 1);
            test.done();
        });
    },
};
