module.exports = exports = {
	complete: require("./complete"),
	continue: require("./continue"),
	draft: require("./draft"),
	loadByShortName: require("./loadByShortName"),
	modifyAndPublish: require("./modifyAndPublish"),
	publish: require("./publish"),
	redo: require("./redo"),
	stampAfterTwoEdits: require("./stampAfterTwoEdits"),
	stampEditStamp: require("./stampEditStamp"),
	stamp: require("./stamp"),
};
