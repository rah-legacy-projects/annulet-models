var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'publish a quiz def': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.quiz.createDraft(),
            testUtility.parts.quiz.updateDraft(),
            testUtility.parts.quiz.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.quiz.loader(models, {
                    quizDefinition: p.published._id,
                    flatten: true
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    logger.silly('p.reloaded: ' + util.inspect(p.reloaded));
                    cb(err, models, p);
                });
            },

            function(models, p, cb) {

                testUtility.parts.quiz.createDraft({
                    quizDefinitionId: p.published._id,
                    member: 'createdSecondDraft'
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('createdSecondDraft: ' + util.inspect(p.createdSecondDraft));
                projectUtility.definition.quiz.loader(models, {
                    quizDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.secondDraft = loaded;

                    logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //change something on the OP itself
                p.secondDraft.title = 'altered title';
                //change something on a question
                p.secondDraft.questions[0].questionText = 'altered question';
                //add a question
                p.secondDraft.questions.push({
                    __t: 'definitions.quiz.questionTypes.MultipleChoice',
                    _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
                    questionText: '7',
                    answerQuota: 4,
                    required: false,
                    answers: [{
                        answerText: '7 one',
                        isCorrect: true,
                    }, {
                        answerText: '7 two',
                        isCorrect: false
                    }, {
                        answerText: '7 three',
                        isCorrect: false
                    }, {
                        answerText: '7 four',
                        isCorrect: false
                    }, ]
                });

                //logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                cb(null, models, p);
            },
            function(models, p, cb) {
                testUtility.parts.quiz.updateDraft({
                    member: 'secondDraftUpdate',
                    quiz: p.secondDraft
                })(models, p, cb);
            },
            function(models, p, cb) {
                testUtility.parts.quiz.publishDraft({
                    member: 'secondPublishUnflat',
                    draftId: p.secondDraft._id
                })(models, p, cb);
            },

            function(models, p, cb) {
                projectUtility.definition.quiz.loader(models, {
                    quizDefinition: p.secondPublishUnflat._id
                }, function(err, models, loaded) {
                    p.secondPublish = loaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                projectUtility.definition.quiz.loader(models, {
                    quizDefinition: p.createdSecondDraft,
                    flatten: false
                }, function(err, models, loaded) {
                    p.loadedDraft = loaded;
                    cb(err, models, p);
                });
            },
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.published, ' first publish not published');
            test.ok(!p.published.isDraftOf, 'first publish is still marked as draft');
            test.ok(p.secondPublish, ' second publish not published');
            test.ok(!p.secondPublish.isDraftOf, 'second publish is still marked as draft');

            //make sure second publish has updates
            test.equal(p.secondPublish.title, 'altered title', 'title not updated');
            test.ok(p.secondPublish.questions, ' no questions found');
            test.ok(p.secondPublish.questions[0], 'introduction not loaded');
            test.equal(p.secondPublish.questions[0].questionText, 'altered question', 'question not updated');
            var newquestion = _.find(p.secondPublish.questions, function(question) {
                return question.questionText == '7';
            });
            test.ok(newquestion, 'new question not found');


            //make sure draft is closed
            test.ok(p.loadedDraft.deleted);
            test.ok(_.all(p.loadedDraft.rangedData, function(range) {
                return range.deleted;
            }));
            test.ok(_.all(p.loadedDraft.questions, function(question) {
                return question.deleted;
            }));
            test.ok(_.chain(p.loadedDraft.questions)
                .map(function(question) {
                    return question.rangedData;
                })
                .reduce(function(a, b) {
                    return a.concat(b);
                }, [])
                .all(function(range) {
                    return range.deleted;
                })
                .value())

            test.done();
        });
    },
};
