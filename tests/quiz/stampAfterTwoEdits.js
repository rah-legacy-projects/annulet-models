var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'create, edit, publish, edit, publish, stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.quiz.createDraft(),
            testUtility.parts.quiz.updateDraft(),
            testUtility.parts.quiz.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.quiz.loader(models, {
                    quizDefinition: p.published._id,
                    flatten: true
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {

                testUtility.parts.quiz.createDraft({
                    quizDefinitionId: p.published._id,
                    member: 'createdSecondDraft'
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('createdSecondDraft: ' + util.inspect(p.createdSecondDraft));
                projectUtility.definition.quiz.loader(models, {
                    quizDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.secondDraft = loaded;

                    logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //change something on the quiz itself
                p.secondDraft.title = 'altered title';
                //change something on a question
                p.secondDraft.questions[0].questionText = 'altered question';
                //add a question
                p.secondDraft.questions.push({
                    __t: 'definitions.quiz.questionTypes.MultipleChoice',
                    _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
                    questionText: 'new',
                    answerQuota: 4,
                    required: false,
                    answers: [{
                        answerText: 'new one',
                        isCorrect: true,
                    }, {
                        answerText: 'new two',
                        isCorrect: false
                    }, {
                        answerText: 'new three',
                        isCorrect: false
                    }, {
                        answerText: 'new four',
                        isCorrect: false
                    }, ]
                });

                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('--- updating draft!');
                testUtility.parts.quiz.updateDraft({
                    member: 'secondDraftUpdate',
                    quiz: p.secondDraft
                })(models, p, cb);
            },
            function(models, p, cb) {
                testUtility.parts.quiz.publishDraft({
                    member: 'secondPublishUnflat',
                    draftId: p.secondDraft._id
                })(models, p, cb);
            },
            function(models, p, cb) {
                projectUtility.definition.quiz.loader(models, {
                    quizDefinition: p.published,
                    flatten: true
                }, function(err, models, loaded) {
                    p.flatDefinition = loaded;
                    cb(err, models, p);
                });
            },
            testUtility.parts.quiz.stamper({
            }),
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedQuiz);
            test.equal(p.stampedQuiz.questions.length, 5);
            test.ok(_.all(p.stampedQuiz.questions, function(question) {
                return /instance/.test(question.__t);
            }));
            test.ok(_.chain(p.stampedQuiz.questions, function(question) {
                return /instance/.test(question._rangeType);
            }));

            testUtility.quizCompare(test, p.flatDefinition, p.stampedQuiz);



            logger.silly('[top] loaded def: ' + util.inspect(p.flatDefinition));
            logger.silly('[top] stamped quiz: ' + util.inspect(p.stampedQuiz));
            test.done();
        });
    },
};
