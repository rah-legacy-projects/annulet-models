module.exports = exports = {
	draft: require("./draft"),
	modifyAndPublish: require("./modifyAndPublish"),
	publish: require("./publish"),
	stampEditStamp: require("./stampEditStamp"),
	stamp: require("./stamp"),
};
