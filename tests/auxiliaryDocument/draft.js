var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'draft and update an auxiliaryDocument': function(test) {
        async.waterfall([

            function(cb) {
                logger.silly('about to create customer!');
                cb(null, {}, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.auxiliaryDocument.createDraft(),
            testUtility.parts.auxiliaryDocument.updateDraft({member: 'updatedAuxiliaryDocument', flatten:true}),
        ], function(err, models, p) {
            logger.silly('[top] ' + util.inspect(p.updatedAuxiliaryDocument));
            test.ifError(err);
            test.ok(p);
            test.ok(p.updatedAuxiliaryDocument);
            test.equal(p.updatedAuxiliaryDocument.displayName, 'test auxiliaryDocument');
            test.equal(p.updatedAuxiliaryDocument.description, 'test description');
            test.ok(moment(p.updatedAuxiliaryDocument.startDate)
                .isSame(moment()
                    .startOf('day')
                    .subtract(1, 'day')));
            test.equal(p.updatedAuxiliaryDocument.endDate, 'forever');
            test.done();
        });
    },
};
