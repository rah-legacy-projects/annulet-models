var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.auxiliaryDocument.createDraft(),
            testUtility.parts.auxiliaryDocument.updateDraft(),
            testUtility.parts.auxiliaryDocument.publishDraft(),
            testUtility.parts.auxiliaryDocument.stamper({
                flatten: true
            }),
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedAuxiliaryDocument);
            test.ok(moment()
                .startOf('day')
                .subtract(1, 'day')
                .isSame(moment(p.stampedAuxiliaryDocument.startDate)));


            test.equal(p.stampedAuxiliaryDocument.endDate, 'forever');
            test.done();
        });
    },
};
