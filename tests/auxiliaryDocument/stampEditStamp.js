var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

//{{{ stamp process
var stamp = function(editStep, cb) {
    async.waterfall([

        function(cb) {
            cb(null, null, {});
        },
        testUtility.parts.customer(),
        testUtility.parts.customerUser(),
        testUtility.parts.auxiliaryDocument.createDraft(),
        testUtility.parts.auxiliaryDocument.updateDraft(),
        testUtility.parts.auxiliaryDocument.publishDraft(),
        testUtility.parts.auxiliaryDocument.stamper({
            flatten: true,
            member: 'instanceOne'
        }),
        function(models, p, cb) {
            projectUtility.definition.auxiliaryDocument.loader(models, {
                auxiliaryDocumentDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.definitionOne = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            projectUtility.definition.auxiliaryDocument.loader(models, {
                auxiliaryDocumentDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.reloaded = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {

            testUtility.parts.auxiliaryDocument.createDraft({
                auxiliaryDocumentDefinitionId: p.published._id,
                member: 'createdSecondDraft'
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.auxiliaryDocument.loader(models, {
                auxiliaryDocumentDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.secondDraft = loaded;

                cb(err, models, p);
            });
        },
        editStep,
        function(models, p, cb) {
            logger.silly('--- updating draft!');
            testUtility.parts.auxiliaryDocument.updateDraft({
                member: 'secondDraftUpdate',
                auxiliaryDocument: p.secondDraft
            })(models, p, cb);
        },
        function(models, p, cb) {
            testUtility.parts.auxiliaryDocument.publishDraft({
                member: 'secondPublishUnflat',
                draftId: p.secondDraft._id
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.auxiliaryDocument.loader(models, {
                auxiliaryDocumentDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.flatDefinition = loaded;
                cb(err, models, p);
            });
        },
        testUtility.parts.auxiliaryDocument.stamper({
            flatten: true,
            member: 'instanceTwo'
        }),
        function(models, p, cb) {
            projectUtility.definition.auxiliaryDocument.loader(models, {
                auxiliaryDocumentDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                logger.silly('[top] ' + JSON.stringify(loaded, null, 2));
                p.definitionTwo = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('test: loading OP def');
            projectUtility.definition.auxiliaryDocument.loader(models, {
                auxiliaryDocumentDefinition: p.published._id,
                flatten: false
            }, function(err, models, loaded) {
                p.fullDefinitionTwo = loaded.toObject();
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, p);
    });
};
//}}}

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'edit nothing': function(test) {

        stamp(function(models, p, cb) {
            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            test.equal(p.fullDefinitionTwo.rangedData.length, 1);
            test.done();
        });
    },
    'edit description': function(test) {

        stamp(function(models, p, cb) {
            //change something on the OP itself
            p.secondDraft.description= 'altered title';
            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            test.equal(p.fullDefinitionTwo.rangedData.length, 2);
            test.equal(p.instanceTwo.description, 'altered title');

            test.done();
        });
    },
};
