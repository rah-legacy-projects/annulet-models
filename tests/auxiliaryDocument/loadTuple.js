var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'load tuple': function(test) {
        async.waterfall([

                function(cb) {
                    cb(null, null, {});
                },
                testUtility.parts.customer(),
                testUtility.parts.customerUser(),
                testUtility.parts.auxiliaryDocument.createDraft(),
                testUtility.parts.auxiliaryDocument.updateDraft(),
                testUtility.parts.auxiliaryDocument.publishDraft(),
                testUtility.parts.auxiliaryDocument.createDraft(),
                function(models, p, cb) {
                    testUtility.parts.auxiliaryDocument.updateDraft({
                        auxiliaryDocument: testUtility.parts.auxiliaryDocument.definition({
                            rangeId: p.draft.rangedData[0]._id,
                            draftId: p.draft._id,
                            displayName: 'open draft',
                            description: 'open draft'
                        })
                    })(models, p, cb);
                },
                testUtility.parts.auxiliaryDocument.adminList(),
                function(models, p, cb) {
                    var pub = _.find(p.adminList, function(al) {
                        return !!al.published;
                    });
                    projectUtility.definition.auxiliaryDocument.tupleLoader(models, {
                        actionCustomerUser: p.customerUser,
                        auxiliaryDocument: pub.published._id
                    }, function(err, models, tuple) {
                        p.publishedTuple = tuple;
                        cb(err, models, p);
                    });
                },
                function(models, p, cb) {
                    var draft = _.find(p.adminList, function(al) {
                        return !!al.draft;
                    });
                    projectUtility.definition.auxiliaryDocument.tupleLoader(models, {
                        actionCustomerUser: p.customerUser,
                        auxiliaryDocument: draft.draft._id
                    }, function(err, models, tuple) {
                        p.draftTuple = tuple;
                        cb(err, models, p);
                    });
                }
            ],
            function(err, models, p) {
                test.ifError(err);
                test.ok(p);
                test.ok(p.draftTuple);
                test.ok(p.publishedTuple);
                test.ok(p.draftTuple.draft);
                test.ok(p.publishedTuple.published);
                test.ok(!p.draftTuple.published);
                test.ok(!p.publishedTuple.draft);
                logger.silly(util.inspect(p.draftTuple));
                logger.silly(util.inspect(p.publishedTuple));
                test.done();
            });
    },
};
