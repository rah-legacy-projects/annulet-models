var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'publish an auxdoc def': function(test) {
        async.waterfall([

                function(cb) {
                    cb(null, null, {});
                },
                testUtility.parts.customer(),
                testUtility.parts.customerUser(),
                testUtility.parts.auxiliaryDocument.createDraft(),
                testUtility.parts.auxiliaryDocument.updateDraft(),
                testUtility.parts.auxiliaryDocument.publishDraft(),
                testUtility.parts.auxiliaryDocument.createDraft(),
                function(models, p, cb) {
                    testUtility.parts.auxiliaryDocument.updateDraft({
                        auxiliaryDocument: testUtility.parts.auxiliaryDocument.definition({
                            rangeId: p.draft.rangedData[0]._id,
                            draftId: p.draft._id,
                            displayName: 'open draft',
                            description: 'open draft'
                        })
                    })(models, p, cb);
                },
                testUtility.parts.auxiliaryDocument.adminList(),
            ],
            function(err, models, p) {
                logger.silly('--- ' + util.inspect(p.adminList));
                test.ifError(err);
                test.ok(p);
                test.ok(p.adminList);
                test.equal(p.adminList.length, 2);
                test.done();
            });
    },
};
