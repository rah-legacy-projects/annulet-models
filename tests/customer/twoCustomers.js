var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtil = require('../../util');

require('../test-config');
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });

    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Stamp two customers': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customer({
                name: 'test customer 2',
                member: 'customer2'
            }),
            function(models, p, cb) {
                //find models for customer 1
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: p.customer._id
                    }
                }, function(err, models) {
                    logger.silly('customer one models: ' + !!models);
                    p.custOneModels = models;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //find models for customer 2
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: p.customer2._id
                    }
                }, function(err, models) {
                    logger.silly('customer two models: ' + !!models);
                    p.custTwoModels = models;
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            test.ifError(err);
            test.ok(p);
            test.ok(p.customer);
            test.ok(p.customer2);
            test.ok(p.custOneModels);
            test.ok(p.custTwoModels);
            test.done();
        });
    },
};
