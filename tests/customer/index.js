module.exports = exports = {
	Customer: require("./customer"),
	IdentifyModelsByCustomer: require("./identifyModelsByCustomer"),
	TwoCustomers: require("./twoCustomers"),
};
