var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    mongoose = require('mongoose'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtil = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });

    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'identify models by customer': function(test) {
        async.waterfall([
            function(cb){cb(null,null,{});},
            testUtility.parts.customer(),
            function(models, p, cb) {
                modeler.db({
                    collection: 'Customer',
                    query: {
                        _id: new mongoose.Types.ObjectId(p.customer._id.toString())
                    }
                }, function(err, models) {
                    p.models = models;
                    cb(err, p);
                });
            }
        ], function(err, p) {
            test.ifError(err);
            test.ok(p);
            test.ok(p.customer);
            test.ok(p.models);
            test.done();
        });
    },
};
