var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.operatingProcedures.createDraft(),
            testUtility.parts.operatingProcedures.updateDraft(),
            testUtility.parts.operatingProcedures.publishDraft(),
            testUtility.parts.operatingProcedures.createDraft({
                member: 'draftTwo'
            }),
            function(models, p, cb) {
                var definition = testUtility.parts.operatingProcedures.definition({
                        rangeId: p.draftTwo.rangedData[0]._id,
                        draftId: p.draftTwo._id
                    });
                definition.title = 'definition two!'
                testUtility.parts.operatingProcedures.updateDraft({
                    member: 'updateTwo',
                    operatingProcedure: definition
                })(models, p, cb);
            },
            function(models, p, cb){
                testUtility.parts.operatingProcedures.publishDraft({
                    member:'publishTwo',
                    draftId: p.draftTwo._id
                })(models, p, cb);
            },
            function(models, p, cb){
                projectUtility.definition.operatingProcedure.stamper.stampByCustomerUser(models, {
                    customerUser: p.customerUser,
                    actionCustomerUser: p.customerUser
                },function(err, models, instances){
                    logger.silly('[top] instances: ' + util.inspect(instances));
                    p.instances = instances;
                    cb(err, models, p);
                });

            }
        ], function(err, models, p) {
            test.ifError(err);
            test.ok(p);
            test.ok(p.instances);
            test.equal(p.instances.length, 2);

            logger.silly('draft id: ' + p.draft._id);
            logger.silly('draft two id: ' + p.draftTwo._id);
            logger.silly('published id: ' + p.published._id);
            logger.silly('publushed two id: ' + p.publishTwo._id);
            logger.silly('instance def ids: ' + util.inspect(_.pluck(p.instances, 'operatingProcedureDefinition')));

            test.done();
        });
    },
};
