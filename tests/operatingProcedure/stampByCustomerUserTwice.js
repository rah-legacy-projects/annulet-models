var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    projectUtil = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Stamp a operatingProcedure': function(test) {
        async.waterfall([

            function(cb) {
                //make a customer
                projectUtil.customer.create({
                    customer: {
                        name: 'test customer',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 1,
                        numberOfEmployees: 1,
                    },
                    createdBy: 'test',
                    modifiedBy: 'test'

                }, function(err, models, customer) {
                    logger.silly('customer made');
                    cb(err, models, {
                        customer: customer
                    });
                });
            },
            function(models, p, cb) {
                projectUtil.auth.manageCustomerUser(models, {
                    user: new mongoose.Types.ObjectId(),
                    customer: p.customer,
                    roles: ['Owner'],
                    modifiedBy: 'test',
                    createdBy: 'test',
                    customerUser: {
                        employeeClass: ['test']
                    }
                }, function(err, models, customerUser) {
                    logger.silly('user: ' + util.inspect(customerUser));
                    p.customerUser = customerUser;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //make a operatingProcedure def
                var operatingProcedure = {
                    title: 'test operatingProcedure',
                    customer: p.customer._id,
                    sections: [{
                            __t: 'definitions.operatingProcedure.sectionTypes.Overview',
                            title: 'Introduction',
                            content: 'This is an introduction!'
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.RelatedProcedures',
                            title: 'Related Procedures!',
                            content: 'No related procedures.'
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.Definitions',
                            title: 'Definitions',
                            content: 'Some definitions... for your enjoyment',
                            terms: [{
                                term: 'test',
                                meaning: 'test meaning'
                            }, {
                                term: 'test again',
                                meaning: ' test meaning again'
                            }]
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.Content',
                            title: 'Some Content',
                            content: 'This is some content on this test.'
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.SectionedContent',
                            title: 'Some _sectioned_ content!',
                            subSections: [{
                                title: 'subsection 1',
                                content: 'pursuant to something'
                            }, {
                                title: 'subsection 2',
                                content: 'another subsection'
                            }]
                        }

                    ]
                };
                require('../../util/definition/operatingProcedure/creator')(models, {
                    customerUser: p.customerUser,
                    user: {
                        firstName: 'test',
                        lastName: 'test',
                        email: 'test@test.test'
                    },
                    customer: p.customer._id,
                    operatingProcedureDefinition: operatingProcedure
                }, function(err, models, operatingProcedureDefinition) {
                    p.operatingProcedureDefinition = operatingProcedureDefinition;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //make a operatingProcedure def
                var operatingProcedure = {
                    title: 'test operatingProcedure two',
                    customer: p.customer._id,
                    sections: [{
                            __t: 'definitions.operatingProcedure.sectionTypes.Overview',
                            title: 'Introduction',
                            content: 'This is an introduction!'
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.RelatedProcedures',
                            title: 'Related Procedures!',
                            content: 'No related procedures.'
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.Definitions',
                            title: 'Definitions',
                            content: 'Some definitions... for your enjoyment',
                            terms: [{
                                term: 'test',
                                meaning: 'test meaning'
                            }, {
                                term: 'test again',
                                meaning: ' test meaning again'
                            }]
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.Content',
                            title: 'Some Content',
                            content: 'This is some content on this test.'
                        }, {
                            __t: 'definitions.operatingProcedure.sectionTypes.SectionedContent',
                            title: 'Some _sectioned_ content!',
                            subSections: [{
                                title: 'subsection 1',
                                content: 'pursuant to something'
                            }, {
                                title: 'subsection 2',
                                content: 'another subsection'
                            }]
                        }

                    ]
                };
                require('../../util/definition/operatingProcedure/creator')(models, {
                    customerUser: p.customerUser,
                    user: {
                        firstName: 'test',
                        lastName: 'test',
                        email: 'test@test.test'
                    },
                    customer: p.customer._id,
                    operatingProcedureDefinition: operatingProcedure
                }, function(err, models, operatingProcedureDefinition) {
                    p.operatingProcedureDefinitionTwo = operatingProcedureDefinition;
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly(util.inspect(_.keys(models)));
                projectUtil.definition.operatingProcedure.stamper.stampOperatingProceduresByCustomerUser(models, {
                    customerUser: p.customerUser._id,
                    user: {
                        firstName: 'test',
                        lastName: 'test',
                        email: 'test@test.test'
                    },
                }, function(err, models, operatingProcedureInstances) {
                    logger.silly(util.inspect(operatingProcedureInstances));
                    cb(err, models, _.defaults(p, {
                        operatingProcedureInstances: operatingProcedureInstances
                    }));
                });
            },
            function(models, p, cb) {
                logger.silly(util.inspect(_.keys(models)));
                projectUtil.definition.operatingProcedure.stamper.stampOperatingProceduresByCustomerUser(models, {
                    customerUser: p.customerUser._id,
                    user: {
                        firstName: 'test',
                        lastName: 'test',
                        email: 'test@test.test'
                    },
                }, function(err, models, operatingProcedureInstances) {
                    logger.silly(util.inspect(operatingProcedureInstances));
                    cb(err, models, _.defaults(p, {
                        operatingProcedureInstancesAgain: operatingProcedureInstances
                    }));
                });
            },
        ], function(err, models, stuff) {
            test.ifError(err);
            test.ok(stuff);
            test.ok(stuff.operatingProcedureInstances);
            test.ok(stuff.operatingProcedureInstancesAgain);
            if (!!stuff.operatingProcedureInstances) {
                test.equal(stuff.operatingProcedureInstances.length, 2);
            }
            if (!!stuff.operatingProcedureInstances && !!stuff.operatingProcedureInstancesAgain) {
                test.equal(stuff.operatingProcedureInstances.length, stuff.operatingProcedureInstancesAgain.length);
            }
            test.done();
        });
    },
};
