var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');
require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'continue operating procedure': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.operatingProcedures.creator(),
            testUtility.parts.operatingProcedures.stamper(),
            testUtility.parts.operatingProcedures.stamper({
                member: 'stampedOperatingProcedureAgain'
            }),
            function(models, p, cb) {
                testUtility.parts.itemActivity({
                    query: {
                        item: p.stampedOperatingProcedure._id
                    }
                })(models, p, cb);
            }
        ], function(err, models, stuff) {
            test.ok(stuff);
            test.ok(stuff.stampedOperatingProcedure);
            test.ok(stuff.stampedOperatingProcedureAgain);
            test.equal(stuff.stampedOperatingProcedure._id.toString(), stuff.stampedOperatingProcedureAgain._id.toString());
            test.equal(stuff.activities.length, 2);
            test.done();
        });
    },
};
