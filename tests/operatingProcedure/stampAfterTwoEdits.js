var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'create, edit, publish, edit, publish, stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.operatingProcedures.createDraft(),
            testUtility.parts.operatingProcedures.updateDraft(),
            testUtility.parts.operatingProcedures.publishDraft(),
            function(models, p, cb) {
                projectUtility.definition.operatingProcedure.loader(models, {
                    operatingProcedureDefinition: p.published._id,
                    flatten: true
                }, function(err, models, loaded) {
                    p.reloaded = loaded;
                    logger.silly('p.reloaded: ' + util.inspect(p.reloaded));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {

                testUtility.parts.operatingProcedures.createDraft({
                    operatingProcedureDefinitionId: p.published._id,
                    member: 'createdSecondDraft'
                })(models, p, cb);
            },
            function(models, p, cb) {
                logger.silly('createdSecondDraft: ' + util.inspect(p.createdSecondDraft));
                projectUtility.definition.operatingProcedure.loader(models, {
                    operatingProcedureDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.secondDraft = loaded;

                    logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                //change something on the OP itself
                p.secondDraft.title = 'altered title';
                //change something on a section
                p.secondDraft.sections[0].title = 'altered introduction';
                //add a section
                p.secondDraft.sections.push({
                    __t: 'definitions.operatingProcedure.sectionTypes.Content',
                    _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content',
                    title: '*** new content',
                    content: 'this is new content for unit testing'
                });

                //logger.silly('[top] p.secondDraft = ' + util.inspect(p.secondDraft));
                cb(null, models, p);
            },
            function(models, p, cb) {
                logger.silly('--- updating draft!');
                testUtility.parts.operatingProcedures.updateDraft({
                    member: 'secondDraftUpdate',
                    operatingProcedure: p.secondDraft
                })(models, p, cb);
            },
            function(models, p, cb) {
                testUtility.parts.operatingProcedures.publishDraft({
                    member: 'secondPublishUnflat',
                    draftId: p.secondDraft._id
                })(models, p, cb);
            },
            function(models, p, cb) {
                projectUtility.definition.operatingProcedure.loader(models, {
                    operatingProcedureDefinition: p.createdSecondDraft,
                    flatten: true
                }, function(err, models, loaded) {
                    p.flatDefinition = loaded;
                    cb(err, models, p);
                });
            },
            testUtility.parts.operatingProcedures.stamper({
                flatten: true
            }),
        ], function(err, models, p) {

            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedOperatingProcedure);
            test.equal(p.stampedOperatingProcedure.title, 'altered title');
            test.equal(p.stampedOperatingProcedure.sections.length, 6);
            test.ok(_.all(p.stampedOperatingProcedure.sections, function(section) {
                return /instance/.test(section.__t);
            }));
            test.ok(_.chain(p.stampedOperatingProcedure.sections, function(section) {
                return /instance/.test(section._rangeType);
            }));

            testUtility.operatingProcedureCompare(test, p.flatDefinition, p.stampedOperatingProcedure);



                    logger.silly('[top] loaded def: ' + util.inspect(p.flatDefinition));
            test.done();
        });
    },
};
