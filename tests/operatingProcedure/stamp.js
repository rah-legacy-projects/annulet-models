var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'stamp': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.operatingProcedures.createDraft(),
            testUtility.parts.operatingProcedures.updateDraft(),
            testUtility.parts.operatingProcedures.publishDraft(),
            testUtility.parts.operatingProcedures.stamper({
                flatten: true
            }),
        ], function(err, models, p) {

            logger.silly('[top] ' + JSON.stringify(p.stampedOperatingProcedure, null, 3));

            test.ifError(err);
            test.ok(p);
            test.ok(p.stampedOperatingProcedure);
            test.equal(p.stampedOperatingProcedure.title, 'test operatingProcedure');
            test.equal(p.stampedOperatingProcedure.sections.length, 5);
            test.ok(_.all(p.stampedOperatingProcedure.sections, function(section) {
                return /instance/.test(section.__t);
            }));
            test.ok(_.chain(p.stampedOperatingProcedure.sections, function(section) {
                return /instance/.test(section._rangeType);
            }));

            test.ok(moment()
                .startOf('day')
                .subtract(1, 'day')
                .isSame(moment(p.stampedOperatingProcedure.startDate)));

            test.ok(p.stampedOperatingProcedure.endDate == 'forever');

            test.ok(_.all(p.stampedOperatingProcedure.sections, function(section) {
                return moment()
                    .startOf('day')
                    .subtract(1, 'day')
                    .isSame(moment(section.startDate));
            }), 'not all section start dates are correct');

            test.ok(_.all(p.stampedOperatingProcedure.sections, function(section) {
                return section.endDate == 'forever';
            }), 'not all section end dates are correct');

            test.ok(_.all(p.stampedOperatingProcedure.sections, function(section){
                return !!section.title;
            }));

            testUtility.operatingProcedureCompare(test, testUtility.parts.operatingProcedures.definition({rangeId: p.draft.rangedData[0]._id}), p.stampedOperatingProcedure);



            test.done();
        });
    },
};
