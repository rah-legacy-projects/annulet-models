var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtil = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Stamp a operatingProcedure by customer user': function(test) {
        async.waterfall([
            function(cb){
                cb(null, null, {});
            },
            testUtility.parts.customer(),
            testUtility.parts.customerUser(),
            testUtility.parts.operatingProcedures.creator(),
            testUtility.parts.operatingProcedures.creator({title: 'test operating procedure two'}),
            testUtility.parts.operatingProcedures.stampOperatingProceduresByCustomerUser(),
        ], function(err, models, stuff) {
            test.ifError(err);
            test.ok(stuff);
            test.ok(stuff.operatingProcedureInstances);
            if (!!stuff.operatingProcedureInstances) {
                test.equal(stuff.operatingProcedureInstances.length, 2);
            }
            test.done();
        });
    },
};
