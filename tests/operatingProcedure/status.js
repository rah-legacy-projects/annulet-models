var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtil = require('../../util');

require('../test-config');

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'user status on OPs': function(test) {
        async.waterfall([

                function(cb) {
                    cb(null, null, {});
                },

                testUtility.parts.customer(),
                testUtility.parts.customerUser(),
                testUtility.parts.operatingProcedures.createDraft(),
                testUtility.parts.operatingProcedures.updateDraft(),
                testUtility.parts.operatingProcedures.publishDraft(),
                testUtility.parts.operatingProcedures.stamper({
                    flatten: true
                }),


                testUtility.parts.operatingProcedures.createDraft({
                    member: 'draftTwo'
                }),
                function(models, p, cb) {
                    testUtility.parts.operatingProcedures.updateDraft({
                        member: 'opTwo',
                        operatingProcedure: testUtility.parts.operatingProcedures.definition({
                            rangeId: p.draftTwo.rangedData[0]._id,
                            draftId: p.draftTwo._id
                        }),
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.operatingProcedures.publishDraft({
                        member: 'publishedTwo',
                        dratId: p.draftTwo._id
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.operatingProcedures.stamper({
                        member: 'stampedOperatingProcedureTwo',
                        operatingProcedureDefinition: p.publishedTwo._id,
                        flatten:true
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    //complete the second OP
                    models.instances.operatingProcedure.ranged.OperatingProcedure.findOne({
                        _id: p.stampedOperatingProcedureTwo._rangeId
                    }).exec(function(err, stampedOpRange){
                        stampedOpRange.completedDate = new Date();
                        stampedOpRange.modifiedBy = p.customerUser._id;
                        stampedOpRange.save(function(err, stampedOpRange){
                             cb(err, models, p);
                        });
                    });
                },
                testUtility.parts.operatingProcedures.createDraft({
                    member: 'draftThree'
                }),
                function(models, p, cb) {
                    testUtility.parts.operatingProcedures.updateDraft({
                        member: 'opThree',
                        operatingProcedure: testUtility.parts.operatingProcedures.definition({
                            rangeId: p.draftThree.rangedData[0]._id,
                            draftId: p.draftThree._id
                        }),
                    })(models, p, cb);
                },
                function(models, p, cb) {
                    testUtility.parts.operatingProcedures.publishDraft({
                        member: 'publishedThree',
                        dratId: p.draftThree._id
                    })(models, p, cb);
                },
                testUtility.parts.operatingProcedures.status()
            ],
            function(err, models, stuff) {
                test.ifError(err);
                test.ok(stuff);
                test.ok(stuff.statusList);
                if (!!stuff.statusList) {
                    test.equal(stuff.statusList.length, 3);

                    var stamped = _.filter(stuff.statusList, function(s) {
                        return !!s.instanceId;
                    });

                    var defOnly = _.filter(stuff.statusList, function(s) {
                        return !s.instanceId;
                    });

                    var stampedAndCompleted = _.filter(stuff.statusList, function(s) {
                        return !!s.instanceId && !!s.completedDate;
                    });

                    test.equal(stamped.length, 2);
                    test.equal(defOnly.length, 1);
                    test.equal(stampedAndCompleted.length, 1);
                }
                test.done();
            });
    },
    'user status on OPs, no OPs to status': function(test) {
        async.waterfall([

                function(cb) {
                    cb(null, null, {});
                },

                testUtility.parts.customer(),
                testUtility.parts.customerUser(),
                testUtility.parts.operatingProcedures.status()
            ],
            function(err, models, stuff) {
                test.ifError(err);
                test.ok(stuff);
                test.equal(stuff.statusList.length, 0);
                test.done();
            });
    },
};
