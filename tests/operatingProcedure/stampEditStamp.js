var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    modeler = require('../../modeler'),
    path = require('path'),
    fs = require('fs'),
    testUtility = require('../util'),
    projectUtility = require('../../util');

require('../test-config');

//{{{ stamp process
var stamp = function(editStep, cb) {
    async.waterfall([

        function(cb) {
            cb(null, null, {});
        },
        testUtility.parts.customer(),
        testUtility.parts.customerUser(),
        testUtility.parts.operatingProcedures.createDraft(),
        testUtility.parts.operatingProcedures.updateDraft(),
        testUtility.parts.operatingProcedures.publishDraft(),
        testUtility.parts.operatingProcedures.stamper({
            flatten: true,
            member: 'instanceOne'
        }),
        function(models, p, cb) {
            projectUtility.definition.operatingProcedure.loader(models, {
                operatingProcedureDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.definitionOne = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            projectUtility.definition.operatingProcedure.loader(models, {
                operatingProcedureDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                p.reloaded = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {

            testUtility.parts.operatingProcedures.createDraft({
                operatingProcedureDefinitionId: p.published._id,
                member: 'createdSecondDraft'
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.operatingProcedure.loader(models, {
                operatingProcedureDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.secondDraft = loaded;

                cb(err, models, p);
            });
        },
        editStep,
        function(models, p, cb) {
            logger.silly('--- updating draft!');
            testUtility.parts.operatingProcedures.updateDraft({
                member: 'secondDraftUpdate',
                operatingProcedure: p.secondDraft
            })(models, p, cb);
        },
        function(models, p, cb) {
            testUtility.parts.operatingProcedures.publishDraft({
                member: 'secondPublishUnflat',
                draftId: p.secondDraft._id
            })(models, p, cb);
        },
        function(models, p, cb) {
            projectUtility.definition.operatingProcedure.loader(models, {
                operatingProcedureDefinition: p.createdSecondDraft,
                flatten: true
            }, function(err, models, loaded) {
                p.flatDefinition = loaded;
                cb(err, models, p);
            });
        },
        testUtility.parts.operatingProcedures.stamper({
            flatten: true,
            member: 'instanceTwo'
        }),
        function(models, p, cb) {
            projectUtility.definition.operatingProcedure.loader(models, {
                operatingProcedureDefinition: p.published._id,
                flatten: true
            }, function(err, models, loaded) {
                logger.silly('[top] ' + JSON.stringify(loaded, null, 2));
                p.definitionTwo = loaded;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('test: loading OP def');
            projectUtility.definition.operatingProcedure.loader(models, {
                operatingProcedureDefinition: p.published._id,
                flatten: false
            }, function(err, models, loaded) {
                p.fullDefinitionTwo = loaded.toObject();
                cb(err, models, p);
            });
        },
    ], function(err, models, p) {
        cb(err, models, p);
    });
};
//}}}

module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        async.waterfall([

            function(cb) {
                require('../test-drop')(path.resolve(__dirname, '../test-databases.json'), cb);
            },
            function(cb) {
                modeler.setup(path.resolve(__dirname, '../test-databases.json'), cb);
            }
        ], function(err, r) {
            cb(err);
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'edit nothing': function(test) {

        stamp(function(models, p, cb) {
            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);

            test.done();
        });
    },
    'edit title and introduction, add section': function(test) {

        stamp(function(models, p, cb) {
            //change something on the OP itself
            p.secondDraft.title = 'altered title';
            //change something on a section
            p.secondDraft.sections[0].title = 'altered introduction';
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.Content',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content',
                title: '*** new content',
                content: 'this is new content for unit testing'
            });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
    'edit title': function(test) {

        stamp(function(models, p, cb) {
            //change something on the OP itself
            p.secondDraft.title = 'altered title';
            //change something on a section

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            logger.silly(p.definitionOne._id + ', ' + p.definitionTwo._id);
            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 2);

            test.done();
        });
    },
    'edit title and related procs, add section': function(test) {

        stamp(function(models, p, cb) {
            //change something on the OP itself
            p.secondDraft.title = 'altered title';
            //change something on a section
            p.secondDraft.sections[1].title = 'altered rp desc';
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.Content',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content',
                title: '*** new content',
                content: 'this is new content for unit testing'
            });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
    'edit title and definitions, add section': function(test) {

        stamp(function(models, p, cb) {
            //change something on the OP itself
            p.secondDraft.title = 'altered title';
            //change something on a section
            p.secondDraft.sections[2].terms = [{
                term: 'test 3',
                meaning: 'meaning 3'
            }, {
                term: 'test 4',
                meaning: 'meaning 4'
            }];
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.Content',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content',
                title: '*** new content',
                content: 'this is new content for unit testing'
            });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
    'edit title and subsections, add section': function(test) {

        stamp(function(models, p, cb) {
            //change something on the OP itself
            p.secondDraft.title = 'altered title';
            //change something on a section
            p.secondDraft.sections[4].subSections = [{
                title: 'test 3',
                content: 'meaning 3'
            }];
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.Content',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content',
                title: '*** new content',
                content: 'this is new content for unit testing'
            });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 2);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
    'add content section': function(test) {

        stamp(function(models, p, cb) {
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.Content',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content',
                title: '*** new content',
                content: 'this is new content for unit testing'
            });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
    'add subsectioned section': function(test) {

        stamp(function(models, p, cb) {
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.SectionedContent',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.SectionedContent',
                title: '*** new content',
                content: 'this is new content for unit testing',
                subSections: [{
                    title: 'one',
                    content: ' one'
                }]
            });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
    'add related procedure section': function(test) {

        stamp(function(models, p, cb) {
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.RelatedProcedures',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.RelatedProcedures',
                title: '*** new content',
                content: 'this is new content for unit testing'
            });

            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
    'add definition section': function(test) {

        stamp(function(models, p, cb) {
            //add a section
            p.secondDraft.sections.push({
                __t: 'definitions.operatingProcedure.sectionTypes.Definitions',
                _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Definitions',
                title: '*** new content',
                content: 'this is new content for unit testing',
                terms: [{
                    term: 'test',
                    meaning: 'test'
                }]
            });
            cb(null, models, p);
        }, function(err, models, p) {

            test.ifError(err);
            test.ok(p);

            test.ok(p.definitionOne);
            test.ok(p.instanceOne);
            test.ok(p.definitionTwo);
            test.ok(p.instanceTwo);

            testUtility.operatingProcedureCompare(test, p.definitionOne, p.instanceOne, 'instance one');
            testUtility.operatingProcedureCompare(test, p.definitionTwo, p.instanceTwo, 'instance two');

            test.equal(p.fullDefinitionTwo.rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[0].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[1].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[2].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[3].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[4].rangedData.length, 1);
            test.equal(p.fullDefinitionTwo.sections[5].rangedData.length, 1);

            test.done();
        });
    },
};
