module.exports = exports = {
	draft: require("./draft"),
	loadByShortName: require("./loadByShortName"),
	modifyAndPublish: require("./modifyAndPublish"),
	publish: require("./publish"),
	stampAfterTwoEdits: require("./stampAfterTwoEdits"),
	stampAllByCustomerUser: require("./stampAllByCustomerUser"),
	stampEditStamp: require("./stampEditStamp"),
	stamp: require("./stamp"),
	status: require("./status"),
};
