var logger = require('winston'),
    memwatch = require('memwatch-next'),
    util = require('util');
require('./util/errorHandler');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
	level: process.env.LOGLEVEL || 'silly',
    //level: 'silly',
    //level: 'info',
    //level: 'warn',
    //level: 'error',
    //level: 'off',
    colorize: true
});


memwatch.on('leak', function(info) {
    logger.error('!!! memleak in test: ' + util.inspect(info));
});

memwatch.on('stats', function(stats) {
    logger.info('!!! memstats in test: ' + util.inspect(stats));
});
