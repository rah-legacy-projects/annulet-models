var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var auxiliaryDocumentSchema = new Schema({
        auxiliaryDocument: {
            type: ObjectId,
            ref: 'auxiliaryDocuments.AuxiliaryDocument',
            required: true
        },
        customerUser: {
            type: ObjectId,
            ref: 'auth.CustomerUser',
            required: true
        }
    }, {});
    auxiliaryDocumentSchema.plugin(audit.plugin);
    auxiliaryDocumentSchema.plugin(plugins.ActiveAndDeleted);
    return auxiliaryDocumentSchema;
}
