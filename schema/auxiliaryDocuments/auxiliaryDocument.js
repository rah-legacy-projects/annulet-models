var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var auxiliaryDocumentSchema = new Schema({
        file: {
            type: ObjectId,
            required: true
        },
        displayName: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: false
        }
    }, {});
    auxiliaryDocumentSchema.plugin(audit.plugin);
    auxiliaryDocumentSchema.plugin(plugins.ActiveAndDeleted);
    return auxiliaryDocumentSchema;
};
