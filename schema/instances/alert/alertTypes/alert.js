var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractAlertSchema = require('../abstractAlert'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var alertSchema = new(AbstractAlertSchema(options))({
        rangedData: [{
            type: ObjectId,
            ref: 'instances.alert.ranged.Alert'
        }],
        alertDefinition: {
            type: ObjectId,
            ref: 'definitions.alert.Alert',
            required: true
        }
    });
    return alertSchema;
}
