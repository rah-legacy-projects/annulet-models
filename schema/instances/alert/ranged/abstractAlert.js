var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    util = require('util'),
    moment = require('moment');

module.exports = exports = function(options) {

    var alertSchema = function() {
        Schema.apply(this, arguments);
        this.add({
            dismissedDate: {
                type: Date,
                required: false
            }
        });
        this.plugin(datesplice.plugin);
        this.plugin(audit.plugin);
        this.plugin(plugins.ActiveAndDeleted);
        this.$base = 'instances.alert.ranged.Alert';
    };
    util.inherits(alertSchema, Schema);
    return alertSchema;
}
