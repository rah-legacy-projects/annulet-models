var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractAlertSchema = require('../abstractAlert'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var alertSchema = new(AbstractAlertSchema(options))({
        markdown: {
            type: String,
            required: true
        },
        alertLevel: {
            type: String,
            required: true
        },
        publishDate: {
            type: Date,
            required: false
        }
    });
    return alertSchema;
}
