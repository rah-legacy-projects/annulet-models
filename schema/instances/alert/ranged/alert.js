var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    AbstractAlertSchema = require('./abstractAlert'),
    moment = require('moment');

module.exports = exports = function(options) {
    var alertSchema = new (AbstractAlertSchema(options))({});
    return alertSchema;
}
