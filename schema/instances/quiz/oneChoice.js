var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    logger = require('winston'),
    _ = require('lodash');

module.exports = exports = function(options) {
    var questionSchema = new Schema({
        questionText: {
            type: String,
            required: true
        },
        answers: [{
            answerText: {
                type: String,
                required: true
            },
            isCorrect: {
                type: Boolean,
                required: true,
                default: false
            }
        }],
        selectedAnswer: {
            type: ObjectId //references the parent _id of the answers collection
        }
    });
    questionSchema.plugin(audit.plugin);
    questionSchema.plugin(plugins.ActiveAndDeleted);

    questionSchema.methods.saveAnswers = function(answer, cb) {
        this.selectedAnswer = answer._id;
        this.save(cb);
    };

    questionSchema.methods.score = function(cb) {
        logger.silly('[onechoice] scoring');
        var self = this;
        if (!self.selectedAnswer) {
            return cb({
                error: 'No answer selected.',
                question: self._id,
                questionText: self.questionText,
                questionType: 'OneChoice'
            });
        }

        var correctAnswer = _.chain(self.answers)
            .find(function(a) {
                return a.isCorrect;
            })
            .value()._id;

        var correctlySelected = (self.selectedAnswer.toString() == correctAnswer.toString() ? 1 : 0);
        var incorrectlySelected = (self.selectedAnswer.toString() != correctAnswer.toString() ? 1 : 0);
        var score = correctlySelected;

        return cb(null, {
            question: self._id,
            questionText: self.questionText,
            correctlySelected: correctlySelected.length,
            incorrectlySelected: incorrectlySelected.length,
            score: score,
            outOf: 1
        });
    };
    return questionSchema;
};
