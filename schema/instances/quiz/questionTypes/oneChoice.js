var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    AbstractQuestionSchema = require('../abstractQuestion'),
    audit = require('annulet-multitenant-audit'),
    logger = require('winston'),
    util = require('util'),
    _ = require('lodash');

module.exports = exports = function(options) {
    var questionSchema = new(AbstractQuestionSchema(options))({
        questionText: {
            type: String,
            required: true
        },
        answers: [{
            answerText: {
                type: String,
                required: true
            },
            isCorrect: {
                type: Boolean,
                required: true,
                default: false
            }
        }],
        selectedAnswer: {
            type: ObjectId //references the parent _id of the answers collection
        }
    });
    questionSchema.plugin(audit.plugin);
    questionSchema.plugin(plugins.ActiveAndDeleted);

    questionSchema.methods.saveAnswers = function(answer, cb) {
        logger.silly('[onechoice save answer] saving ' + this.questionText + ' with id ' + answer._id);
        this.selectedAnswer = answer._id;
        this.save(cb);
    };

    questionSchema.methods.score = function(cb) {
        logger.silly('[onechoice] scoring');
        var self = this;
        if (!self.selectedAnswer) {
            logger.silly('[onechoice] answer not found for ' + self.questionText);
            return cb({
                error: 'No answer selected.',
                question: self._id,
                questionText: self.questionText,
                questionType: 'OneChoice'
            });
        }

        logger.silly('[onechoice] what are the answers to this question? ' + util.inspect(self.answers));

        var temp = _.chain(self.answers)
            .find(function(a) {
                logger.silly('[onechoice] \t\t' + a.answerText + ' is correct? ' + a.isCorrect);
                return a.isCorrect;
            })
            .value();
        logger.silly('[onechoice] correct answer: ' + util.inspect(temp));
        var correctAnswer = temp._id;

        var correctlySelected = (self.selectedAnswer.toString() == correctAnswer.toString() ? 1 : 0);
        var incorrectlySelected = (self.selectedAnswer.toString() != correctAnswer.toString() ? 1 : 0);
        var score = correctlySelected;

        return cb(null, {
            question: self._id,
            questionText: self.questionText,
            correctlySelected: correctlySelected.length,
            incorrectlySelected: incorrectlySelected.length,
            score: score,
            outOf: 1
        });
    };
    return questionSchema;
};
