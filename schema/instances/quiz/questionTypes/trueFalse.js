var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    logger = require('winston'),
    audit = require('annulet-multitenant-audit'),
    AbstractQuestionSchema = require('../abstractQuestion'),
    moment = require('moment');

module.exports = exports = function(options) {
    var questionSchema = new (AbstractQuestionSchema(options))({
        questionText: {
            type: String,
            required: true
        },
        correctAnswer: {
            type: Boolean,
            required: true
        },
        selectedAnswer: {
            type: Boolean,
            required: false
        }
    });
    questionSchema.plugin(audit.plugin);
    questionSchema.plugin(plugins.ActiveAndDeleted);


    questionSchema.methods.saveAnswers = function(answers, cb) {
        logger.silly('[true false] saving answers for ' + this.questionText);
        this.selectedAnswer = answers;
        this.save(cb);
    };

    questionSchema.methods.score = function(cb) {
        logger.silly('[true false] scoring');
        var self = this;
        if (self.selectedAnswer === null || self.selectedAnswer === undefined) {
            logger.silly('[true false] no answer for ' + self.questionText);
            return cb({
                error: 'No answer selected.',
                question: self._id,
                questionText: self.questionText,
                questionType: 'TrueFalse'
            });
        }

        var correctlySelected = (self.selectedAnswer == self.correctAnswer ? 1 : 0);
        var incorrectlySelected = (self.selectedAnswer != self.correctAnswer ? 1 : 0);
        var score = correctlySelected;

        return cb(null, {
            question: self._id,
            questionText: self.questionText,
            correctlySelected: correctlySelected,
            incorrectlySelected: incorrectlySelected,
            score: score,
            outOf: 1
        });
    };
    return questionSchema;
};
