var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    AbstractQuestionSchema = require('../abstractQuestion'),
    logger = require('winston'),
    _ = require('lodash');

module.exports = exports = function(options) {
    var questionSchema = new (AbstractQuestionSchema(options))({
        questionText: {
            type: String,
            required: true
        },
        answers: [{
            answerText: {
                type: String,
                required: true
            },
            isCorrect: {
                type: Boolean,
                required: true,
                default: false
            }
        }],
        selectedAnswers: [{
            type: ObjectId //references the parent _id of the answers collection
        }]
    });
    questionSchema.plugin(audit.plugin);
    questionSchema.plugin(plugins.ActiveAndDeleted);

    questionSchema.methods.saveAnswers = function(answers, cb) {
        logger.silly('[multichoice save answer] saving ' + this.questionText);
        this.selectedAnswers = _.map(answers, function(answer) {
            return answer._id;
        });
        this.save(cb);
    };

    questionSchema.methods.score = function(cb) {
        logger.silly('[multichoice] scoring');
        var self = this;
        if (!self.selectedAnswers || self.selectedAnswers.length == 0) {
            logger.silly('[multichoice] no answers for ' + self.questionText);
            return cb({
                error: 'No answers selected.',
                question: self._id,
                questionText: self.questionText,
                questionType: 'MultipleChoice'
            });
        }

        var correctAnswers = _.chain(self.answers)
            .filter(function(a) {
                return a.isCorrect;
            })
            .pluck('_id')
            .map(function(a) {
                return a.toString();
            })
            .value();

        var selectedAnswers = _.map(self.selectedAnswers, function(a) {
            return a.toString();
        });

        var correctlySelected = _.intersection(correctAnswers, selectedAnswers);
        var incorrectlySelected = _.difference(selectedAnswers, correctlySelected);

        var score = correctlySelected.length - incorrectlySelected.length;
        if (score < 0) {
            score = 0;
        }

        return cb(null, {
            question: self._id,
            questionText: self.questionText,
            correctlySelected: correctlySelected.length,
            incorrectlySelected: incorrectlySelected.length,
            score: score,
            outOf: correctAnswers.length
        });
    };
    return questionSchema;
};
