var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var quizSchema = new Schema({
        attempts:[{
            type:ObjectId,
            ref: 'instances.quiz.ranged.Attempt'
        }],
        quizDefinition: {
            type:ObjectId,
            ref: 'definitions.quiz.Quiz'
        },
        customerUser:{
            type:ObjectId,
            ref: 'auth.CustomerUser'
        }
    }, {});

    quizSchema.plugin(audit.plugin);
    quizSchema.plugin(plugins.ActiveAndDeleted);
    return quizSchema;
}
