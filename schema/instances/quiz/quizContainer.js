var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var quizContainerSchema = new Schema({
        quizzes: [{
            type: ObjectId,
            ref: 'Quiz'
        }]
    }, {});
    quizContainerSchema.plugin(audit.plugin);
    quizContainerSchema.plugin(plugins.ActiveAndDeleted);
    return quizContainerSchema;
}
