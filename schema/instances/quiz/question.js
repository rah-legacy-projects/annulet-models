var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ObjectId = Schema.Types.ObjectId,
	AbstractQuestionSchema = require('./abstractQuestion');

module.exports = exports = function(options){
	 var questionSchema = new (AbstractQuestionSchema(options))({});
	 return questionSchema;
};
