var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    _ = require('lodash'),
    plugins = require('aio-mongoose-plugins'),
    datesplice = require('mongoose-date-splice'),
    async = require('async'),
    logger = require('winston'),
    util = require('util'),
    audit = require('annulet-multitenant-audit');

module.exports = exports = function(options) {
    var attemptSchema = new Schema({
        questions: [{
            type: ObjectId,
            ref: 'instances.quiz.Question'
        }],
        quizDefinitionRange: {
            type: ObjectId,
            ref: 'definitions.quiz.ranged.Quiz'
        },
        finalizedDate: {
            type: Date,
            required: false
        }
    });

    attemptSchema.methods.score = function(cb) {
        //assumes questions and answers are loaded
        //assumes quiz definition range is loaded
        //reduce the out of into a total
        var self = this;
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            function(p, cb) {
                //check to see if the quiz definition range is loaded
                if (/^[0-9a-fA-F]{24}$/.test(self.quizDefinitionRange.toString())) {
                    //todo: use self's models to derive the quiz definition range
                    throw "quiz definition range must be loaded to score"
                } else {
                    if (!self.quizDefinitionRange.rangeActive) {
                        p = {
                            attempt: self._id,
                            finalizedDate: self.finalizedDate,
                            passing: false,
                        };
                        return cb({
                            rangeInactive:true
                        }, p);
                    }

                    cb(null, p);
                }
            },
            function(p, cb) {
                if (!self.finalizedDate) {
                    cb({
                        error: 'Attempt was not finalized by user.',
                        attempt: self._id
                    });
                } else {
                    cb(null, {});
                }
            },
            function(p, cb) {
                async.parallel(_.map(self.questions, function(question) {
                    return function(cb) {
                        logger.silly('[attempt score] question: ' + util.inspect(question));
                        question.score(function(err, score) {
                            cb(err, score);
                        });
                    }
                }), function(err, scores) {
                    if (!!err) {
                        return cb({
                            error: err,
                            attempt: self._id
                        });
                    }
                    p.scores = scores;
                    cb(err, p);
                });
            },
            function(p, cb) {
                var outOf = _.chain(p.scores)
                    .map(function(score) {
                        return score.outOf;
                    })
                    .reduce(function(sum, num) {
                        return sum + num;
                    }, 0)
                    .value();
                //reduce the correctly selected into a total
                var totalScore = _.chain(p.scores)
                    .map(function(score) {
                        return score.score;
                    })
                    .reduce(function(sum, num) {
                        return sum + num;
                    }, 0)
                    .value();
                //call back with the overall score
                p = {
                    attempt: self._id,
                    finalizedDate: self.finalizedDate,
                    outOf: outOf,
                    totalScore: totalScore,
                    overallScore: ((totalScore / outOf) * 100),
                    passing: ((totalScore / outOf) * 100) >= self.quizDefinitionRange.passingPercentage,
                    questionScores: p.scores
                };
                cb(null, p);
            }
        ], function(err, p) {
            //hack: figure out a better way to account for range inactivity
            if(!!err && !!err.rangeInactive){
                return cb(null, p);
            }
            cb(err, p);
        });
    };

    attemptSchema.plugin(audit.plugin);
    attemptSchema.plugin(datesplice.plugin);
    attemptSchema.plugin(plugins.ActiveAndDeleted);

    return attemptSchema;
};
