var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ObjectId = Schema.Types.ObjectId,
	audit = require('annulet-multitenant-audit'),
	plugins = require('aio-mongoose-plugins'),
	util = require('util');

module.exports = exports = function(options){
	var abstractQuestionSchema = function(){
		Schema.apply(this, arguments);
		this.add({
			questionDefinition: {
				type:ObjectId,
				ref: 'definitions.quiz.Question'
			},
			questionDefinitionRange:{
				type:ObjectId,
				ref: 'definitions.quiz.ranged.Question'
			}
		});

		this.plugin(plugins.ActiveAndDeleted);
		this.plugin(audit.plugin);
		this.$base = 'instances.quiz.Question';
	};
	util.inherits(abstractQuestionSchema, Schema);
	return abstractQuestionSchema;
};
