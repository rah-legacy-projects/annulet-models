var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    logger = require('winston'),
    audit = require('annulet-multitenant-audit'),
    plugins = require('aio-mongoose-plugins');

module.exports = exports = function(options) {
    var questionSchema = new Schema({
        questionText: {
            type: String,
            required: true
        },
        questionOrdinal: {
            type: Number,
            required: true,
            default: 0
        },
        answers: [{
            leftText: {
                type: String,
                required: true
            },
            rightText: {
                type: String,
                required: true
            },
            isRequired: {
                type: Boolean,
                required: true
            },
        }],
        selectedAnswers: [{
            leftText: {
                type: String,
                required: true
            },
            rightText: {
                type: String,
                required: true
            }
        }]
    });
    questionSchema.plugin(audit.plugin);
    questionSchema.plugin(plugins.ActiveAndDeleted);

    questionSchema.methods.saveAnswers = function(answers, cb) {
        this.selectedAnswers = _.map(answers, function(answer) {
            return {
                leftText: answer.leftText,
                rightText: answer.rightText,
            };
        });
        this.save(cb);
    };

    questionSchema.methods.score = function(cb) {
        logger.silly('[matching] scoring question');
        var self = this;
        if (!self.selectedAnswers || self.selectedAnswers.length == 0) {
            return cb({
                error: 'No answers selected.',
                question: self._id
            });
        }
        if (self.selectedAnswers.length !== self.answers.length) {
            return cb({
                error: 'Not all selections were made.',
                question: self._id
            });
        }

        var correctlySelected = _.filter(self.selectedAnswers, function(sa) {
            return _.any(self.answers, function(a) {
                return sa.leftText === a.leftText && sa.rightText === a.rightText;
            });
        });

        var incorrectlySelected = _.reject(self.selectedAnswers, function(sa) {
            return _.any(self.answers, function(a) {
                return sa.leftText === a.leftText && sa.rightText === a.rightText;
            });
        });

        return cb(null, {
            question: self._id,
            correctlySelected: correctlySelected.length,
            incorrectlySelected: incorrectlySelected.length,
            score: correctlySelected.length,
            outOf: self.answers.length
        });
    };
    return questionSchema;
};
