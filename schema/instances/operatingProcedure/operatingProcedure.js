var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var operatingProcedureSchema = new Schema({
        rangedData: [{
            type: ObjectId,
            ref: 'instances.operatingProcedure.ranged.OperatingProcedure'
        }],
        sections: [{
            type: 'ObjectId',
            ref: 'instances.operatingProcedure.Section'
        }],
        customerUser: {
            type: ObjectId,
            ref: 'auth.CustomerUser',
            required: true
        },
        operatingProcedureDefinition: {
            type: ObjectId,
            ref: 'definitions.operatingProcedure.OperatingProcedure',
            required: true
        },
        shortName: {
            type: String,
            required: true
        }
    }, {});
    operatingProcedureSchema.plugin(audit.plugin);
    operatingProcedureSchema.plugin(plugins.ActiveAndDeleted);
    return operatingProcedureSchema;
};
