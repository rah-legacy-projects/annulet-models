var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractSectionSchema = function() {
        Schema.apply(this, arguments);
        this.add({
            sectionDefinition: {
                type: ObjectId,
                required: true
            }
        });

        this.plugin(audit.plugin);
        this.plugin(plugins.ActiveAndDeleted);

        //hack: specify the model to use as the base for discrimination;
        this.$base = 'instances.operatingProcedure.Section';
    };
    util.inherits(abstractSectionSchema, Schema);
    return abstractSectionSchema;
};
