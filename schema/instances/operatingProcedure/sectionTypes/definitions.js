var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var definitionsSchema = new(AbstractSectionSchema(options))({
        rangedData: [{
            type: ObjectId,
            ref: 'instances.operatingProcedure.ranged.sectionTypes.Content'
        }]

    });
    return definitionsSchema;
};
