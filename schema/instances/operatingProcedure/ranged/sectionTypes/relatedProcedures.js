var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var relatedProceduresSchema = new(AbstractSectionSchema(options))({
        relatedProcedures: [{
            type: ObjectId,
            ref: 'definitions.training.Training'
        }]
    });
    return relatedProceduresSchema;
};
