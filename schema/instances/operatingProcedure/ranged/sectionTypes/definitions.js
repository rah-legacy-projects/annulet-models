var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var definitionsSchema = new(AbstractSectionSchema(options))({
        terms: [{
            term: {
                type: String,
                required: true
            },
            meaning: {
                type: String,
                required: true
            }
        }]
    });
    return definitionsSchema;
};
