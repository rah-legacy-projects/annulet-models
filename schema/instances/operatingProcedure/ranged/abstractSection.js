var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractSectionSchema = function() {
        Schema.apply(this, arguments);
        this.add({
            views: {
                type: Number,
                required: true,
                default: 0
            },
            sectionDefinitionRange: {
                type: ObjectId,
                required: true
            }
        });

        this.plugin(audit.plugin);
        this.plugin(plugins.ActiveAndDeleted);
        this.plugin(datesplice.plugin);

        //hack: specify the model to use as the base for discrimination;
        this.$base = 'instances.operatingProcedure.ranged.Section';
    };
    util.inherits(abstractSectionSchema, Schema);
    return abstractSectionSchema;
};
