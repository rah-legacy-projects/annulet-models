var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');

module.exports = exports = function(options) {
    var operatingProcedureSchema = new Schema({
        completedDate: {
            type: Date,
            required: false,
        },
        operatingProcedureDefinitionRange: {
            type: ObjectId,
            ref: 'definitions.operatingProcedure.ranged.OperatingProcedure',
            required:true
        }
    }, {});
    operatingProcedureSchema.plugin(audit.plugin);
    operatingProcedureSchema.plugin(datesplice.plugin);
    operatingProcedureSchema.plugin(plugins.ActiveAndDeleted);
    return operatingProcedureSchema;
};
