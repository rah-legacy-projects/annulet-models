var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var auxiliaryDocumentSchema = new Schema({
        rangedData: [{
            type: ObjectId,
            ref: 'definitions.auxiliaryDocument.ranged.AuxiliaryDocument',
        }],
        customerUser:{
            type:ObjectId,
            ref: 'auth.CustomerUser'
        },
        auxiliaryDocumentDefinition:{
            type: ObjectId,
            ref: 'definitions.auxiliaryDocument.AuxiliaryDocument',
            required:true
        },
    }, {});
    auxiliaryDocumentSchema.plugin(audit.plugin);
    auxiliaryDocumentSchema.plugin(plugins.ActiveAndDeleted);
    return auxiliaryDocumentSchema;
};
