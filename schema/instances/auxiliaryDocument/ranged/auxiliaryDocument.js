var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');

module.exports = exports = function(options) {
    var auxiliaryDocumentSchema = new Schema({
        views: {
            type: Number,
            required: true,
            default: 0
        },
        lastViewed: {
            type: Date,
            required: true,
            default: moment()
                .toDate()
        },
        auxiliaryDocumentDefinitionRange: {
            type: ObjectId,
            ref: 'definitions.auxiliaryDocument.ranged.AuxiliaryDocument',
            required: true
        }
    }, {});
    auxiliaryDocumentSchema.plugin(audit.plugin);
    auxiliaryDocumentSchema.plugin(datesplice.plugin);
    auxiliaryDocumentSchema.plugin(plugins.ActiveAndDeleted);
    return auxiliaryDocumentSchema;
};
