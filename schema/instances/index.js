module.exports = exports = {
	workflow: require("./workflow"),
	auxiliaryDocument: require("./auxiliaryDocument"),
	operatingProcedure: require("./operatingProcedure"),
	training: require("./training"),
	quiz: require("./quiz"),
	alert: require("./alert"),
};
