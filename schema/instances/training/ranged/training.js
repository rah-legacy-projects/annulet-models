var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');

module.exports = exports = function(options) {
    var trainingSchema = new Schema({
        completedDate: {
            type: Date,
            required: false,
        },
        trainingDefinitionRange: {
            type: ObjectId,
            ref: 'definitions.training.ranged.Training'
        }
    }, {});
    trainingSchema.plugin(audit.plugin);
    trainingSchema.plugin(plugins.ActiveAndDeleted);
    trainingSchema.plugin(datesplice.plugin);
    return trainingSchema;
};
