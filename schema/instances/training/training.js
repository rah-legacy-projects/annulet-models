var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var trainingSchema = new Schema({
        rangedData: [{
            type: ObjectId,
            ref: 'instances.training.ranged.Training'
        }],
        sections: [{
            type: 'ObjectId',
            ref: 'instances.training.Section'
        }],
        trainingDefinition: {
            type: ObjectId,
            ref: 'definitions.training.Training',
            required: true
        },
        customerUser: {
            type: ObjectId,
            ref: 'auth.CustomerUser',
            required: true
        },
    }, {});
    trainingSchema.plugin(audit.plugin);
    trainingSchema.plugin(plugins.ActiveAndDeleted);
    return trainingSchema;
};
