var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var contentSchema = new(AbstractSectionSchema(options))({
        rangedData: [{
            type: ObjectId,
            ref: 'instances.training.ranged.sectionTypes.MarkdownContent'
        }]
    });
    return contentSchema;
};
