var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    var quizWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        rangedData:[{
            type:ObjectId,
            ref: 'instances.workflow.ranged.workflowItemTypes.QuizWorkflowItem'
        }]
    });
    return quizWorkflowItemSchema;
};
