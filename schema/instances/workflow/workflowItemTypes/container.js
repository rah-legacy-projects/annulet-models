var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    var containerWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        rangedData:[{
            type:ObjectId,
            ref: 'instances.workflow.ranged.workflowItemTypes.ContainerWorkflowItem'
        }]
    });
    return containerWorkflowItemSchema;
};
