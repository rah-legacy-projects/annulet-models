var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    rootWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        rangedData:[{
            type:ObjectId,
            ref: 'instances.workflow.ranged.workflowItemTypes.RootWorkflowItem'
        }]
    });
    return rootWorkflowItemSchema;
};
