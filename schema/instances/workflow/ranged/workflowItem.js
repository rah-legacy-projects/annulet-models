var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('./abstractWorkflowItem'),
    util = require('util');

module.exports = exports = function(options) {
    var workflowItemSchema = new(AbstractWorkflowItemSchema(options))({});
    return workflowItemSchema;
};
