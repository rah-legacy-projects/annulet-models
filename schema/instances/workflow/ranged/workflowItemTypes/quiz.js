var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    var quizWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        quiz: {
            type: ObjectId,
            ref: 'instances.quiz.Quiz',
            required: false
        }
    });
    return quizWorkflowItemSchema;
};
