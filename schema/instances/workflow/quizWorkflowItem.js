var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('./abstractWorkflowItem');

module.exports = exports = function(options) {
    var quizWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        quizContainer: {
            type: ObjectId,
            ref: 'instances.quiz.QuizContainer',
            required: false
        }
    });
    return quizWorkflowItemSchema;
};
