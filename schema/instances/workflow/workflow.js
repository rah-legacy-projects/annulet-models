var mongoose = require('mongoose'),
    audit = require('annulet-multitenant-audit'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

module.exports = exports = function(options) {
    var workflowSchema = new Schema({
        name: {
            type: String,
            required: false
        },
        description:{
            type:String,
            required:false
        },
        rootItem: {
            type: ObjectId,
            ref: 'instances.workflow.WorkflowItem',
            required: true
        },
        workflowDefinition: {
            type: ObjectId,
            ref: "instances.workflow.WorkflowDefinition",
            required: true
        }
    }, {});
    workflowSchema.plugin(audit.plugin);
    workflowSchema.plugin(plugins.ActiveAndDeleted);
    return workflowSchema;
};
