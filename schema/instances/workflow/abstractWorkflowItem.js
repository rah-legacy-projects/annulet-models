var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractWorkflowItemSchema = function() {
        Schema.apply(this, arguments);
        this.add({
            itemDefinition: {
                type: ObjectId,
                required: true
            },
            items: [{
                type: ObjectId
            }],
            sequence: [{
                type: ObjectId
            }],
            customerUser:{
                type:ObjectId,
                ref: 'auth.CustomerUser',
                required:true
            }
        });
        this.plugin(plugins.ActiveAndDeleted);
        this.plugin(datesplice.plugin);
        this.plugin(audit.plugin);
        //hack: specify the model to use as the base for discrimination
        this.$base = 'instances.workflow.WorkflowItem';
    };

    util.inherits(abstractWorkflowItemSchema, Schema)
    return abstractWorkflowItemSchema;
};
