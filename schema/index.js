module.exports = exports = {
	Shim: require("./shim"),
	Customer: require("./customer"),
	complaints: require("./complaints"),
	definitions: require("./definitions"),
	auth: require("./auth"),
	tracking: require("./tracking"),
	instances: require("./instances"),
};
