var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');

module.exports = exports = function(options) {
    var auxiliaryDocumentSchema = new Schema({
        file: {
            type: ObjectId,
            required: false
        },
        displayName: {
            type: String,
            required: false
        },
        extension: {
            type:String,
            required:false
        },
        description: {
            type: String,
            required: false
        },
        publishDate: {
            type: Date,
            required: false
        }
    }, {});
    auxiliaryDocumentSchema.plugin(audit.plugin);
    auxiliaryDocumentSchema.plugin(datesplice.plugin);
    auxiliaryDocumentSchema.plugin(plugins.ActiveAndDeleted);
    auxiliaryDocumentSchema.statics.compare = function(a, b) {
        return a.file.toString() == b.file.toString() &&
            a.displayName == b.displayName &&
            a.description == b.description;
    };
    return auxiliaryDocumentSchema;
};
