var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    audit = require('annulet-multitenant-audit'),
    plugins = require('aio-mongoose-plugins'),
    moment = require('moment');

module.exports = exports = function(options) {
    var workflowSchema = new Schema({
        name: {
            type: String,
            required: false
        },
        description: {
            type:String,
            required:false
        },
        rootItem: {
            type: ObjectId,
            ref: 'WorkflowItemDefinition',
            required: true
        },
        customer: {
            type: ObjectId,
            ref: 'Customer',
            required: true,
            $tenant: false
        },
        employeeClass: [{
            type: String,
            required: true
        }]
    }, {});
    workflowSchema.plugin(audit.plugin);
    workflowSchema.plugin(plugins.ActiveAndDeleted);
    return workflowSchema;
};
