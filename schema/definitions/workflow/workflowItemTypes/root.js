var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    rootWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        shortName: {
            type:String,
            required:true
        },
        rangedData:[{
            type:ObjectId,
            ref: 'definitions.workflow.ranged.workflowItemTypes.Root'
        }]
    });
    return rootWorkflowItemSchema;
};
