module.exports = exports = {
	Container: require("./container"),
	Quiz: require("./quiz"),
	Training: require("./training"),
	Root: require("./root"),
};
