var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractWorkflowItemSchema = function() {
        Schema.apply(this, arguments);
        this.add({
            isDraftOf: {
                type: Schema.Types.Mixed
            },
            sequence: [{
                type: ObjectId,
                ref: 'definitions.workflow.WorkflowItem'
            }],
            items: [{
                type: ObjectId,
                ref: 'definitions.workflow.WorkflowItem'
            }],
        });
        this.plugin(plugins.ActiveAndDeleted);
        this.plugin(audit.plugin);
        //hack: specify the model to use as the base for discrimination
        this.$base = 'definitions.workflow.WorkflowItem';
    };

    util.inherits(abstractWorkflowItemSchema, Schema)
    return abstractWorkflowItemSchema;
};
