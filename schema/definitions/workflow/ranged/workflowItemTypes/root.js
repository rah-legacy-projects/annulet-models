var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    rootWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        employeeClass: [{
            type: String
        }]
    });
    rootWorkflowItemSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.description = a.description;
        self.employeeClass = a.employeeClass;
    };
    rootWorkflowItemSchema.statics.compare = function(a, b) {
        return a.title == b.title && a.description == b.description && _.isEqual(a.employeeClass, b.employeeClass);
    };
    return rootWorkflowItemSchema;
};
