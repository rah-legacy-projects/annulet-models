var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    var trainingWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        training: {
            type: ObjectId,
            ref: 'definitions.training.Training',
            required: false
        }
    });
    trainingWorkflowItemSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.description = a.description;
        self.training = a.training;
    };
    trainingWorkflowItemSchema.statics.compare = function(a, b){
        return a.title == b.title && 
            a.description == b.description &&
            (a.training||'').toString() == (b.training||'').toString()
    };
    return trainingWorkflowItemSchema;
};
