var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    var quizWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({
        quiz: {
            type: ObjectId,
            ref: 'definitions.quiz.Quiz',
            required: false
        }
    });
    quizWorkflowItemSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.description = a.description;
        self.quiz = a.quiz;
    };
    quizWorkflowItemSchema.statics.compare = function(a, b) {
        return a.title == b.title &&
            a.description == b.description &&
            (a.quiz || '')
            .toString() == (b.quiz || '')
            .toString();
    };
    return quizWorkflowItemSchema;
};
