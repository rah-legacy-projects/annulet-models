var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractWorkflowItemSchema = require('../abstractWorkflowItem');

module.exports = exports = function(options) {
    var containerWorkflowItemSchema = new(AbstractWorkflowItemSchema(options))({});
    containerWorkflowItemSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.description = a.description;
    };
    containerWorkflowItemSchema.statics.compare = function(a, b){
        return a.title == b.title && a.description == b.description;
    };

    return containerWorkflowItemSchema;
};
