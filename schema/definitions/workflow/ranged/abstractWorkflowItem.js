var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractWorkflowItemSchema = function() {
        Schema.apply(this, arguments);

        this.add({
            title: {
                type: String,
                required: false
            },
            description: {
                type: String,
                required: false
            }
        });
        this.plugin(plugins.ActiveAndDeleted);
        this.plugin(datesplice.plugin);
        this.plugin(audit.plugin);
        //hack: specify the model to use as the base for discrimination
        this.$base = 'definitions.workflow.ranged.WorkflowItem';
    };

    util.inherits(abstractWorkflowItemSchema, Schema)
    return abstractWorkflowItemSchema;
};
