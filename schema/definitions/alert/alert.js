var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');


module.exports = exports = function(options) {
    var alertSchema = new Schema({
        rangedData:[{
            type:ObjectId,
            ref:'definitions.alerts.ranged.Alert'
        }],
        isDraftOf:{
            type:Schema.Types.Mixed,
            required:false
        }
    }, {});
    alertSchema.plugin(audit.plugin);
    alertSchema.plugin(plugins.ActiveAndDeleted);
    return alertSchema;
};
