var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');


module.exports = exports = function(options) {
    var alertSchema = new Schema({
        markdown: {
            type: String,
            required: true
        },
        alertLevel: {
            type: String,
            required: true
        },
        publishDate:{
            type:Date,
            required:false
        }
    }, {});
    alertSchema.plugin(datesplice.plugin);

    alertSchema.plugin(audit.plugin);
    alertSchema.plugin(plugins.ActiveAndDeleted);

    alertSchema.statics.compare = function(a, b){
        return a.markdown == b.markdown &&
            a.alertLevel == b.alertLevel;
    };

    return alertSchema;
};
