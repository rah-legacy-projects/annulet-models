var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ObjectId = Schema.Types.ObjectId,
	audit = require('annulet-multitenant-audit'),
	plugins = require('aio-mongoose-plugins'),
	util = require('util');

module.exports = exports = function(options){
	var abstractQuestionSchema = function(){
		Schema.apply(this, arguments);
		this.add({
			isDraftOf:{
				type:Schema.Types.Mixed,
				required:false
			},
			isRequired:{
				type:Boolean,
				required:true,
				default:false
			}
		});

		this.plugin(plugins.ActiveAndDeleted);
		this.plugin(audit.plugin);
		this.$base = 'definitions.quiz.Question';
	};
	util.inherits(abstractQuestionSchema, Schema);
	return abstractQuestionSchema;
};
