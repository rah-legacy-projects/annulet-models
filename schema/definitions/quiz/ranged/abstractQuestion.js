var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    audit = require('annulet-multitenant-audit'),
    plugins = require('aio-mongoose-plugins'),
    datesplice = require('mongoose-date-splice'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractQuestionSchema = function() {
        Schema.apply(this, arguments);
        this.add({
            questionText: {
                type: String,
                required: true
            },
            isRequired:{
                type:Boolean,
                required:true,
                default:false
            }
        });

        this.plugin(plugins.ActiveAndDeleted);
        this.plugin(audit.plugin);
        this.plugin(datesplice.plugin);
        this.$base = 'definitions.quiz.ranged.Question';
    };
    util.inherits(abstractQuestionSchema, Schema);
    return abstractQuestionSchema;
};
