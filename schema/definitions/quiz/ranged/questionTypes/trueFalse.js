var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    AbstractQuestionSchema = require('../abstractQuestion'),
    moment = require('moment');

module.exports = exports = function(options) {
    var questionSchema = new (AbstractQuestionSchema(options))({
        correctAnswer: {
            type: Boolean,
            required: true
        }
    });

    questionSchema.methods.merge = function(a){
        var self = this;
        self.questionText = a.questionText;
        self.isRequired = a.isRequired;
        self.correctAnswer = a.correctAnswer;
    };

    questionSchema.statics.compare = function(a, b){
        return a.isRequired == b.isRequired &&
            a.correctAnswer == b.correctAnswer &&
            a.questionText == b.questionText;

    };
    return questionSchema;
}
