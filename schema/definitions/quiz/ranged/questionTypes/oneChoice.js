var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    _ = require('lodash'),
    datesplice = require('mongoose-date-splice'),
    AbstractQuestionSchema = require('../abstractQuestion'),
    moment = require('moment');

module.exports = exports = function(options) {
    var questionSchema = new(AbstractQuestionSchema(options))({
        answerQuota: {
            type: Number,
            required: true
        },
        answers: [{
            answerText: {
                type: String,
                required: true
            },
            isCorrect: {
                type: Boolean,
                required: true,
                default: false
            },
            isRequired: {
                type: Boolean,
                required: true,
                default: false
            }
        }]
    });

    questionSchema.methods.merge = function(a) {
        var self = this;
        self.questionText = a.questionText;
        self.isRequired = a.isRequired;
        self.answerQuota = a.answerQuota;
        self.answers = _.map(a.answers, function(answer) {
            return {
                answerText: answer.answerText,
                isCorrect: answer.isCorrect,
                isRequired: answer.isRequired
            };
        });
    };

    questionSchema.statics.compare = function(a, b) {
        return a.isRequired == b.isRequired &&
            a.questionText == b.questionText &&
            a.answerQuota == b.answerQuota &&
            _.all(a.answers, function(aa) {
                return _.any(b.answers, function(bb) {
                    return aa.answerText == bb.answerText &&
                        aa.isCorrect == bb.isCorrect &&
                        aa.isRequired == bb.isRequired;
                });
            }) &&
            _.all(b.answers, function(aa) {
                return _.any(a.answers, function(bb) {
                    return aa.answerText == bb.answerText &&
                        aa.isCorrect == bb.isCorrect &&
                        aa.isRequired == bb.isRequired;
                });
            });

    };
    return questionSchema;
}
