module.exports = exports = {
	TrueFalse: require("./trueFalse"),
	OneChoice: require("./oneChoice"),
	MultipleChoice: require("./multipleChoice"),
	Matching: require("./matching"),
};
