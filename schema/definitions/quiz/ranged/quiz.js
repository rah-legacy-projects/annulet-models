var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');

module.exports = exports = function(options) {
    var quizSchema = new Schema({
        questionQuota: {
            type: Number,
            required: false
        },
        directions: {
            type: String,
            required: false
        },
        description:{
            type:String,
            required:false
        },
        title: {
            type: String,
            required: false
        },
        passingPercentage: {
            type: Number,
            required: false
        }
    }, {});
    quizSchema.plugin(audit.plugin);
    quizSchema.plugin(datesplice.plugin);
    quizSchema.plugin(plugins.ActiveAndDeleted);

    quizSchema.statics.compare = function(a, b){
        return a.questionQuota == b.questionQuota &&
            a.directions == b.directions &&
            a.description == b.description &&
            a.title == b.title &&
            a.passingPercentage == b.passingPercentage;
    };

    return quizSchema;
}
