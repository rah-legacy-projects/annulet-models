var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var questionSchema = new Schema({
        questionText: {
            type: String,
            required: true
        },
        questionOrdinal: {
            type: Number,
            required: false
        },
        isRequired: {
            type: Boolean,
            required: true,
            default: true
        },
        answerQuota: {
            type: Number,
            required: true
        },
        answers: [{
            leftText: {
                type: String,
                required: true
            },
            rightText: {
                type: String,
                required: true
            },
            isRequired: {
                type: Boolean,
                required: true
            },
        }]
    });
    questionSchema.plugin(audit.plugin);
    questionSchema.plugin(plugins.ActiveAndDeleted);
    return questionSchema;
};
