var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    AbstractQuestionSchema = require('../abstractQuestion'),
    moment = require('moment');

module.exports = exports = function(options) {
    var questionSchema = new (AbstractQuestionSchema(options))({
        isDraftOf:{
            type:Schema.Types.Mixed
        },
        rangedData:[{
            type:ObjectId,
            ref: 'definitions.quiz.ranged.questionTypes.MultipleChoice'
        }]
    });
    return questionSchema;
};
