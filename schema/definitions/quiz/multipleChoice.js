var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var questionSchema = new Schema({
        questionText: {
            type: String,
            required: true
        },
        isRequired: {
            type: Boolean,
            required: true,
            default: false
        },
        answerQuota: {
            type: Number,
            required: true
        },
        answers: [{
            answerText: {
                type: String,
                required: true
            },
            isCorrect: {
                type: Boolean,
                required: true,
                default: false
            },
            isRequired: {
                type: Boolean,
                required: true,
                default: false
            }
        }]
    });
    questionSchema.plugin(audit.plugin);
    questionSchema.plugin(plugins.ActiveAndDeleted);
    return questionSchema;
}
