module.exports = exports = {
	Question: require("./question"),
	Quiz: require("./quiz"),
	AbstractQuestion: require("./abstractQuestion"),
	questionTypes: require("./questionTypes"),
	ranged: require("./ranged"),
};
