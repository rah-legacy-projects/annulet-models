var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var quizSchema = new Schema({
        questions: [{
            type:ObjectId,
            ref: 'definitions.quiz.Question'
        }],
        rangedData:[{
            type:ObjectId,
            ref: 'definitions.quiz.ranged.Quiz'
        }],
        isDraftOf:{
            type:Schema.Types.Mixed
        },
        shortName:{
            type:String,
            required:true
        }
    }, {});
    quizSchema.plugin(audit.plugin);
    quizSchema.plugin(plugins.ActiveAndDeleted);
    return quizSchema;
}
