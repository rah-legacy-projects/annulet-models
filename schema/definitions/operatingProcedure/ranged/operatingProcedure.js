var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');

module.exports = exports = function(options) {
    var operatingProcedureSchema = new Schema({
        title: {
            type: String,
            required: false
        },
        description: {
            type: String,
            required: false
        },
        publishDate: {
            type: Date,
            required: false,
        }
    }, {});
    operatingProcedureSchema.plugin(audit.plugin);
    operatingProcedureSchema.plugin(datesplice.plugin);
    operatingProcedureSchema.plugin(plugins.ActiveAndDeleted);

    operatingProcedureSchema.statics.compare = function(a, b) {
        return a.title == b.title &&
            a.description == b.description;
    };

    return operatingProcedureSchema;
};
