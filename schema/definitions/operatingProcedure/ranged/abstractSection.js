var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractSectionSchema = function() {
        Schema.apply(this, arguments);
        this.add({
            title: {
                type: String,
                required: true
            },
            content: {
                type: String,
                required: false
            },
            publishDate:{
                type:Date,
                required:false,
            }
        });

        this.plugin(plugins.ActiveAndDeleted);
        this.plugin(audit.plugin);
        this.plugin(datesplice.plugin);
        //hack: specify the model to use as the base for discrimination
        this.$base = 'definitions.operatingProcedure.ranged.Section';
    };
    util.inherits(abstractSectionSchema, Schema);
    return abstractSectionSchema;
};
