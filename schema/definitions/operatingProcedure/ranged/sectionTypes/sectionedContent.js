var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    _ = require('lodash'),
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var sectionedContentSchema = new(AbstractSectionSchema(options))({
        subSections: [{
            title: {
                type: String,
                required: true
            },
            content: {
                type: String,
                required: true
            }
        }]
    });
    sectionedContentSchema.statics.compare = function(a, b) {
        return a.title == b.title &&
            a.content == b.content &&
            _.all(a.subSections, function(aterm) {
                return _.any(b.subSections, function(bterm) {
                    return bterm.title == aterm.title && bterm.content == aterm.content;
                });
            });
    };

    sectionedContentSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.content = a.content;
        self.publishDate = a.publishDate; //todo: should publish dates be copied at merge time?
        self.subSections = [];
        _.each(a.subSections, function(section) {
            self.subSections.push({
                title: section.title,
                content: section.content
            });
        });
    };

    return sectionedContentSchema;
};
