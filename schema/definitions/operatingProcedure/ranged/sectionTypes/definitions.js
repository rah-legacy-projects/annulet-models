var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    logger = require('winston'),
    ObjectId = Schema.Types.ObjectId,
    _ = require('lodash'),
    util = require('util'),
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var definitionsSchema = new(AbstractSectionSchema(options))({
        terms: [{
            term: {
                type: String,
                required: true
            },
            meaning: {
                type: String,
                required: true
            }
        }]
    });

    definitionsSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.content = a.content;
        self.publishDate = a.publishDate; //todo: should publish dates be copied at merge time?
        self.terms = [];
        _.each(a.terms, function(term) {
            self.terms.push({
                term: term.term,
                meaning: term.meaning
            });
        });
    };

    definitionsSchema.statics.compare = function(a, b) {
        var termSameness = _.all(a.terms, function(aterm) {
            var innerSameness = _.any(b.terms, function(bterm) {
                return bterm.term == aterm.term && bterm.meaning == aterm.meaning;
            });
            return innerSameness;
        });
        return a.title == b.title &&
            a.content == b.content && termSameness;

    };
    return definitionsSchema;
};
