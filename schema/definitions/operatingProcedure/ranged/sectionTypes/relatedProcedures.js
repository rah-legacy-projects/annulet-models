var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    _ = require('lodash'),
    AbstractSectionSchema = require('../abstractSection');
module.exports = exports = function(options) {
    var relatedProceduresSchema = new(AbstractSectionSchema(options))({
        relatedProcedures: [{
            type: ObjectId,
            ref: 'definitions.training.Training'
        }]
    });
    relatedProceduresSchema.statics.compare = function(a, b) {
        return a.title == b.title &&
            a.content == b.content &&
            _.all(a.relatedProcedures, function(aterm) {
                return _.any(b.relatedProcedures, function(bterm) {
                    return bterm.toString() == aterm.toString()
                });
            });
    };

    relatedProceduresSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.content = a.content;
        self.publishDate = a.publishDate; //todo: should publish dates be copied at merge time?
        self.relatedProcedures = [];
        _.each(a.relatedProcedures, function(relatedProcedures) {
            self.relatedProcedures.push(relatedProcedures);
        });
    };
    return relatedProceduresSchema;
};
