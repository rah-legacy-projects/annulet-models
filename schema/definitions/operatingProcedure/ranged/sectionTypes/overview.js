var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    _ = require('lodash'),
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var overviewSchema = new(AbstractSectionSchema(options))({});
    overviewSchema.statics.compare = function(a, b) {
        return a.title == b.title &&
            a.content == b.content;
    };
    overviewSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.content = a.content;
        self.publishDate = a.publishDate; //todo: should publish dates be copied at merge time?
    };
    return overviewSchema;
};
