var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var overviewSchema = new(AbstractSectionSchema(options))({
        isDraftOf: {
            type: Schema.Types.Mixed,
            //ref: 'definitions.operatingProcedure.sectionTypes.Overview' || 'new'
        },
        rangedData:[{
            type:ObjectId,
            ref: 'definitions.operatingProcedure.ranged.sectionTypes.Overview'
        }]
    });
    return overviewSchema;
};
