var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var sectionedContentSchema = new(AbstractSectionSchema(options))({
        isDraftOf: {
            type: Schema.Types.Mixed,
            //ref: 'definitions.operatingProcedure.sectionTypes.SectionedContent' || 'new'
        },
        rangedData:[{
            type:ObjectId,
            ref: 'definitions.operatingProcedure.ranged.sectionTypes.SectionedContent'
        }]
    });
    return sectionedContentSchema;
};

