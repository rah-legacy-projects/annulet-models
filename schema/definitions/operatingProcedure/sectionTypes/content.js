var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var contentSchema = new(AbstractSectionSchema(options))({
        isDraftOf: {
            type: Schema.Types.Mixed,
            //ref: 'definitions.operatingProcedure.sectionTypes.Content' || 'new'
        },
        rangedData: [{
            type: ObjectId,
            ref: 'definitions.operatingProcedure.ranged.sectionTypes.Content'
        }]
    });
    return contentSchema;
};
