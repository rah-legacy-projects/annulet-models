var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var definitionsSchema = new(AbstractSectionSchema(options))({
        isDraftOf: {
            type: Schema.Types.Mixed,
            //ref: 'definitions.operatingProcedure.sectionTypes.Definitions' || 'new'
        },
        rangedData:[{
            type:ObjectId,
            ref: 'definitions.operatingProcedure.ranged.sectionTypes.Definitions'
        }]
    });
    return definitionsSchema;
};
