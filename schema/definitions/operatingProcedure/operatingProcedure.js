var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var operatingProcedureSchema = new Schema({
        sections: [{
            type: ObjectId,
            ref: 'definitions.operatingProcedure.Section'
        }],
        rangedData: [{
            type: ObjectId,
            ref: 'definitions.operatingProcedure.ranged.OperatingProcedure'
        }],
        isDraftOf: {
            //ref: 'definitions.operatingProcedure.OperatingProcedure' || new
            type: Schema.Types.Mixed,
        },
        shortName: {
            type: String,
            required: true
        }
    }, {});
    operatingProcedureSchema.plugin(audit.plugin);
    operatingProcedureSchema.plugin(plugins.ActiveAndDeleted);
    return operatingProcedureSchema;
};
