var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment'),
    util = require('util');

module.exports = exports = function(options) {
    var abstractSectionSchema = function() {
        Schema.apply(this, arguments);

        this.add({
            isDraftOf: {
                type: Schema.Types.Mixed,
                required: false
            },
        });

        this.plugin(plugins.ActiveAndDeleted);
        this.plugin(audit.plugin);
        //hack: specify the model to use as the base for discrimination
        this.$base = 'definitions.operatingProcedure.Section';
    };
    util.inherits(abstractSectionSchema, Schema);
    return abstractSectionSchema;
};
