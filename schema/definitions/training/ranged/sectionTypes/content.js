var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var contentSchema = new(AbstractSectionSchema(options))({});

    contentSchema.statics.compare = function(a, b) {
        return a.title == b.title && a.content == b.content;
    };

    contentSchema.methods.merge = function(a) {
        var self = this;
        self.title = a.title;
        self.content = a.content;
        self.publishDate = a.publishDate;
    };

    return contentSchema;
};
