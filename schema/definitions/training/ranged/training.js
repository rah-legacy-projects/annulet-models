var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    datesplice = require('mongoose-date-splice'),
    moment = require('moment');

module.exports = exports = function(options) {
    var trainingSchema = new Schema({
        title: {
            type: String,
            required: false
        },
        description:{
            type:String, required:false
        },
        publishDate:{
            type:Date,
            required:false
        },
    }, {});

    trainingSchema.plugin(audit.plugin);
    trainingSchema.plugin(datesplice.plugin);
    trainingSchema.plugin(plugins.ActiveAndDeleted);

    trainingSchema.statics.compare = function(a, b){
        return a.title == b.title && a.description == b.description;
    };

    return trainingSchema;
};
