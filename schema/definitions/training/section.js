var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('./abstractSection');

module.exports = exports = function(options) {
    var sectionSchema = new(AbstractSectionSchema(options))({});
    return sectionSchema;
};
