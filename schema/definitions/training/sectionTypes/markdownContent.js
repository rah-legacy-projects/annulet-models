var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    AbstractSectionSchema = require('../abstractSection');

module.exports = exports = function(options) {
    var contentSchema = new (AbstractSectionSchema(options))({
        isDraftOf: {
            type: Schema.Types.Mixed
        },
        rangedData: [{
            type: ObjectId,
            ref: 'definitions.training.ranged.sectionTypes.MarkdownContent'
        }]
    });
    return contentSchema;
};
