var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var trainingSchema = new Schema({
        sections: [{
            type: ObjectId,
            ref: 'definitions.training.Section'
        }],
        rangedData: [{
            type: ObjectId,
            ref: 'definitions.training.ranged.Training'
        }],
        isDraftOf: {
            type: Schema.Types.Mixed,
        },
        shortName: {
            required:true,
            type:String
        }
    }, {});

    trainingSchema.plugin(audit.plugin);
    trainingSchema.plugin(plugins.ActiveAndDeleted);

    return trainingSchema;
};
