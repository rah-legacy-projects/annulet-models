var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    _ = require('lodash'),
    logger = require('winston'),
    autoIncrement = require('mongoose-auto-increment'),
    moment = require('moment');

module.exports = exports = function(options) {
    var trackingSchema = new Schema({
        verb: {
            type: String,
            required: false
        },
        path: {
            type: String,
            required: false
        },
        query: {
            type: Schema.Types.Mixed,
            required: false
        },
        params:{
            type:Schema.Types.Mixed,
            required:false
        },
        body: {
            type: Schema.Types.Mixed,
            required: false
        },
        headers: {
            type: Schema.Types.Mixed,
            required: false
        },
        customer: {
            type: Schema.Types.Mixed,
            required: false,
        },
        userAgent: {
            type: Schema.Types.Mixed,
            required: false
        },
        requestedBy:{
            type:String,
            required:false
        }
    }, {});
    trackingSchema.plugin(plugins.CreatedAndModified);
    return trackingSchema;
};
