var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    Mixed = Schema.Types.Mixed,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    _ = require('lodash'),
    logger = require('winston'),
    autoIncrement = require('mongoose-auto-increment'),
    moment = require('moment');

module.exports = exports = function(options) {
    var activity = new Schema({
        changedBy: {
            type: String,
            required: false
        },
        state: {
            type: Mixed,
            required: true
        },
        itemType: {
            type: String,
            required: true
        },
        item: {
            type: Mixed,
            required: true
        },
        message: {
            type: Mixed,
            required: false
        }

    }, {});
    activity.plugin(plugins.CreatedAndModified);
    return activity;
};
