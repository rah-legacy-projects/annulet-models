var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    _ = require('lodash'),
    logger = require('winston'),
    autoIncrement = require('mongoose-auto-increment'),
    moment = require('moment');

module.exports = exports = function(options) {
    var exceptionSchema = new Schema({
        exception: {
            type: Schema.Types.Mixed,
            required: false
        },
        customer: {
            type: Schema.Types.Mixed,
            required: false,
        },
        userAgent: {
            type: Schema.Types.Mixed,
            required: false
        },
        environment: {
            type: Schema.Types.Mixed,
            required: false
        },
        server: {
            type: Schema.Types.Mixed,
            required: false
        }
    }, {});
    exceptionSchema.plugin(autoIncrement.plugin, options.name || 'Exception');
    exceptionSchema.plugin(audit.plugin);

    exceptionSchema.virtual('ticketNumber')
        .get(function() {
            var str = '',
                map = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            var number = this._id;
            while (number >= 0) {
                str += map.charAt(number % map.length);
                number = Math.floor(number / map.length) - 1;
            }
            return _.padLeft(str.split('')
                .reverse()
                .join(''), 8, '0')
        });
    exceptionSchema.set('toJSON', {
        getters: true,
        virtuals: true
    });
    exceptionSchema.set('toObject', {
        virtuals: true
    });
    return exceptionSchema;
};
