var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var complaintSchema = new Schema({
        text: {
            type: String,
            required: true
        },
        complainerName: {
            type: String,
            required: false
        },
        status: {
            type: String,
            required: true
        },
        product: {
            type: String,
            required: true
        },
        subProduct: {
            type: String,
            required: false
        },
        issueType: {
            type: String,
            requried: true
        },
        subIssueType: {
            type: String,
            required: false
        },
        state: {
            type: String,
            required: true
        },
        zip: {
            type: String,
            required: true
        },
        submittedVia: {
            type: String,
            required: true
        },
        dateReceived: {
            type: Date,
            required: true
        },
        dateResponseSent: {
            type: Date,
            required: false
        },
        companyResponseNote: {
            type: String,
            required: false
        },
        timelyResponse: {
            type: String,
            required: false
        },
        consumerDisputed: {
            type: String,
            required: false
        },

        changes: [{
            type: ObjectId,
            ref: 'complaints.Change'
        }],
        comments: [{
            type: ObjectId,
            ref: 'complaints.Comment'
        }],

        createdByCustomerUser: {
            type: ObjectId,
            ref: 'auth.CustomerUser',
            required: true
        },
        customer: {
            type: ObjectId,
            ref: 'Customer',
            required: true,
            $tenant: false
        }

    }, {});
    complaintSchema.plugin(plugins.ActiveAndDeleted);
    complaintSchema.plugin(audit.plugin);
    return complaintSchema;
};
