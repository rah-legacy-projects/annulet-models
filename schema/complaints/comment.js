var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var commentSchema = new Schema({
        text: {
            type: String,
            required: true
        },
        madeBy: {
            type: ObjectId,
            ref: 'auth.CustomerUser'
        }
    }, {});
    commentSchema.plugin(plugins.ActiveAndDeleted);
    commentSchema.plugin(audit.plugin);
    return commentSchema;
};
