var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    logger = require('winston'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var changeSchema = new Schema({
        field: {
            type: String,
            required: false
        },
        changeType: {
            type: String,
            required: true
        },
        before: {
            type: String,
            required: false
        },
        after: {
            type: String,
            required: false
        },
        madeBy: {
            type: ObjectId,
            ref: 'auth.CustomerUser',
            required: true
        }
    }, {});
    changeSchema.plugin(audit.plugin);
    changeSchema.plugin(plugins.ActiveAndDeleted);

    logger.warn('###change schema is a: ' + (changeSchema instanceof mongoose.Schema));

    return changeSchema;
};
