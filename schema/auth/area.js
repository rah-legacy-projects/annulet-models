var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    audit = require('annulet-multitenant-audit'),
    plugins = require('aio-mongoose-plugins');

module.exports = exports = function(options) {
    var areaSchema = new Schema({
        name: {
            type: String,
            required: true
        }
    }, {});
    areaSchema.plugin(plugins.AclSubject, {
        key: function() {
            return 'Area:' + this._id;
        }
    });
    areaSchema.plugin(audit.plugin);
    return areaSchema;
};
