var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    audit = require('annulet-multitenant-audit'),
    plugins = require('aio-mongoose-plugins');

module.exports = exports = function(options) {
    var customerUserSchema = new Schema({
        employeeClass: [{
            type: String,
            required: true
        }],
        workflowInstances: [{
            type: ObjectId,
            ref: 'instances.workflow.WorkflowItem'
        }],
        customer: {
            type: ObjectId,
            ref: 'Customer',
            $tenant: false
        },
        user: {
            type: ObjectId,
            required: true
        }
    }, {});
    customerUserSchema.plugin(audit.plugin);
    customerUserSchema.plugin(plugins.AclObject);
    customerUserSchema.plugin(plugins.ActiveAndDeleted);
    return customerUserSchema;
};
