var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    moment = require('moment');

module.exports = function(options) {
    var shimSchema = new Schema({
        message: {
            type: String,
            required: true
        }
    });
    return shimSchema;
};
