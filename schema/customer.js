var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins'),
    audit = require('annulet-multitenant-audit'),
    moment = require('moment');

module.exports = exports = function(options) {
    var customerSchema = new Schema({
        name: {
            type: String,
            required: true
        },
        address: {
            street1: {
                type: String,
                required: true
            },
            street2: {
                type: String,
                required: false
            },
            city: {
                type: String,
                required: true
            },
            state: {
                type: String,
                required: true
            },
            zip: {
                type: String,
                required: true
            }
        },
        phone: {
            type: String,
            required: true
        },
        numberOfLocations: {
            type: Number,
            required: true
        },
        numberOfEmployees: {
            type: Number,
            required: true
        },
        stripeCustomerId: {
            type: String,
            required: false
        }
    }, {});

    customerSchema.plugin(plugins.AclSubject, {
        key: function() {
            return 'Customer:' + this._id;
        }
    });
    customerSchema.plugin(audit.plugin);
    customerSchema.plugin(plugins.ActiveAndDeleted);
    
    return customerSchema;
};
