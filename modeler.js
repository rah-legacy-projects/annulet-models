var _ = require('lodash'),
    mongoose = require('mongoose'),
    schemalib = require('./schema'),
    async = require('async'),
    util = require('util'),
    path = require('path'),
    owl = require('owl-deepcopy'),
    multitenant = require('mongoose-multitenant'),
    audit = require('annulet-multitenant-audit'),
    logger = require('winston'),
    autoIncrement = require('mongoose-auto-increment'),
    Grid = require('gridfs-stream');

_.mixin(('annulet-util')
    .lodashMixins);

multitenant.setDelimiter('.');

var makeSchemaCopy = function(schema) {
    //create clone of schema
    var newSchema = owl.deepCopy(schema);
    //fix callQueue arguments (borrowed from mongoose-schema-extend, even though we're not using it)
    //this prevents 'numAsyncPres of undefined' error
    //hack: there is probably a better way to fix callQueue arugments
    newSchema.callQueue.forEach(function(k) {
        var args = [];
        var ref = k[1];
        for (key in ref) {
            args.push(ref[key]);
        }
        return k[1] = args;
    });

    return newSchema;
};

logger.warn(mongoose.version);

module.exports = exports = {
    tenantHash: {},
    db: function(options, cb) {
        var dbhash = this.dbhash,
            tenantHash = this.tenantHash;

        if (!options) {
            logger.warn('no options passed, using default');
            var d = _.findDeep(dbhash, {
                isDefault: true
            });
            return cb(null, d);
        }
        async.series(_.map(_.keys(dbhash), function(key) {
            return function(cb) {
                async.waterfall([

                    function(cb) {
                        //get collection names
                        cb(null, {
                            names: _.keys(dbhash[key].connection.collections)
                        });
                    },
                    function(p, cb) {
                        //filter those names to just the collection(s) being looked for
                        p.names = _.filter(p.names, function(name) {
                            return new RegExp(options.collection, 'i')
                                .test(name);
                        });
                        cb(null, p);
                    },
                    function(p, cb) {
                        //issue the query against those collections
                        async.parallel(_.map(p.names, function(name) {
                            return function(cb) {

                                //make a collection for that name
                                //nb: the slice takes off the database name
                                var dbname = dbhash[key].connection.name;
                                var modName = name;
                                if (name.indexOf(dbname) >= 0) {
                                    modName = modName.slice(dbname.length + 1);
                                } else {
                                    //logger.silly(modName + ' in ' + key + ' did not have db prefix');
                                }
                                dbhash[key].connection.db.collection(modName, function(err, collection) {
                                    //issue the query against that collection
                                    collection.count(options.query, function(err, result) {
                                        //should that collection have a result, bubble back the collection
                                        if (!!result) {
                                            cb(err, modName);
                                        } else {
                                            cb(err, null);
                                        }
                                    });
                                });
                            };
                        }), function(err, r) {
                            //r should contain the collections that matched, if any
                            //filter out misses
                            p.collectionNames = _.compact(r);
                            if (p.collectionNames.length == 0) {
                                p.collectionNames = null;
                            }
                            cb(err, p);
                        });
                    }
                ], function(err, p) {
                    //only bubble back with keys that have data
                    //include the full names of the collections
                    if (!!p.collectionNames) {
                        cb(err, {
                            key: key,
                            collectionNames: p.collectionNames
                        });
                    } else {
                        cb(err, null);
                    }
                });
            };
console.log('###\n\nmongoose tiiiime\n\n');
        }), function(err, connectionKeys) {
            connectionKeys = _.compact(connectionKeys);
            //derive the tenant (C+customerId) ID
            //check to see if that exists in the customer hash
            //if not, create the tenant modelset
            if (connectionKeys == 0) {
                logger.warn("[modeler] connection not found by criteria, using default");
                cb(err, _.findDeep({
                    isDefault: true
                }, dbhash));
            } else {
                if (connectionKeys > 1) {
                    logger.warn('[modeler] more than one valid connection found, using first');
                }
                var connectionKey = connectionKeys[0];
                if (connectionKey.collectionNames.length > 1) {
                    logger.warn('[modeler] more than one valid collection found, using first');
                }
                var collectionName = connectionKey.collectionNames[0];

                async.waterfall([

                    function(cb) {
                        if (/^C[0-9a-fA-F]{24}/.test(collectionName)) {
                            //tenant data, use the tenant ID
                            var tenantId = (/^(C[0-9a-fA-F]{24})\./.exec(collectionName))[1];
                            cb(null, tenantId);
                        } else if (collectionName == 'customers') {
                            //identify the customer from the connection key/options query
                            //use C+customerId convention
                            dbhash[connectionKey.key].Customer.findOne(options.query, function(err, customer) {
                                cb(err, 'C' + customer._id.toString());
                            });
                        } else {
                            throw new Error('Tenant ID could not be derived!');
                        }
                    }
                ], function(err, tenantId) {
                    if (!tenantHash[tenantId]) {
                        //logger.silly('[modeler] creating tenant hash');
                        var discriminatedSchemas = [];
                        var addTenantSchema = function(path, schema, hash) {
                            _.each(schema, function(value, schemaName) {
                                //todo: look at using _.getObjectPath instead
                                var fullQual = path + ((path || '') == '' ? '' : '.') + schemaName;
                                if (schemaName == 'Customer') {
                                    //customer is a special case
                                    //use the customer from the db model hash
                                    var customerModel = _.resolveObjectPath(dbhash[connectionKey.key], fullQual);
                                    hash[schemaName] = customerModel;
                                } else if (schemaName == 'Shim') {
                                    //shims are a special case, use the shim from the db model hash
                                    var shimModel = _.resolveObjectPath(dbhash[connectionKey.key], fullQual);
                                    hash[schemaName] = shimModel;
                                } else if (schemaName == 'Exception' || schemaName == 'Activity') {
                                    //logger.silly('adding tracking ' + schemaName);
                                    hash[schemaName] = dbhash[connectionKey.key].connection.mtModel(tenantId, fullQual);
                                    dbhash[connectionKey.key].connection.audit(tenantId, fullQual);
                                } else if (/Abstract/.test(fullQual)) {
                                    //hack: skip abstract items
                                    //logger.silly('[modeler] skipping ' + fullQual);
                                } else if (_.isFunction(schema[schemaName])) {
                                    var tenantSchema = schema[schemaName]();

                                    if (!!tenantSchema.$base) {
                                        if (tenantSchema.$base == fullQual) {
                                            //this schema is a base type, make as a model
                                            hash[schemaName] = dbhash[connectionKey.key].connection.mtModel(tenantId, fullQual);
                                            dbhash[connectionKey.key].connection.audit(tenantId, fullQual);
                                        } else {
                                            //this schema is discriminated.
                                            //add to the post-schema processing list for later compilation.
                                            //use a copy of the schema
                                            discriminatedSchemas.push({
                                                container: path,
                                                base: tenantSchema.$base,
                                                collection: fullQual,
                                                schema: makeSchemaCopy(tenantSchema),
                                                schemaName: schemaName
                                            });

                                        }
                                    } else {
                                        //this schema is a regular model
                                        //use a copy of the schema
                                        hash[schemaName] = dbhash[connectionKey.key].connection.mtModel(tenantId, fullQual);
                                        if (/(?:Item|Navigation)Activity$/.test(fullQual)) {
                                            //logger.info('Item and navigation activities are not audited.');
                                        } else {
                                            dbhash[connectionKey.key].connection.audit(tenantId, fullQual);
                                        }
                                    }
                                } else {
                                    //otherwise, treat as plain (container) object and recurse
                                    hash[schemaName] = {};
                                    addTenantSchema(fullQual, schema[schemaName], hash[schemaName]);
                                }
                            });
                        };

                        tenantHash[tenantId] = {};
                        addTenantSchema('', schemalib, tenantHash[tenantId]);
                        tenantHash[tenantId].$customerId = tenantId;

                        //add the discriminated schemas now that the models have all been created
                        _.each(discriminatedSchemas, function(ds) {
                            //identify hash
                            var hash = _.resolveObjectPath(tenantHash[tenantId], ds.container);
                            //get the base model
                            var mtModel = _.resolveObjectPath(tenantHash[tenantId], ds.base);

                            //apply discriminator on base with ds.collectionName + schema
                            //logger.silly('[modeler] discriminating to ' + ds.schemaName + ' for ' + ds.collection + ' at base ' + ds.base);
                            hash[ds.schemaName] = mtModel.discriminator(ds.collection, ds.schema);
                        });

                        //copy a reference to the grid
                        tenantHash[tenantId].grid = dbhash[connectionKey.key].grid;
                        //hack: copy a reference to the connection
                        tenantHash[tenantId].connection = dbhash[connectionKey.key].connection;

                        //logger.info('[tenant modeler] tenant ' + tenantId + ' modeled.');
                    }
                    cb(err, tenantHash[tenantId]);
                });


            }
        });
    },
    setup: function(dbListing, cb) {
        logger.silly('setting up modeler');
        var dbList = null;
        if (!dbListing) {
            logger.debug('db listing does not exist, using defaults');
            var p = path.resolve(__dirname, 'databaseList.json');
            dbList = require(p);
        } else if (_.isString(dbListing)) {
            logger.debug('db listing a string, assuming file');
            dbList = require(dbListing);
        } else if (_.isArray(dbListing)) {
            logger.debug('db listing is an array, assuming list');

            dbList = dbListing;
        }

        var shims = [];
        var dbhash = this.dbhash;
        _.each(dbList, function(dbInfo) {
            dbhash[dbInfo.memberName] = {};
            dbhash[dbInfo.memberName].isDefault = !!dbInfo.isDefault;
            dbhash[dbInfo.memberName].connection = mongoose.createConnection(dbInfo.uri);
            dbhash[dbInfo.memberName].connection.on('error', function(err) {
                logger.error('[connection error] ' + util.inspect(err));
            });
            multitenant.setup(dbhash[dbInfo.memberName].connection);
            //logger.silly('setting up connection audit ' + dbInfo.memberName);
            audit.setup(dbhash[dbInfo.memberName].connection);
            dbhash[dbInfo.memberName].grid = Grid(dbhash[dbInfo.memberName].connection.db, mongoose.mongo);
            autoIncrement.initialize(dbhash[dbInfo.memberName].connection);

            var addSchema = function(path, schema, hash) {
                _.each(_.keys(schema), function(schemaName) {
                    //todo: look at using _.getObjectPath instead
                    var fullQual = path + ((path || '') == '' ? '' : '.') + schemaName;
                    if (_.isFunction(schema[schemaName])) {
                        if (schemaName == 'Customer') {
                            //special case: customer
                            hash[schemaName] = dbhash[dbInfo.memberName].connection.model(fullQual, makeSchemaCopy(schema[schemaName]()));
                        } else if (schemaName == 'Exception' || schemaName == 'Activity') {
                            //logger.silly('adding tracking ' + schemaName);
                            hash[schemaName] = dbhash[dbInfo.memberName].connection.model(fullQual, makeSchemaCopy(schema[schemaName]({
                                name: fullQual
                            })));
                        } else if (schemaName == 'Shim') {
                            //hack: without shimming and saving the the shims at setup time,
                            //hack: the connection will fail on identifying collections
                            hash[schemaName] = dbhash[dbInfo.memberName].connection.model(fullQual, makeSchemaCopy(schema[schemaName]()));
                            shims.push(function(cb) {
                                (new hash[schemaName]({
                                    message: fullQual + ' on ' + dbInfo.memberName + ' shimmed.'
                                }))
                                    .save(function(err, shim) {
                                        if (!!err) {
                                            logger.error('problem shimming ' + dbInfo.memberName + ': ' + util.inspect(err));
                                        }
                                        cb(err, shim);
                                    });
                            });
                        } else if (/Abstract/.test(fullQual)) {
                            //hack: skip abstract items
                            //logger.silly('[modeler] setup skipping ' + fullQual);
                        } else if (!!schema[schemaName]()
                            .$base) {
                            if (schema[schemaName]()
                                .$base == fullQual) {
                                //this schema is a base type, make as a model
                                logger.silly('making basetype ' + fullQual);
                                hash[schemaName] = dbhash[dbInfo.memberName].connection.mtModel(fullQual, schema[schemaName]());
                            } else {
                                //this schema is discriminated.
                                //post-processing list happens during customer setup.
                            }
                        } else {
                            //this schema is a regular model
                            //logger.silly('[modeler] making ' + fullQual + ' into a registered model.');
                            var x = (schema[schemaName])();
                            logger.warn('### schema is an instance: ' + (x instanceof mongoose.Schema));
                            x = makeSchemaCopy(x);
                            logger.warn('### schema copy is an instance: ' + (x instanceof mongoose.Schema));
                            
                            hash[schemaName] = dbhash[dbInfo.memberName].connection.mtModel(
                                fullQual,
                                x);
                        }
                    } else {
                        //otherwise, treat as plain (container) object and recurse
                        hash[schemaName] = {};
                        addSchema(fullQual, schema[schemaName], hash[schemaName]);
                    }
                });
            };

            addSchema('', schemalib, dbhash[dbInfo.memberName]);
        });

        async.parallel(shims, function(err, r) {
            logger.silly('[modeler] ' + r.length + ' shims set up.');
            if (!!cb) {
                cb(null);
            }
        });
    },
    dbhash: {},
    forceDisconnect: function(cb) {
        mongoose.disconnect(function(err) {
            cb(err);
        });
    }
}
